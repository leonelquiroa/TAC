<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/User/general.php";
    $generalFn1 = new generalUserFunctions();
    $generalVar1 = new generalUserVariables();
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    $today = date("Y-m-d");
    
    switch ($type_data){
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;
        //show
        case 'all_images':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_person_picture('".$idPatient."','ALL')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["active"] == 1){
                        $output .= '<img src="../../Multimedia/img/people/'.$row["url"].'" class="mainImage" alt=""/>
                                   <br/>
                                   <label>'.$row["DatePic"].'</label>
                                   <br/>';
                    }
                    else{
                        $output .= '<img src="../../Multimedia/img/people/'.$row["url"].'" class="miniatureImage" alt=""/>&nbsp;';
                    }
                }
            }
            
        break;
        case 'showInformation':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_administrator_people_top('".$idPatient."')";
            $result = $sqlOps->sql_single_row($sql);
            if(!is_null ($result["Nombre"])){
                $output .= '
                    <p class="pNameUser">'.$result["Nombre"].'</p>
                    <p class="pNick">'.$result["Nick"].'</p>
                    <p class="pPlace">'.$result["namePlace"].'</p>
                    <span class="label"> Meta peso : '.$result["Peso"].' lbs</span>
                    <span class="label"> Meta grasa : '.$result["GrasaCorporal"].' %</span>
                    <span class="label"> IMC : '.$result["IMC"].'</span>
                    <span class="label"> Talla: 36</span>
                    <span class="label"> Edad : '.$result["Edad"].'</span>';
            }
        break;
        case 'showBars':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $Points = $generalFn1->fn_getBars($idPatient,'Ordinario',$today,$sqlOps);
            $sessionPoints = $Points[0];
            $monthPoints = $Points[1];
            $Miles = $generalFn1->fn_getBars($idPatient,'Extraordinario',$today,$sqlOps);
            $sessionMiles = $Miles[0];
            $monthMiles = $Miles[1];

            $percentageSessionPoints = round(($sessionPoints/$generalVar1->maxPointsSession)*100,2);
            $percentageSessionMiles = round(($sessionMiles/$generalVar1->maxMilesSession)*100,2);
            $percentageMonthlyPoints = round(($monthPoints/$generalVar1->maxPointsMonthly)*100,2);
            $percentageMonthlyMiles = round(($monthMiles/$generalVar1->maxMilesMonthly)*100,2);

            $position = 0;
            $sql = "CALL sp_person_monthly_rank('".$today."','".$idPatient."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["idPerson"] == $idUser){
                        $position = $row["position"];
                        break;
                    }
                }
            }
            $percentagePosition = round(($position/$count)*100,2);

            $output .= ' 
                <div class="row">
                    <div class="col-md-6">
                        <p class="pIndicators">Temporada</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom1" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxPointsSession.'" style="width: '.$percentageSessionPoints.'%">
                                <span>PO: '.$sessionPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom2" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxMilesSession.'" style="width: '.$percentageSessionMiles.'%">
                                <span>ME: '.$sessionMiles.'</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="pIndicators">Mes</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom3" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxPointsMonthly.'" style="width: '.$percentageMonthlyPoints.'%">
                                <span>PO: '.$percentageMonthlyPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom2" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxMilesMonthly.'" style="width: '.$percentageMonthlyMiles.'%">
                                <span>ME: '.$monthMiles.'</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="'.$count.'" style="width: '.$percentagePosition.'%">
                        <span>Posición: '.$position.' de '.$count.'</span>
                    </div>
                </div>';
        break;
        //notes
        case 'FollowingNotes':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_notesession_list('".$idPatient."')";
            $resultReason = $sqlOps->sql_single_row($sql);
            if($resultReason == "")
            {
                $output .= '
                <div class="firstLine"></div>
                <div class="secondLine"></div>
                <br/>
                <p class="pSubTitle"><i class="fa fa-pencil" aria-hidden="true"></i> Notas de Seguimiento</p>
                <label>Razón de Ingreso</label>
                <textarea class="form-control" rows="2" id="txtReason"></textarea>
                <div class="row">
                    <div class="col-md-6">
                        <label>Glucosa Inicial</label>
                        <input type="text" class="form-control" id="txtGlucosaBgn">
                    </div>
                    <div class="col-md-6">
                        <label>Glucosa Final</label>
                        <input type="text" class="form-control" id="txtGlucosaEnd">
                    </div>
                </div>
                <label>Notas</label>
                <textarea class="form-control" rows="5" id="txtNote"></textarea>
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success btnStyle" id="btnNewNote">Guardar</button>
                    </div>
                    <div class="col-md-1"></div>
                </div>';
            }
            else
            {
                $output .= '
                <div class="firstLine"></div>
                <div class="secondLine"></div>
                <br/>
                <p class="pSubTitle"><i class="fa fa-pencil" aria-hidden="true"></i> Notas de Seguimiento</p>
                <div class="row">
                    <div class="col-md-6">
                        <label>Nueva nota</label>
                        <textarea class="form-control" rows="5" id="txtAnotherNote"></textarea>                        
                        <button type="button" class="btn btn-success btnStyle" id="btnMoreNotes">Guardar</button>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Razón de Ingreso</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td contenteditable="true" data-id1="'.$resultReason["idNoteSession"].'#reason" class="editReason" id="reason">'.$resultReason["reason"].'</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Glucosa Inicial</th>
                                    <th>Glucosa Final</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td contenteditable="true" data-id1="'.$resultReason["idNoteSession"].'#glucosaBegin" class="editGlucoseBegin" id="glucosaBegin">'.$resultReason["glucosaBegin"].'</td>
                                    <td contenteditable="true" data-id1="'.$resultReason["idNoteSession"].'#glucosaEnd" class="editGlucoseFinal" id="glucosaEnd">'.$resultReason["glucosaEnd"].'</td>
                                </tr>
                            </tbody>
                        </table>   
                    </div>
                </div>
                <br/>
                <div class="firstLine"></div>
                <div class="secondLine"></div>
                <br/>';
                
                $sql = "CALL sp_notedescription_list('".$idPatient."')";
                $resultNotes = $sqlOps->sql_multiple_rows($sql);
                $countNotes = $resultNotes ? mysqli_num_rows($resultNotes) : -1;
                $previousNotes = "";
                if($countNotes > 0){
                    while($row = $resultNotes->fetch_assoc()){
                        $previousNotes .= '
                        <tr>
                            <td width="20%">'.$row["dateNote"].'</td>
                            <td width="75%" contenteditable="true" class="editNote" data-id1="'.$row["idNoteDescription"].'#description" id="description'.$row["idNoteDescription"].'">'.$row["description"].'</td>
                            <td width="5%">
                                <i class="fa fa-trash-o deleteNote" aria-hidden="true" data-id1="'.$row["idNoteDescription"].'"></i>
                            </td>
                        </tr>';
                    }
                }
                if($previousNotes != ""){
                    $output .= '
                    <table class="table" style="font-size: 12px;">
                        <thead>
                          <tr>
                            <th>Fecha</th>
                            <th>Nota</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>';
                    $output .= $previousNotes;
                    $output .= '
                        </tbody>
                    </table>';
                }
            }
        break;
        case 'AddFirstNote':
            $reason = isset($_POST["reason"]) ? $_POST["reason"] : '';
            $glucosaBegin = isset($_POST["glucosaBegin"]) ? $_POST["glucosaBegin"] : '';
            $glucosaEnd = isset($_POST["glucosaEnd"]) ? $_POST["glucosaEnd"] : '';
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_notesession_add('".$reason."', '".$glucosaBegin."', '".$glucosaEnd."', '".$idPatient."', @si)";
            $lastId = $sqlOps->sql_exec_op_return($sql);  
            
            $description = isset($_POST["description"]) ? $_POST["description"] : '';
            $sql2 = "CALL sp_notedescription_add('".$description."', '".$lastId."')";
            $output = $sqlOps->sql_exec_op($sql2);  
        break;
        case 'AddMoreNotes':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_notesession_list('".$idPatient."')";
            $resultSession = $sqlOps->sql_single_row($sql);
            if($resultSession != ""){
                $description = isset($_POST["description"]) ? $_POST["description"] : '';
                $sql2 = "CALL sp_notedescription_add('".$description."', '".$resultSession["idNoteSession"]."')";
                $output = $sqlOps->sql_exec_op($sql2);  
            }
        break;
        case 'deleteDescriptionNote':
            $sql = "CALL sp_notedescription_remove('".$_POST['idNote']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'updateNoteDescription':
            $sql = "CALL sp_notedescription_edit('".$_POST['idNoteDescription']."','".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'updateNoteSession':
            $sql = "CALL sp_notesession_edit('".$_POST['idNoteSession']."','".$_POST['column']."','".$_POST['value']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
    }
    echo $output == '' ? '' : $output;