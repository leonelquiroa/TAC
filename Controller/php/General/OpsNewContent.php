<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    include "../../php/Administration/general.php";
    $generalFn = new generalAdministratorFunctions();    
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break; 
        //type and category
        case 'ddl_type':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('Contenido')",
                        "Tipo de Contenido",
                        "ddl_type",
                        $sqlOps,
                        "idCatalog",
                        "nameCatalog",
                        array("Esfuerzo","Dietas","Videos")); 
        break;
        case 'ddl_category':
            $sql0 = "CALL sp_catalog_name(".$_POST['TypeContent'].")";
            $typeContent = $sqlOps->sql_single_row($sql0);
            
            $sql = '';
            if($typeContent["nameCatalog"] == 'Recetas')
            {
                $sql = "CALL sp_catalog_list('CategoriaDeReceta')";
            }
            elseif($typeContent["nameCatalog"] == 'Articulos')
            {
                $sql = "CALL sp_catalog_list('CategoriaDeArticulo')";
            }

            $output = $generalFn->getList(
                        $sql,
                        "Categoria",
                        "ddl_category",
                        $sqlOps,
                        "idCatalog",
                        "nameCatalog",
                        array("")); 
        break; 
        case 'ddl_title':
            $output = $generalFn->getList(
                "CALL sp_digitalcontent_list_admin('".$_POST['TypeContent']."','".$_POST['categoryContent']."')",
                "Titulo",
                "ddl_title",
                $sqlOps,
                "idCont",
                "nameCont",
                array(""));
        break;
        case 'getContent':
            $sql = "CALL sp_digitalcontent_get('".$_POST['idContent']."')";
            $result = $sqlOps->sql_single_row($sql);
            $res = array();
            $res[0][0] = $result["nameCont"];
            $res[0][1] = $result["valueCont"];
            $res[0][2] = $result["beginDate"];
            $res[0][3] = $result["endDate"];
            echo json_encode($res);
        break;
        //new
        case 'save_content_db':
            $sql = "CALL sp_digitalcontent_add("
                . "'".$_POST['title']."',"
                . "NULL,"
                . "'".$_POST['data']."',"
                . "'".$_POST['typeId']."',"
                . "'".$_POST['category']."',"
                . "'".$_POST['dateFrom']."',"
                . "'".$_POST['dateTo']."',@si)";
            $output = $sqlOps->sql_exec_op_return($sql);
        break;
        //update
        case 'update_content_db':
            $sql = "CALL sp_digitalcontent_update("
                . "'".$_POST['idCont']."',"
                . "'".$_POST['title']."',"
                . "NULL,"
                . "'".$_POST['data']."',"
                . "'".$_POST['typeId']."',"
                . "'".$_POST['category']."',"
                . "'".$_POST['dateFrom']."',"
                . "'".$_POST['dateTo']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        //save a physical file
        case 'save_content_file':
            $myfile = fopen("../../../Multimedia/files/".$_POST['title'].".php", "w") 
                or die("El archivo no se pudo guardar!");
            $head = '
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="description" content="TAC Peakfit site">
                        <meta name="author" content="@leoquiroa">
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
                        <title>Usuario</title>
                        <!--Third CSS-->
                        <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
                        <!--Own CSS-->
                        <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
                        <!--Third JS-->
                        <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
                        <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
                        <!--Own JS-->
                        <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>
                    </head>
                    <body>
                        <!-- ################################################# MENU ################################################# -->
                        <div id="mySidenav" class="sidenav">
                            <a href="../../View/User/Home.php"><img src="../img/MenuIcons/inicio.png" height="30" alt=""><br>Inicio</a>
                            <a href="../../View/User/Calendar.php"><img src="../img/MenuIcons/calendarios.png" height="30" alt=""><br>Calendario</a>
                            <a href="../../View/User/Attendance.php"><img src="../img/MenuIcons/asistencias.png" height="30" alt=""><br>Asistencias</a>
                            <a href="../../View/User/Control.php"><img src="../img/MenuIcons/controlnutricional.png" height="30" alt=""><br>Control Nutricional</a>
                            <a href="../../View/User/Diet_Effort.php"><img src="../img/MenuIcons/dietas.png" height="30" alt=""><br>Dietas & Pruebas</a>
                            <a href="../../View/User/Routine.php"><img src="../img/MenuIcons/videos.png" height="30" alt=""><br>Rutinas</a>
                            <a href="../../View/User/Article_Recipe.php"><img src="../img/MenuIcons/articulosrecetas.png" height="30" alt=""><br>Articulos & Recetas</a>
                            <a href="../../View/General/logout.php"><img src="../img/MenuIcons/salir.png" height="30" alt=""><br>Salir</a>
                        </div>
                        <div id="top">
                            <div class="col-xs-10" id="div_logo">
                                <img src="../img/LogoTigofit.png" alt="" id="logo">
                            </div>
                            <div class="col-xs-2" id="div_three_bars">
                                <span onclick="openCloseNavMenu()">
                                    <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 white-div"></div>
                        <!-- ################################################# MENU ################################################# -->
                        <div style="padding-left: 10px; padding-right: 10px;">';
            $body = $_POST['data'];
            $foot = '        
                        </div>
                    </body>
                </html>';
            fwrite($myfile,$head.$body.$foot);
            fclose($myfile);
        break;  
    }
    echo $output == '' ? '' : $output;    
    
