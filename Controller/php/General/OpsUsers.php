<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();  
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        case 'ddl_place_search':
            $output = $generalFn->getList(
            "CALL sp_catalog_list('Lugar')",
            $_POST['title'],
            "ddl_place",
            $sqlOps);
        break;
        case 'table_users':
            $sql = "CALL sp_administrator_peopleList_by_place('".$_POST['typePlace']."','".$_POST['placeID']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $list = '';
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                while($row = $result->fetch_assoc()){  
                    $op = isset($row["OrdinaryPoints"]) ? $row["OrdinaryPoints"] : '0';
                    $em = isset($row["ExtraMiles"]) ? $row["ExtraMiles"] : '0';
                    $list .= '
                        <tr>
                            <td class="ix_usr" data-id0="'.$row["Id"].'" style="cursor: pointer;">'.$row["Name"].'</td>
                            <td >'.$row["Mail"].'</td>
                            <td >'.$row["Place"].'</td>
                            <td >'.$op.'</td>
                            <td >'.$em.'</td>
                            <td class="deleteUsr" data-id1="'.$row["Id"].'" style="cursor: pointer;">
                                <span class="fa fa-trash"></span>
                            </td>
                        </tr>';
                }
            }
            $output .= '
            <table class="table table-condensed table-hover table-bordered" id="tableBegin" style="font-size: 12px; width: 100%;">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Mail</th>
                    <th>Lugar</th>
                    <th>Puntos Ordinarios</th>
                    <th>Millas Extras</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '
                </tbody>
            </table>';
        break;
        //add
        case 'new_user_gender':
            $output = $generalFn->getList(
                "CALL sp_catalog_list('Genero')",
                "Genero",
                "ddl_new_user_gender",
                $sqlOps);            
        break;
        case 'new_user_place':
            $output = $generalFn->getList(
                "CALL sp_catalog_list('Lugar')",
                "Sede",
                "ddl_new_user_place",
                $sqlOps);            
        break;
        case 'new_user_type':
            session_start();
            $typePerson = $_SESSION['typePerson'];
            if($typePerson=='Administrador'){
                $output = $generalFn->getList(
                    "CALL sp_catalog_list('RolDeUsuario')",
                    "Tipo",
                    "ddl_new_user_type",
                    $sqlOps);            
            }
        break;
        case 'add_user':
            $name = isset($_POST["name"]) ? $_POST["name"] : '';  
            $nick = isset($_POST["nick"]) ? $_POST["nick"] : '';  
            $gender = isset($_POST["gender"]) ? $_POST["gender"] : '';  
            $height = isset($_POST["height"]) ? $_POST["height"] : '';  
            $birthday = isset($_POST["birthday"]) ? $_POST["birthday"] : ''; 
            $phone = isset($_POST["phone"]) ? $_POST["phone"] : ''; 
            $email = isset($_POST["email"]) ? $_POST["email"] : ''; 
            $place = isset($_POST["place"]) ? $_POST["place"] : '';
            $userType = isset($_POST["userType"]) ? $_POST["userType"] : '';
            $sql = "CALL sp_person_add(" .
                    "'".$name."', "
                    . "'".$nick."', "
                    . "'".$gender."', "
                    . "'".$height."', "
                    . "'".$birthday."', "
                    . "'".$phone."', "
                    . "'".$email."', "
                    . "'".$place."',"
                    . "'".$userType."',"
                    . "@si)";
            $output = $sqlOps->sql_exec_op_return($sql);            
        break;
        case 'add_user_pic':
            $newId = isset($_POST["newId"]) ? $_POST["newId"] : ''; 
            $ext = isset($_POST["ext"]) ? $_POST["ext"] : '';
            $sql = "CALL sp_picsperperson_add('".$newId."', '".$ext."',@si)";
            $output = $sqlOps->sql_exec_op_return($sql); 
        break;
        //delete    
        case 'deactivate_user':
            $idPerson = $_POST["idPerson"];  
            $reason = $_POST["reason"];  
            $sql = "CALL sp_person_delete('".$idPerson."', '".$reason."')"; 
            $output = $sqlOps->sql_exec_op($sql);
        break;   
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;
        //save Id User
        case 'saveUserInfo':{
            session_start();
            $_SESSION['idPatient'] = isset($_POST["idPerson"]) ? $_POST["idPerson"] : 0;
            $sql = "CALL sp_administrator_people_details('".$_POST["idPerson"]."','user')";
            $result = $sqlOps->sql_single_row($sql);
            $_SESSION['idPlacePatient'] = $result == '' ? 0 : $result["idPlace"];
        }
    }
    echo $output == '' ? '' : $output;
    