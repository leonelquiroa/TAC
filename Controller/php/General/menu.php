<?php
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    switch ($type_data){
        case 'menuAdmin':
            $output .= '
            <div class="collapse navbar-collapse" style="text-align:center;">
                <ul class="nav navbar-nav side-nav">
                    <!-- User -->
                    <li>
                        <a href="#" data-toggle="collapse" data-target="#demoUser">
                            <i class="fa fa-users"></i> 
                            <p style="color:white">Usuarios <i class="fa fa-fw fa-caret-down"></i></p>
                        </a>
                        <ul id="demoUser" class="collapse">
                            <li>
                                <a href="../General/users.php">
                                    <i class="fa fa-list-alt"></i> 
                                    <p style="color:white">Inicio</p>
                                </a>
                            </li>
                            <li>
                                <a href="../Admin/statistics.php">
                                    <i class="fa fa-fw fa-bar-chart-o"></i>
                                    <p style="color:white">Estadisticas</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- Pro -->
                    <li>
                        <a href="#" data-toggle="collapse" data-target="#demoPro">
                            <i class="fa fa-user-md"></i> 
                            <p style="color:white">Profesional <i class="fa fa-fw fa-caret-down"></i></p>
                        </a>
                        <ul id="demoPro" class="collapse">
                            <li>
                                <a href="../Admin/professional.php">
                                    <i class="fa fa-list-ol"></i>
                                    <p style="color:white">Inicio</p>
                                </a>
                            </li>
                            <li>
                                <a href="../Admin/rate.php">
                                    <i class="fa fa-area-chart"></i>
                                    <p style="color:white">Rendimiento</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- Content -->                
                    <li>
                        <a href="#" data-toggle="collapse" data-target="#demoContent">
                            <i class="fa fa-connectdevelop"></i>
                            <p style="color:white">Contenido<i class="fa fa-fw fa-caret-down"></i></p>
                        </a>
                        <ul id="demoContent" class="collapse">
                            <li>
                                <a href="../General/newContent.php">
                                    <i class="fa fa-cutlery"></i> / <i class="fa fa-book"></i>
                                    <p style="color:white">Recetas / Articulos</p>
                                </a>
                            </li>
                            <li>
                                <a href="../General/uploadContent.php">
                                    <i class="fa fa-balance-scale"></i> / <i class="fa fa-heartbeat"></i>
                                    <p style="color:white">Dietas / Pruebas de esfuerzo</p>
                                </a>
                            </li>
                            <li>
                                <a href="../General/routines.php">
                                    <i class="fa fa-video-camera"></i>
                                    <p style="color:white">Rutinas</p>
                                </a>
                            </li>
                            <li>
                                <a href="../General/events.php">
                                    <i class="fa fa-users"></i>
                                    <p style="color:white">Eventos</p>
                                </a>
                            </li>
                            <li>
                                <a href="../General/badges.php">
                                    <i class="fa fa-trophy"></i>
                                    <p style="color:white">Badges</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->';  
        break;
        case 'menuNutri':
            $output .= '
                <div class="collapse navbar-collapse" style="text-align:center;">
                    <ul class="nav navbar-nav side-nav">
                        <!-- User -->
                        <li>
                            <a href="../General/users.php">
                                <i class="fa fa-user"></i>
                                <p style="color:white">Usuarios</p>
                            </a>
                        </li>
                        <li>
                            <a href="../Nutricionist/appointment.php">
                                <i class="fa fa-calendar"></i>
                                <p style="color:white">Citas</p>
                            </a>
                        </li>
                        <li>
                            <a href="../Nutricionist/indicators.php">
                                <i class="fa fa-fw fa-bar-chart-o"></i>
                                <p style="color:white">Indicadores</p>
                            </a>
                        </li>
                        <li>
                            <a href="../Nutricionist/phrase.php">
                                <i class="fa fa-commenting-o"></i>
                                <p style="color:white">Frases</p>
                            </a>
                        </li>
                        <!-- Content -->                
                        <li>
                            <a href="#" data-toggle="collapse" data-target="#demoContent">
                                <i class="fa fa-connectdevelop"></i>
                                <p style="color:white">Contenido<i class="fa fa-fw fa-caret-down"></i></p>
                            </a>
                            <ul id="demoContent" class="collapse">
                                <li>
                                    <a href="../General/newContent.php">
                                        <i class="fa fa-cutlery"></i> / <i class="fa fa-book"></i>
                                        <p style="color:white">Recetas / Articulos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="../General/uploadContent.php">
                                        <i class="fa fa-balance-scale"></i> / <i class="fa fa-heartbeat"></i>
                                        <p style="color:white">Dietas / Pruebas de esfuerzo</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="../General/routines.php">
                                        <i class="fa fa-video-camera"></i>
                                        <p style="color:white">Rutinas</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="../General/events.php">
                                        <i class="fa fa-users"></i>
                                        <p style="color:white">Eventos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="../General/badges.php">
                                        <i class="fa fa-trophy"></i>
                                        <p style="color:white">Badges</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->';
        break;
        case 'topLeftAdmin':
            $output .= '
            <a href="../Nutricionist/home.php">
                <img src="../../Multimedia/img/topLeftAdmin.png" height="90" width="200">
            </a>';
        break;
        case 'topLeftSales':
            $output .= '
            <a href="../Nutricionist/home.php">
                <img src="../../Multimedia/img/topLeftSales.png" height="90" width="200">
            </a>';
        break;
        case 'topLeftNutri':
            $output .= '
            <a href="../Nutricionist/home.php">
                <img src="../../Multimedia/img/topLeftNutri.png" height="90" width="200">
            </a>';
        break;
        case 'topBar':
            $output .= '
                <ul class="nav navbar-right top-nav">
                    <div class="navbar-form navbar-left">
                        <br/>
                        <input type="text" class="form-control" placeholder="Buscar Integrantes" id="txtSearch">
                    </div>
                    <!-- Top Menu Items - Alerts -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">View All</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Top Menu Items - Messages -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">View All</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Top Menu Items - Calendar -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">View All</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Top Menu Items - User operations -->
                    <li class="dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <div class="infoUser">
                                 Nick<b class="caret"></b>
                            </div>
                            <div class="infoUser">
                                 <img src="../../Multimedia/img/people/no_image.png" height="50px" width="50px" alt="" class="img-responsive img-rounded">
                            </div>
                        </div>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="../General/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>                    
                    </div>
                </ul>
            ';
        break;
    }
    echo $output == '' ? '' : $output;
?>