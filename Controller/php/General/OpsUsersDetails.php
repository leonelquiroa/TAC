<?php
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    $today = date("Y-m-d");
    
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();  
    $generalVar = new generalNutricionistVariables();
    include "../../php/User/general.php";
    $generalFn1 = new generalUserFunctions();
    $generalVar1 = new generalUserVariables();
    
    switch ($type_data){
        //menu
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;
        //show
        case 'all_images':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_person_picture('".$idPatient."','ALL')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["active"] == 1){
                        $output .= '<img src="../../Multimedia/img/people/'.$row["url"].'" class="mainImage" alt=""/>
                                   <br/>
                                   <label>'.$row["DatePic"].'</label>
                                   <br/>';
                    }
                    else{
                        $output .= '<img src="../../Multimedia/img/people/'.$row["url"].'" class="miniatureImage" alt=""/>&nbsp;';
                    }
                }
            }
            
        break;
        case 'showInformation':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;  
            $sql = "CALL sp_administrator_people_top('".$idPatient."')";
            $result = $sqlOps->sql_single_row($sql);
            if(!is_null ($result["Nombre"])){
                $output .= '
                    <p class="pNameUser">'.$result["Nombre"].'</p>
                    <p class="pNick">'.$result["Nick"].'</p>
                    <p class="pPlace">'.$result["namePlace"].'</p>
                    <span class="label"> Meta peso : '.$result["Peso"].' lbs</span>
                    <span class="label"> Meta grasa : '.$result["GrasaCorporal"].' %</span>
                    <span class="label"> IMC : '.$result["IMC"].'</span>
                    <span class="label"> Talla: 36</span>
                    <span class="label"> Edad : '.$result["Edad"].'</span>';
            }
        break;
        case 'showBars':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $Points = $generalFn1->fn_getBars($idPatient,'Ordinario',$today,$sqlOps);
            $sessionPoints = $Points[0];
            $monthPoints = $Points[1];
            $Miles = $generalFn1->fn_getBars($idPatient,'Extraordinario',$today,$sqlOps);
            $sessionMiles = $Miles[0];
            $monthMiles = $Miles[1];

            $percentageSessionPoints = round(($sessionPoints/$generalVar1->maxPointsSession)*100,2);
            $percentageSessionMiles = round(($sessionMiles/$generalVar1->maxMilesSession)*100,2);
            $percentageMonthlyPoints = round(($monthPoints/$generalVar1->maxPointsMonthly)*100,2);
            $percentageMonthlyMiles = round(($monthMiles/$generalVar1->maxMilesMonthly)*100,2);

            $position = 0;
            $sql = "CALL sp_person_monthly_rank('".$today."','".$idPatient."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["idPerson"] == $idUser){
                        $position = $row["position"];
                        break;
                    }
                }
            }
            $percentagePosition = round(($position/$count)*100,2);

            $output .= ' 
                <div class="row">
                    <div class="col-md-6">
                        <p class="pIndicators">Temporada</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom1" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxPointsSession.'" style="width: '.$percentageSessionPoints.'%">
                                <span>PO: '.$sessionPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom2" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxMilesSession.'" style="width: '.$percentageSessionMiles.'%">
                                <span>ME: '.$sessionMiles.'</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="pIndicators">Mes</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom3" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxPointsMonthly.'" style="width: '.$percentageMonthlyPoints.'%">
                                <span>PO: '.$percentageMonthlyPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom2" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar1->maxMilesMonthly.'" style="width: '.$percentageMonthlyMiles.'%">
                                <span>ME: '.$monthMiles.'</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="'.$count.'" style="width: '.$percentagePosition.'%">
                        <span>Posición: '.$position.' de '.$count.'</span>
                    </div>
                </div>';
        break;
        case 'editableInformation':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_administrator_people_details('".$idPatient."','user')";
            $result = $sqlOps->sql_single_row($sql);
            if($result != ''){
                $list = '';
                for($i = 0; $i<count($generalVar->lbl); $i++)
                {
                    $labelName = $generalVar->lbl[$i];
                    if($labelName == 'Genero' || $labelName == 'Sede')
                        {
                        if($labelName == 'Genero')
                        {
                            $list_diff = $generalFn->getListWithDataId(
                                        "CALL sp_catalog_list('Genero')",
                                        $result[$labelName],
                                        "ddl_".$labelName."_update",
                                        $sqlOps,
                                        $result["idPerson"]);
                        }
                        elseif($labelName == 'Sede')
                        {
                            $list_diff = $generalFn->getListWithDataId(
                                        "CALL sp_catalog_list('Lugar')",
                                        $result[$labelName],
                                        "ddl_".$labelName."_update",
                                        $sqlOps,
                                        $result["idPerson"]);
                        }
                        $list .= '
                            <tr>
                                <td>'.$labelName.'</td>
                                <td contenteditable="false">'.$list_diff.'</td>
                            </tr>';
                    }
                    else
                    {
                        $id_row = '';
                        $editable = 'true';
                        if($labelName == "Nombre"){
                            $id_row .= 'name'; 
                        }
                        elseif($labelName == "Nick"){
                            $id_row .= 'nick'; 
                        }
                        elseif($labelName == "Fecha de Nac."){
                            $id_row .= 'birthday';
                        }
                        elseif($labelName == "Altura"){
                            $id_row .= 'height';
                        }
                        elseif($labelName == "Telefono"){
                            $id_row .= 'telephone';
                        }
                        elseif($labelName == "Correo"){
                            $id_row .= 'mail';
                        }
                        elseif($labelName == "Temporada"){
                            $editable = 'false';
                            $id_row .= 'season';
                        }
                        $list .= '
                            <tr>
                                <td >'.$labelName.'</td>
                                <td contenteditable="'.$editable.'" class="class_'.$id_row.'" data-id_'.$id_row.'="'.$result["idPerson"].'" id="'.$result["idPerson"].''.substr($id_row,0,2).'">'.$result[$labelName].'</td>
                            </tr>';
                    }
                }
                $output .= '
                    <table class="table table-condensed table-hover table-bordered" style="font-size: 13px;">
                        <thead>
                            <tr>
                                <th>Campo</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody>';
                $output .= $list;
                $output .= '
                        </tbody>
                    </table>';
            }   
        break;
        case 'show_qr':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $path = '../../../Multimedia/img/qrcodes/'.$idPatient.'.png';
            if(file_exists($path) == 1){
                $output .= '
                <img src="../../Multimedia/img/qrcodes/'.$path.'" alt="..." style="width: 150px; height: 150px;">';
            }
        break;
        case 'show_image':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $sql = "CALL sp_person_picture('".$idPatient."','ACTIVE')";
            $result = $sqlOps->sql_single_row($sql);
            if($result != ''){
                $output .= '
                <form>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                            <img src="../../Multimedia/img/people/'.$result["url"].'" alt="..." id="pp_preview">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Cambiar</span>
                                <span class="fileinput-exists">¿Otra?</span>
                                <input type="file" name="..." id="ephoto-upload" >
                            </span>
                            <button type="button" class="btn btn-default fileinput-exists" id="update_profile_pic">Esta si</button>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Cancelar</a>
                        </div>
                    </div>
                </form>';
            }
        break;
        //update
        case 'update_user':
            session_start();
            $idPatient = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $id = trim($idPatient,"\n");  
            $column_name = $_POST["column_name"];  
            $texto = trim($_POST["texto"],"\n");

            if($_POST["object"] == 'user'){
                $sql = "CALL sp_person_update('".$id."', '".$column_name."', '".$texto."')";}  
            elseif($_POST["object"] == 'session'){
                $sql = "CALL sp_administrator_session_update('".$id."', '".$column_name."', '".$texto."')";}

            $output = $sqlOps->sql_exec_op($sql);
        break; 
        case 'add_user_pic':
            session_start();
            $ext = isset($_POST["ext"]) ? $_POST["ext"] : '';
            $sql = "CALL sp_picsperperson_add('".$_SESSION['idPatient']."', '".$ext."',@si)";
            $output = $sqlOps->sql_exec_op_return($sql); 
        break;
        //regenerate
        case 'getUserId':
            session_start();
            $output = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
        break;
    }
    echo $output == '' ? '' : $output;