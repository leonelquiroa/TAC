<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();      
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        //show
        case 'ddl_place':
            $output = $generalFn->getList(
                        "CALL sp_catalog_list('Lugar')",
                        "Sede",
                        "ddl_place",
                        $sqlOps); 
        break;
        case 'ddl_people':
            $list_people = '';
            $sql = "CALL sp_administrator_people_list_by_place('','".$_POST['placeID']."','ALL')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $output = '<select class="selectpicker" data-style="btn-info" data-width="100%" title="Nombre" id="ddl_people" data-live-search="true" data-size="5">';
                while($row = $result->fetch_assoc())
                {  
                    $output .= '<option value="'.$row["idPerson"].'">'.$row["namePerson"].'</option>';
                }
                $output .= '</select>';
            }
        break;
        case 'badges_list':
            $sql = "CALL sp_badgesavailables_list(".$_POST['idUser'].")";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <li class="sortable-item" id="'.$row["idBadge"].'">
                            <img src="../../Multimedia/img/badges/'.$row["image"].'" alt="" style="width:70px;height:70px;"/> 
                            <p style="font-size:10px;">'.$row["name"].' - '.$row["description"].'</p>
                        </li>';             
                }
                $output .= '<ul class="sortable-list">';
                $output .= $list;
                $output .= '</ul>';
            }
        break;
        case 'badgesperperson_list':
            $sql = "CALL sp_badgesperperson_list(".$_POST['idUser'].")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '';
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <li class="sortable-item" id="'.$row["idBadge"].'">
                            <img src="../../Multimedia/img/badges/'.$row["image"].'" alt="" style="width:70px;height:70px;"/>
                            <p style="font-size:10px;">'.$row["name"].' - '.$row["description"].'</p>
                        </li>';    
                }    
            }
            $output .= '<ul class="sortable-list">';
            $output .= $list;
            $output .= '</ul>';
        break;
        //operations
        case 'save_badges':
            $sql = "CALL sp_badges_add("
                    ."'".$_POST['title']."',"
                    ."'".$_POST['description']."',"
                    ."'".$_POST['value']."',@si)";
            $output = $sqlOps->sql_exec_op_return($sql);
        break;    
        case 'update_image_badge':
            $sql1 = "CALL sp_badges_update("
                    . "'".$_POST['idBadge']."',"
                    . "'".$_POST['columnDB']."',"
                    . "'".$_POST['textDB']."')";
            $output = $sqlOps->sql_exec_op($sql1);
        break;
        case 'update_badges_all':
            $sql1 = "CALL sp_badges_update_all("
                    . "'".$_POST['title']."',"
                    . "'".$_POST['value']."',"
                    . "'".$_POST['description']."',"
                    . "'".$_POST['idBadge']."')";
            $output = $sqlOps->sql_exec_op($sql1);
        break;
        //assign
        case 'badgesperperson_ops':
            $sql = "CALL sp_badgesperperson_list(".$_POST['idUser'].")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $listDb = array(); 
            $i = 0;
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $listDb[$i++] = $row["idBadge"];
                }
            }
            //
            $listWeb = split(",",$_POST['idBadges']);
            //
            session_start();
            $idNutri = $_SESSION['idPerson'];
            $ToAdd = array_diff($listWeb, $listDb);
            foreach ($ToAdd as $uniqueBadge) {
                if($uniqueBadge != ''){
                    $sql = "CALL sp_badgesperperson_add(".$_POST['idUser'].",".$uniqueBadge.",".$idNutri.")";
                    $sqlOps->sql_exec_op($sql);
                }
            }
            $ToRemove = array_diff($listDb,$listWeb);
            foreach ($ToRemove as $uniqueBadge) {
                if($uniqueBadge != ''){
                    $sql = "CALL sp_badgesperperson_remove(".$_POST['idUser'].",".$uniqueBadge.")";
                    $sqlOps->sql_exec_op($sql);
                }
            }
        break;
        //menu type
        case 'menuType':
            session_start();
            $output = $_SESSION['typePerson'];
        break;        
        //initial list
        case 'all_badges_list':
            $sql = "CALL sp_badges_list()";
            $result = $sqlOps->sql_multiple_rows($sql);
            $list = '';
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                while($row = $result->fetch_assoc()){  
                    $list .= '
                        <tr>
                            <td style="cursor: pointer; width: 25%;" class="ix_Badge" data-id0="'.$row["idBadge"].'">'.$row["name"].'</td>
                            <td style="width: 40%;">'.$row["description"].'</td>
                            <td style="width: 10%;">'.$row["value"].'</td>
                            <td style="width: 20%; text-align:center;">
                                <img src="../../Multimedia/img/badges/'.$row["image"].'" alt="'.$row["name"].'" style="width:70px;height:70px;">
                            </td>
                            <td style="cursor: pointer; width: 5%;" class="deleteBadge" data-id1="'.$row["idBadge"].'">
                                <span class="fa fa-trash"></span>
                            </td>
                        </tr>';
                }
            }
            $output .= '
            <table class="table table-hover table-bordered scroll" style="font-size: 13px;">
                <tbody>';
            $output .= $list;
            $output .= '
                </tbody>
            </table>';
        break;
        case 'see_badge':
            $sql = "CALL sp_badges_get('".$_POST['idBadge']."')";
            $row = $sqlOps->sql_single_row($sql);
            $res = array();
            if($row != ''){
                $res[0] = $row["idBadge"];
                $res[1] = $row["name"];
                $res[2] = $row["description"];
                $res[3] = $row["value"];
                $res[4] = $row["image"];
                echo json_encode($res);
            }
        break;        
        case 'delete_badge':
            $sql = "CALL sp_badges_remove('".$_POST['idBadge']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;        
    }
    echo $output == '' ? '' : $output;    