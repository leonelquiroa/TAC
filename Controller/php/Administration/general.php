<?php
    class generalAdministratorFunctions{
        ////////////////////////////////////////////////////////////////////////////////
        // FUNCTIONS
        ////////////////////////////////////////////////////////////////////////////////
        public function getList($sql,$title,$idDll,$sqlOps,$idColumn,$idName,$NoList){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$idDll.'">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if(!in_array($row[$idName],$NoList)){
                        $list .= '<option value="'.$row[$idColumn].'">'.$row[$idName].'</option>';
                    }
                }
            }
            return $list .= '</select>';
        }
    }
?>