<?php
    ///////////////// VARIABLES /////////////////
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/Administration/general.php";
    $generalFn = new generalAdministratorFunctions();
    ////////////////////////////////////////////////////////////////////////////////
    // HOME
    ////////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'dll_rol_type':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('RolDeUsuario')",
                    "Rol",
                    "ddl_rol_type",
                    $sqlOps,
                    "nameCatalog",
                    "nameCatalog",
                    array("Integrante","Administrador"));
        break;
        case 'table_pro':
            $sql = "CALL sp_professional_performance('".$_POST['month']."','".$_POST['proType']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $body = '';$head = '';
            if($count > 0){
                while($row = $result->fetch_assoc())
                {  
                    $rate = $row["Rate"] == -1.00 ? '-' : $row["Rate"];
                    switch ($_POST['proType']){
                        case "Entrenador":
                            $body .= '
                            <tr>
                                <td data-id0="'.$row["idPerson"].'" style="cursor:pointer" class="who">'.$row["namePerson"].'</td>
                                <td>'.$row["Horas"].'</td>
                                <td>'.$row["Asistencias"].'</td>
                                <td>'.$row["AsistenciaPorClase"].'</td>
                                <td>'.$rate.'</td>
                            </tr>';
                        break;
                        case "Nutricionista":
                            $body .= '
                            <tr>
                                <td data-id0="'.$row["idPerson"].'" style="cursor:pointer" class="who">'.$row["namePerson"].'</td>
                                <td>'.$row["Solicitada"].'</td>
                                <td>'.$row["ElMismoDia"].'</td>
                                <td>'.$row["NoSePresento"].'</td>
                                <td>'.$row["Realizada"].'</td>
                                <td>'.$rate.'</td>
                            </tr>';
                        break;
                    }
                }    
            }
            switch ($_POST['proType'])
            {
                case "Entrenador":
                    $head .= '
                        <th>Entrenador</th>
                        <th>Horas</th>
                        <th>Asistencias</th>
                        <th>Asistencia Por Clase</th>
                        <th>Calificación</th>';
                break;
                case "Nutricionista":
                    $head .= '
                        <th>Nutricionista</th>
                        <th>Solicitada</th>
                        <th>El mismo día</th>
                        <th>No se presentó</th>
                        <th>Realizada</th>
                        <th>Calificación</th>';
                break;
            }
            $output .= '
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>';
            $output .= $head;
            $output .= '
                    </tr>
                </thead>
                <tbody>';
            $output .= $body;
            $output .= '
                </tbody>
            </table>';
        break;
        case 'table_details':
            $sql = "CALL sp_professional_attendance('".$_POST['month']."','".$_POST['idPro']."','".$_POST['proType']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $body = '';$head = '';
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    switch ($_POST['proType']){
                        case "Entrenador":
                            $body .= '
                            <tr>
                                <td>'.$row["Fecha"].'</td>
                                <td>'.$row["Hora"].'</td>
                                <td>'.$row["Asistencia"].'</td>
                            </tr>';
                        break;
                        case "Nutricionista":
                            $body .= '
                            <tr>
                                <td>'.$row["Fecha"].'</td>
                                <td>'.$row["Hora"].'</td>
                                <td>'.$row["AppointType"].'</td>
                            </tr>';
                        break;
                    }
                }
            }
            switch ($_POST['proType'])
            {
                case "Entrenador":
                    $head .= '
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>No. de Personas</th>';
                break;
                case "Nutricionista":
                    $head .= '
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Tipo de Cita</th>';
                break;
            }
            $output .= '
            <table class="table table-hover table-bordered scroll">
                <thead>
                    <tr>';
            $output .= $head;
            $output .= '
                    </tr>
                </thead>
                <tbody>';
            $output .= $body;
            $output .= '
                </tbody>
            </table>';
        break;
        case 'chart_pro':
            $output .= '<canvas id="graph-area"></canvas>';
        break;
        case 'pro_graph':
            $sql = "CALL sp_professional_graph_monthly('".$_POST['month']."','".$_POST['idPro']."','".$_POST['proType']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $res = array();
                $ix = 1;
                $name = $_POST['proType'] == 'Entrenador' ? 'Fecha' : 'TipoDeCita';
                $valor = $_POST['proType'] == 'Entrenador' ? 'Asistencia' : 'Citas';
                while($row = $result->fetch_assoc())
                {  
                    $res[$ix][0] = $row[$name];
                    $res[$ix][1] = $row[$valor];
                    $ix++;
                }
                echo json_encode($res);
            }
        break;  
    }
    ////////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;    
?>