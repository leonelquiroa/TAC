<?php
    ///////////////// VARIABLES /////////////////
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();  
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/Administration/general.php";
    $generalFn = new generalAdministratorFunctions();    
    ////////////////////////////////////////////////////////////////////////////////
    // HOME
    ////////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'ddl_place_date':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('Lugar')",
                    "Lugar",
                    "ddl_place",
                    $sqlOps,
                    "idCatalog",
                    "nameCatalog",
                    array());
        break;    
        
        case 'graphPeople':
            $output .= '
                <canvas id="chart-pie"></canvas>
                <div id="table-pie"></div>';
        break;    
        case 'PeopleMovementGraph':
            $res = array();
            $sql1 = "CALL sp_administrator_people_statistics('".$_POST['dateBgn']."','".$_POST['placeID']."','UserMovement')";  
            $result = $sqlOps->sql_multiple_rows($sql1);
            $ix = 1;
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0)  
            {
                while($row1 = $result->fetch_assoc())
                {
                    $res[$ix][0] = $row1["TIPO"];
                    $res[$ix][1] = $row1["TOTAL"];
                    $ix++;
                }
                echo json_encode($res);
            }
        break;
        case 'PeopleMovementTable':
            $rows = $_POST['data'];
            $data = '';
            foreach ($rows as $row)
            {
                $data .= '
                    <tr>
                        <td>'.$row[0].'</td>
                        <td>'.$row[1].'</td>
                    </tr>';
            }
            $output .= '
            <table class="table table-hover table-bordered table-condensed">
                <thead>
                  <tr>
                    <th>Tipo de Usuario</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody>';
            $output .= $data;
            $output .= '
                </tbody>
            </table>';     
        break;
        
        case 'graphGender':
            $output .= '
                <canvas id="chart-doughnut"></canvas>
                <div id="table-doughnut"></div>';
        break;   
        case 'GenderAttendGraph':
            $res = array();
            $sql1 = "CALL sp_administrator_people_statistics('".$_POST['dateBgn']."','".$_POST['placeID']."','UserGender')";  
            $result = $sqlOps->sql_multiple_rows($sql1);
            $ix = 1;
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0)  
            {
                while($row1 = $result->fetch_assoc())
                {
                    $res[$ix][0] = $row1["GENERO"];
                    $res[$ix][1] = $row1["TOTAL"];
                    $ix++;
                }
                echo json_encode($res);
            }
        break;    
        case 'GenderAttendTable':
            $rows = $_POST['data'];
            $data = '';
            foreach ($rows as $row)
            {
                $data .= '
                    <tr>
                        <td>'.$row[0].'</td>
                        <td>'.$row[1].'</td>
                    </tr>';
            }
            $output .= '
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Genero</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $data;
            $output .= '
                </tbody>
            </table>';    
        break;   
        
        case 'graphSchedule':
            $output .= '
                <canvas id="canvas-line"></canvas>
                <div id="table-line"></div>';
        break; 
        case 'ScheduleGraph':
            $res = array();
            $sql1 = "CALL sp_administrator_people_statistics('".$_POST['dateBgn']."','".$_POST['placeID']."','UserHour')";  
            $result = $sqlOps->sql_multiple_rows($sql1);
            $ix = 1;
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0)  
            {
                while($row1 = $result->fetch_assoc())
                {
                    $res[$ix][0] = $row1["HORA"];
                    $res[$ix][1] = $row1["TOTAL"];
                    $ix++;
                }
                echo json_encode($res);         
            }
        break;
        case 'ScheduleTable':
            $rows = $_POST['data'];
            $data = '';
            foreach ($rows as $row)
            {
                $data .= '
                    <tr>
                        <td>'.$row[0].'</td>
                        <td>'.$row[1].'</td>
                    </tr>';
            }
            $output .= '
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Hora</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $data;
            $output .= '
                </tbody>
            </table>';       
        break;    
        
        case 'graphClass':
            $output .= '
                <canvas id="chart-bar"></canvas>
                <div id="table-bar"></div>';
        break; 
        case 'ClassGraph':
            $res = array();
            $sql1 = "CALL sp_administrator_people_statistics('".$_POST['dateBgn']."','".$_POST['placeID']."','UserClass')";  
            $result = $sqlOps->sql_multiple_rows($sql1);
            $ix = 1;
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0)  
            {
                while($row1 = $result->fetch_assoc())
                {
                    $res[$ix][0] = $row1["CLASE"];
                    $res[$ix][1] = $row1["TOTAL"];
                    $ix++;
                }
                echo json_encode($res);       
            }    
        break;    
        case 'ClassTable':
            $rows = $_POST['data'];
            $data = '';
            foreach ($rows as $row)
            {
                $data .= '
                    <tr>
                        <td>'.$row[0].'</td>
                        <td>'.$row[1].'</td>
                    </tr>';
            }
            $output .= '
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Clase</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $data;
            $output .= '
                </tbody>
            </table>';  
        break;     
    }
    ////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;
?>