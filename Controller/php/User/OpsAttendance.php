<?php
    session_start();
    ///////////////// VARIABLES /////////////////
    $idUser = $_SESSION['idPerson'];
    //$idUser = 229;
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    $generalVar = new generalUserVariables();
    ////////////////////////////////////////////////////////////////////////////////
    // HOME
    ////////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'Attend_ddl':
            $output = $generalFn->getList("CALL sp_catalog_list('Tiempo')","Tiempo","ddl_horz",$sqlOps,false);
            $output .= '<input type="hidden" class="form-control" id="time_input" placeholder="dd/mm/yyyy">';
        break;
        case 'Temporada':
            $sql1 = "CALL sp_person_attendance_by_time('".$idUser."',NULL,NULL,'Range')";
            $row1 = $sqlOps->sql_single_row($sql1);
            if($row1 != '')  
            {  
                $res = $generalFn->get_Month_Range($row1["BeginDate"],$row1["EndDate"]);
                for($x = 0; $x < count($res); $x++)
                {
                    $res[$x][2] = 0;
                    $res[$x][3] = 0;
                }
                $sql2 = "CALL sp_person_attendance_by_time('".$idUser."',NULL,NULL,'Session')";
                $result = $sqlOps->sql_multiple_rows($sql2);
                $count = $result ? mysqli_num_rows($result) : -1;
                if($count > 0){
                    while($row = $result->fetch_assoc()){
                        $name = $row["TYPE"];
                        for($x = 0; $x < count($res); $x++){
                            //0 - '01-mm-yyyy'
                            //1 - nameMonth
                            //2 - ordinary points
                            //3 - extra milles
                            if($res[$x][0] == $row["MES"]){
                                if($name == 'Ordinario'){
                                    $res[$x][2] = $row["ASISTENCIAS"]*$generalVar->pointPerClass;
                                }
                                elseif($name == 'Extraordinario'){
                                    $res[$x][3] = $row["ASISTENCIAS"]*$generalVar->pointPerClass;
                                }
                                break;
                            }
                        }
                    }
                    $newRes = array();
                    for($x = 0; $x < count($res); $x++){                    
                        $newRes[$x][0] = $res[$x][1];
                        $newRes[$x][1] = $res[$x][2];
                        $newRes[$x][2] = $res[$x][3];
                    }
                }
                echo json_encode($newRes);
            }
        break;
        case 'Mes':
            $sql = "CALL sp_person_attendance_by_time('".$idUser."','".$_POST['dateBgn']."','".$_POST['dateEnd']."','Month')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $res = array();
                while($row = $result->fetch_assoc()){
                    $name = $row["TYPE"];
                    $weekNo = $row["SEMANA"]-1;
                    $value = $row["ASISTENCIAS"];
                    //0 - weekNumber
                    //1 - ordinary points
                    //2 - extra milles
                    //initialize
                    if (!in_array ($weekNo+1, $res)) {
                        $res[$weekNo][0] = $weekNo+1;
                    }
                    //fill
                    if($name == 'Ordinario'){
                        $res[$weekNo][1] = $value*$generalVar->pointPerClass;
                        if(!isset($res[$weekNo][2]))
                            $res[$weekNo][2] = 0;
                    }
                    elseif($name == 'Extraordinario'){
                        if(!isset($res[$weekNo][1]))
                            $res[$weekNo][1] = 0;
                        $res[$weekNo][2] = $value*$generalVar->pointPerClass;
                    }
                    else{
                        $res[$weekNo][1] = 0;
                        $res[$weekNo][2] = 0;
                    }
                }
            }
            echo json_encode($res);
        break;
        case 'Semana':
            $sql = "CALL sp_person_attendance_by_time('".$idUser."','".$_POST['dateBgn']."','".$_POST['dateEnd']."','Week')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $res = array();
                for($x = 0; $x < count($dias); $x++)
                {
                    $res[$x][0] = $dias[$x];
                    $res[$x][1] = 0;
                    $res[$x][2] = 0;
                }
                while($row = $result->fetch_assoc()){
                    $name = $row["TYPE"];
                    $DiaNo = $row["DIA"];
                    $value = $row["ASISTENCIAS"];
                    //0 - DiaNo
                    //1 - ordinary points
                    //2 - extra milles
                    for($x = 0; $x < count($dias); $x++)
                    {
                        if($res[$x][0] == $DiaNo){
                            if($name == 'Ordinario'){
                                $res[$x][1] = $value*$generalVar->pointPerClass;
                            }
                            elseif($name == 'Extraordinario'){
                                $res[$x][2] = $value*$generalVar->pointPerClass;
                            }
                            break;
                        }
                    }
                }
            }
            echo json_encode($res);
        break;
        case 'AttendTable':
            $sql = '';
            if ($_POST["dateBgn"] == "" && $_POST["dateEnd"] == ""){
                $sql2 = "CALL sp_person_attendance_by_time('".$idUser."',NULL,NULL,'Range')";
                $row2 = $sqlOps->sql_single_row($sql2);
                $output = '';
                if($row2 != ''){  
                    $sql = "CALL sp_person_attendance_by_time('".$idUser."','".$row2["BeginDate"]."','".$row2["EndDate"]."','Details')";
                }
            }
            else{
                $sql = "CALL sp_person_attendance_by_time('".$idUser."','".$_POST["dateBgn"]."','".$_POST["dateEnd"]."','Details')";}
            $result9 = $sqlOps->sql_multiple_rows($sql);
            $output .= '
            <div class="container">    
                <table class="scroll table table-hover table-bordered table-condensed" style="font-size: 12px;">
                    <tbody>';
            $count9 = $result9 ? mysqli_num_rows($result9) : -1;
            if($count9 > 0)  
            {  
                while($row9 = $result9->fetch_assoc())
                {
                    $styleColor = $row9["TYPE"] == "Puntos Ordinarios" ? "black" : "blue" ;
                    $output .= '
                            <tr style="color:'.$styleColor.';">  
                                <td width="30%">'.$row9["FECHA_LIST"].'</td>  
                                <td width="25%">'.$row9["CLASE"].'</td>  
                                <td width="20%">'.$row9["POINTS"].'</td>  
                                <td width="25%">'.$row9["TYPE"].'</td>  
                            </tr>
                    ';  
                }
                $output .= '        
                    </tbody>
                </table>
            </div>';
            }
        break;
        case 'AttendCalendar':
            $sql2 = "CALL sp_person_attendance_by_time('".$idUser."',NULL,NULL,'Range')";
            $row2 = $sqlOps->sql_single_row($sql2);
            $sql = "CALL sp_person_attendance_by_time('".$idUser."','".$row2["BeginDate"]."','".$row2["EndDate"]."','Details')";
            $res33 = $generalFn->get_array_calendar_attend($sql,$sqlOps);
            echo json_encode($res33);
        break;
        case 'RatePro':
            $output = $generalFn->ProGrade(
                    isset($_POST['pro']) ? $_POST['pro'] : ''
                    ,$sqlOps);
        break;
        case 'SaveRate':
            $generalFn->SaveRate(
                    isset($_POST['proID']) ? $_POST['proID'] : '',
                    $idUser,
                    isset($_POST['rateNo']) ? $_POST['rateNo'] : '',
                    isset($_POST['comment']) ? $_POST['comment'] : '',
                    $sqlOps);
        break;
        case 'graph':
            $output = $generalFn->canvasGraph();
        break;
        case 'ProfilePic':
            $output = $generalFn->profilePic(
                   isset($_POST['coachID']) ? $_POST['coachID'] : ''
                    ,$sqlOps);
        break;
    }
    ////////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;
?>