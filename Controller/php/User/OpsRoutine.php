<?php
    session_start();
    ///////////////// VARIABLES /////////////////
    $idUser = $_SESSION['idPerson'];
    //$idUser = 229;
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    ////////////////////////////////////////////////////////////////////////////
    // ROUTINES
    ////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'ddl_routine':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('NivelesDeEjercicios')",
                    "Nivel de Ejercicios",
                    "ddl_level",
                    $sqlOps,
                    true);
        break;
        case 'table_routine':
            $output = $generalFn->TableContent(
                    'Videos',
                    $_POST['idTypeRoutine'],
                    $idUser,
                    18,
                    'rutinas',
                    'routineScroll',
                    1,
                    $sqlOps);
        break;
        case 'FavAction':
            $sql0 = "CALL sp_fav_rate_exist('".$_POST['idCont']."','".$idUser."')";
            $result0 = $sqlOps->sql_single_row($sql0);
            if($result0 == ''){
                $sql1 = "CALL sp_fav_rate_add('".$_POST['idCont']."','".$idUser."',0,1)";
                $sqlOps->sql_exec_op($sql1);
            }else{
                $sql2 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'FAVORITE',1)";
                $sqlOps->sql_exec_op($sql2);
            }
        break;
        case 'NotFavAction':
            $sql3 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'FAVORITE',0)";
            $sqlOps->sql_exec_op($sql3);
        break;
        case 'RateAction':
            $sql4 = "CALL sp_fav_rate_exist('".$_POST['idCont']."','".$idUser."')";
            $result0 = $sqlOps->sql_single_row($sql4);
            if($result0 == ''){
                $sql5 = "CALL sp_fav_rate_add('".$_POST['idCont']."','".$idUser."','".$_POST['star']."',0)";
                $sqlOps->sql_exec_op($sql5);
            }else{
                $sql6 = "CALL sp_fav_rate_update(".$_POST['idCont'].",'RATE','".$_POST['star']."')";
                $sqlOps->sql_exec_op($sql6);
            }
        break;
        case 'signs_fav_rate_video':
            $output = $generalFn->FavRateSigns("Mis videos","Videos");
        break;
        case 'fav_rate_Videos':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'A','Videos',$sqlOps);
        break;
        case 'fav_Videos':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'R','Videos',$sqlOps);
        break;    
        case 'rate_Videos':
            $output = $generalFn->TableFavRate($idUser,$_POST['cont'],'F','Videos',$sqlOps);
        break;    
    }
    ////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;
?>