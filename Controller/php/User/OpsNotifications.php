<?php
include "../../../Model/SqlOperations.php";
$sqlOps = new SqlOperations();
include "../../php/User/general.php";
$generalFn = new generalUserFunctions();
$generalVar = new generalUserVariables();

session_start();
$idUser = $_SESSION['idPerson'];
$idPlace = $_SESSION['idPlace'];
$today = date("Y-m-d");
$output = '';
$type_data = isset($_POST['type']) ? $_POST['type'] : '';

switch ($type_data){
    case 'TypeNoti':
        $output = $_SESSION['NotificationType'];
    break;
    case 'BellEvents':
        $sqlB = "CALL sp_notifications_list(".$idUser.",'Personal')";
        $resultB = $sqlOps->sql_multiple_rows($sqlB);
        $countB = $resultB ? mysqli_num_rows($resultB) : -1;
        if($countB > 0){
            $output .= '
            <table class="table table-condensed" style="text-align:center; font-size: 10px;">
                <tbody>';
            while($row = $resultB->fetch_assoc()){
                $colorVisited = $row["idNoti"] > 0 ? 'white' :'#8CF2ED';
                $legend = '';$imgPath = '';$urlDestination = '';
                switch ($row["NameObj"])
                {
                    case 'Badge':
                        $legend = 'te ha asignado el badge';
                        $imgPath = '../../Multimedia/img/badges/';
                        $urlDestination = '../../View/User/Home.php';
                    break;
                    case 'Dietas':
                        $legend = 'te ha asignado la dieta';
                        $imgPath = '../../Multimedia/files/thumbnails/';
                        $urlDestination = '../../View/User/Diet_Effort.php';
                    break;
                    case 'Esfuerzo':
                        $legend = 'te ha asignado la prueba de esfuerzo';
                        $imgPath = '../../Multimedia/files/thumbnails/';
                        $urlDestination = '../../View/User/Diet_Effort.php';
                    break;
                    case 'Nutricionist':
                        $legend = 'te ha registrado una cita nutricional de tipo';
                        $urlDestination = '../../View/User/Control.php';
                    break;
                }
                $output .= '
                        <tr onclick="document.location='."'".$urlDestination."'".';" 
                        style="background-color: '.$colorVisited.';" 
                        data-id0="'.$row["idObj"].'#'.$row["NameObj"].'" class="NotificationRow">
                            <td width="30%">
                                '.$row["DiaNombre"].'<h3>'.$row["Dia"].'</h3>'.$row["Mes"].'
                            </td>
                            <td width="45%" style="vertical-align: middle;">
                                <b>'.$row["ProName"].'</b> '.$legend.' <b>'.$row["Data1"].'</b></br>'.$row["hourObj"].'
                            </td>';
                if($row["NameObj"] == 'Nutricionist'){
                    $output .= '
                            <td width="25%" style="vertical-align: middle;">
                                '.$row["Data2"].' Puntos
                            </td>';
                }else{
                    $output .= '
                            <td width="25%" style="vertical-align: middle;">
                                <img src="'.$imgPath.$row["Data2"].'" class="img-responsive" width="50" height="50">
                            </td>';
                }
                $output .= '</tr>';
            }
            $output .= '
                </tbody>
            </table>';
        }
    break;
    case 'CalendarEvents':
        $sql = "CALL sp_notifications_list(".$idUser.",'Event')";
        $result = $sqlOps->sql_multiple_rows($sql);
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $output .= '
            <table class="table table-condensed" style="text-align:center; font-size: 12px;">
                <thead>
                    <tr>
                        <th width="30%" style="text-align:center; color: #37A8E2;"><b>Fecha</b></th>
                        <th width="25%" style="text-align:center; color: #22CEDC;"><b>Hora</b></th>
                        <th width="45%" style="text-align:center; color: #CCB05E;"><b>Evento y Lugar</b></th>
                    </tr>
                </thead>
                <tbody>';
            while($row = $result->fetch_assoc()){
                $colorVisited = $row["idNoti"] > 0 ? 'white' :'#8CF2ED';
                $output .= '
                        <tr 
                        onclick="document.location='."'../../Multimedia/img/flyers/".$row["flyerPath"]."'".';" '
                        . 'style="background-color: '.$colorVisited.';" 
                        data-id0="'.$row["idEvent"].'#Badge" class="NotificationRow">
                            <td width="30%">'.$row["DiaNombre"].'<h1>'.$row["Dia"].'</h1>'.$row["Mes"].'</td>
                            <td width="25%" style="vertical-align: middle;">'.$row["hourEvent"].'</td>                            
                            <td width="45%" style="vertical-align: middle;"><b>'.$row["titleEvent"].'</b><br/>'.$row["placeEvent"].'</td>
                        </tr>
                    ';
            }
            $output .= '
                </tbody>
            </table>';
        }
    break;
    case 'ViewCalendarEvent':
        $res = $sqlOps->sql_single_row("CALL sp_notification_get('".$_POST['typeNoti']."',".$_POST['idNewObj'].",".$idUser.")");
        if($res["Total"] == 0){
            $sqlOps->sql_exec_op("CALL sp_notification_add('".$_POST['typeNoti']."',".$_POST['idNewObj'].",".$idUser.")");
        }
    break;
}
echo $output == '' ? '' : $output;