<?php
    session_start();
    ///////////////// VARIABLES /////////////////
    $idUser = $_SESSION['idPerson'];
    //$idUser = 229;
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    ////////////////////////////////////////////////////////////////////////////
    // DB Model
    ////////////////////////////////////////////////////////////////////////////
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    ////////////////////////////////////////////////////////////////////////////
    // General Functions & Variables
    ////////////////////////////////////////////////////////////////////////////
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    ////////////////////////////////////////////////////////////////////////////////
    // NUTRITIONAL CONTROL
    ////////////////////////////////////////////////////////////////////////////////
    switch ($type_data){
        case 'Indicador_ddl':
            $output = $generalFn->getList(
                    "CALL sp_catalog_list('Indicadores')",
                    "Indicador",
                    "ddl_indicador",
                    $sqlOps,
                    true);
        break;
        case 'Indicador_graph':
            $res = array();
            $ref = array();
            //
            $sql0 = "CALL sp_person_nutrition_list(NULL,'".$_POST['idIndicador']."','UDM')";
            $row0 = $sqlOps->sql_single_row($sql0);
            $res[0][0] = $row0["nameIndicador"];
            $res[0][1] = $row0["UDM"];
            $res[0][2] = '';
            //
            $sql4 = "CALL sp_person_nutrition_list('".$idUser."','".$_POST['idIndicador']."','Ranges')";
            $result4 = $sqlOps->sql_multiple_rows($sql4);
            $count4 = $result4 ? mysqli_num_rows($result4) : -1;
            if($count4 > 0){
                $ix = 1;
                while($row4 = $result4->fetch_assoc()){
                    $ref[$ix][0] = $row4["valueMin"];
                    $ref[$ix][1] = $row4["valueMax"];
                    $ref[$ix][2] = $row4["Color"];
                    $ix++;
                }
            }
            //  
            $sql8 = "CALL sp_person_nutrition_list('".$idUser."','".$_POST['idIndicador']."','Details')";
            $result8 = $sqlOps->sql_multiple_rows($sql8);
            $count8 = $result8 ? mysqli_num_rows($result8) : -1;
            if($count8 > 0){
                $ix = 1;
                while($row8 = $result8->fetch_assoc()){
                    $res[$ix][0] = $row8["Tiempo"];
                    $res[$ix][1] = $row8["Valor"];
                    $res[$ix][2] = 'blue';
                    foreach($ref as $value){
                        if($value[0] < $row8["Valor"] && $row8["Valor"] < $value[1]){
                            $res[$ix][2] = $value[2];
                            break;
                        }
                    }
                    $ix++;
                }
            }
            //
            echo json_encode($res);
        break;
        case 'goal_icon_div':
            $output = '
            <div style="font-size: 16px;">
                <img src="../../Multimedia/img/finish-flag-icon.png" height="30" alt=""> TU META
            </div>';
        break;
        case 'Goal_bar':
            //lectura
            //fecha
            //valor
            $res = array();
            $sql0 = "CALL sp_person_nutrition_list('".$idUser."','".$_POST['idIndicador']."','Last')";
            $row0 = $sqlOps->sql_single_row($sql0);
    //        $res[0][0] = 'Actual';
    //        $res[0][1] = $row0["Tiempo"];
            $res[0][2] = $row0["Valor"];
            //
            $sql8 = "CALL sp_person_nutrition_list('".$idUser."','".$_POST['idIndicador']."','Status')";
            $result8 = $sqlOps->sql_multiple_rows($sql8);
            $count8 = $result8 ? mysqli_num_rows($result8) : -1;
            if($count8 > 0){
                $ix = 1;
                while($row8 = $result8->fetch_assoc()){
                    if($row8["Lectura"] == 'medida meta'){
    //                    $res[2][0] = $row8["Lectura"];
    //                    $res[2][1] = $row8["GoalDate"];
                        $res[2][2] = $row8["Valor"];
                    }
                    elseif($row8["Lectura"] == 'medida inicial'){
    //                    $res[1][0] = $row8["Lectura"];
    //                    $res[1][1] = $row8["FirstDate"];
                        $res[1][2] = $row8["Valor"];
                    }
                    $ix++;
                }
            }
            //
            $current = $res[0][2];
            $begin = $res[1][2];
            $goal = $res[2][2];
            $barsText = '';
            $title = '<div style="font-size: 12px;">';
            $width = '0';
            $Progress = round((($current-$begin)/($goal-$begin))*100,0);
            $negativeChange = ($goal-$begin)<0 ? true : false ; //negative change
            $word = $negativeChange==true ? 'Perdiste' : 'Ganaste';
            if($current==$goal){
                //goal
                $title .= '¡Felicidades! cumpliste tu meta. '.$word.' '.abs($goal-$begin).'.';
                $width = 100;
            }
            elseif($current==$begin){
                //same
                $title .= '¡Animo! estas a '.abs($goal-$begin).' de tu meta.';
                $width = 3;
            }
            else{
                //in progress
                if($negativeChange){
                    if($current<$goal){
                        $title .= '¡Felicidades! cumpliste tu meta. '.$word.' '.abs($goal-$begin).'. Ademas, lograste un avance del '.($Progress-100).'%.';
                        $width = 100;
                    }
                    elseif($current>$begin){
                        $title .= '¡Animo! estas a '.abs($goal-$current).' de tu meta.';
                        $width = 3;
                    }
                    else{
                        $title .= $word.' un '.$Progress.'%, te falta un '.(100-$Progress).'%.';
                        $width = $Progress;
                    }
                }
                else{
                    if($current>$goal){
                        $title .= '¡Felicidades! cumpliste tu meta. '.$word.' '.abs($goal-$begin).'. Ademas, lograste un avance del '.($Progress-100).'%.';
                        $width = 100;
                    }
                    elseif($current<$begin){
                        $title .= '¡Animo! estas a '.abs($goal-$current).' de tu meta.';
                        $width = 3;
                    }
                    else{
                        $title .= $word.' un '.$Progress.'%, te falta un '.(100-$Progress).'%.';
                        $width = $Progress;
                    }
                }            
            }
            $title .= '</div>';
            $output = $title.'<hr/>';
            $barsText = '
                <p style="width:'.$width.'%;" data-value="'.$current.'" class="progress-top"></p>
                <div class="progress">
                    <div class="progress-bar progress-bar-custom" role="progressbar" style="width:'.($width-1).'%"></div>    
                </div>
                <p style="width:100%" data-value="'.$goal.'" class="progress-bottom">'.$begin.'</p>';
            $output .= $barsText;            
        break;
        case 'graph':
            $output = $generalFn->canvasGraph();
        break;
        case 'RatePro':
            $output = $generalFn->ProGrade(
                    isset($_POST['pro']) ? $_POST['pro'] : ''
                    ,$sqlOps);
        break;
        case 'SaveRate':
            $generalFn->SaveRate(
                    isset($_POST['proID']) ? $_POST['proID'] : '',
                    $idUser,
                    isset($_POST['rateNo']) ? $_POST['rateNo'] : '',
                    isset($_POST['comment']) ? $_POST['comment'] : '',
                    $sqlOps);
        break;
        case 'ProfilePic':
            $output = $generalFn->profilePic(
                   isset($_POST['coachID']) ? $_POST['coachID'] : ''
                    ,$sqlOps);
        break;
    }
    ////////////////////////////////////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////////////////////////////////////
    echo $output == '' ? '' : $output;
?>