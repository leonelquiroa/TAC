<?php
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    $generalVar = new generalUserVariables();
    
    session_start();
    $idUser = $_SESSION['idPerson'];
    $idPlace = $_SESSION['idPlace'];
    $today = date("Y-m-d");
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        case 'Notification':
            session_start();
            $_SESSION['NotificationType'] = $_POST['typeNoti'];
        break;
        case 'NumberPendientNoti':
            $sql = "CALL sp_notifications_number(".$idUser.",'".$_POST['eventType']."')";
            $row = $sqlOps->sql_single_row($sql);
            $output = ($row == '') ? 0 : $row["Pendient"];
        break;
    
        case 'profilePic':
            $sql = "CALL sp_person_picture(".$idUser.",'ACTIVE')";
            $row = $sqlOps->sql_single_row($sql);
            if($row != '')  
            {  
                $output .= '<img src="../../Multimedia/img/people/min'.$row["url"].'" alt="profile Pic" class="img-circle profilePic" height="40">';
            }
        break;
        case 'Phrase':
            $output = '';
            $sql = "CALL sp_phrase_list('Today',NULL,NULL)";
            $row = $sqlOps->sql_single_row($sql);
            if($row != '')  
            {  
                $output .= '<div style="text-align: center; font-size: 14px;">'
                        . '<i>"'.$row["titlePhrase"].'"</i>'
                        . ' - '.$row["textPhrase"].''
                        . '</div>';
            }
        break;
        case 'Bars':
            $Points = $generalFn->fn_getBars($idUser,'Ordinario',$today,$sqlOps);
            $sessionPoints = $Points[0];
            $monthPoints = $Points[1];
            $Miles = $generalFn->fn_getBars($idUser,'Extraordinario',$today,$sqlOps);
            $sessionMiles = $Miles[0];
            $monthMiles = $Miles[1];

            $percentageSessionPoints = round(($sessionPoints/$generalVar->maxPointsSession)*100,2);
            $percentageSessionMiles = round(($sessionMiles/$generalVar->maxMilesSession)*100,2);
            $percentageMonthlyPoints = round(($monthPoints/$generalVar->maxPointsMonthly)*100,2);
            $percentageMonthlyMiles = round(($monthMiles/$generalVar->maxMilesMonthly)*100,2);

            $sql = "CALL sp_person_monthly_rank('".$today."','".$idPlace."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $position = -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["idPerson"] == $idUser){
                        $position = $row["position"];
                        break;
                    }
                }
            }
            $percentagePosition = round(($position/$count)*100,2);

            $output .= ' 
                <div class="row"><div class="col-xs-12" style="font-size: 14px;"><strong>Temporada</strong></div></div>
                <div class="row">
                    <div class="col-xs-2" style="font-size: 10px; text-align: right;">
                        Puntos Ordinarios 
                        <div class="col-xs-12" style="height:10px;"></div>
                        Millas Extras
                    </div>
                    <div class="col-xs-10">
                        <div class="progress">
                            <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar->maxPointsSession.'" style="width: '.$percentageSessionPoints.'%">
                                <span>'.$sessionPoints.'</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-light-blue" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar->maxMilesSession.'" style="width: '.$percentageSessionMiles.'%">
                                <span>'.$sessionMiles.'</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row"><div class="col-xs-12" style="font-size: 14px;"><strong>Mes</strong></div></div>
                <div class="row">
                    <div class="col-xs-2" style="font-size: 10px; text-align: right;">
                        <div class="col-xs-12" style="height:1px;"></div>
                        Puntos Ordinarios
                        <div class="col-xs-12" style="height:10px;"></div>
                        Millas Extras
                    </div>
                    <div class="col-xs-10">
                        <div class="progress">
                            <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar->maxPointsMonthly.'" style="width: '.$percentageMonthlyPoints.'%">
                                <span>'.$percentageMonthlyPoints.' %</span>
                            </div> 
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-light-blue" role="progressbar" aria-valuemin="0" aria-valuemax="'.$generalVar->maxMilesMonthly.'" style="width: '.$percentageMonthlyMiles.'%">
                                <span>'.$monthMiles.'</span>
                            </div>
                        </div>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2" style="font-size: 8px; text-align: right;">
                        <strong>Posición</strong> '.$position.' de '.$count.' 
                    </div>
                    <div class="col-xs-10">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="'.$count.'" style="width: '.$percentagePosition.'%">
                            </div>
                        </div>   
                    </div>
                    <div class="col-xs-1"></div>
                </div>
            ';
        break;
        case 'Badges':
            $output = '';
            $sql = "CALL sp_badgesperperson_list('".$idUser."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $ix = 0;
            $InLine = 4;
            if($count > 0)  
            {  
                while($row = $result->fetch_assoc())
                {
                    $ix = ($ix % $InLine); 
                    if($ix == 0){
                        $output .= '<div class="row">';
                    }
                    $output .= '
                            <div class="col-xs-3" style="text-align:center; font-size: 12px;">
                                <img src="../../Multimedia/img/badges/'.$row["image"].'" class="img-responsive" width="100" height="100">
                                '.$row["name"].'
                            </div>
                    ';
                    if($ix == ($InLine-1)){
                        $output .= '</div>';}
                    $ix++;
                }
            }
        break;
        
        case 'PlaceTopTen':
            $output = $generalFn->getList("CALL sp_catalog_list('Lugar')","Lugar","ddl_place",$sqlOps,true);
        break;
        case 'TimeTopTen':
            $output = '<select class="selectpicker" title="Mes" data-width="100%" id="ddl_time" data-style="btn-info">';
            foreach(generalUserVariables::$meses as $id => $name) {
                $output .= '<option value="'.$id.'">'.$name.'</option>';
            }
            $output .= '</select>';
        break;
        case 'TopTenSpecificTable':
            $sql = "CALL sp_person_monthly_rank('".date("Y")."-".$_POST['date']."-01', '".$_POST['place']."')";
            $output = $generalFn->TopTenTable($idUser,$sql,$sqlOps);
        break;
    
        case 'TopTenNowTable':
            $sql = "CALL sp_person_monthly_rank('".$today."', '".$idPlace."')";
            $output = $generalFn->TopTenTable($idUser,$sql,$sqlOps);
        break;
        
        case 'SaveChocale':
            $sql = "CALL sp_chocale_save('".$idUser."','".$_POST['idPerson']."')";
            $sqlOps->sql_exec_op($sql);
        break;
        case 'RemoveChocale':
            $sql = "CALL sp_chocale_remove('".$idUser."','".$_POST['idPerson']."')";
            $sqlOps->sql_exec_op($sql);
        break;
    
        case 'show_qr':
            $output .= '
                <img src="../../Multimedia/img/qrcodes/'.$idUser.'.png" alt="..." style="width: 200px; height: 200px;">';
        break;
    }
    echo $output == '' ? '' : $output;