<?php
    session_start();
    $idUser = $_SESSION['idPerson'];
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    
    include "../../php/User/general.php";
    $generalFn = new generalUserFunctions();
    
    switch ($type_data){
        case 'Calendar':
            $sql = "CALL sp_person_calendar_list('".$idUser."',NULL,'General')";
            $res22 = $generalFn->get_array_calendar_event($sql,$sqlOps);
            echo json_encode($res22);
        break;
        case 'Details':
            $output = '';
            $sql = "CALL sp_person_calendar_list('".$idUser."','".$_POST['fecha']."','Details')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $flag = true;
            if($count > 0){
                $output .= '
                <br/>
                <table class="table table-bordered table-condensed" style="text-align:center; font-size: 12px; ">
                    <thead>
                        <tr>
                            <th width="30%" style="text-align:center; color: #37A8E2;"><b>Fecha</b></th>
                            <th width="25%" style="text-align:center; color: #22CEDC;"><b>Hora</b></th>
                            <th width="45%" style="text-align:center; color: #CCB05E;"><b>Evento y Lugar</b></th>
                        </tr>
                    </thead>
                    <tbody>';
                while($row = $result->fetch_assoc()){
                    if($count == 1){ //only one event
                        $output .= '
                            <tr>
                                <td width="30%">'.$row["DiaNombre"].'<h1>'.$row["Dia"].'</h1>'.$row["Mes"].', '.$row["Anio"].'</td>
                                <td width="25%" style="vertical-align: middle;">'.$row["Hora"].'</td>                            
                                <td width="45%" style="vertical-align: middle;"><b>'.$row["Nombre"].'</b><br/>'.$row["Lugar"].'</td>
                            </tr>';
                    }else{ //two or more event
                        if($flag){ //the first event
                            $output .= '
                            <tr>
                                <td width="30%" rowspan="'.$count.'">'.$row["DiaNombre"].'<h1>'.$row["Dia"].'</h1>'.$row["Mes"].', '.$row["Anio"].'
                                </td>
                                <td width="25%" style="vertical-align: middle;">'.$row["Hora"].'</td>
                                <td width="45%" style="vertical-align: middle;"><b>'.$row["Nombre"].'</b><br/>'.$row["Lugar"].'</td>
                            </tr>';
                            $flag = false;
                        }else{ //the n-events
                            $output .= '
                            <tr>
                                <td width="25%" style="vertical-align: middle;">'.$row["Hora"].'</td>
                                <td width="45%" style="vertical-align: middle;"><b>'.$row["Nombre"].'</b><br/>'.$row["Lugar"].'</td>
                            </tr>';
                        }
                    }
                }
                $output .= '
                    </tbody>
                </table>';
            }
        break;
        case 'Event':
            $output = ''; $head = ''; $posters = ''; $foot = '';
            $sql = "CALL sp_event_list(".$idUser.")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0)  
            {  
                $output .= '
                <div class="horizontalScroll" id="eventScroll">
                    <table class="table table-condensed table-bordered" style="text-align:center;">
                        <tbody>';
                while($row = $result->fetch_assoc()){
                    $text="";
                    $icon="";
                    $color="";
                    if($row["idPPE"]>0){
                        $text="Asistiré";
                        $icon="fa fa-check-square-o";
                        $color="#22CEDC";
                    }
                    else{
                        $text="Confirmar";
                        $icon="fa fa-square-o";
                        $color="gray";
                    }
                    $head .= '
                        <td style="font-size: 15px;">
                            <div style="color:'.$color.';" data-id1="'.$row["idEvent"].'" class="goingEvent" id="div_event'.$row["idEvent"].'">
                                <i class="'.$icon.'" aria-hidden="true" id="i_event'.$row["idEvent"].'"></i>
                                <span id="span_event'.$row["idEvent"].'">'.$text.'</span>
                            </div>
                        </td>' ;
                    $posters .= '
                        <td>
                            <a href="../../Multimedia/img/flyers/'.$row["flyerPath"].'">'
                                . '<img src="../../Multimedia/img/flyers/'.$row["flyerPath"].'" height="190">
                            </a>
                        </td>' ;
                    //list($dateEvent, $hourEvent, $placeEvent) = split(',', $row["titleEvent"]);
                    $foot .= '
                        <td>
                            <p style="font-size:12px;font-weight:bold;">'.$row["titleEvent"].'<p/>'
                            .'<p style="font-size:10px;">El '.$row["dateEvent"].'<p/>'
                            .'<p style="font-size:10px;">En '.$row["placeEvent"].'<p/>'
                            .'<p style="font-size:10px;">A las '.$row["hourEvent"].'<p/>'.
                        '</td>' ;
                }
                $output .= '<tr>'.$head.'</tr>';
                $output .= '<tr>'.$posters.'</tr>';
                $output .= '<tr>'.$foot.'</tr>';
                $output .= '
                        </tbody>
                    </table>
                </div>';
            }
        break;
        case 'AddPeopleEvent':
            $sqlOps->sql_exec_op("CALL sp_peopleperevent_add(".$_POST['idEvent'].",".$idUser.")");
        break;
        case 'RemovePeopleEvent':
            $sqlOps->sql_exec_op("CALL sp_peopleperevent_remove(".$_POST['idEvent'].",".$idUser.")");
        break;
    }
    echo $output == '' ? '' : $output;
?>