<?php
    class generalUserFunctions{
        ////////////////////////////////////////////////////////////////////////////////
        // FUNCTIONS
        ////////////////////////////////////////////////////////////////////////////////
        //NO
        public function get_Month_Range($fecha_begin,$fecha_end){
            $index = 0;
            $div_bgn = split("-", $fecha_begin); 
            $div_end = split("-", $fecha_end); 
            $results = array();
            do {
                $next = mktime (0,0,0,$div_bgn[1]+$index,$div_bgn[2], $div_bgn[0]);
                $newDate = getdate($next);
                $rawNameMonth = $newDate["mon"];
                $newNameMonth = strlen($rawNameMonth) == 1 ? '0'.$rawNameMonth : $rawNameMonth;
                $results[$index][0] = "01-".$newNameMonth."-".$newDate["year"];
                $results[$index][1] = generalUserVariables::$meses[$rawNameMonth];
                $index++;
            }while($newNameMonth != $div_end[1]);
        return $results;
        }
        public function get_array_calendar_attend($sql,$sqlOps){
            $result = $sqlOps->sql_multiple_rows($sql);
            $path1 = array();
            while($row = $result->fetch_assoc()){
                $color = $row["TYPE"] == 'Puntos Ordinarios' ? "ordinary_points" : "extra_milles";
                $path1[] = array("date"=>$row["FECHA_CALENDAR"], "title"=>$row["TYPE"], "classname"=>$color);
            }
            return $path1;    
        }
        public function get_array_calendar_event($sql,$sqlOps){
            $result = $sqlOps->sql_multiple_rows($sql);
            $path1 = array();
            while($row = $result->fetch_assoc()){
                $color = '';
                switch($row["Nombre"]){
                    case 'CitaNutricional': 
                        $color = "calendar_dates"; 
                    break;
                    case 'Cumpleanos': 
                        $color = "calendar_birthday"; 
                    break;
                    default: 
                        $color = "calendar_event";
                    break;
                }
                $path1[] = array("date"=>$row["Fecha"], "title"=>$row["Nombre"], "classname"=>$color);
            }
            return $path1;    
        }
        //NO
        public function fn_getBars($idUser,$typePoins,$today,$sqlOps) { 
            $list_total = 0;
            $list_month = 0;
            $this_month = date("m", strtotime($today));
            $sql = "CALL sp_person_attendance_details('".$idUser."','".$typePoins."','Both')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            if(isset($result->num_rows)){
                while($row = $result->fetch_assoc())
                {  
                    if($this_month == date("m",strtotime($row["ASISTENCIAS"]))){
                        $list_month += $row["PUNTOS"];
                    }
                    $list_total += $row["PUNTOS"];
                }
            }
            $list = array();
            $list[0] = $list_total; 
            $list[1] = $list_month; 
            return $list;
        }
        public function getList($sql,$title,$id,$sqlOps,$id_name){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$id.'">';
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["nameCatalog"] == 'Dia'){continue;}
                    $list .= $id_name == true ? '<option value="'.$row["idCatalog"].'">'.$row["nameCatalog"].'</option>' : '<option value="'.$row["nameCatalog"].'">'.$row["nameCatalog"].'</option>';
                }
            }
            return $list .= '</select>';
        }
        public function FavRateSigns($title,$type){
            return "            
            <div class='row'>
                <div class='col-xs-6'>
                    <div style='color: #3366ff; font-size: 18px;'>
                        <i class='fa fa-thumb-tack' aria-hidden='true'></i>
                        <strong>".$title."</strong>
                    </div>
                </div>
                <div class='col-xs-3' style='text-align: right;'>
                    <div style='color: red; font-size:14px; text-decoration: none;' id='fav_".$type."' data-id0='fav_".$type."' class='FavRateContent'>
                        <i class='fa fa-bookmark' aria-hidden='true'></i> Fav's
                    </div>
                </div>
                <div class='col-xs-3'>
                    <div style='color: #CCB05E; font-size: 14px; text-decoration: none;' id='rate_".$type."' data-id0='rate_".$type."' class='FavRateContent'>
                        <i class='fa fa-star' aria-hidden='true'></i> Rate
                    </div>
                </div>
            </div>";
        }
        public function TopTenTable($idUser,$sql,$sqlOps){
            $privateClass = new privateUserFunctions();
            $result = $sqlOps->sql_multiple_rows($sql);
            $table = '  
                <div class="container">
                    <table class="scroll table table-condensed">  
                        <tbody>';
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0)  
            {  
                while($row = $result->fetch_assoc()){
                    $table .= $privateClass->buildTable($idUser, $row, $sqlOps);
                }
                $table .= '
                        </tbody>
                    </table>
                </div>';
            }
            return $table;
        }
        public function ProGrade($pro, $sqlOps){
            $output = '';
            $proList = '';
            $sql = "CALL sp_professional_list('".$pro."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $proList .= '<option value="'.$row["idPerson"].'">'.$row["namePerson"].'</option>';
                }
            }   
            $privateClass = new privateUserFunctions();
            $output .= $privateClass->proList($pro, $proList);
            $output .= $privateClass->proRate();
            $output .= $privateClass->proComment();
            $output .= $privateClass->proSubmit();
            return $output;
        }
        public function SaveRate($proId,$idUser,$rateNo,$comment,$sqlOps){
            $sql = "CALL sp_rateteacher_add('".
                    $proId."','".
                    $idUser."','".
                    $rateNo."','".
                    $comment."')";
            $sqlOps->sql_exec_op($sql);
        }
        public function canvasGraph(){
            return '<canvas id="canvas_div" height="100px" width="100%"></canvas>';
        }
        public function profilePic($coachId,$sqlOps){
            $sql0 = "CALL sp_person_picture('".$coachId."','ACTIVE')";
            $row0 = $sqlOps->sql_single_row($sql0);
            $imageUrl = $row0 == '' ? "no_image.png" : $row0["url"];
            return '<img '
                    . 'src="../../Multimedia/img/people/'.$imageUrl.'" '
                    . 'width="40px" '
                    . 'height="40px" '
                    . 'class="img-responsive img-circle" '
                    . 'style="float:left">';
        }
        public function TableContent($typeContent,$idContent,$idUser,$titleFont,$elementName,$idTable,$NoStar,$sqlOps){
            $titles = ''; 
            $bodies = ''; 
            $rates = ''; 
            $output = ''; 
            $lcl_array = array(); 
            $lcl_array[0] = ''; 
            $lcl_array[1] = $NoStar;
            $privateClass = new privateUserFunctions();
            $sql = "CALL sp_digitalcontent_list('".$typeContent."','".$idContent."','".$idUser."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $titles .= $privateClass->titleContent($row["favorite"],$titleFont,$row["nameCont"],$typeContent,$row["idCont"]);
                    $bodies .= $privateClass->bodyContent($typeContent,$row["valueCont"],$row["thumbnail"],$row["nameCont"]);
                    $lcl_array = $privateClass->footRate($row["rate"],$lcl_array[1],$row["idCont"],$typeContent);
                    $rates .= $lcl_array[0];
                }
            }
            if($titles == '' || $bodies == '' || $rates == ''){ 
                $output = $privateClass->validateContent($elementName); 
            }else{ 
                $output = $privateClass->buildContentTable($titles,$idTable,$bodies,$rates); 
            }
            return $output;
        }
        public function TableFavRate($idUser,$cont,$type,$typeContent,$sqlOps){
            $query = "CALL sp_fav_rate_list('".$idUser."','".$cont."','".$type."')";
            $result = $sqlOps->sql_multiple_rows($query);
            $count = $result ? mysqli_num_rows($result) : -1;
            $privateClass = new privateUserFunctions();
            $output = '';
            if($count > 0){
                $titles = ''; $bodies = '';
                while($row = $result->fetch_assoc()){
                    $titles .= '<td>'.$row["nameCont"].'</td>';
                    $bodies .= $privateClass->bodyContent($typeContent,$row["valueCont"],$row["thumbnail"],"");
                }
                $output = $privateClass->buildShowFavRateTable($titles, $bodies);
            }
            return $output;
        }
    }
    class privateUserFunctions{
        public function buildTable($idUser,$row,$sqlOps){
            $infoChocales = $sqlOps->sql_single_row("CALL sp_chocale_list(".$row["idPerson"].",'General')");
            $hasChocale = $sqlOps->sql_single_row("CALL sp_chocale_check(".$idUser.",".$row["idSession"].")");
            $imgUsed = $hasChocale["NoChocales"] > 0 ? "../../Multimedia/img/Chocales/high_five_on.png" : "../../Multimedia/img/Chocales/high_five_off.png";
            $table = '
                <tr>  
                    <td width="25%">
                        <img src="../../Multimedia/img/stars/star-'.$row["position"].'.png" class="img-responsive" width="50px" height="50px">
                    </td>  
                    <td width="25%" style="align-content: center; vertical-align: middle;">
                        <img src="../../Multimedia/img/people/min'.$row["Picture"].'" class="img-circle img-responsive" width="50px" height="50px">
                    </td>  
                    <td width="25%"><strong>'.$row["namePerson"].'</strong><br/><p style="font-size:10px;">'.$row["Points"].' millas extras</p></td>  
                    <td width="25%" data-id0="'.$row["idPerson"].'" class="chocalePic">
                        <img id="ImgChocale'.$row["idPerson"].'" src="'.$imgUsed.'" class="img-responsive" width="80%" height="80%">'
                            . '<p id="NoChocale'.$row["idPerson"].'" style="font-size:8px;">'.$infoChocales["NoChocales"].' Chocales</p>
                    </td>  
                </tr>
            ';
            return $table;
        }
        //
        public function proList($pro, $proList){
            $dll = '
                <div class="row">
                    <div class="col-xs-9">
                       <select class="selectpicker" data-style="btn-info" title="Nombre del '.$pro.'" data-width="100%" id="coach">';
            $dll .= $proList;
            $dll .= '   </select>
                    </div>
                    <div class="col-xs-3">
                        <div id="profilePic_div"></div>
                    </div>
                </div><br/>';
            return $dll;
        }
        public function proRate(){
            return '
                <div class="row">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-8" style="text-align:center; color:#CCB05E; font-size: 20px; vertical-align: top;">
                        <span class="glyphicon glyphicon-star-empty star-set" id="star1"></span>
                        <span class="glyphicon glyphicon-star-empty star-set" id="star2"></span>
                        <span class="glyphicon glyphicon-star-empty star-set" id="star3"></span>
                        <span class="glyphicon glyphicon-star-empty star-set" id="star4"></span>
                        <span class="glyphicon glyphicon-star-empty star-set" id="star5"></span>
                        <span class="label label-default" id="rate_text" style="color:white; background-color:white;"> </span>
                    </div>
                    <div class="col-xs-2"></div>
                </div><br/>';
        }
        public function proComment(){
            return '
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <textarea style="text-align:center;" class="form-control" rows="5" id="comment" placeholder="¿Algún comentario extra?"></textarea>
                        </div>
                    </div>
                </div>';
        }
        public function proSubmit(){
            return '
                <div class="row">
                    <div class="col-xs-3"></div>
                    <div class="col-xs-6">
                        <button type="button" class="btn btn-success" style="width:100%" id="saveRate">Guardar</button>
                    </div>
                    <div class="col-xs-3"></div>
                </div>';
        }
        //
        public function titleContent($favorite,$titleFont,$nameCont,$typeContent,$idCont){
            $favContent = $favorite>0 ? "fa fa-bookmark fa-2x favContent" : "fa fa-bookmark-o fa-2x favContent";
            return $title = '
            <td>
                <div class="row">
                    <div class="col-xs-9">
                        <p style="color: #1DB5C0; font-size: '.$titleFont.'px;">'.$nameCont.'</p>
                    </div>
                    <div class="col-xs-3" style="text-align:right; font-size: 8px;">
                        <span style="color:red;" class="'.$favContent.'" id="'.$typeContent.'-'.$idCont.'" data-id0="'.$idCont.'%'.$typeContent.'"></span>
                    </div>
                </div>
            </td>';
        }
        public function bodyContent($typeContent,$valueCont,$thumbnail,$nameCont){
            $body = '';
            if($typeContent == 'Videos'){
                $body .= '
                    <td>
                        <a href="player.php?idVideo='.$valueCont.'">
                            <img src="https://img.youtube.com/vi/'.$valueCont.'/mqdefault.jpg" height="200" width="200">
                        </a>
                    </td>';
            }
            else if($typeContent == 'Articulos' || $typeContent == 'Recetas'){
                $body .= '
                    <td>
                        <a href="../../Multimedia/files/'.$nameCont.'.php">
                            <img src="../../Multimedia/files/'.$valueCont.'" height="190">
                        </a>
                    </td>';
            }
            else
            {
                $body .= '
                    <td>
                        <a href="../../Tools/pdfjs/web/viewer.html?file=../../../Multimedia/files/upload/'.$valueCont.'">
                            <img src="../../Multimedia/files/thumbnails/'.$thumbnail.'" height="190">
                        </a>
                    </td>';
            }
            return $body;
        }
        public function footRate($rate,$idStar,$idCont,$typeContent){
            $lcl_star = $idStar;
            $lcl_rate = '<td style="color:#CCB05E;">';
            for ($i = 1; $i < 6; $i++) {
                $currentRate = $i <= $rate ? "glyphicon glyphicon-star star-set" : "glyphicon glyphicon-star-empty star-set" ;
                $lcl_rate .= '<span '
                        . 'class="'.$currentRate.'" '
                        . 'id="star-'.$lcl_star++.'" '
                        . 'data-id3="'.$idCont.'%'.$i.'%'.$typeContent.'"></span>';
            }
            $text = $rate > 0 ? $rate : ' ';
            $lcl_rate .= '<span class="label label-default" id="rate-text-'.($lcl_star-1).'" style="color:#464040; background-color:#CCB05E;">'.$text.'</span>';
            $lcl_rate .= '</td>';
            $result10 = array();
            $result10[0] = $lcl_rate;
            $result10[1] = $lcl_star;
            return $result10;
        }
        public function validateContent($elementName){
            return $output = '
                    <div class="row">
                        <div class="col-xs-2">
                            <img src="../../Multimedia/img/sad_smiley.png" class="img-responsive" width="50px" height="50px">
                        </div>
                        <div class="col-xs-10">
                            Lo sentimos, no hay '.$elementName.' en esta categoria.
                        </div>
                    </div>';
        }
        public function buildContentTable($titles,$idTable,$bodies,$rates){
            $output = ' 
                <div class="horizontalScroll"  id="'.$idTable.'">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>';
            $output .= $titles;
            $output .= '
                            </tr>
                            <tr>';
            $output .= $bodies;
            $output .= '
                            </tr>
                            <tr style="text-align: center">';
            $output .= $rates;
            $output .= '    </tr>
                        </tbody>
                    </table>
                </div>';
            return $output;
        }
        public function buildShowFavRateTable($titles,$bodies){
            $output = ' 
                    <div class="horizontalScroll">
                        <table class="table table-bordered">
                            <tbody style="color: #1DB5C0; text-align: center;">
                                <tr>';
                $output .= $titles;
                $output .= '    </tr>';
                $output .= '    <tr>';
                $output .= $bodies;
                $output .= '    </tr>
                            </tbody>
                        </table>
                    </div>';
            return $output;
        }
    }
    class generalUserVariables{
        /*
            attendance                  +   nutrition
            (days*weeks*months*points)  +   (months*points)
             * session
            2*4*6*130 + 6*300 = 8,040
            3*4*130*6 + 0 = 9360
             * month
            2*4*130 + 300 = 1,340
            3*4*130 + 0 =  1560
        */
        ////////////////////////////////////////////////////////////////////////////////
        // VARIABLES
        ////////////////////////////////////////////////////////////////////////////////
        public $maxPointsSession = 8040;
        public $maxMilesSession = 9360;
        public $maxPointsMonthly = 1340; //per session
        public $maxMilesMonthly = 1560; //per session
        public $pointPerClass = 130;
        public static $meses = array(
            1=>"Ene",2=>"Feb",3=>"Mar",4=>"Abr",
            5=>"May",6=>"Jun",7=>"Jul",8=>"Ago",
            9=>"Sep",10=>"Oct",11=>"Nov",12=>"Dic");
        public $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
    }
?>
