<?php
    class generalNutricionistFunctions{
        ////////////////////////////////////////////////////////////////////////////////
        // FUNCTIONS
        ////////////////////////////////////////////////////////////////////////////////
        public function get_array_calendar_event($sql,$sqlOps){
            $result = $sqlOps->sql_multiple_rows($sql);
            $path1 = array();
            while($row = $result->fetch_assoc()){
                $path1[] = array("date"=>$row["Fecha"], "classname"=>"grade-extra");
            }
            return $path1;    
        }
        public function getListUploadContent($sql,$title,$id,$sqlOps){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="form-control selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$id.'">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    if($row["nameCatalog"] == 'Dietas' || $row["nameCatalog"] == 'Esfuerzo'){
                        $list .= '<option value="'.$row["idCatalog"].'">'.$row["nameCatalog"].'</option>';
                    }
                }
            }
            return $list .= '</select>';
        }
        public function getList($sql,$title,$id,$sqlOps){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="form-control selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$id.'">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $list .= '<option value="'.$row["idCatalog"].'">'.$row["nameCatalog"].'</option>';
                }
            }
            return $list .= '</select>';
        }
        public function getListCol($sql,$title,$id,$sqlOps,$idcCol,$nameCol){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select class="form-control selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$id.'">';    
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $list .= '<option value="'.$row[$idcCol].'">'.$row[$nameCol].'</option>';
                }
            }
            return $list .= '</select>';
        }
        public function getListWithDataId($sql,$title,$id,$sqlOps,$value){
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '<select  data-id0="'.$value.'" class="selectpicker" data-style="btn-info" title="'.$title.'" data-width="100%" id="'.$id.'">';
            if($count > 0){
                while($row = $result->fetch_assoc()){
                    $list .= '<option value="'.$row["idCatalog"].'">'.$row["nameCatalog"].'</option>';
                }
            }
            return $list .= '</select>';
        }
    }
    class generalNutricionistVariables{
        public $meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
        public $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
        public $lbl = array("Nombre", "Nick", "Fecha de Nac.", "Altura", "Genero", "Telefono", "Correo", "Temporada", "Sede");
        public $NutriInfo = array("Nutricionista", "Lugar", "Fecha", "Hora", "Puntos", "Tipo de Cita", "Tipo de Medición", "Tipo");
    }    
?>