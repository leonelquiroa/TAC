<?php
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    include "../../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();
    include "../../php/Nutrition/general.php";
    $generalFn = new generalNutricionistFunctions();
    $generalVar = new generalNutricionistVariables();      
    switch ($type_data){
        //options
        case 'ddl_places':
             $output = $generalFn->getList(
                        "CALL sp_catalog_list('".$_POST['name_catalog']."')",
                        $_POST['title'],
                        $_POST['title'],
                        $sqlOps); 
        break;
        case 'people_ddl':
            $sql = "CALL sp_administrator_people_list_by_place('','".$_POST['placeID']."','ALL')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $output = '<select class="selectpicker" data-style="btn-info" data-width="100%" title="Nombre" id="ddl_people" data-live-search="true" data-size="5">';
                while($row = $result->fetch_assoc())
                {  
                    $output .= '<option value="'.$row["idPerson"].'">'.$row["namePerson"].'</option>';
                }
                $output .= '</select>';
            }
        break;        
        //watches
        case 'tanita':
            session_start();
            $idPerson = isset($_SESSION['idPatient']) ? $_SESSION['idPatient'] : -1;
            $res = array();
            $ix = 0;

            $sql1 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Porcentaje De Agua'),'Last')";
            $indicator1 = $sqlOps->sql_single_row($sql1);
            if($indicator1 !== ''){
                $res[$ix][0] = $indicator1["Tiempo"];
                $res[$ix][1] = $indicator1["Valor"];
                $ix++;
            }
            
            $sql2 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Porcentaje De Grasa Corporal'),'Last')";
            $indicator2 = $sqlOps->sql_single_row($sql2);
            if($indicator2 !== ''){
                $res[$ix][0] = $indicator2["Tiempo"];
                $res[$ix][1] = $indicator2["Valor"];
                $ix++;
            }

            $sql3 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Indice de Masa Corporal - IMC'),'Last')";
            $indicator3 = $sqlOps->sql_single_row($sql3);
            if($indicator3 !== ''){
                $res[$ix][0] = $indicator3["Tiempo"];
                $res[$ix][1] = $indicator3["Valor"];
                $ix++;
            }

            $sql4 = "CALL sp_person_nutrition_list('".$idPerson."',getCategoryId('Indicadores','Porcentaje Grasa Visceral'),'Last')";
            $indicator4 = $sqlOps->sql_single_row($sql4);
            if($indicator4 !== ''){
                $res[$ix][0] = $indicator4["Tiempo"];
                $res[$ix][1] = $indicator4["Valor"];
                $ix++;
            }

            echo count($res) > 0 ? json_encode($res) : '';
        break;    
        //table
        case 'indicators_ddl':
            $sql = "CALL sp_nutriappoint_past_list('".$_POST['idPerson']."')";  
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $output = '<select class="selectpicker" data-style="btn-info" data-width="100%" title="Cita" id="ddl_indicators">';
                while($row = $result->fetch_assoc())
                {  
                    $output .= '<option data-subtext="-  '.$row["MeasureType"].'" value="'.$row["idNutriAppoint"].'#'.$row["MeasureType"].'">'.$row["Fecha"].'</option>';
                }
                $output .= '</select>';
            }
        break;
        case 'indicators_past_info':
            $sql = "CALL sp_nutriappoint_past_general('".$_POST['idNutriAppoint']."')";  
            $row = $sqlOps->sql_single_row($sql);
            if($row !== ''){
                $output = '
                <table class="table table-hover table-bordered table-condensed">
                    <thead>
                      <tr>
                        <th>Campo</th>
                        <th>Valor</th>
                      </tr>
                    </thead>
                    <tbody style="font-size:14px;">';
                $list = ''; $ix = 0;
                foreach ($row as $field){
                    if($generalVar->NutriInfo[$ix] == 'Puntos'){
                        $list .= '
                        <tr>
                            <td>'.$generalVar->NutriInfo[$ix].'</td>
                            <td class="assignPoints" contenteditable="true" data-id0="'.$_POST['idNutriAppoint'].'" id="color_'.$_POST['idNutriAppoint'].'">'.$field.'</td>
                        </tr>';
                    }
                    else
                    {
                        $list .= '
                        <tr>
                            <td>'.$generalVar->NutriInfo[$ix].'</td>
                            <td>'.$field.'</td>
                        </tr>';
                    }
                    $ix++;
                }
                $output .= $list.'
                    </tbody>
                </table>';
            }
        break;
        case 'indicators_table':
            $sql1 = "CALL sp_detailsnutriappoint_list(".$_POST['idPerson'].",".$_POST['idNutriAppoint'].",'Ranges')";
            $result = $sqlOps->sql_multiple_rows($sql1);
            $list = '';
            $initialValue = '';
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $colorInitial = '';$colorCurrent = '';$colorGoal = '';
                while($row = $result->fetch_assoc())
                {  
                    $glyph = (intval($row["Meta"]) > intval($row["Inicial"])) ? 'up' : 'down';
                    $color = (intval($row["Meta"]) > intval($row["Inicial"])) ? 'green' : 'red';
                    switch($_POST['measureType']){
                        case 'inicial':
                            $colorInitial = 'blue';$colorCurrent = 'gray';$colorGoal = 'gray';
                            $list .= '
                            <tr>
                                <td style="cursor: pointer;" data-id1="'.$row["idIndicador"].'" class="indicatorSee">'
                                    . '<i class="fa fa-chevron-circle-'.$glyph.'" aria-hidden="true" style="color:'.$color.';"></i> '
                                    .$row["nameIndicador"].
                                '</td>
                                <td contenteditable="true" class="indicatorUpdate" data-id0="'.$row["idIndicador"].'" id="color_'.$row["idIndicador"].'">'.$row["Inicial"].'</td>
                                <td></td>
                                <td></td>
                            </tr>';
                        break;
                        case 'actual':
                            $colorInitial = 'gray';$colorCurrent = 'blue';$colorGoal = 'gray';
                            $list .= '
                            <tr>
                                <td style="cursor: pointer;" data-id1="'.$row["idIndicador"].'" class="indicatorSee">'
                                    . '<i class="fa fa-chevron-circle-'.$glyph.'" aria-hidden="true" style="color:'.$color.';"></i> '
                                    .$row["nameIndicador"].
                                '</td>
                                <td>'.$row["Inicial"].'</td>
                                <td contenteditable="true" class="indicatorUpdate" data-id0="'.$row["idIndicador"].'" id="color_'.$row["idIndicador"].'">'.$row["Actual"].'</td>
                                <td>'.$row["Meta"].'</td>
                            </tr>';
                        break;
                        case 'meta':
                            $colorInitial = 'gray';$colorCurrent = 'gray';$colorGoal = 'blue';
                            $list .= '
                            <tr>
                                <td style="cursor: pointer;" data-id1="'.$row["idIndicador"].'" class="indicatorSee">'
                                    . '<i class="fa fa-chevron-circle-'.$glyph.'" aria-hidden="true" style="color:'.$color.';"></i> '
                                    .$row["nameIndicador"].
                                '</td>
                                <td></td>
                                <td></td>
                                <td contenteditable="true" class="indicatorUpdate" data-id0="'.$row["idIndicador"].'" id="color_'.$row["idIndicador"].'">'.$row["Meta"].'</td>
                            </tr>';
                        break;
                    }
                }
                $output = '
                <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Indicador</th>
                        <th style="color:'.$colorInitial.'">Inicial</th>
                        <th style="color:'.$colorCurrent.'">Actual</th>
                        <th style="color:'.$colorGoal.'">Meta</th>
                      </tr>
                    </thead>
                    <tbody style="font-size:14px;">';
                $output .= $list.'
                    </tbody>
                </table>';
            }            
        break;
        //table - new lecture
        case 'edit_measure':
            $sql1 = "CALL sp_detailsnutriappoint_exist(".$_POST['idNutriAppoint'].",".$_POST['idData'].")";
            $exist = $sqlOps->sql_single_row($sql1);
            $sql2 = "CALL sp_detailsnutriappoint_edit(".
                        $exist["idDetails"].",'".trim(str_replace(' ','',$_POST['dataValue']), "\n")."')";
            $output = $sqlOps->sql_exec_op($sql2);
        break;
        case 'assign_points':
            $sql2 = "CALL sp_nutriappoint_assign_points('".$_POST['idNutriAppoint']."','".$_POST['dataValue']."')";
            $output = $sqlOps->sql_exec_op($sql2);            
        break;    
        //graph
        case 'refresh_canvas':
            $output = '<canvas id="canvas-line"></canvas>';            
        break;
        case 'lectures':
            $res = array();
            $res[0][0] = "Tiempo";
            $res[0][1] = "Valor";
            $res[0][2] = "Name";
            $sql1 = "CALL sp_person_nutrition_list('".$_POST['personID']."','".$_POST['idIndicator']."','Details')";
            $result = $sqlOps->sql_multiple_rows($sql1);
            $ix = 0;
            while($row = $result->fetch_assoc())
            {
                $res[$ix][0] = $row["Tiempo"];
                $res[$ix][1] = $row["Valor"];
                $res[$ix][2] = $row["NameIndicator"];
                $ix++;
            }
            echo json_encode($res);            
        break;
    }
    echo $output == '' ? '' : $output;    