<?php
    include "../../../Model/SqlOperations.php";
    include "../../php/Sales/general.php";    
    $sqlOps = new SqlOperations();
    $generalFn = new generalSalesFunctions();  
    $generalVar = new generalSalesVariables();   
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data){
        case 'place_ddl':
            $output = $generalFn->getList(
                "CALL sp_catalog_list('Lugar')",
                "Lugar",
                "ddl_place",
                $sqlOps,
                "idCatalog",
                "nameCatalog");
        break;
        //graph1
        case 'sedentary_active_graph':
            $res = array();
            $total_people = 0;
            $total_people_fitness = 0;
            //list
            $sql8 = "CALL sp_administrator_people_list_by_place('".$_POST['dateSelect']."','".$_POST['placeID']."','DATE')";
            $result8 = $sqlOps->sql_multiple_rows($sql8);
            $count1 = $result8 ? mysqli_num_rows($result8) : -1;
            if($count1 > 0) {
                while($row = $result8->fetch_assoc()){$total_people++;}
                //points and nutri
                $sql2 = "CALL sp_person_calculate_positions('".$_POST['dateSelect']."','".$_POST['placeID']."','Ordinario')";
                $result2 = $sqlOps->sql_multiple_rows($sql2);
                $count2 = $result2 ? mysqli_num_rows($result2) : -1;
                if($count2 > 0) {
                    while($row = $result2->fetch_assoc()){$total_people_fitness++;}
                }
            }
            $total_people = $total_people == 0 ? 1 : $total_people;
            //
            $res[0][0] = "Activo";
            $res[0][1] = $total_people_fitness;
            $res[0][2] = round((($total_people_fitness/$total_people)*100), 2);
            $res[1][0] = "Sedentario";
            $res[1][1] = $total_people-$total_people_fitness;
            $res[1][2] = round(((1-$total_people_fitness/$total_people)*100), 2);
            //
            echo json_encode($res);          
        break;
        case 'sedentary_active_table':
            $rows = $_POST['data'];
            $output .= '
            <table class="table scroll table-condensed table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>No. de Personas</th>
                        <th>Porcentaje</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.$rows[0][0].'</td>
                        <td>'.$rows[0][1].'</td>
                        <td>'.$rows[0][2].' %</td>
                    </tr>
                    <tr>
                        <td>'.$rows[1][0].'</td>
                        <td>'.$rows[1][1].'</td>
                        <td>'.$rows[1][2].' %</td>
                    </tr>
                </tbody>
            </table>';  
        break;
        //graph2
        case 'gender_graph':
            $sql = "CALL sp_sales_age_gender_by_place('".$_POST['placeID']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list_people = '';
            $res = array();
            if($count > 0) {
                $no_women = 0;
                $no_men = 0;
                while($row = $result->fetch_assoc()){
                    //gender
                    if($row["Genero"] == 'Hombre'){$no_men++;}
                    elseif($row["Genero"] == 'Mujer'){$no_women++;}
                }
                //
                $res[0][0] = "Hombre";
                $res[0][1] = $no_men;
                $res[0][2] = round($no_men/($no_men + $no_women)*100, 2);
                $res[1][0] = "Mujer";
                $res[1][1] = $no_women;
                $res[1][2] = round($no_women/($no_men + $no_women)*100, 2);
                //
                echo json_encode($res); 
            }
        break;
        case 'gender_table':
            $rows = $_POST['data'];
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Genero</th>
                            <th>Personas</th>
                            <th>Porcentaje</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>'.$rows[0][0].'</td>
                            <td>'.$rows[0][1].'</td>
                            <td>'.$rows[0][2].' %</td>
                        </tr>
                        <tr>
                            <td>'.$rows[1][0].'</td>
                            <td>'.$rows[1][1].'</td>
                            <td>'.$rows[1][2].' %</td>
                        </tr>
                    </tbody>
                </table>';
        break;
        //graph3
        case 'age_graph':
            //initialize the ages table/array
            $ages = array();
            $contador = array();
            $res = array();
            for($i = 0; $i < $generalVar->jumps; $i++){
                $ages[$i] = $generalVar->low_age;
                $contador[$i] = 0;
                $generalVar->low_age += $generalVar->range;
            }
            //
            $sql = "CALL sp_sales_age_gender_by_place('".$_POST['placeID']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list_people = '';
            $ix = 0;
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    //age
                    $diff = $row["Edad"] % $generalVar->range;
                    $pos = array_search($row["Edad"] - $diff, $ages);
                    $contador[$pos] = $contador[$pos] + 1;
                }
                $total = array_sum($contador); 
                for($i = 0; $i < $generalVar->jumps; $i++){
                    $label = '';
                    if($i == ($generalVar->jumps-1)){ $label = '+'.$ages[$i];}
                    else if($i == 0){ $label = ($ages[$i]).'-'.$ages[$i+1];}
                    else{ $label = ($ages[$i]+1).'-'.$ages[$i+1];}
                    //
                    $res[$ix][0] = $label;
                    $res[$ix][1] = $contador[$i];
                    $res[$ix][2] = round($contador[$i]/$total*100, 2);
                    $ix++;
                }
            }
            echo json_encode($res); 
        break;
        case 'age_table':
            $rows = $_POST['data'];
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Edad</th>
                            <th>Personas</th>
                            <th>Porcentaje</th>
                        </tr>
                    </thead>';
            $list_people = '';
            foreach($rows as $row) {
                $list_people .= '
                    <tr>
                        <td>'.$row[0].'</td>
                        <td>'.$row[1].'</td>
                        <td>'.$row[2].' %</td>
                    </tr>';
            }
            $output .= '<tbody>'.$list_people.'</tbody>';
            $output .= '</table>';
        break;
        //graph4
        case 'camp_graph':
            $res = array();
            $camp = array();
            $contador = array();
            for($i = 0; $i < $generalVar->no_camps; $i++){
                $contador[$i] = 0;
                $camp[$i] = $i + 1;
            }
            //
            $sql = "CALL sp_sales_camp_by_place('".$_POST['placeID']."')";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list_people = '';
            $ix = 0;
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    //camp
                    $pos = array_search($row["Campo"], $camp);
                    $contador[$pos] = $contador[$pos] + 1;
                }
                $total = array_sum($contador); 
                for($i = 0; $i < $generalVar->no_camps; $i++){
                    $res[$ix][0] = $camp[$i];
                    $res[$ix][1] = $contador[$i];
                    $res[$ix][2] = round($contador[$i]/$total*100, 2);
                    $ix++;
                }
            }    
            echo json_encode($res); 
        break;
        case 'camp_table':
            $rows = $_POST['data'];
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Campo</th>
                            <th>Personas</th>
                            <th>Porcentaje</th>
                        </tr>
                    </thead>';
            $list_camp = '';
            foreach($rows as $row) {
                $list_camp .= '
                    <tr>
                        <td>'.$row[0].'</td>
                        <td>'.$row[1].'</td>
                        <td>'.$row[2].' %</td>
                    </tr>';
            }
            $output .= '<tbody>'.$list_camp.'</tbody>';
            $output .= '</table>';  
        break;
        //graph5
        case 'statistical_measures':
            $table = $generalFn->get_current_measure("CALL sp_sales_indicators_by_place('".$_POST['placeID']."')",$sqlOps);
            $weightValues = $generalFn->get_statistical_values($table, 'idData','22');
            $fatValues = $generalFn->get_statistical_values($table, 'idData','33');
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Indicador</th>
                            <th>Minimo</th>
                            <th>Promedio</th>
                            <th>Maximo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Peso</td>
                            <td>'.$weightValues[0]['min'].' lbs.</td>
                            <td>'.$weightValues[0]['avg'].' lbs.</td>
                            <td>'.$weightValues[0]['max'].' lbs.</td>
                        </tr>
                        <tr>
                            <td>Porcentaje de Grasa Corporal</td>
                            <td>'.$fatValues[0]['min'].' %</td>
                            <td>'.$fatValues[0]['avg'].' %</td>
                            <td>'.$fatValues[0]['max'].' %</td>
                        </tr>
                    </tbody>
                </table>';                
        break;
        //graph6
        case 'goal_measures':
            //initial
            $Initial = $generalFn->getArrayMeasures("CALL sp_sales_goal_by_place('".$_POST['placeID']."','medida inicial')",$sqlOps);
            $initialWeight = $generalFn->filter_by_value($Initial, 'idData','22');
            $initialFat = $generalFn->filter_by_value($Initial, 'idData','33');
            //current
            $preCurrent = $generalFn->get_current_measure("CALL sp_sales_indicators_by_place('".$_POST['placeID']."')",$sqlOps);
            $x = 0;
            foreach ($preCurrent as $key => $value) {
                $Current[] = array(
                    "idPerson" => $preCurrent[$key]['idPerson']
                    ,"idData" => $preCurrent[$key]['idData']
                    ,"value" => $preCurrent[$key]['valueIndicador']
                );
            }
            $currentWeight = $generalFn->filter_by_value($Current, 'idData','22');
            $currentFat = $generalFn->filter_by_value($Current, 'idData','33');
            //goal
            $Goal = $generalFn->getArrayMeasures("CALL sp_sales_goal_by_place('".$_POST['placeID']."','medida meta')",$sqlOps);
            $goalWeight = $generalFn->filter_by_value($Goal, 'idData','22');
            $goalFat = $generalFn->filter_by_value($Goal, 'idData','33');
            //
            $weightResults = $generalFn->getSucces_Fail_Goal($initialWeight,$currentWeight,$goalWeight);
            $fatResults = $generalFn->getSucces_Fail_Goal($initialFat,$currentFat,$goalFat);
            $output .= '
                <table class="table scroll table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Indicador</th>
                            <th>Exito</th>
                            <th>Fallo</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Peso</td>
                            <td>'.$weightResults[0]['success'].'</td>
                            <td>'.$weightResults[0]['fail'].'</td>
                            <td>'.($weightResults[0]['success'] + $weightResults[0]['fail']).'</td>
                        </tr>
                        <tr>
                            <td>Porcentaje de Grasa Corporal</td>
                            <td>'.$fatResults[0]['success'].'</td>
                            <td>'.$fatResults[0]['fail'].'</td>
                            <td>'.($fatResults[0]['success'] + $fatResults[0]['fail']).'</td>
                        </tr>
                    </tbody>
                </table>';        
        break;
        //update graph canvas
        case 'chart_pro':
            $output .= '<canvas id="chart-area-'.$_POST['graphNo'].'"></canvas>';
        break;   
    }
    echo $output == '' ? '' : $output;