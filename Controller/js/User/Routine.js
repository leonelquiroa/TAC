var levelID = -1;
var path = "../../Controller/php/User/OpsRoutine.php";

function phpServer(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init(){ 
    phpServer(path,{type:'ddl_routine'},"level_div");
    phpServer(path,{type:'signs_fav_rate_video'},"signs_fav_rate_div");
    phpServer(path,{type:'fav_rate_Videos',cont:'Videos'},"table_Videos_Fav_Rate_div");
    $('.selectpicker').selectpicker('show');
}

$(document).ready(function(){
    init();
});
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDDL = this.id;
    var selected = $(this).val();
    if(nameDDL === 'ddl_level'){
        levelID = selected;
        phpServer(path,{type:'table_routine',idTypeRoutine:levelID},"table_routine_div");
    }
});