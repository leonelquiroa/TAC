var typeRecipeID = -1;
var uploadRecipeID = -1;
var path = "../../Controller/php/User/OpsDietEffort.php";

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()
{ 
    //Diets
    ajax_call(path,{type:'signs_fav_rate_diet'},"signs_Dietas_fav_rate_div");
    ajax_call(path,{type:'table_diet'},"table_diet_div");
    ajax_call(path,{type:'fav_rate_Dietas',cont:'Dietas'},"table_Dietas_Fav_Rate_div");
    //Effort
    ajax_call(path,{type:'signs_fav_rate_effort'},"signs_Esfuerzo_fav_rate_div");
    ajax_call(path,{type:'table_effort'},"table_effort_div");
    ajax_call(path,{type:'fav_rate_Esfuerzo',cont:'Esfuerzo'},"table_Esfuerzo_Fav_Rate_div");
}

$(document).ready(function() {
    init();
});


