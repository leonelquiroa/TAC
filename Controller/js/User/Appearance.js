var path = "../../Controller/php/User/OpsAppearance.php";
var idActivity = -1;

function phpServer(sent_url,sent_data,div,async){
    $.ajax({
        cache: false,
        async: async,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
    });
}
function init(){ 
    showUserInfo();
    phpServer(path,{type:'fillActivityDdl'},"ddl_activity_div",false);
    $("#a").show("fast").delay(1).fadeOut('fast'); 
    $("#b").show("fast").delay(1).fadeOut('fast'); 
}
function message(divName, message, iconType){
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, 
        function(m,key,value) {
            vars[key] = value;
    });
    return vars;
}
function showUserInfo(){
    var first = getUrlVars()["id"];
    phpServer(path,{type:'showUserInfo',idUser:first},"userInfo_div",true);
}

$(document).ready(function(){
    init();
});
$(document).on('click', '#btn_register', function(e){
    var idPerson = $('#id_div').text();
    if(idPerson != ''){
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'checkActivity',
                idClass:idActivity,
                idPerson:idPerson
            },  
            dataType:"text",  
            success:function(d){  
                if(d>0){
                    phpServer(path,{type:'fillActivityDdl'},"ddl_activity_div",false);
                    $('.selectpicker').selectpicker('show');
                    var aa = $("#a");
                    aa.text('Se ha registrado la asistencia satisfactoriamente.');
                    aa.show("fast").delay(1000).fadeOut('fast'); 
                }
                else{
                    var bb = $("#b");
                    bb.text('Lo sentimos, la asistencia no se pudo registrar.');
                    bb.show("fast").delay(1000).fadeOut('fast'); 
                }
            }  
        });  
    }
}); 
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDDL = this.id;
    var selected = $(this).find("option:selected").val();
    if(nameDDL === 'ddl_activity'){
        idActivity = selected;
    }
});