var chocale = true;
var path = "../../Controller/php/User/OpsHome.php";
var topTenDate = -1;
var topTenPlace = -1;
var IsShow = false;

function phpServer(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init(){ 
    phpServer(path,{type:'profilePic'},"profilePic_div");
    phpServer(path,{type:'Bars'},"Bars_div");
    phpServer(path,{type:'Phrase'},"Phrase_div");
    phpServer(path,{type:'Badges'},"Badges_div");
    phpServer(path,{type:'PlaceTopTen'},"ddl_place_div");
    phpServer(path,{type:'TimeTopTen'},"ddl_time_div");
    phpServer(path,{type:'TopTenNowTable'},"TopTen_div");
    phpServer(path,{type:'show_qr'},"qrimage_div");
    pendientNotification('Event');
    pendientNotification('Personal');
    $("#anima_div").css("background-image", "url(\"http://res.cloudinary.com/leoquiroa/image/upload/v1489209058/Cumbres/campo1.png\")");
}
function pendientNotification(eventType){
    var objectName = '';
    if(eventType === 'Personal'){ objectName = 'span_bell'; }
    else if(eventType === 'Event'){ objectName = 'span_calendar';}
    
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'NumberPendientNoti',
            eventType:eventType
        },  
        dataType:"text",  
        success:function(d){
            
            if(d>0){
                $("#"+objectName).show();
                $("#"+objectName).text(d);
            }else{
                $("#"+objectName).hide();
            }
        }  
   });
}

$(document).ready(function(){
    init();
});
$(document).on('changed.bs.select', '.selectpicker', function()
{
    var nameDDL = this.id;
    var selected = $(this).val();
    if(nameDDL == 'ddl_place'){
        topTenPlace = selected;
    }
    else if(nameDDL == 'ddl_time'){
        topTenDate = selected;
    }
    if(topTenPlace > 0 && topTenDate > 0){
        phpServer(path,{
            type:'TopTenSpecificTable'
            ,place:topTenPlace
            ,date:topTenDate
        },"TopTen_div");
    }
});
$(document).on('click', '.chocalePic', function()
{
    var idPerson = $(this).data("id0");
    var objImage = document.getElementById('ImgChocale'+idPerson);
    var objText = document.getElementById('NoChocale'+idPerson);
    var sourceText = objText.textContent;
    var on = "../../Multimedia/img/Chocales/high_five_on.png";
    var off = "../../Multimedia/img/Chocales/high_five_off.png";
    if(objImage.attributes[1].nodeValue === off){
        phpServer(path,{type:'SaveChocale',idPerson:idPerson},"dummy_div");
        objImage.attributes[1].nodeValue = on;
        objText.textContent = parseInt(sourceText)+1 + " Chocales";
    }
    else{
        phpServer(path,{type:'RemoveChocale',idPerson:idPerson},"dummy_div");
        objImage.attributes[1].nodeValue = off;
        objText.textContent = parseInt(sourceText)-1 + " Chocales";
    }
});   
$(document).on('click', '#anima_div', function()
{    
    var elm = this;
    var newone = elm.cloneNode(true);
    elm.parentNode.replaceChild(newone, elm);
    document.getElementById("anima_div").style.WebkitAnimationPlayState = "running";    
});   
$(document).on('click', '#notifyPersonal', function()
{   
    phpServer(path,{type:'Notification',typeNoti:'Personal'},"dummy_div");
});
$(document).on('click', '#notifyCalendar', function()
{    
    phpServer(path,{type:'Notification',typeNoti:'Calendar'},"dummy_div");
});