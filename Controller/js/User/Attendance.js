var dateBgn = '';
var dateEnd = '';
var coachID = -1;
var path = "../../Controller/php/User/OpsAttendance.php";
var time_span_es = '';
//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function session_ajax_call(sent_url,sent_data,div,xAxisTitle){
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            drawGraph(d,xAxisTitle);
        }
    });
}
function calendar_ajax_call(sent_url,sent_data){
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            $("#CalendarAttend").zabuto_calendar({
                legend: [
                    {type: "block", label: "Puntos Ordinarios","classname":'ordinary_points'},
                    {type: "block", label: "Millas Extras", "classname":'extra_milles'}
                ],
                language: "es",
                today: true,
                cell_border: true,
                data: obj,
                weekstartson: 0
            });
        }  
    });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{ 
    list_ajax_call(path,{type:'Attend_ddl'},"ddls_div");
    
    $("#time_input").attr("type","hidden");
    list_ajax_call(path,{type:'graph'},"graph_div");
    session_ajax_call(path,{type:'Temporada'},"canvas_div","Mes");
    
    list_ajax_call(path,{type:'AttendTable',dateBgn: '',dateEnd: ''},"AttendTable_div");
    calendar_ajax_call(path,{type:'AttendCalendar'},"AttendCalendar_div");
    list_ajax_call(path,{type:'RatePro',pro:'Entrenador'},"RateCoach_div");
}
//------------------------- CALENDAR ----------------------------------
function getLastDayMonth(m, y) {
    return m===2 ? y & 3 || !(y%25) && y & 15 ? 28 : 29 : 30 + (m+(m>>3)&1);
}
function fn_getDay(d){
    //return d.getUTCDate();
    return d.getDate();
}
function fn_getMonth(m){
    //return parseInt(m.getUTCMonth()+1);
    return parseInt(m.getMonth()+1);
}
function fn_getYear(y){
    return y.getUTCFullYear();
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
//------------------------- MESSAGE ALERT ----------------------------------
function messages(msg,clss){
    $('#myAlert').attr("class", clss);
    $('#myAlert').text(msg);
    $('#myAlert').show('slow');
    setTimeout ( function () {$('#myAlert').hide('slow');}, 2000);
}
//------------------------- GENERATE GRAPH ----------------------------------
function drawGraph(d,xAxisTitle){
    var obj = JSON.parse(d);
    var lbl = new Array(obj.length);
    var po = new Array(obj.length);
    var me = new Array(obj.length);
    for(var j = 0; j < obj.length; j++){
        lbl[j] = obj[j][0];
        po[j] = obj[j][1];
        me[j] = obj[j][2];
    }
    var ctx = document.getElementById("canvas_div");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: lbl,
            datasets: [
                {
                    label: 'Millas Extras',
                    data: me,
                    backgroundColor: 'rgba(183,105,92,0.5)',
                    borderColor: 'rgba(220,220,220,1)',
                    borderWidth: 1
                },
                {
                    label: 'Puntos Ordinarios',
                    data: po,
                    backgroundColor: 'rgba(0,114,187,0.5)',
                    borderColor: 'rgba(151,187,205,1)',
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Puntos'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: xAxisTitle
                    }
                }]
            },
            responsive: true
        }
    });
}
$(document).ready(function() {
    fetch_data();
    //------------------------------- DROP DOWN LIST -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL == 'ddl_horz'){
            //clean
            $('#time_input').val('');
            dateBgn = '';
            dateEnd = '';
            //show/hide
            if(selected == 'Semana' || selected == 'Mes'){
                $("#time_input").attr("type","text");
                time_span_es = selected;
            }
            else if(selected == 'Temporada'){
                $("#time_input").attr("type","hidden");
                list_ajax_call(path,{type:'graph'},"graph_div");
                session_ajax_call(path,{type:'Temporada'},"canvas_div","Mes");
                list_ajax_call(path,{type:'AttendTable',dateBgn: '',dateEnd: ''},"AttendTable_div");
            }
            //messages('Selecciona un periodo de tiempo',"alert alert-danger");
        }
        else if(nameDDL == 'coach'){
            coachID = selected;
            list_ajax_call(path,{type:'ProfilePic',coachID:coachID},"profilePic_div");
        }
    });
    //------------------------- CALENDAR ----------------------------------
    $('#time_input').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy'
    });
    $('#time_input').datepicker().on('changeDate', function(ev){
        if( $('#time_input').val() != ''){
            var selected1 = addDays(ev.date, 1);
            var weekBgn = '';
            var weekEnd = '';

            if (time_span_es == 'Semana'){
                //for week
                var offset = selected1.getDay();
                weekBgn = addDays(selected1, -offset); 
                weekEnd = addDays(weekBgn, 6);
            }
            //for month and session
            switch(time_span_es)
            {
                case 'Semana':
                    dateBgn = fn_getYear(weekBgn) + '-' + fn_getMonth(weekBgn) + '-' + fn_getDay(weekBgn);
                    dateEnd = fn_getYear(weekEnd) + '-' + fn_getMonth(weekEnd) + '-' + fn_getDay(weekEnd);
                    xAxisTitle = "Día";
                break;                
                case 'Mes':
                    dateBgn = fn_getYear(selected1) + '-' + fn_getMonth(selected1) + '-01';
                    dateEnd = fn_getYear(selected1) + '-' + fn_getMonth(selected1) + '-' + getLastDayMonth(fn_getMonth(selected1), fn_getYear(selected1));
                    xAxisTitle = "Semana";
                break;
            }
            //graph
            list_ajax_call(path,{type:'graph'},"graph_div");
            session_ajax_call(path,{type:time_span_es,dateBgn: dateBgn,dateEnd: dateEnd},"canvas_div",xAxisTitle);
            list_ajax_call(path,{type:'AttendTable',dateBgn: dateBgn,dateEnd: dateEnd},"AttendTable_div");
        }
    });    
    //------------------------- SAVE RATE ----------------------------------
    $(document).on('click', '#saveRate', function(){
        var rateNo = $('#rate_text').text();
        if(coachID > 0){
            if(rateNo == ' '){
                messages('Ingrese al menos la calificación',"alert alert-danger");
            }
            else{
                var comment = $('#comment').val();    
                list_ajax_call(path,
                                {type:'SaveRate',
                                proID:coachID,
                                rateNo:rateNo,
                                comment:comment},
                                "dummy_div");
                //alert
                messages('¡Gracias! por tus valiosos comentarios.',"alert alert-info");
                //clean
                list_ajax_call(path,{type:'RatePro',pro:'Entrenador'},"RateCoach_div");
                $('.selectpicker').selectpicker('show');
                coachID = -1;
            }
        }
        else
            messages('Seleccione al entrenador',"alert alert-danger");
    });
});