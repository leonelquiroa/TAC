var path = "../../Controller/php/User/OpsCalendar.php";

function phpServer(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function calendar_phpServer(sent_url,sent_data){
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            $("#calendarEvents").zabuto_calendar({
                legend: [
                    {type: "block", label: "Evento","classname":'calendar_event'},
                    {type: "block", label: "Cita Nutricional", "classname":'calendar_dates'},
                    {type: "block", label: "Cumpleaños", "classname":'calendar_birthday'}
                ],
                language: "es",
                today: true,
                cell_border: true,
                action: function() { myDateFunction(this.id); },
                data: obj,
                weekstartson: 0
            });
        }  
    });
}
function myDateFunction(id){
    var res = id.substring(id.length - 10);
    phpServer(path,{type:'Details', fecha: res},"Details_div");
}
function init(){ 
    phpServer(path,{type:'Event'},"Events_div");
    calendar_phpServer(path,{type:'Calendar'});
}

$(document).ready(function() {
    init();
});
$(document).on('click', '.goingEvent', function(){
    var id1 = $(this).data("id1");
    var status = $("#span_event"+id1).text();
    if(status==="Asistiré"){
        $("#span_event"+id1).text("Confirmar");
        $("#i_event"+id1).attr("class","fa fa-square-o");
        $("#div_event"+id1).attr("style","color: gray;");
        phpServer(path,{type:'RemovePeopleEvent',idEvent:id1},"dummy_div");
    }
    else{
        $("#span_event"+id1).text("Asistiré");
        $("#i_event"+id1).attr("class","fa fa-check-square-o");
        $("#div_event"+id1).attr("style","color: #22CEDC;");
        phpServer(path,{type:'AddPeopleEvent',idEvent:id1},"dummy_div");
    }
});