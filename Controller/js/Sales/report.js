var placeIdSelected = -1;
var dateSelected = '';
var path = "../../Controller/php/Sales/OpsSales.php";
var color_array = 
[
    ["#893fcb","#36A2EB","#F7464A","#4D5360","#FFCE56","#855b48","#5fcb3f","#FF6384","#3f42cb"],
    ["#cfb2ea","#86c7f3","#fa9092","#b7babf","#ffebbb","#cebdb5","#bfeab2","#ffa1b5","#b2b3ea"]
];
//------------------------- DATE FUNCTIONS ----------------------------------
function fn_getDay(d){
    return d.getDate();
}
function fn_getMonth(m){
    return parseInt(m.getMonth()+1);
}
function fn_getYear(y){
    return y.getUTCFullYear();
}
//------------------------- AJAX CALL ----------------------------------
function ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function session_ajax_call(sent_data,type,No){
    $.ajax({
        cache: true,
        async: true,
        url:path,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(A){
            var obj = JSON.parse(A);
            if(type === 'sedentary_active'){
                setPie(obj,No);
                ajax_call(path,{type:'sedentary_active_table',data:obj},"sedentary_active_div");
            }
            else if(type === 'gender'){
                setPie(obj,No);
                ajax_call(path,{type:'gender_table',data:obj},"gender_div");
            }
            else if(type === 'age'){
                setPie(obj,No);
                ajax_call(path,{type:'age_table',data:obj},"age_div");
            }
            else if(type === 'camp'){
                setPie(obj,No);
                ajax_call(path,{type:'camp_table',data:obj},"camp_div");
            }
        }  
    });
}
//------------------------------- SHOW -----------------------------------------
function init()  
{
    var generalPath = "../../Controller/php/General/menu.php";
    ajax_call(generalPath,{type:'topLeftSales'},"MenuHeaderDiv");
    ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
    ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
    ajax_call(path,{type:'place_ddl'},"place_div");
    $('.selectpicker').selectpicker('show');
}
//------------------------- GENERATE GRAPHS ----------------------------------    
function setPie(obj,No){
    var lbl = [];
    var dat = [];
    var col = [];
    var hgh = [];
    for (i = 0; i < obj.length; i++) {
        lbl[i] = obj[i][0];
        dat[i] = obj[i][1];
        col[i] = color_array[0][i];
        hgh[i] = color_array[1][i];
    }
    var pieData = {
        labels: lbl,
        datasets: [
            {
                data: dat,
                backgroundColor: col,
                hoverBackgroundColor: hgh
            }
        ]
    };
    var ctx = document.getElementById("chart-area-"+No).getContext("2d");
    ctx.canvas.height = 150;
    new Chart(ctx,    
    {
        type: "pie",
        data: pieData, 
        responsive: true
    });
}

$(document).ready(function(){
    init(); 
    //------------------------- SELECT DATE ----------------------------------
    $(document).on('changeDate', '#date_sedentary_active', function(ev){
        var originalDate = new Date(ev.date);
        originalDate.setDate(originalDate.getDate() + 1);
        dateSelected = fn_getYear(originalDate) + '-' + fn_getMonth(originalDate) + '-' + fn_getDay(originalDate);
        if(placeIdSelected > 0){
            //sedentary_active
            ajax_call(path,{type:'chart_pro',graphNo:'1'},"chart_div_1");
            session_ajax_call({type:'sedentary_active_graph',placeID:placeIdSelected,dateSelect:dateSelected},'sedentary_active','1');
        }
    });
    //------------------------- SELECT PLACE ----------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        placeIdSelected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_place'){
            if(dateSelected != ''){
                //sedentary_active
                ajax_call(path,{type:'chart_pro',graphNo:'1'},"chart_div_1");
                session_ajax_call({type:'sedentary_active_graph',placeID:placeIdSelected,dateSelect:dateSelected},'sedentary_active','1');
            }
            //gender table
            ajax_call(path,{type:'chart_pro',graphNo:'2'},"chart_div_2");
            session_ajax_call({type:'gender_graph',placeID:placeIdSelected},"gender","2");
            //age table
            ajax_call(path,{type:'chart_pro',graphNo:'3'},"chart_div_3");
            session_ajax_call({type:'age_graph',placeID:placeIdSelected},"age","3");
            //camp
            ajax_call(path,{type:'chart_pro',graphNo:'4'},"chart_div_4");
            session_ajax_call({type:'camp_graph',placeID:placeIdSelected},"camp","4");
            //stasticial
            ajax_call(path,{type:'statistical_measures',placeID:placeIdSelected},"global_stats_table");
            //goals
            ajax_call(path,{type:'goal_measures',placeID:placeIdSelected},"goal_table");
        }
    }); 
    //------------------------- DATE SET ----------------------------------
    $('#date_sedentary_active').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
});