var path = "../../Controller/php/Nutrition/OpsPhrase.php";
var generalPath = "../../Controller/php/General/menu.php";
var categoryId = -1;
var phraseId = -1;

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d)
        {  
            $('#'+div).html(d);
        }  
   });
}
function init()  
{
    cleanCategoryNewDdl();
    cleanCategoryExistentDdl();
    $('.selectpicker').selectpicker('show');
    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
    ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
}
function cleanCategoryNewDdl()
{
    ajax_call(path,{
        type:'ddl_category',
        id:'ddl_category_new',
        title:"Categoria"},
    "ddl_category_new_div");
}
function cleanCategoryExistentDdl()
{
    ajax_call(path,{
        type:'ddl_category',
        id:'ddl_category_existent',
        title:"Categoria"},
    "ddl_category_existent_div");
}
function cleanNewPhrase()
{
    $('#txt_title').val('');
    $('#comment').val('');
    $('#txt_date_from').val('');
    $('#txt_date_to').val('');
    cleanCategoryNewDdl();
    cleanCategoryExistentDdl();
    $('#mainCheck')[0].checked = false;
    $('.selectpicker').selectpicker('show');
    $('#table_existent_div').html("");
}
function cleanAllCheck(a)
{
    $(".which").not(a).prop("checked", false);
}
function setValues(obj)
{
    $('#txt_title').val(obj['titlePhrase']);
    $('#comment').val(obj['textPhrase']);
    categoryId = obj['idCategory'];
    ajax_call(path,{
        type:'ddl_category', 
        id:'ddl_category_new', 
        title:obj['CategoryName']},
    "ddl_category_new_div");
    $('.selectpicker').selectpicker('show');
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}

$(document).ready(function() {
    init();
    $('#txt_date_from').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    $('#txt_date_to').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    $(document).on('click', '#btn_phrase', function(){
        var txt_title = $('#txt_title').val();
        var comment = $('#comment').val();
        var txt_date_from = $('#txt_date_from').val();
        var txt_date_to = $('#txt_date_to').val();
        if(txt_title === '')
            return message("#txt_title", "Sin titulo", "info");
        if(comment === '')
            return message("#comment", "Sin frase", "info");
        if(categoryId < 0)
            return message("#ddl_category_new", "Sin categoria", "info");
        if(txt_date_from === '')
            return message("#txt_date_from", "Sin fecha inicial", "info");
        if(txt_date_to === '')
            return message("#txt_date_to", "Sin fecha final", "info");
        $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{
                    type:'new_phrase'
                    ,txt_title:txt_title
                    ,comment:comment
                    ,categoryId:categoryId
                    ,txt_date_from:txt_date_from
                    ,txt_date_to:txt_date_to
                },  
                dataType:"text",  
                success:function(d){  
                    if(d>0){
                        message("", "Frase guardada exitosamente", "success");
                    }else{
                        message("", "Fallo al guardar la frase", "error");
                    }
                }  
            });
        cleanNewPhrase();
    });
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_category_new')
        {
            categoryId = selected;
        }
        else if(nameDDL === 'ddl_category_existent')
        {
            categoryId = selected;
            ajax_call(path,{type:'existent_phrase', content_category:categoryId},"table_existent_div");
            $('#mainCheck')[0].checked = false;
        }
    });
    $(document).on('click', '#mainCheck', function(){
        var all = $('#mainCheck')[0].checked;
        if(Boolean(all))
        {
            var obj;
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{
                    type:'random'
                },  
                dataType:"text",  
                success:function(d){  
                    obj = JSON.parse(d);
                }  
            });
            setValues(obj);
            $('#table_existent_div').html("");
            cleanCategoryExistentDdl();
            $('.selectpicker').selectpicker('show');
        }
        else
            cleanNewPhrase();
    });
    $(document).on('click', '.which', function(){
        phraseId = $(this).val();
        cleanAllCheck(this);
        var obj;
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'pick_phrase', 
                idCont: phraseId
            },  
            dataType:"text",  
            success:function(d){  
                obj = JSON.parse(d);
            }  
        });
        setValues(obj);
    });
});
