var path = "../../Controller/php/Nutrition/OpsAppointment.php";
var date = '';
var placeID = -1;
var idPerson = -1;
var measureType = -1;
var idNutriAppoint = -1;
var IsNew = true;

function getDay(id)
{
    var res = id.substring(id.length - 10);
    var selected = new Date(res);
    date = selected;
    ajax_call(path,{
        type:'DayInLetters', 
        nameDay: selected.getUTCDay(),
        numberDay: selected.getUTCDate(),
        nameMonth: selected.getUTCMonth(),
        nameYear: selected.getFullYear()
    },"Date_div");
    loadNewDateForm(res);
    $('#people_div').html("");
}
function changeMonth()
{
    cleanForm();
    cancel();
}
function setCalendar(obj,name)
{
    $("."+name).html("");
    $("."+name).zabuto_calendar({
        language: "es",
        today: true,
        cell_border: true,
        action: function() { getDay(this.id); },
        action_nav: function() { changeMonth(this.id); },
        data: obj,
        weekstartson: 0
    });
}
function init()  
{
    calendar_ajax_call(path,{type:'Calendar'},"zabuto_calendar");
    $("#input_hour").hide();
    $("#btn_update").hide();
    $("#btn_cancel").hide();
    var generalPath = "../../Controller/php/General/menu.php";
    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
    ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
}
function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function calendar_ajax_call(sent_url,sent_data,name)
{
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            setCalendar(obj,name);
        }  
    });
}
function cleanForm()
{
    $("#Date_div").html("");
    $("#input_hour").hide();
    $("#place_div").html("");
    $("#people_div").html("");
    $("#dateType_div").html("");
    $("#measurement_div").html("");
    $("#Details_div").html("");
}
function loadNewDateForm(res)
{
    ajax_call(path,{type:'CalendarDetails', fecha: res},"Details_div");
    ajax_call(path,{type:'place_ddl', title:'Lugar'},"place_div");
    ajax_call(path,{type:'measurement_ddl', title:'Tipo de Medición'},"measurement_div");
    $('#txt_hour').val('');
    
    $('.selectpicker').selectpicker('show');
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Listo'
    });
    $("#input_hour").show();
    $('#tableAppoint').DataTable({
        responsive: true
    });
}
function cancel()
{
    IsNew = true;
    var selected = new Date();
    var lcl_date = selected.getFullYear() + '-' + (parseInt(selected.getMonth())+1) + '-' + (parseInt(selected.getDate())+1);
    loadNewDateForm(lcl_date);
    $("#btn_save").show();
    $("#btn_update").hide();
    $("#btn_cancel").hide();
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}

$(document).ready(function() {
    init();
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL == 'ddl_place'){
            if(IsNew){
                ajax_call(path,{type:'people_ddl',placeID:selected},"people_div");    
            }
            placeID = selected;
        }
        else if(nameDDL == 'ddl_people')
            idPerson = selected;
        else if(nameDDL == 'ddl_measurement')
            measureType = selected;
        $('.selectpicker').selectpicker('show');
    }); 
    $(document).on('click', '#btn_save', function(){
        var hour = $('#txt_hour').val();
        if(date == '')
            return message("", "Sin fecha", "info");
        if(hour == '')
            return message("", "Sin hora", "info");
        if(placeID < 0)
            return message("", "Sin lugar", "info");
        if(idPerson < 0)
            return message("", "Sin persona", "info");
        if(measureType < 0)
            return message("", "Sin tipo de medición", "info");
        
        var lcl_date = date.getFullYear() + '-' + (parseInt(date.getMonth())+1) + '-' + (parseInt(date.getDate())+1);
        
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'SaveAppointment', 
                idPerson: idPerson,
                idPlace: placeID,
                date_appoint: lcl_date,
                hour_appoint: hour,
                measureType: measureType
            },  
            dataType:"text",  
            success:function(d){  
                if(d>0){
                    message("", "Cita guardada", "success");
                }else{
                    message("", "Fallo al guardar la cita", "error");
                }
            }  
        });
            
        init();
        loadNewDateForm(lcl_date);
        $("#people_div").html("");
    }); 
    $(document).on('click', '.delete_who', function(){
        var id = $(this).data("id0");
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'DeleteNutriAppointment', 
                idNutriAppoint: id
            },  
            dataType:"text",  
            success:function(d){  
                if(d>0){
                    message("", "La cita se ha eliminado", "success");
                }else{
                    message("", "Fallo al eliminar la cita", "error");
                }
            }  
        });
        init();
        var lcl_date = date.getFullYear() + '-' + (parseInt(date.getMonth())+1) + '-' + (parseInt(date.getDate())+1);
        ajax_call(path,{type:'CalendarDetails', fecha: lcl_date},"Details_div");
    });
    $(document).on('click', '.edit_who', function(){
        idNutriAppoint = $(this).data("id1");
        IsNew = false;
        $('#people_div').html("");
        var obj;
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'ListAppointment', 
                idNutriAppoint: idNutriAppoint
            },  
            dataType:"text",  
            success:function(d){  
                obj = JSON.parse(d);
            }  
        });
        
        placeID = obj['idPlace'];
        measureType = obj['idMeasureType'];
        
        $('#txt_hour').val(obj['Hora']);
        ajax_call(path,{type:'place_ddl', title:obj['namePlace']},"place_div");
        ajax_call(path,{type:'measurement_ddl', title:obj['NameMeasureType']},"measurement_div");
        $('.selectpicker').selectpicker('show');
        
        $("#btn_save").hide();
        $("#btn_update").show();
        $("#btn_cancel").show();
    });
    $(document).on('click', '#btn_update', function(){
        IsNew = true;
        var hour = $('#txt_hour').val();
        if(hour == '')
            return message("", "Sin hora", "info");
        if(placeID < 0)
            return message("", "Sin lugar", "info");
        if(measureType < 0)
            return message("", "Sin tipo de medición", "info");
        
        var lcl_date = date.getFullYear() + '-' + (parseInt(date.getMonth())+1) + '-' + (parseInt(date.getDate())+1);
        
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'UpdateAppointment', 
                idNutriAppoint: idNutriAppoint,
                idPlace: placeID,
                dt_appoint: lcl_date + ' ' + hour,
                measureType: measureType
            },  
            dataType:"text",  
            success:function(d){  
                if(d>0){
                    message("", "Cita actualizada", "success");
                }else{
                    message("", "Fallo al actualizar la cita", "error");
                }
            }  
        });
        
        var lcl_date = date.getFullYear() + '-' + (parseInt(date.getMonth())+1) + '-' + (parseInt(date.getDate())+1);
        loadNewDateForm(lcl_date);
        $("#btn_save").show();
        $("#btn_update").hide();
        $("#btn_cancel").hide();
    });
    $(document).on('click', '#btn_cancel', function(){
        cancel();
    });
});