var path = "../../Controller/php/Nutrition/OpsIndicators.php";
var placeID = -1;
var personID = -1;
var idNutriAppoint = -1;

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()
{
    ajax_call(path,{type:'ddl_places',title:'Sede',name_catalog:'Lugar'},"place_div");
    $('.selectpicker').selectpicker('show');
    var generalPath = "../../Controller/php/General/menu.php";
    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
    ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
}
function disableEnter(e)
{
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
}  
function editColumn(e, dataId, dataValue,type)
{
    if(e.keyCode == 13)
    {    
        $.post( path, { 
            type:type,  
            idNutriAppoint:idNutriAppoint,
            idData:dataId,
            dataValue:dataValue
        })
        .done(function(data) 
        {
            if(data > 0)
                highlightRow(dataId, "#8dc70a");
            else
                highlightRow(dataId, "#d64b2f");
        });
    }
}
function highlightRow(rowId, bgColor)
{
    var rowSelector = $("#color_" + rowId);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function water_indicator(currentValue)
{
    var nameComponent = 'temp-gauge-0';
    var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: '%',
        title: 'Agua',
        minValue: 20,
        maxValue: 80,
        majorTicks: [ 20, 30, 40, 50, 60, 70, 80 ],
        minorTicks: 10,
        strokeTicks: false,
        highlights: [
            { from: 20, to: 50, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 50, to: 65, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 65, to: 80, color: 'rgba(255, 255, 0, .5)' }, //yellow
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function fat_indicator(currentValue)
{
   var nameComponent = 'temp-gauge-1';
   var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: '%',
        title: 'Grasa',
        minValue: 5,
        maxValue: 40,
        majorTicks: [ 5, 10, 15, 20, 25, 30, 35, 40 ],
        //minorTicks: 10,
        strokeTicks: false,
        highlights: [
            { from: 5, to: 11, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 11, to: 22, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 22, to: 28, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 28, to: 40, color: 'rgba(255, 0, 0, .5)' } //red
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function imc_indicator(currentValue)
{
   var nameComponent = 'temp-gauge-2';
   var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: 'kg/m2',
        title: 'IMC',
        minValue: 20,
        maxValue: 40,
        majorTicks: [ 20, 25, 30, 35, 40 ],
        strokeTicks: false,
        highlights: [
            { from: 20, to: 25, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 25, to: 30, color: 'rgba(255, 255, 0, .5)' }, //yellow
            { from: 30, to: 40, color: 'rgba(255, 0, 0, .5)' } //red
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function vfr_indicator(currentValue)
{
   var nameComponent = 'temp-gauge-3';
   var tempGauge = new Gauge({
        renderTo: nameComponent,
        width: 175,
        height: 175,
        glow: true,
        units: 'cm2',
        title: 'VFR',
        minValue: 0,
        maxValue: 60,
        majorTicks: [0, 10, 20, 30, 40, 50, 60 ],
        //minorTicks: 10,
        strokeTicks: false,
        highlights: [
            { from: 0, to: 12, color: 'rgba(0, 255, 0, .5)' }, //green
            { from: 12, to: 60, color: 'rgba(255, 0, 0, .5)' } //red
        ],
        colors: {
            plate: '#f5f5f5',
            majorTicks: '#666',
            minorTicks: '#888',
            title: '#444',
            units: '#000',
            numbers: '#222',
            needle: {
                start: 'rgba(240, 128, 128, 2)',
                end: 'rgba(255, 160, 122, .9)'
            }
        },
        animation: {
            delay : 25,
            duration: 500,
            fn : 'linear'
        },
        valueFormat: {
            int: 2,
            dec: 1
        }
    });
    tempGauge.setValue(currentValue);
    tempGauge.draw();
}
function setLinear(lbl, data, name)
{
    var lineChartData = {
        labels : lbl,
        datasets : 
        [
            {
                label: name,
                backgroundColor: "rgba(183,102,25,0.5)",
                borderColor: "rgba(145,82,14,0.7)",
                data : data
            }
        ]
    };

    var ctx = document.getElementById("canvas-line").getContext("2d");
    ctx.canvas.height = 125;
    var myLineGraph = new Chart(ctx,    
    {
        type: "line",
        data: lineChartData, 
        responsive: true
    });
}

$(document).ready(function() 
{
    init();
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL == 'Sede')
        {
            placeID = selected;
            ajax_call(path,{type:'people_ddl',placeID:selected},"people_div");
            $('.selectpicker').selectpicker('show');
        }
        else if(nameDDL == 'ddl_people')
        {
            personID = selected;
            //watches
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{
                    type:'tanita', 
                    personID:personID
                },  
                dataType:"text",  
                success:function(d){
                    if(d != ''){
                        var obj = JSON.parse(d);
                        water_indicator(obj[0][1]);
                        fat_indicator(obj[1][1]);
                        imc_indicator(obj[2][1]);
                        vfr_indicator(obj[3][1]);
                    }
                }  
            });
            ajax_call(path,{type:'indicators_ddl',idPerson:personID},"indicator_ddl_div");
            $('.selectpicker').selectpicker('show');
        }
        else if(nameDDL === 'ddl_indicators')
        {
            var res = selected.split("#"); 
            idNutriAppoint = res[0];
            ajax_call(path,{type:'indicators_past_info',idNutriAppoint:res[0]},"indicator_past_div");
            ajax_call(path,{
                type:'indicators_table'
                ,idPerson:personID
                ,idNutriAppoint:res[0]
                ,measureType:res[1]
            },"indicator_table_div");
        }
    });
    $(document).on('keyup', '.indicatorUpdate', function(e)
    {  
        editColumn(e, $(this).data("id0"), $(this).text(), 'edit_measure');
    });
    $(document).on('keyup', '.assignPoints', function(e)
    {  
        editColumn(e, $(this).data("id0"), $(this).text(),'assign_points');
    });
    $(document).on('click', '.indicatorSee', function()
    {  
        var idIndicator = $(this).data("id1");
        var obj;
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'lectures', 
                idIndicator:idIndicator,
                personID:personID
            },  
            dataType:"text",  
            success:function(d){  
                obj = JSON.parse(d);
                var lbl = new Array(obj.length-1);
                var dat = new Array(obj.length-1);
                for(var j = 0; j < obj.length; j++){
                    lbl[j] = obj[j][0];
                    dat[j] = obj[j][1];
                }
                ajax_call(path,{type:'refresh_canvas'},"canvas_div");
                setLinear(lbl,dat,obj[0][2]);
            }  
        });
    });
    $(document).on('keydown', '.indicatorUpdate', function(e)
    {
        disableEnter(e);
    });
    $(document).on('keydown', '.assignPoints', function(e)
    {
        disableEnter(e);
    });
});