var path = "../../Controller/php/General/OpsUsers.php";
var today = moment().format('YYMMDD');

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()  
{ 
    var generalPath = "../../Controller/php/General/menu.php";
    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
    ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
    
    ajax_call(path,{type:'new_user_gender'},"new_user_gender_div");
    ajax_call(path,{type:'new_user_place'},"new_user_place_div");
    $('.selectpicker').selectpicker('show');
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}
function activateMask(flag)
{
    if(flag){
        $("#mask").css({"opacity": "0.7", "pointer-events": "none"});
    }
    else{
        $("#mask").css({"opacity": "1", "pointer-events": "auto"});
    }
}
function cleanForm()
{
    $('#new_name').val('');
    $('#new_nick').val('');
    $('#new_height').val('');
    $('#new_birthday').val('');
    $('#new_phone').val('');
    $('#new_email').val('');
    $('#new_photo_form_preview').val('');
}

$(document).ready(function()
{
    init();
    //------------------------------- DROP DOWN LISTS --------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_new_user_gender'){
            new_usr_sex = selected;
        }
        else if(nameDDL === 'ddl_new_user_place'){
            new_usr_place = selected;
        }
    });
    //------------------------- POP-UP NEW USER ----------------------------------
    $(document).on('click', '#showAddForm', function(){
        $("#addform").show();
    });
    $(document).on('click', '#cancelsave', function(){
        $("#addform").hide();
    });
    //------------------------- SAVE NEW USER ----------------------------------    
    $(document).on('click', '#performsave', function(){
        var new_name = $('#new_name').val();
        var new_nick = $('#new_nick').val();
        var new_height = $('#new_height').val();
        var new_birthday = $('#new_birthday').val();
        var new_phone = $('#new_phone').val();
        var new_email = $('#new_email').val();
        
        if(new_name === '' || new_phone === '' || new_email === '' || new_usr_sex < 0 || new_usr_place < 0)
            message("","Porfavor ingresa todos los campos.","warn");
        else{
            //USER DB
            $.ajax({
                cache: false,
                async: false,
                url:path,  
                method:"POST",
                data:{ 
                    type:'add_user',  
                    name:new_name, 
                    nick:new_nick,
                    gender: new_usr_sex,
                    height: new_height, 
                    birthday: new_birthday, 
                    phone: new_phone,
                    email: new_email,
                    place: new_usr_place,
                    userType: -1
                },  
                dataType:"text",  
                success:function(PersonIdDb){
                    if(PersonIdDb > 0){
                        message("","La persona se ha guardado satisfactoriamente.","success");
                        //IMAGE
                        var nameImage = $("#new_photo_form").val();
                        var formData = new FormData();
                        if(nameImage!=='')
                        {
                            //IMAGE DB
                            $.ajax({
                                cache: false,
                                async: false,
                                url:path,  
                                method:"POST",
                                data:{ 
                                    type:'add_user_pic', 
                                    newId:PersonIdDb, 
                                    ext:nameImage.slice(-3)
                                },  
                                dataType:"text",  
                                success:function(PicIdDb){
                                    if(PicIdDb>0){
                                        message("","La fotografía de la persona se ha asociado satisfactoriamente.","success");
                                        //IMAGE FILE
                                        formData.append('file', $("#new_photo_form")[0].files[0]);
                                        $.ajax({
                                            url: '../../Controller/php/General/uploadImage.php?var='+PicIdDb+'&url=../../../Multimedia/img/people/',
                                            type: 'POST',
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success: function (r) {
                                                var res = r.split("#");
                                                if(res[0]>0){
                                                    message("","La fotografía de la persona se ha guardado satisfactoriamente en el servidor.","success");
                                                }
                                                else{
                                                    message("","Lo sentimos, hubo un problema al guardar la foto del nuevo usuari@ en el servidor.".res[1],"danger");
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        message("","Lo sentimos, hubo un problema al asociar la imagen del nuevo usuario.","danger");
                                    }                                    
                                }
                            });
                            //delete image
                            $('.fileinput-preview img').attr('src',"../../Multimedia/img/people/no_image.png");
                        }
                        //qr-code
                        $.ajax({
                            url: '../../Tools/qrcode/generate.php?var='+PersonIdDb,
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                        message("","El QR de la persona se ha generado correctamente.","success");
                        //hide form
                        $("#addform").hide();
                        //deactivate mask
                        activateMask(false);
                        //clean form
                        cleanForm();
                        //refresh ddl for new users
                        ajax_call(path,{type:'new_user_gender'},"new_user_gender_div");
                        ajax_call(path,{type:'new_user_place'},"new_user_place_div");
                        $('.selectpicker').selectpicker('show');
                        //refresh general table
                        ajax_call(path,{type:'table_users',typePlace:'All',placeID:0},"table_div");
                        makeDynamicTable();
                        //clean image form
                        $('.fileinput').fileinput('clear');
                    }
                    else{
                        message("","Hubo un problema con el registro del usuario","error");
                    }
                } 
           });
        }
    });
    //------------------------- CALENDAR ---------------------------------------
    $('#new_birthday').datepicker({
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
});
