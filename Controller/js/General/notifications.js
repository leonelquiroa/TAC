var path = "../../Controller/php/General/OpsNotifications.php";
localStorage.setItem("FirstLoad",1);
//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function chooseMenu(){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    list_ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    list_ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    list_ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    list_ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
			list_ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
}

$(document).ready(function() {
    fetch_data();
});
