var placeId = -1;
var personId = -1;
var idBadge = -1;
var path = "../../Controller/php/General/OpsBadges.php";
localStorage.setItem("FirstLoad",1);

function phpServer(sent_url,sent_data,div,async){
    $.ajax({
        cache: false,
        async: async,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function getItems(exampleNr){
    var columns = [];
    $(exampleNr + ' ul.sortable-list').each(function(){
        columns.push(
            $(this).sortable('toArray').join(','));			
    });
    return columns.join('|');
}
function chooseMenu(){
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    phpServer(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv",true);
                    phpServer(generalPath,{type:'menuNutri'},"MenuContentDiv",true);
                break;
                case 'Administrador':
                    phpServer(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv",true);
                    phpServer(generalPath,{type:'menuAdmin'},"MenuContentDiv",true);
                break;
            }
            phpServer(generalPath,{type:'topBar'},"MenuTopDiv",true);
        }  
    });
}
function init(){
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    phpServer(path,{type:'all_badges_list'},"all_badges_list_div",true);
    phpServer(path,{type:'ddl_place'},"place_div",false);
}
function activateDragAndDrop(){
    $('#example-2-1 .sortable-list').sortable({
        connectWith: '#example-2-1 .sortable-list'
    });
}
function saveFileBadge(newId, title){
    var formData = new FormData();
    formData.append('file', $("#ephoto-upload")[0].files[0]);
    $.ajax({
        url: '../../Controller/php/General/uploadImage.php?var='+newId+'&url=../../../Multimedia/img/badges/',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (r) {
            updateImageUrl(r,title,newId);
        }
    });
}
function updateImageUrl(dataIn, title, newId){
    if(dataIn !== '')
    {
        var res = dataIn.split("#");
        if(res[0]>0){
            $.ajax({
                cache: false,
                async: true,
                url:path,  
                method:"POST",
                data:{
                    type:'update_image_badge', 
                    idBadge:newId, 
                    columnDB:'image', 
                    textDB:newId+'.'+res[1]
                },  
                dataType:"text",  
                success:function(d){   
                    cleanAfterSave(title);
                }
            });
        }
        else
            message("","Lo sentimos, tu hubo un problema al guardar la foto del badge "+res[1],"warn");
    }
}
function cleanAfterSave(title){
    clear();
    message("","El badge "+title+" se ha guardado satisfactoriamente.","success");
    phpServer(path,{type:'all_badges_list'},"all_badges_list_div",true);
}
function clearDDList(){
    phpServer(path,{type:'ddl_place'},"place_div",false);
    $('.selectpicker').selectpicker('show');
    $('#people_div').html('');
    $('#badges_list_div').html('');
    $('#badges_assigned_div').html('');
}
function clear(){
    $('#txt_title').val('');
    $('#txt_value').val('');
    $('#txt_description').val('');
    $(".fileinput").fileinput("clear");
    $('#imgBadge').attr("src","../../Multimedia/img/badges/no_image.png");
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}

$(document).ready(function(){
    init();
});
//SEE
$(document).on('click', '.ix_Badge', function(e){
    idBadge = $(this).data("id0");
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{
            type:'see_badge',
            idBadge:idBadge
        },  
        dataType:"text",  
        success:function(d){
            if(d != ''){
                var obj = JSON.parse(d);
                $('#txt_title').val(obj[1]);
                $('#txt_description').val(obj[2]);
                $('#txt_value').val(obj[3]);
                $('#imgBadge').attr("src","../../Multimedia/img/badges/" + obj[4]);
            }
        }  
    });
});
//DELETE
$(document).on('click', '.deleteBadge', function(e){
    idBadge = $(this).data("id1");
    $.ajax({
        cache: false,
        async: true,
        url:path,  
        method:"POST",
        data:{
            type:'delete_badge',
            idBadge:idBadge
        },  
        dataType:"text",  
        success:function(d){
            if(parseInt(d)>0){
                message("","El badge se ha eliminado.","success");
                phpServer(path,{type:'all_badges_list'},"all_badges_list_div",true);
                idBadge = -1;
                clearDDList();
            }
            else
                message("","El badge no se ha podido eliminar.","warn");
        }  
    });
});
// BUTTON SAVE 
$(document).on('click', '#btn_save', function(e){        
    var title = $('#txt_title').val();
    var value = $('#txt_value').val();
    var description = $('#txt_description').val();
    var file_data = $('#ephoto-upload').prop('files')[0];   

    if(title === '')
        return message("","Sin titulo para el badge.","warn");
    if(description === '')
        return message("","Sin descripción para el badge","warn");
    
    clearDDList();
    if(idBadge > 0){ //UPDATE
        $.ajax({
            cache: false,
            async: true,
            url:path,  
            method:"POST",
            data:{ 
                type:'update_badges_all',  
                title:title,
                value:value,
                description:description,
                idBadge:idBadge
            },  
            dataType:"text",  
            success:function(d){    
                if(parseInt(d) > 0){
                    if(file_data === undefined){
                        cleanAfterSave(title);
                    }else{
                        saveFileBadge(idBadge, title);
                    }
                    idBadge = -1;
                }
            }
        });
    }
    else
    { //SAVE
        if(file_data === undefined)
            return message("","Sin archivo","warn");
        $.ajax({
            cache: false,
            async: true,
            url:path,  
            method:"POST",
            data:{ 
                type:'save_badges',  
                title:title,
                description:description,
                value:value
            },  
            dataType:"text",  
            success:function(newId){    
                if(newId > 0 &&  $("#ephoto-upload").val()!==''){
                    saveFileBadge(newId, title);
                }
            }
        });
    }
});
// SELECT 
$(document).on('changed.bs.select', '.selectpicker', function(e){
    var nameDDL = this.id;
    var selected = $(this).find("option:selected").val();
    if(nameDDL === 'ddl_place'){
        placeId = selected;
        phpServer(path,{type:'ddl_people',placeID:selected},"people_div",false);
        $('.selectpicker').selectpicker('show');
        $('#badges_list_div').html("");
        $('#badges_assigned_div').html("");
    }
    else if(nameDDL === 'ddl_people'){
        personId = selected;
        phpServer(path,{type:'badges_list',idUser:personId},"badges_list_div",false);
        phpServer(path,{type:'badgesperperson_list',idUser:personId},"badges_assigned_div",false);
        activateDragAndDrop();
    }
}); 
// BUTTON ASSIGN 
$(document).on('click', '#btn_assign', function(e){        
    var a = getItems('#example-2-1');
    var elements = a.split("|");
    phpServer(path,{type:'badgesperperson_ops',idUser:personId,idBadges:elements[1]},"dummy_div",true);
    message("","El badge se ha asignado satisfactoriamente.","success");
});
