var placeID = -1;
var new_usr_sex = -1;
var new_usr_place = -1;
var idUser = -1;
var path = "../../Controller/php/General/OpsUsersBadges.php";
localStorage.setItem("FirstLoad",1);

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
    });
}
function chooseMenu()
{
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
            ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
function message(divName, message, iconType)
{
    if(divName === ''){
        $.notify(message,iconType,
        { 
            position:"bottom center",
            clickToHide: true
        });
    }else{
        $(divName).notify(message,iconType,
        { 
            position:"top center",
            clickToHide: true
        });
    }
    return '';
}
function init()  
{ 
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    ajax_call(path,{type:'listBadges'},"list_badges_div")
} 

$(document).ready(function()
{
    init();
});
