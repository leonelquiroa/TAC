var placeID = -1;
var contentType = -1;
var path = "../../Controller/php/General/OpsUploadContent.php";
var Going = [];
var allPlaces = false;
localStorage.setItem("FirstLoad",1);

function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function chooseMenu()
{
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{type:'menuType'},  
        dataType:"text",  
        success:function(d){
            var generalPath = "../../Controller/php/General/menu.php";
            switch(d){
                case 'Nutricionista':
                    ajax_call(generalPath,{type:'topLeftNutri'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuNutri'},"MenuContentDiv");
                break;
                case 'Administrador':
                    ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
                    ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
                break;
            }
            ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
        }  
    });
}
function init()
{
    if(parseInt(localStorage.getItem("FirstLoad"))) 
    {
	localStorage.setItem("FirstLoad",0);
        chooseMenu();
    }
    ajax_call(path,{type:'ddlPlace',title:'Sede',name_catalog:'Lugar'},"place_div");
    ajax_call(path,{type:'ddlContentType',title:'Tipo de Contenido',name_catalog:'Contenido'},"content_div");
}
function cleanAllCheck()
{
    $(".who").prop("checked", false);
    Going.length = 0;
}
function cleanAllWindow()
{
    $(".fileinput").fileinput("clear");
    init();
    $('.selectpicker').selectpicker('show');
    cleanAllCheck();
    $('#table_div').html("");
    $('#txt_date_from').val('');
    $('#txt_date_to').val('');
    $('#txt_title').val('');
}
function addPerson(newPerson,onlyOne)
{
    var flagNew = true;
    for (var index = 0; index < Going.length; index++) {
        var savedPerson = Going[index];
        //check if the person already exist
        if(savedPerson === newPerson){
            flagNew = false;
            if(Boolean(onlyOne))
                Going.splice(index, 1);
            break;
        }
    } 
    //add only if the person is new
    if(Boolean(flagNew))
        Going.push(newPerson);
}

$(document).ready(function() {
    init();
    //------------------------------- CALENDAR -----------------------------------------
    $('#txt_date_from,#txt_date_to').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    //------------------------- SELECT ----------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'Lugar'){
            cleanAllCheck();
            placeID = selected;
            ajax_call(path,{type:'users_table_venue',placeID:selected},"table_div");
        }
        else if(nameDDL === 'Contenido'){
            contentType = selected;
        }
    }); 
    //------------------------- REGISTER CHECKBOX ----------------------------------
    $(document).on('click', '.who', function(e){
        //get the id
        var newPerson = $(this).val();
        //all the elemnts    
        if(newPerson == 0){
            var all = $('#mainCheck')[0].checked;
            if(Boolean(all)){
                $(".who").prop("checked", true);
                var full_table = document.getElementsByClassName("who");
                for (var index = 1; index < full_table.length; index++) {
                    var all_row = full_table[index].value;
                    addPerson(all_row,false);
                }
            }
            else
                cleanAllCheck();
        }//only one element
        else{
            //the first one
            if(Going.length == 0)
                Going.push(newPerson);
            else{ //otherwise for the rest of the elements
                $("#mainCheck").prop("checked", false);
                addPerson(newPerson,true);
            }
        }
    });
    //------------------------- BUTTON ----------------------------------
    $(document).on('click', '#btn_upload_recipe', function(e){
        //variables
        var title = $('#txt_title').val();
        var file_data = $('#fileToUpload').prop('files')[0];   
        var date_from = $('#txt_date_from').val();
        var date_to = $('#txt_date_to').val();
        //validation
        if(file_data === undefined)
            return alert('Sin archivo');
        if(contentType < 0)
            return alert('Sin Tipo de Contenido');
        if(title == '')
            return alert('Sin titulo');
        if(Going.length == 0)
            return alert('Sin personas');
        if(date_from == '')
            return alert('Sin fecha inicial');
        if(date_to == '')
            return alert('Sin fecha final');
        //---------------------------------
        var name = file_data.name.split(".");
        var ext = name[name.length-1];
        if(ext != 'pdf')
            return alert('El archivo debe ser formato .pdf');
        if(file_data.size > 10000000)
            return alert('El archivo debe ser máximo de 10 MB');
        var newID = -1;
        //database
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'save_content'
                ,title:title
                ,contentType:contentType
                ,date_from:date_from
                ,date_to:date_to
                ,Going:Going
                ,allPlaces:allPlaces
            },  
            dataType:"text",  
            success:function(d){  
                newID = d;
            }  
        });
        //file   
        var form_data = new FormData();                  
        form_data.append('file', file_data);
        $.ajax({
            url: '../../Controller/php/General/uploadFile.php?var='+newID+'&url=../../../Multimedia/files/upload/', // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(response){
                if(response>0){
                    alert("El artículo " + file_data.name + " fué subido satisfactoriamente.");
                    cleanAllWindow();
                }
                else{
                    alert("Lo sentimos, tu documento no se pudo subir.");
                }
            }
        });
        //thumbnail
        PDFJS.workerSrc = "../../Controller/js/External/pdf.worker.js";
        var pathThumbnail = "../../Multimedia/files/thumbnails/";
        var pathFile = "../../Multimedia/files/upload/";
        PDFJS.getDocument( pathFile + newID + ".pdf").then( function (pdf) {
            pdf.getPage(1).then( function (page) {
                var viewport = page.getViewport(0.5);
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                var renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                };
                page.render(renderContext).then( function() {
                    ctx.globalCompositeOperation = "destination-over";
                    ctx.fillStyle = "#fff";
                    ctx.fillRect( 0, 0, canvas.width, canvas.height );
                    var img_src = canvas.toDataURL();
                    $.ajax({
                        cache: false,
                        async: false,
                        url:"saveThumbnail.php",  
                        method:"POST",
                        data:{
                            title: pathThumbnail + newID,
                            base64data:img_src
                        },  
                        dataType:"text",  
                        success:function(d)
                        {
                            if(parseInt(d)>0)
                            {
                                ajax_call(path,{type:'save_thumbnail',IdContent:newID},"dummy_div");
                                alert("El thumbnail del artículo " + file_data.name + " fué generado satisfactoriamente.");
                            }
                            else
                                alert("Lo sentimos, el thumbnail del documento no se pudo generar.");
                        }  
                    });
                    canvas.remove();                    
                });
            });
        });
    });
    //------------------------- REGISTER CHECKBOX ----------------------------------
    $(document).on('click', '#all_places_check', function(e){
        allPlaces = $('#all_places_check')[0].checked;
        if(Boolean(allPlaces)){
            ajax_call(path,{type:'ddls',title:'Sede',name_catalog:'Lugar'},"place_div");
            $('#table_div').html("");
            $('.selectpicker').selectpicker('show');
            $('#Lugar').attr('disabled', true);
        }
        else
            $('#Lugar').attr('disabled', false);
    });
});