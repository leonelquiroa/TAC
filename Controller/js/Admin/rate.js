//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
//------------------------- GENERATE GRAPHS ----------------------------------    
function setLabels(xLabel, yLabel){
    var lbl = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                    display: true,
                    labelString: yLabel
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: xLabel
                }
            }]
        },
        responsive: true
    };
    return lbl;
}
function getInfo(lcl_idPro, lcl_month, lcl_proType){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'pro_graph',
            idPro:lcl_idPro,
            month:lcl_month,
            proType:lcl_proType
        },  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            if(lcl_proType === 'Nutricionista')
                setPie(obj);
            else if(lcl_proType === 'Entrenador')
                setBar(obj);
        }  
   });
}
function setBar(obj){
    var lbl = [];
    var dat = [];
    for (i = 1; i <= Object.keys(obj).length; i++) {
        lbl[i-1] = obj[i][0];
        dat[i-1] = obj[i][1];
    }
    var barChartData = {
    labels : lbl,
    datasets : [
            {
                label: "Número de Personas",
                backgroundColor : "rgba(255, 159, 64, 0.2)",
                data : dat
            }
        ]
    };
    var ctx = document.getElementById("graph-area").getContext("2d");
    ctx.canvas.height = 150;
    new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: setLabels('Fecha', 'Asistencia')
    });
}
function setPie(obj){
    var lbl = [];
    var dat = [];
    var col = [];
    var hgh = [];
    for (i = 1; i <= Object.keys(obj).length; i++) {
        lbl[i-1] = obj[i][0];
        dat[i-1] = obj[i][1];
        col[i-1] = color_array[0][i-1];
        hgh[i-1] = color_array[1][i-1];
    }
    var pieData = {
        labels: lbl,
        datasets: [
            {
                data: dat,
                backgroundColor: col,
                hoverBackgroundColor: hgh
            }
        ]
    };
    var ctx = document.getElementById("graph-area").getContext("2d");
    ctx.canvas.height = 200;
    new Chart(ctx,    
    {
        type: "pie",
        data: pieData, 
        responsive: true
    });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{ 
    var generalPath = "../../Controller/php/General/menu.php";
    list_ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
    list_ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
    list_ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
    list_ajax_call(path,{type:'dll_rol_type'},"rol_type_div");
}
function fn_getDay(d){
    //return d.getUTCDate();
    return d.getDate();
}
function fn_getMonth(m){
    //return parseInt(m.getUTCMonth()+1);
    return parseInt(m.getMonth()+1);
}
function fn_getYear(y){
    return y.getUTCFullYear();
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

var path = "../../Controller/php/Administration/OpsRate.php";
var month = '';
var proType = '';
var color_array = 
[
    ["#893fcb","#36A2EB","#F7464A","#4D5360","#FFCE56","#855b48","#5fcb3f","#FF6384","#3f42cb"],
    ["#cfb2ea","#86c7f3","#fa9092","#b7babf","#ffebbb","#cebdb5","#bfeab2","#ffa1b5","#b2b3ea"]
];

$(document).ready(function(){
    fetch_data();
    //------------------------- CALENDAR DATE PICKER ----------------------------------
    $('#date_div').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    $('#date_div').datepicker().on('changeDate', function(ev){
        if( $('#date_div').val() !== ''){
            var selected = addDays(ev.date, 1);
            month = fn_getYear(selected) + '/' + fn_getMonth(selected) + '/' + fn_getDay(selected);
            if(proType !== ''){
                list_ajax_call(path,{
                    type:'table_pro',
                    month:month,
                    proType:proType
                },"table_pro_div");
            }
            //alert(month);
        }
    });
    //------------------------------- DROP DOWN LISTS -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_rol_type'){
            proType = selected;
            if(month !== ''){
                list_ajax_call(path,{
                    type:'table_pro',
                    month:month,
                    proType:proType
                },"table_pro_div");
            }
            $("#table_details_div").html('');
            $("#graph_div").html('');
        }
    });
    //------------------------------- DETAILS -----------------------------------------
    $(document).on('click', '.who', function(){
        var idPro = $(this).data("id0");
        list_ajax_call(path,{
            type:'table_details',
            idPro:idPro,
            month:month,
            proType:proType
        },"table_details_div");
        list_ajax_call(path,{type:'chart_pro'},"graph_div");
        getInfo(idPro, month, proType);
    });
});