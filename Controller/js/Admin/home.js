//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{ 
    var generalPath = "../../Controller/php/General/menu.php";
    list_ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
    list_ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
    list_ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
}
$(document).ready(function(){
    fetch_data();
});
