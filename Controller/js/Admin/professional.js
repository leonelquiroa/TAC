/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var time_span_es = '';
var dateBgn = '';
var dateEnd = '';
var rol = '';
var idPerson = -1;
var color_array = 
[
    ["#893fcb","#36A2EB","#F7464A","#4D5360","#FFCE56","#855b48","#5fcb3f","#FF6384","#3f42cb"],
    ["#cfb2ea","#86c7f3","#fa9092","#b7babf","#ffebbb","#cebdb5","#bfeab2","#ffa1b5","#b2b3ea"]
];
var path = "../../Controller/php/Administration/OpsProfessional.php";
//------------------------- AJAX CALL ----------------------------------
function list_ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
//------------------------------- SHOW -----------------------------------------
function fetch_data()  
{ 
    var generalPath = "../../Controller/php/General/menu.php";
    list_ajax_call(generalPath,{type:'topLeftAdmin'},"MenuHeaderDiv");
    list_ajax_call(generalPath,{type:'menuAdmin'},"MenuContentDiv");
    list_ajax_call(generalPath,{type:'topBar'},"MenuTopDiv");
    list_ajax_call(path,{type:'dll_rol_type'},"rol_type_div");
    list_ajax_call(path,{type:'dll_pro_time'},"dll_pro_time_div");
    $("#panel_details").hide();
}

function fn_getDay(d){
    //return d.getUTCDate();
    return d.getDate();
}
function fn_getMonth(m){
    //return parseInt(m.getUTCMonth()+1);
    return parseInt(m.getMonth()+1);
}
function fn_getYear(y){
    return y.getUTCFullYear();
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
//------------------------- GENERATE GRAPHS ----------------------------------    
    function getInfo(time, dateBgn, dateEnd, rol, person){
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{
                type:'data_pro',
                idPerson:person,
                dateBgn:dateBgn,
                dateEnd:dateEnd,
                type_date:time,
                type_pro:rol
            },  
            dataType:"text",  
            success:function(d){
                var obj = JSON.parse(d);
                //var lbl = Object.keys(obj[0]).map(function(k) { return obj[0][k] });
                //var val = Object.keys(obj[1]).map(function(k) { return obj[1][k] });
                setPie(obj);
                list_ajax_call(path,{
                    type:'pro_table',
                    type_pro:rol,
                    obj:obj},
                "table_div");
            }  
       });
    }
    function setPie(obj){
        var lbl = [];
        var dat = [];
        var col = [];
        var hgh = [];
        for (i = 1; i <= Object.keys(obj).length; i++) {
            lbl[i-1] = obj[i][0];
            dat[i-1] = obj[i][1];
            col[i-1] = color_array[0][i-1];
            hgh[i-1] = color_array[1][i-1];
        }
        var pieData = {
            labels: lbl,
            datasets: [
                {
                    data: dat,
                    backgroundColor: col,
                    hoverBackgroundColor: hgh
                }
            ]
        };
        var ctx = document.getElementById("chart-area").getContext("2d");
        ctx.canvas.height = 200;
        new Chart(ctx,    
        {
            type: "pie",
            data: pieData, 
            responsive: true
        });
    }
    
    function cleanDetails(){
        $("#pro_details_div").html('');
        $("#chart_div").html('');
        $("#table_div").html('');
    }
$(document).ready(function(){
    fetch_data();
    //------------------------------- DROP DOWN LISTS -----------------------------------------
    $(document).on('changed.bs.select', '.selectpicker', function(){
        var nameDDL = this.id;
        var selected = $(this).find("option:selected").val();
        if(nameDDL === 'ddl_rol_type'){
            list_ajax_call(path,{type:'dll_rol_list', rol:selected},"rol_list_div");
            rol = selected;
            //list_ajax_call(path,{type:'dll_pro_time'},"dll_pro_time_div");
            //$("#pro_details_div").empty();
            $("#panel_details").hide();
            cleanDetails();
        }
        else if(nameDDL === 'ddl_rol_list'){
            list_ajax_call(path,{type:'pro_details', userID:selected},"pro_details_div");
            idPerson = selected;
            $("#panel_details").show();
        }
        else if(nameDDL === 'ddl_pro_time'){
            if(idPerson>0){
                list_ajax_call(path,{type:'chart_pro'},"chart_div");
                $('#table_div').html('');
                //list_ajax_call(path,{type:'table_pro'},"table_div");
                time_span_es = selected;
                switch(time_span_es)
                {
                    case 'Dia':
                        $('#txt_time_pro_bgn').attr("type","text");
                        $('#txt_time_pro_end').attr("type","hidden");
                        $('#txt_time_pro_bgn').val('');
                    break;
                    case 'Mes':
                        $('#txt_time_pro_bgn').attr("type","text");
                        $('#txt_time_pro_end').attr("type","hidden");
                        $('#txt_time_pro_bgn').val('');
                    break;
                    case 'Temporada':
                        $('#txt_time_pro_bgn').attr("type","text");
                        $('#txt_time_pro_end').attr("type","text");
                        $('#txt_time_pro_bgn').val('');
                        $('#txt_time_pro_end').val('');
                    break;
                }
            }
        }
        $('.selectpicker').selectpicker('show');
    });
    //------------------------- CALENDAR DATE PICKER ----------------------------------
    $('#txt_time_pro_bgn').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    $('#txt_time_pro_end').datepicker({
        //todayBtn: true,
        language: "es",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
    $('#txt_time_pro_bgn').datepicker().on('changeDate', function(ev){
        if( $('#txt_time_pro_bgn').val() !== ''){
            list_ajax_call(path,{type:'chart_pro'},"chart_div");
            $('#table_div').html('');
            //list_ajax_call(path,{type:'table_pro'},"table_div");
            var selected1 = addDays(ev.date, 1);
            switch(time_span_es)
            {
                case 'Dia':
                    dateBgn = fn_getYear(selected1) + '/' + fn_getMonth(selected1) + '/' + fn_getDay(selected1);
                    getInfo(time_span_es, dateBgn, '', rol, idPerson);
                    //alert(dateBgn + " rol: " + rol + " person: " + idPerson);
                break;
                case 'Mes':
                    dateBgn = fn_getYear(selected1) + '/' + fn_getMonth(selected1) + '/01';
                    getInfo(time_span_es, dateBgn, '', rol, idPerson);
                    //alert(dateBgn + " rol: " + rol + " person: " + idPerson);
                break;
                case 'Temporada':
                    dateBgn = fn_getYear(selected1) + '/' + fn_getMonth(selected1) + '/' + fn_getDay(selected1);
                break;
            }
        }
    });
    $('#txt_time_pro_end').datepicker().on('changeDate', function(ev){
        if( $('#txt_time_pro_end').val() !== ''){
            list_ajax_call(path,{type:'chart_pro'},"chart_div");
            $('#table_div').html('');
            //list_ajax_call(path,{type:'table_pro'},"table_div");
            var selected2 = addDays(ev.date, 1);
            switch(time_span_es)
            {
                case 'Temporada':
                    dateEnd = fn_getYear(selected2) + '/' + fn_getMonth(selected2) + '/' + fn_getDay(selected2);
                    getInfo(time_span_es, dateBgn, dateEnd, rol, idPerson);
                    //alert(dateBgn + " " + dateEnd  + " rol: " + rol + " person: " + idPerson);
                break;
            }
        }
    });    
});
