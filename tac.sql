-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2017 a las 22:34:30
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tac`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_administrator_peopleList_by_place` (IN `Type_IN` VARCHAR(25), IN `idPlace_IN` INT(11))  BEGIN

  IF STRCMP(Type_IN, 'All') = 0 THEN
      
    SELECT 
    List.idPerson AS Id
    ,List.namePerson AS Name
    ,List.mail AS Mail
    ,getCategoryName(List.idPlace) AS Place
    ,IFNULL(withAssistance.OrdinaryPoints,0) AS OrdinaryPoints
    ,IFNULL(withAssistance.ExtraMiles,0) AS ExtraMiles
    FROM 
    (
        SELECT P.idPerson, P.namePerson, P.mail, S.active, S.idPlace
            FROM person as P
            INNER JOIN session AS S 
            ON P.idPerson = S.idPerson
        WHERE S.active = 1  
    ) AS List
    LEFT JOIN 
    (
        SELECT 
            Id, 
            Name, 
            GROUP_CONCAT(IF(IdPointType = getCategoryId('AcumulacionDePuntos','Ordinario'), Points, NULL)) AS OrdinaryPoints,  
            GROUP_CONCAT(IF(IdPointType = getCategoryId('AcumulacionDePuntos','Extraordinario'), Points, NULL)) AS ExtraMiles
        FROM (
            SELECT
                TotalPoint.idPerson AS Id
                ,TotalPoint.namePerson AS Name
                ,TotalPoint.type AS IdPointType
                ,SUM(TotalPoint.Puntos) AS Points
            FROM (
                    #---------------------------------------------------------
                    #points for assistance
                    #---------------------------------------------------------
                    SELECT P.idPerson, P.namePerson, A.type, SUM(A.Points) AS Puntos
                    FROM person as P
                    INNER JOIN session AS S ON P.idPerson = S.idPerson
                    INNER JOIN assistance as A ON S.idSession = A.idSession
                        WHERE S.active = 1
                    GROUP BY P.idPerson, P.namePerson, A.type
                    #---------------------------------------------------------
                        UNION
                    #---------------------------------------------------------
                    #points for nutrition dates      
                    #---------------------------------------------------------
                    SELECT P.idPerson, P.namePerson, N.type, SUM(N.Points) AS Puntos
                    FROM person as P
                    INNER JOIN session AS S ON P.idPerson = S.idPerson
                    INNER JOIN nutriappoint as N ON S.idSession = N.idSession
                        WHERE S.active = 1
                        AND N.idAppointType IN (
                            getCategoryId('TipoDeCitaNutricional','Realizada')
                            ,getCategoryId('TipoDeCitaNutricional','El mismo dia')
                        )
                    GROUP BY P.idPerson, P.namePerson, N.type
                    #---------------------------------------------------------
                ) AS TotalPoint
            GROUP BY TotalPoint.namePerson,TotalPoint.idPerson,TotalPoint.type
        ) AS TwoColumn
        GROUP BY Id, Name
    ) AS withAssistance
    ON List.idPerson = withAssistance.Id
    ORDER BY List.namePerson;    
      
  ELSEIF STRCMP(Type_IN, 'Specific') = 0 THEN
      
    SELECT 
    List.idPerson AS Id
    ,List.namePerson AS Name
    ,List.mail AS Mail
    ,getCategoryName(List.idPlace) AS Place
    ,IFNULL(withAssistance.OrdinaryPoints,0) AS OrdinaryPoints
    ,IFNULL(withAssistance.ExtraMiles,0) AS ExtraMiles
    FROM 
    (
        SELECT P.idPerson, P.namePerson, P.mail, S.active, S.idPlace
            FROM person as P
            INNER JOIN session AS S 
            ON P.idPerson = S.idPerson
        WHERE S.active = 1  
        AND S.idPlace = idPlace_IN
    ) AS List
    LEFT JOIN 
    (
        SELECT 
            Id, 
            Name, 
            GROUP_CONCAT(IF(IdPointType = getCategoryId('AcumulacionDePuntos','Ordinario'), Points, NULL)) AS OrdinaryPoints,  
            GROUP_CONCAT(IF(IdPointType = getCategoryId('AcumulacionDePuntos','Extraordinario'), Points, NULL)) AS ExtraMiles
        FROM (
            SELECT
                TotalPoint.idPerson AS Id
                ,TotalPoint.namePerson AS Name
                ,TotalPoint.type AS IdPointType
                ,SUM(TotalPoint.Puntos) AS Points
            FROM (
                    #---------------------------------------------------------
                    #points for assistance
                    #---------------------------------------------------------
                    SELECT P.idPerson, P.namePerson, A.type, SUM(A.Points) AS Puntos
                    FROM person as P
                    INNER JOIN session AS S ON P.idPerson = S.idPerson
                    INNER JOIN assistance as A ON S.idSession = A.idSession
                        WHERE S.active = 1
                        AND S.idPlace = idPlace_IN
                    GROUP BY P.idPerson, P.namePerson, A.type
                    #---------------------------------------------------------
                        UNION
                    #---------------------------------------------------------
                    #points for nutrition dates      
                    #---------------------------------------------------------
                    SELECT P.idPerson, P.namePerson, N.type, SUM(N.Points) AS Puntos
                    FROM person as P
                    INNER JOIN session AS S ON P.idPerson = S.idPerson
                    INNER JOIN nutriappoint as N ON S.idSession = N.idSession
                        WHERE S.active = 1
                        AND S.idPlace = idPlace_IN
                        AND N.idAppointType IN (
                            getCategoryId('TipoDeCitaNutricional','Realizada')
                            ,getCategoryId('TipoDeCitaNutricional','El mismo dia')
                        )
                    GROUP BY P.idPerson, P.namePerson, N.type
                    #---------------------------------------------------------
                ) AS TotalPoint
            GROUP BY TotalPoint.namePerson,TotalPoint.idPerson,TotalPoint.type
        ) AS TwoColumn
        GROUP BY Id, Name
    ) AS withAssistance
    ON List.idPerson = withAssistance.Id
    ORDER BY List.namePerson;
            
  END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_administrator_people_details` (IN `idPerson_IN` INT(11), IN `type_user` VARCHAR(25))  BEGIN

  IF STRCMP(type_user, 'user') = 0 THEN
  
    SELECT P.idPerson
        ,P.namePerson AS Nombre
        ,P.nickName AS Nick
        ,DATE_FORMAT(P.birthday,'%d/%m/%Y') AS 'Fecha de Nac.'
        ,TIMESTAMPDIFF(YEAR,P.birthday,NOW()) AS Edad
        ,P.height as Altura
        ,getCategoryName(P.gender) as Genero
        ,P.telephone as Telefono
        ,P.mail as Correo
        ,CONCAT(PPP.idPicPerPerson,'.',PPP.extension) as Foto
        ,getSessionNumber(P.idPerson) as Temporada
        ,S.idPlace AS idPlace
        ,getCategoryName(S.idPlace) as Sede
    FROM person AS P
      INNER JOIN session as S ON P.idPerson = S.idPerson
      INNER JOIN picsperperson as PPP ON PPP.idPerson = S.idPerson
    WHERE P.idPerson = idPerson_IN
    AND S.active = 1
    AND PPP.active = 1;
  
  ELSEIF STRCMP(type_user, 'professional') = 0 THEN
  
    SELECT P.idPerson
        ,P.namePerson AS Nombre
        ,DATE_FORMAT(P.birthday,'%d/%m/%Y') AS 'Fecha de Nac.'
        ,TIMESTAMPDIFF(YEAR,P.birthday,NOW()) AS Edad
        ,P.height as Altura
        ,getCategoryName(P.gender) as Genero
        ,P.telephone as Telefono
        ,P.mail as Correo
        ,CONCAT(PPP.idPicPerPerson,'.',PPP.extension) as Foto
    FROM person AS P
      INNER JOIN picsperperson as PPP ON PPP.idPerson = P.idPerson
    WHERE P.idPerson = idPerson_IN;
  
  END IF; 

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_administrator_people_list_by_place` (IN `date_IN` VARCHAR(10), IN `idPlace_IN` INT(11), IN `type_IN` VARCHAR(10))  BEGIN

  IF STRCMP(type_IN, 'ALL') = 0 THEN
  
    SELECT P.idPerson, P.namePerson
        FROM person AS P 
        INNER JOIN session AS S 
        ON P.idPerson = S.idPerson
    WHERE S.idPlace = idPlace_IN
    AND S.active = 1;
  
  ELSEIF STRCMP(type_IN, 'DATE') = 0 THEN
  
    SELECT P.idPerson, P.namePerson
        FROM person AS P 
        INNER JOIN session AS S 
        ON P.idPerson = S.idPerson
    WHERE S.active = 1
    AND S.idPlace = idPlace_IN
    AND DATE_FORMAT(STR_TO_DATE(date_IN,'%Y-%m-%d'),'%Y-%m')
    BETWEEN DATE_FORMAT(S.BeginDate,'%Y-%m') 
    AND DATE_FORMAT(S.EndDate,'%Y-%m')
    ORDER BY P.namePerson;
  
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_administrator_people_statistics` (IN `date_IN` VARCHAR(100), IN `idPlace_IN` INT(11), IN `type_IN` VARCHAR(25))  BEGIN

  IF STRCMP(type_IN, 'UserMovement') = 0 THEN
      #NEW
      SELECT 'Nuevo' AS TIPO, COUNT(idSession) AS TOTAL
          FROM session
      WHERE YEAR(BeginDate) = YEAR(STR_TO_DATE(date_IN,'%Y-%m-%d'))
          AND MONTH(BeginDate) = MONTH(STR_TO_DATE(date_IN,'%Y-%m-%d'))
          AND idPlace = idPlace_IN
          AND active = 1
      UNION
      #DOWN
      SELECT 'De Baja' AS TIPO, COUNT(idSession) AS TOTAL
          FROM session
      WHERE YEAR(EndDate) = YEAR(STR_TO_DATE(date_IN,'%Y-%m-%d'))
          AND MONTH(EndDate) = MONTH(STR_TO_DATE(date_IN,'%Y-%m-%d'))
          AND idPlace = idPlace_IN
          AND active = 0
      UNION
      #NORMAL
      SELECT 'Reingreso' AS TIPO, COUNT(idSession) AS TOTAL
          FROM session             
      WHERE STR_TO_DATE(date_IN,'%Y-%m-%d') BETWEEN BeginDate AND EndDate
          AND idPlace = idPlace_IN
          AND active = 1;

  ELSEIF STRCMP(type_IN, 'UserGender') = 0 THEN
  
      SELECT getCategoryName(P.gender) AS GENERO, COUNT(P.gender) AS TOTAL
          FROM session AS S INNER JOIN person AS P ON S.idPerson = P.idPerson
      WHERE STR_TO_DATE(date_IN,'%Y-%m-%d') 
      BETWEEN STR_TO_DATE(CONCAT(YEAR(BeginDate),'-',MONTH(BeginDate),'-01'),'%Y-%m-%d') 
      AND LAST_DAY(EndDate)
          AND S.idPlace = idPlace_IN
          AND S.active = 1
      GROUP BY P.gender;
      
  ELSEIF STRCMP(type_IN, 'UserClass') = 0 THEN
      
      SELECT getCategoryName(idClass) AS CLASE, COUNT(idClass) AS TOTAL
          FROM session AS S 
          INNER JOIN assistance AS A ON S.idSession = A.idSession
      WHERE YEAR(A.dt_Assistance) = YEAR(STR_TO_DATE(date_IN,'%Y-%m-%d'))
      AND MONTH(A.dt_Assistance) = MONTH(STR_TO_DATE(date_IN,'%Y-%m-%d'))
      AND S.idPlace = idPlace_IN
      GROUP BY idClass;
      
  ELSEIF STRCMP(type_IN, 'UserHour') = 0 THEN
      
      SELECT CONCAT(HORA,':00') AS HORA, COUNT(HORA) AS TOTAL FROM (
          SELECT HOUR(A.dt_Assistance) AS HORA
              FROM session AS S 
              INNER JOIN assistance AS A 
              ON S.idSession = A.idSession
          WHERE YEAR(A.dt_Assistance) = YEAR(STR_TO_DATE(date_IN,'%Y-%m-%d'))
          AND MONTH(A.dt_Assistance) = MONTH(STR_TO_DATE(date_IN,'%Y-%m-%d'))
          AND S.idPlace = idPlace_IN
      ) AS T
      GROUP BY HORA;
      
  END IF;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_administrator_people_top` (IN `idPerson_IN` INT(11))  BEGIN

    SELECT P.namePerson AS Nombre
        ,P.nickName AS Nick
        ,getCategoryName(S.idPlace) AS namePlace
        ,TIMESTAMPDIFF(YEAR,P.birthday,NOW()) AS Edad
        ,GROUP_CONCAT(IF(getCategoryName(DNA.data)='Peso', DNA.value, NULL)) AS 'Peso'
        ,GROUP_CONCAT(IF(getCategoryName(DNA.data)='Porcentaje De Grasa Corporal', DNA.value, NULL)) AS 'GrasaCorporal'
        ,GROUP_CONCAT(IF(getCategoryName(DNA.data)='Indice de Masa Corporal - IMC', DNA.value, NULL)) AS 'IMC'
    FROM person AS P
      INNER JOIN session as S ON P.idPerson = S.idPerson
      LEFT JOIN nutriappoint AS NA ON NA.idSession = S.idSession
      LEFT JOIN detailsnutriappoint AS DNA ON DNA.idNutriAppoint = NA.idNutriAppoint
    WHERE P.idPerson = idPerson_IN
    AND S.active = 1
    AND NA.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional','medida meta')
    AND getCategoryName(DNA.data) IN ('Peso','Porcentaje De Grasa Corporal','Indice de Masa Corporal - IMC');

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_administrator_session_update` (IN `idPerson` INT(11), IN `column_name` VARCHAR(15), IN `texto` VARCHAR(200))  BEGIN
  
  #begin
  SET @QUERY = CONCAT(
    'UPDATE session', 
    ' SET ', column_name, ' = ');
    
  SET @QUERY = CONCAT(@QUERY,'''',texto,'''');
  SET @QUERY = CONCAT(@QUERY,' WHERE idPerson = ', idPerson);
  SET @QUERY = CONCAT(@QUERY,' AND active = 1');
  
  #SELECT @QUERY;
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_assistance_check` (IN `idCoach_IN` INT(11), IN `idClass_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN
    SET @idDetails = NULL;
    SET @idDetails = IFNULL((
      SELECT COUNT(*)
        FROM assistance
      WHERE idSession = getSessionId(idPerson_IN)
      AND DATE_FORMAT(dt_Assistance,'%Y-%m-%d') = 
      DATE_FORMAT(NOW(),'%Y-%m-%d')
    ),0);
    
    SET @PointType = NULL;
    IF (@idDetails > 0) THEN
      SET @PointType = getCategoryId('AcumulacionDePuntos','Extraordinario');
    ELSEIF (@idDetails = 0) THEN
      SET @PointType = getCategoryId('AcumulacionDePuntos','Ordinario');
    END IF;
    
    INSERT INTO assistance
      (dt_Assistance, 
      idPlace, 
      idClass, 
      idCoach, 
      Points, 
      idSession, 
      type) 
    VALUES (
      NOW(), 
      getPlaceId(idPerson_IN), 
      idClass_IN, 
      idCoach_IN, 
      130, #points per assistance
      getSessionId(idPerson_IN), 
      @PointType);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badgesavailables_list` (IN `idPerson_IN` INT(11))  BEGIN

    SELECT B.idBadge, B.name, B.description, B.image
        FROM badges AS B
    WHERE B.idBadge NOT IN (
        SELECT BPP.idBadge
            FROM badgeperperson AS BPP 
            INNER JOIN session AS S ON BPP.idSession = S.idSession
        WHERE S.idPerson = idPerson_IN
        AND S.active = 1
    );

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badgesperperson_add` (IN `idPerson_IN` INT(11), IN `idBadge_IN` INT(11), IN `idNutri_IN` INT(11))  BEGIN
    INSERT INTO badgeperperson
        (idSession, idBadge, dt_Badge, idNutri) 
    VALUES 
        (getSessionId(idPerson_IN), idBadge_IN, NOW(),idNutri_IN);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badgesperperson_all` (IN `type_IN` VARCHAR(10))  BEGIN
  IF STRCMP(type_IN, 'List') = 0 THEN
    SELECT getCategoryName(S.idPlace) AS Place, P.idPerson, P.namePerson, COUNT(BPP.idBadge) AS Total
    FROM badgeperperson AS BPP
      INNER JOIN session AS S ON BPP.idSession = S.idSession
      INNER JOIN person AS P ON P.idPerson = S.idPerson
    WHERE S.active = 1
    GROUP BY S.idPlace, P.idPerson, P.namePerson
    ORDER BY COUNT(BPP.idBadge) DESC;
  ELSEIF STRCMP(type_IN, 'Images') = 0 THEN
    SELECT P.idPerson, B.name, B.image
    FROM badgeperperson AS BPP
      INNER JOIN session AS S ON BPP.idSession = S.idSession
      INNER JOIN person AS P ON P.idPerson = S.idPerson
      INNER JOIN badges AS B ON B.idBadge = BPP.idBadge
    WHERE S.active = 1
    ORDER BY P.idPerson;
  END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badgesperperson_list` (IN `idPerson_IN` INT(11))  BEGIN

    SELECT B.idBadge, B.name, B.description, B.image
        FROM badgeperperson AS BPP 
        INNER JOIN badges AS B ON BPP.idBadge = B.idBadge
        INNER JOIN session AS S ON BPP.idSession = S.idSession
    WHERE S.idPerson = idPerson_IN
    AND S.active = 1;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badgesperperson_remove` (IN `idPerson_IN` INT(11), IN `idBadge_IN` INT(11))  BEGIN

    DELETE FROM badgeperperson 
    WHERE idSession = getSessionId(idPerson_IN) 
    AND idBadge = idBadge_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badges_add` (IN `name_IN` VARCHAR(100), IN `description_IN` VARCHAR(200), IN `value_IN` INT(11), OUT `last_id` INT(11))  BEGIN

    INSERT INTO badges
        (name, description, value, image) 
    VALUES (name_IN, description_IN, value_IN, 'no_image.png');
    
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badges_get` (`idBadge_IN` INT(11))  BEGIN
    SELECT idBadge, name, description, IFNULL(value,0) AS value, image 
    FROM badges
    WHERE idBadge = idBadge_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badges_list` ()  BEGIN
    SELECT idBadge, name, description, value, image 
    FROM badges;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badges_remove` (`idBadge_IN` INT(11))  BEGIN
    DELETE FROM badges
    WHERE idBadge = idBadge_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badges_update` (IN `idBadge_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(200))  BEGIN
    
    SET @QUERY = CONCAT(
      'UPDATE badges', 
      ' SET ', column_name_IN, ' = ');
    SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
    SET @QUERY = CONCAT(@QUERY,' WHERE idBadge = ', idBadge_IN);
    
    #SELECT @QUERY;
    
    PREPARE stmt FROM @QUERY;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
    
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_badges_update_all` (IN `name_IN` VARCHAR(100), IN `value_IN` INT(11), IN `description_IN` VARCHAR(200), IN `idBadge_IN` INT(11))  BEGIN
    UPDATE badges
        SET name = name_IN
        ,description = description_IN
        ,value = value_IN
    WHERE idBadge = idBadge_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_catalog_id` (IN `metaCategory_IN` VARCHAR(50), IN `category_IN` VARCHAR(50))  BEGIN
  
  SELECT C.idCatalog
      FROM metacatalog AS MC
      INNER JOIN catalog AS C
      ON MC.idMeta = C.idMeta
  WHERE MC.nameMeta = metaCategory_IN
  AND C.nameCatalog = category_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_catalog_list` (`meta_IN` VARCHAR(50))  BEGIN

    SELECT idCatalog, nameCatalog
        FROM catalog
    WHERE idMeta = getMetaId(meta_IN)
    ORDER BY idCatalog DESC;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_catalog_name` (IN `idCatalog_IN` VARCHAR(50))  BEGIN
  
  SELECT nameCatalog
      FROM catalog
  WHERE idCatalog = idCatalog_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_chocale_check` (`idGiver_IN` INT(11), `idSession_IN` INT(11))  BEGIN
    
    SELECT COUNT(idChocale) as NoChocales
    FROM chocale
    WHERE idGiver = idGiver_IN
    AND idSession = idSession_IN;
    
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_chocale_list` (IN `idPerson_IN` INT(11), IN `Type_IN` VARCHAR(25))  BEGIN

  IF STRCMP(Type_IN, 'General') = 0 THEN
  
      SELECT 
          P.namePerson as nameReceiver
          ,COUNT(CH.idGiver) AS NoChocales
      FROM person AS P 
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      LEFT JOIN chocale AS CH ON S.idSession = CH.idSession
      WHERE P.idPerson = idPerson_IN
      AND S.active = 1
      GROUP BY CH.idSession;
  
  ELSEIF STRCMP(Type_IN, 'Details') = 0 THEN
  
      SELECT 
          CH.idGiver
          ,getPersonName(CH.idGiver) as nameGiver
      FROM person AS P 
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      INNER JOIN chocale AS CH ON S.idSession = CH.idSession
      WHERE P.idPerson = idPerson_IN
      AND S.active = 1;
      
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_chocale_remove` (`idGiver_IN` INT(11), `idReceiver_IN` INT(11))  BEGIN

    DELETE FROM chocale
        WHERE idSession = getSessionId(idReceiver_IN)
        AND idGiver = idGiver_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_chocale_save` (`idGiver_IN` INT(11), `idReceiver_IN` INT(11))  BEGIN
    
    INSERT INTO chocale(
        idSession
        ,idGiver
        ,dt_Chocale) 
    VALUES (
        getSessionId(idReceiver_IN)
        ,idGiver_IN
        ,NOW());
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_detailsnutriappoint_add` (IN `idNutriAppoint_IN` INT(11), IN `data_IN` INT(11), IN `value_IN` VARCHAR(100))  BEGIN
  INSERT INTO detailsnutriappoint
    (idNutriAppoint, data, value) 
  VALUES 
    (idNutriAppoint_IN, data_IN, value_IN);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_detailsnutriappoint_delete` (IN `idNutriAppoint_IN` INT(11))  BEGIN
  
  DELETE FROM detailsnutriappoint 
  WHERE idNutriAppoint = idNutriAppoint_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_detailsnutriappoint_edit` (IN `idDetails_IN` INT(11), IN `value_IN` VARCHAR(100))  BEGIN
  UPDATE detailsnutriappoint 
    SET value = value_IN
  WHERE idDetails = idDetails_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_detailsnutriappoint_exist` (IN `idNutriAppoint_IN` INT(11), IN `data_IN` INT(11))  BEGIN
    SELECT idDetails
      FROM detailsnutriappoint
    WHERE idNutriAppoint = idNutriAppoint_IN
    AND data = data_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_detailsnutriappoint_list` (IN `idPerson_IN` INT(11), IN `idNutriAppoint_IN` INT(11), IN `Type_IN` VARCHAR(20))  BEGIN
    
    IF STRCMP(Type_IN, 'Current') = 0 THEN
    
      SELECT N.idNutriAppoint
        FROM nutriappoint AS N
        INNER JOIN session AS S 
        ON N.idSession = S.idSession
      WHERE S.idPerson = idPerson_IN
      AND S.active = 1
      AND N.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional', 'medida actual')
      ORDER BY N.dt_Appointment DESC
      LIMIT 1;
      
    ELSEIF STRCMP(Type_IN, 'Ranges') = 0 THEN

      SELECT 
      T.data AS idIndicador
      ,getCategoryName(T.data) AS nameIndicador
      ,GROUP_CONCAT(IF(T.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional', 'medida inicial'), T.value, NULL)) AS 'Inicial'
      ,GROUP_CONCAT(IF(T.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional', 'medida actual'), T.value, NULL)) AS 'Actual'
      ,GROUP_CONCAT(IF(T.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional', 'medida meta'), T.value, NULL)) AS 'Meta'
      FROM (
          SELECT D.data
          ,D.value
          ,N.idMeasureType
              FROM session AS S
              INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
              INNER JOIN detailsnutriappoint AS D ON N.idNutriAppoint = D.idNutriAppoint
          WHERE S.idPerson = idPerson_IN
          AND S.active = 1
          AND N.idNutriAppoint = idNutriAppoint_IN
              UNION
          SELECT D.data
          ,D.value
          ,N.idMeasureType
              FROM session AS S
              INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
              INNER JOIN detailsnutriappoint AS D ON N.idNutriAppoint = D.idNutriAppoint
          WHERE S.idPerson = idPerson_IN
          AND S.active = 1
          AND N.idMeasureType IN (
          getCategoryId('TiposDeMedicionEnCitaNutricional', 'medida inicial'),
          getCategoryId('TiposDeMedicionEnCitaNutricional', 'medida meta'))
      ) AS T
      GROUP BY T.data 
      ORDER BY T.data,T.idMeasureType;

    END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_add` (IN `nameCont_IN` VARCHAR(200), IN `thumbnail_IN` VARCHAR(200), IN `valueCont_IN` MEDIUMTEXT, IN `contType_IN` INT(11), IN `contCategory_IN` INT(11), IN `beginDate_IN` VARCHAR(10), IN `endDate_IN` VARCHAR(10), OUT `last_id` INT(11))  BEGIN

    SET lc_time_names = 'es_GT';
    SET @lcl_type = getCategoryName(contType_IN);

    IF STRCMP(@lcl_type, 'Articulos') = 0 OR 
    STRCMP(@lcl_type, 'Recetas') = 0 OR 
    STRCMP(@lcl_type, 'Videos') = 0 
    THEN
        INSERT INTO digitalcontent(
            nameCont, 
            thumbnail, 
            valueCont, 
            contType, 
            contCategory, 
            beginDate, 
            endDate) 
        VALUES (
            nameCont_IN, 
            thumbnail_IN, 
            valueCont_IN, 
            contType_IN, 
            contCategory_IN, 
            DATE_FORMAT(STR_TO_DATE(CONCAT(beginDate_IN,' 5:00 PM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i'),
            DATE_FORMAT(STR_TO_DATE(CONCAT(endDate_IN,' 4:59 AM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i'));
            
        SET last_id = LAST_INSERT_ID();

    ELSEIF STRCMP(@lcl_type, 'Esfuerzo') = 0 
    OR STRCMP(@lcl_type, 'Dietas') = 0
    THEN
        
        START TRANSACTION;
        
        INSERT INTO digitalcontent
            (nameCont 
            ,thumbnail
            ,valueCont
            ,contType
            ,contCategory
            ,beginDate
            ,endDate) 
        VALUES 
            (nameCont_IN
            ,thumbnail_IN
            ,valueCont_IN
            ,contType_IN
            ,contCategory_IN
            ,DATE_FORMAT(STR_TO_DATE(CONCAT(beginDate_IN,' 5:00 PM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i')
            ,DATE_FORMAT(STR_TO_DATE(CONCAT(endDate_IN,' 4:59 AM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i'));
            
        SET last_id = LAST_INSERT_ID();
        
        UPDATE digitalcontent
            SET valueCont = CONCAT(last_id,'.pdf')
        WHERE idCont = last_id;
        
        COMMIT;
            
    END IF;
    
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_count` (IN `type_IN` VARCHAR(25), IN `level_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN

  IF STRCMP(type_IN, 'Articulos') = 0 OR 
  STRCMP(type_IN, 'Recetas') = 0 OR 
  STRCMP(type_IN, 'Videos') = 0 
  THEN
  
      SELECT COUNT(*) AS TOTAL
        FROM digitalcontent AS DC 
        INNER JOIN contentperperson AS CPP ON CPP.idCont = DC.idCont
        LEFT JOIN (
            SELECT FC.idFavRate, FC.idCont
                FROM favoriteratecontent AS FC
            WHERE getPersonId(FC.idSession) = idPerson_IN) AS FC
        ON DC.idCont = FC.idCont
      WHERE DC.contType = getCategoryId('Contenido',type_IN)
      AND DC.contCategory = level_IN
      AND NOW() BETWEEN DC.beginDate AND DC.endDate
      AND CPP.idPerson = idPerson_IN;
      
  ELSEIF STRCMP(type_IN, 'Esfuerzo') = 0 OR 
  STRCMP(type_IN, 'Dietas') = 0
  THEN
  
      SELECT COUNT(*) AS TOTAL
        FROM digitalcontent AS DC 
        INNER JOIN contentperperson AS CPP ON CPP.idCont = DC.idCont
        LEFT JOIN (
            SELECT FC.idFavRate, FC.idCont
                FROM favoriteratecontent AS FC
            WHERE getPersonId(FC.idSession) = idPerson_IN) AS FC
        ON DC.idCont = FC.idCont
      WHERE DC.contType = getCategoryId('Contenido',type_IN)
      AND DC.contCategory IS NULL
      AND NOW() BETWEEN DC.beginDate AND DC.endDate
      AND CPP.idPerson = idPerson_IN;  
      
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_delete` (IN `idCont_IN` INT(11))  BEGIN
  
    DELETE FROM digitalcontent
    WHERE idCont = idCont_IN;
            
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_get` (IN `idContent_IN` INT(11))  BEGIN
    
    SELECT idCont, 
    nameCont, 
    thumbnail, 
    valueCont, 
    contType, 
    contCategory, 
    DATE_FORMAT(beginDate, '%d/%m/%Y') AS beginDate, 
    DATE_FORMAT(endDate, '%d/%m/%Y') AS endDate
      FROM digitalcontent
    WHERE idCont = idContent_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_list` (IN `type_IN` VARCHAR(25), IN `level_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN
    
    IF STRCMP(type_IN, 'Articulos') = 0 OR 
    STRCMP(type_IN, 'Recetas') = 0 OR 
    STRCMP(type_IN, 'Videos') = 0 
    THEN
    
      SELECT DC.idCont
        ,DC.nameCont
        ,DC.thumbnail
        ,DC.valueCont
        ,IFNULL(FC.idFavRate,-1) AS idFavRate
        ,FC.rate
        ,FC.favorite
            FROM digitalcontent AS DC 
            LEFT JOIN (
                SELECT FC.idFavRate, FC.idCont, FC.rate, FC.favorite
                    FROM favoriteratecontent AS FC
                WHERE getPersonId(FC.idSession) = idPerson_IN) AS FC
            ON DC.idCont = FC.idCont
        WHERE DC.contType = getCategoryId('Contenido',type_IN)
        AND DC.contCategory = level_IN
        AND NOW() BETWEEN DC.beginDate AND DC.endDate
        ORDER BY DC.nameCont;
        
    ELSEIF STRCMP(type_IN, 'Esfuerzo') = 0 
    OR STRCMP(type_IN, 'Dietas') = 0
    THEN
    
        SELECT DC.idCont
        ,DC.nameCont
        ,DC.thumbnail
        ,DC.valueCont
        ,IFNULL(FC.idFavRate,-1) AS idFavRate
        ,FC.rate
        ,FC.favorite
            FROM digitalcontent AS DC 
            INNER JOIN contentperperson AS CPP ON CPP.idCont = DC.idCont
            LEFT JOIN (
                SELECT FC.idFavRate, FC.idCont, FC.rate, FC.favorite
                    FROM favoriteratecontent AS FC
                WHERE getPersonId(FC.idSession) = idPerson_IN) AS FC
            ON DC.idCont = FC.idCont
        WHERE  CPP.idPerson = idPerson_IN
        AND DC.contType = getCategoryId('Contenido',type_IN)
        AND DC.contCategory IS NULL
        AND NOW() BETWEEN DC.beginDate AND DC.endDate
        ORDER BY DC.nameCont;
    
    END IF;
      
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_list_admin` (IN `type_IN` INT(11), IN `level_IN` INT(11))  BEGIN
    SET @lcl_type = getCategoryName(type_IN);
    
    IF STRCMP(@lcl_type, 'Articulos') = 0 OR 
    STRCMP(@lcl_type, 'Recetas') = 0 OR 
    STRCMP(@lcl_type, 'Videos') = 0 
    THEN

        SELECT DC.idCont
        ,DC.nameCont
        ,DC.thumbnail
        ,DC.valueCont
            FROM digitalcontent AS DC 
        WHERE DC.contType = type_IN
        AND DC.contCategory = level_IN
        ORDER BY DC.nameCont;

    ELSEIF STRCMP(@lcl_type, 'Esfuerzo') = 0 
    OR STRCMP(@lcl_type, 'Dietas') = 0
    THEN

        SELECT DC.idCont
        ,DC.nameCont
        ,DC.thumbnail
        ,DC.valueCont
            FROM digitalcontent AS DC 
        WHERE DC.contType = type_IN
        AND DC.contCategory IS NULL
        ORDER BY DC.nameCont;
        
    END IF;  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_per_person` (IN `idCont_IN` INT(11), IN `idPerson_IN` INT(11), IN `idNutri_IN` INT(11))  BEGIN
    INSERT INTO contentperperson(
        idCont
        ,idPerson
        ,idNutri
        ,dateContent) 
    VALUES (
        idCont_IN
        ,idPerson_IN
        ,idNutri_IN
        ,NOW());
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_update` (IN `idCont_IN` INT(11), IN `nameCont_IN` VARCHAR(200), IN `thumbnail_IN` VARCHAR(200), IN `valueCont_IN` MEDIUMTEXT, IN `contType_IN` INT(11), IN `contCategory_IN` INT(11), IN `beginDate_IN` VARCHAR(10), IN `endDate_IN` VARCHAR(10))  BEGIN

    UPDATE digitalcontent 
      SET nameCont = nameCont_IN, 
      thumbnail = thumbnail_IN, 
      valueCont = valueCont_IN, 
      contType = contType_IN, 
      contCategory = contCategory_IN, 
      beginDate = DATE_FORMAT(STR_TO_DATE(CONCAT(beginDate_IN,' 5:00 PM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i'),
      endDate = DATE_FORMAT(STR_TO_DATE(CONCAT(endDate_IN,' 4:59 AM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i')
    WHERE idCont = idCont_IN;
    
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_updateThumbnail` (IN `idCont_IN` INT(11))  BEGIN

    UPDATE digitalcontent 
      SET thumbnail = CONCAT(idCont_IN,".png") 
    WHERE idCont = idCont_IN;
    
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_digitalcontent_visibility` (IN `idCont_IN` INT(11), IN `dt_visibility_begin_IN` VARCHAR(10), IN `dt_visibility_end_IN` VARCHAR(10), OUT `last_id` INT(11))  BEGIN
    UPDATE digitalcontent 
    SET beginDate = STR_TO_DATE(dt_visibility_begin_IN,'%d/%m/%Y')
    ,endDate = STR_TO_DATE(dt_visibility_end_IN,'%d/%m/%Y') 
    WHERE idCont = idCont_IN;
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_event_add` (IN `titleEvent_IN` VARCHAR(100), IN `dateEvent_IN` VARCHAR(16), IN `placeEvent_IN` VARCHAR(100), IN `idCategory_IN` INT(11), IN `beginDate_IN` VARCHAR(10), IN `endDate_IN` VARCHAR(10), OUT `last_id` INT(11))  BEGIN
      
  INSERT INTO event(
      titleEvent, 
      flyerPath, 
      dateEvent, 
      placeEvent, 
      idCategory, 
      beginDate, 
      endDate) 
  VALUES (
      titleEvent_IN, 
      'no_image.png', 
      DATE_FORMAT(STR_TO_DATE(dateEvent_IN,'%d/%m/%Y %H:%i'),'%Y-%m-%d %H:%i'), 
      placeEvent_IN, 
      idCategory_IN, 
      DATE_FORMAT(STR_TO_DATE(CONCAT(beginDate_IN,' 5:00 AM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i'), 
      DATE_FORMAT(STR_TO_DATE(CONCAT(endDate_IN,' 4:59 AM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i'));
  
  SET last_id = LAST_INSERT_ID();  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_event_list` (IN `idPerson_IN` INT(11))  BEGIN
      
   SELECT E.idEvent, 
    DATE_FORMAT(E.dateEvent,'%Y-%m-%d') as dateEvent,
    DATE_FORMAT(E.dateEvent,'%h:%i %p') AS hourEvent,
    E.placeEvent,
    E.flyerPath,  
    E.titleEvent,
    getCategoryName(E.idCategory) AS EventType,
    IFNULL(A.idPPE,-1) AS idPPE
        FROM event AS E
        LEFT JOIN (
          SELECT PPE.idEvent, PPE.idPPE
            FROM peopleperevent AS PPE
          WHERE PPE.idPerson = idPerson_IN) AS A
        ON E.idEvent = A.idEvent
    WHERE NOW() BETWEEN E.beginDate AND E.endDate;
    
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_event_update` (IN `idEvent_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(200))  BEGIN
  
  SET @QUERY = CONCAT(
    'UPDATE Event', 
    ' SET ', column_name_IN, ' = ');
  SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
  SET @QUERY = CONCAT(@QUERY,' WHERE idEvent = ', idEvent_IN);
  
  #SELECT @QUERY;
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_fav_rate_add` (`idCont_IN` INT(11), `idPerson_IN` INT(11), `rate_IN` INT(11), `fav_IN` INT(11))  BEGIN
    INSERT INTO favoriteratecontent
        (idCont, idSession, rate, favorite, `date`) 
    VALUES (
      idCont_IN
      ,getSessionId(idPerson_IN)
      ,rate_IN
      ,fav_IN
      ,NOW());
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_fav_rate_delete` (`idFav_IN` INT(11))  BEGIN
    DELETE FROM favoriteratecontent
    WHERE idFavRate = idFav_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_fav_rate_exist` (`idCont_IN` INT(11), `idPerson_IN` INT(11))  BEGIN
    SELECT idFavRate
    FROM favoriteratecontent
    WHERE idCont = idCont_IN
    AND getPersonId(idSession) = idPerson_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_fav_rate_list` (`idPerson_IN` INT(11), `contentType_IN` VARCHAR(100), `type_IN` VARCHAR(50))  BEGIN

  IF STRCMP(type_IN, 'F') = 0 THEN
  
    SELECT DC.nameCont
    ,DC.valueCont
    ,DC.thumbnail
        FROM digitalcontent AS DC 
        LEFT JOIN favoriteratecontent AS FC
        ON DC.idCont = FC.idCont
    WHERE DC.contType = getCategoryId('Contenido',contentType_IN)
    AND getPersonId(FC.idSession) = idPerson_IN
    AND FC.favorite = 1;
    
  ELSEIF STRCMP(type_IN, 'R') = 0 THEN
    
    SELECT DC.nameCont
    ,DC.valueCont
    ,DC.thumbnail
        FROM digitalcontent AS DC 
        LEFT JOIN favoriteratecontent AS FC
        ON DC.idCont = FC.idCont
    WHERE DC.contType = getCategoryId('Contenido',contentType_IN)
    AND getPersonId(FC.idSession) = idPerson_IN
    AND FC.rate > 0;
    
  ELSEIF STRCMP(type_IN, 'A') = 0 THEN
    
    SELECT DC.nameCont
    ,DC.valueCont
    ,DC.thumbnail
        FROM digitalcontent AS DC 
        LEFT JOIN favoriteratecontent AS FC
        ON DC.idCont = FC.idCont
    WHERE DC.contType = getCategoryId('Contenido',contentType_IN)
    AND getPersonId(FC.idSession) = idPerson_IN;
    #AND (FC.favorite = 1 OR FC.rate > 0);
    
  END IF;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_fav_rate_update` (`idCont_IN` INT(11), `type_IN` VARCHAR(50), `value_IN` INT(11))  BEGIN
    IF STRCMP(type_IN, 'RATE') = 0 THEN
    
        UPDATE favoriteratecontent 
        SET rate = value_IN
        ,`date` = NOW()
        WHERE idCont = idCont_IN;
        
    ELSEIF STRCMP(type_IN, 'FAVORITE') = 0 THEN
    
        UPDATE favoriteratecontent 
        SET favorite = value_IN
        ,`date` = NOW()
        WHERE idCont = idCont_IN;
        
        IF value_IN = 0 THEN
          DELETE FROM favoriteratecontent 
          WHERE favorite = 0
          AND rate = 0;
        END IF;
        
    END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notedescription_add` (IN `description_IN` VARCHAR(500), IN `idNoteSession_IN` INT(11))  BEGIN
  INSERT INTO notedescription
    (description, dateNote, idNoteSession) 
  VALUES (description_IN, NOW(), idNoteSession_IN);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notedescription_edit` (`idNoteDescription_IN` INT(11), `Description_IN` VARCHAR(500))  BEGIN
  UPDATE notedescription 
    SET description = Description_IN
  WHERE idNoteDescription = idNoteDescription_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notedescription_list` (IN `idPerson_IN` INT(11))  BEGIN
    
  SELECT ND.idNoteDescription, ND.dateNote, ND.description
  FROM notesession AS NS
    INNER JOIN notedescription ND
    ON NS.idNoteSession = ND.idNoteSession
    INNER JOIN session AS S
    ON S.idSession = NS.idSession
  WHERE S.idPerson = idPerson_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notedescription_remove` (`idNoteDescription_IN` INT(11))  BEGIN
  DELETE FROM notedescription
  WHERE idNoteDescription = idNoteDescription_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notesession_add` (IN `reason_IN` VARCHAR(500), IN `glucosaBegin_IN` INT(11), IN `glucosaEnd_IN` INT(11), IN `idPerson_IN` INT(11), OUT `last_id` INT(11))  BEGIN
  INSERT INTO notesession
  (dateNote, reason, glucosaBegin, glucosaEnd, idSession) 
  VALUES (
    NOW(), 
    reason_IN, 
    glucosaBegin_IN, 
    glucosaEnd_IN, 
    getSessionId(idPerson_IN));
  SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notesession_edit` (IN `idNoteSession_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(200))  BEGIN
  SET @QUERY = CONCAT(
    'UPDATE notesession', 
    ' SET ', column_name_IN, ' = ');
  SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
  SET @QUERY = CONCAT(@QUERY,' WHERE idNoteSession = ', idNoteSession_IN);
  
  #SELECT @QUERY;
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notesession_list` (IN `idPerson_IN` INT(11))  BEGIN
    
  SELECT NS.idNoteSession,
  NS.dateNote, 
  NS.reason, 
  IFNULL(NS.glucosaBegin,'N/A') AS glucosaBegin, 
  IFNULL(NS.glucosaEnd,'N/A') AS glucosaEnd
    FROM notesession AS NS
    INNER JOIN session AS S
    ON S.idSession = NS.idSession
  WHERE S.idPerson = idPerson_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notifications_list` (IN `idPerson_IN` INT(11), IN `typeNotification_IN` VARCHAR(25))  BEGIN
  SET lc_time_names = 'es_GT';  
  IF STRCMP(typeNotification_IN, 'Event') = 0 THEN
    
    SELECT E.idEvent 
    ,DATE_FORMAT(E.dateEvent,'%W') AS DiaNombre
    ,DATE_FORMAT(E.dateEvent,'%d') AS Dia
    ,DATE_FORMAT(E.dateEvent,'%M') AS Mes
    ,DATE_FORMAT(E.dateEvent,'%h:%i %p') AS hourEvent
    ,E.titleEvent
    ,E.placeEvent
    ,E.flyerPath
    ,IFNULL(A.idNotification,-1) AS idNoti
        FROM event AS E
        LEFT JOIN (
          SELECT idNotification, idNewObject
            FROM notification
          WHERE idPerson = idPerson_IN
          AND idTypeNoti = getCategoryId('NotificationType',typeNotification_IN)
        ) AS A
        ON E.idEvent = A.idNewObject
    WHERE NOW() BETWEEN E.beginDate AND E.endDate
    ORDER BY E.dateEvent DESC;
    
  ELSEIF STRCMP(typeNotification_IN, 'Personal') = 0 THEN
    
      SELECT BPP.idBadge AS idObj
      ,getPersonName(BPP.idNutri) AS ProName
      ,B.name AS Data1
      ,B.image AS Data2
      ,BPP.dt_Badge AS RealDate
      ,DATE_FORMAT(BPP.dt_Badge,'%W') AS DiaNombre
      ,DATE_FORMAT(BPP.dt_Badge,'%d') AS Dia
      ,DATE_FORMAT(BPP.dt_Badge,'%M') AS Mes
      ,DATE_FORMAT(BPP.dt_Badge,'%h:%i %p') AS hourObj
      ,'Badge' as NameObj
      ,IFNULL(A.idNotification,-1) AS idNoti
        FROM badgeperperson AS BPP
        LEFT JOIN (
          SELECT idNotification, idNewObject
            FROM notification
          WHERE idPerson = idPerson_IN
          AND idTypeNoti = getCategoryId('NotificationType','Badge')
        ) AS A
        ON BPP.idBadge = A.idNewObject
        INNER JOIN badges AS B
        ON BPP.idBadge = B.idBadge
      WHERE idSession = getSessionId(idPerson_IN)
      UNION
      SELECT NA.idNutriAppoint AS idObj
      ,getPersonName(NA.idNutricionist) AS ProName
      ,getCategoryName(NA.idMeasureType) AS Data1
      ,NA.Points AS Data2
      ,NA.dt_Appointment AS RealDate
      ,DATE_FORMAT(NA.dt_Appointment,'%W') AS DiaNombre
      ,DATE_FORMAT(NA.dt_Appointment,'%d') AS Dia
      ,DATE_FORMAT(NA.dt_Appointment,'%M') AS Mes
      ,DATE_FORMAT(NA.dt_Appointment,'%h:%i %p') AS hourObj
      ,'Nutricionist' as NameObj
      ,IFNULL(A.idNotification,-1) AS idNoti
        FROM nutriappoint AS NA
        LEFT JOIN (
          SELECT idNotification, idNewObject
            FROM notification
          WHERE idPerson = idPerson_IN
          AND idTypeNoti = getCategoryId('NotificationType','Nutricionist')
        ) AS A
        ON NA.idNutriAppoint = A.idNewObject
      WHERE idSession = getSessionId(idPerson_IN)
      UNION
      SELECT BPP.idBadge AS idObj
      ,getPersonName(BPP.idNutri) AS ProName
      ,B.name AS Data1
      ,B.image AS Data2
      ,BPP.dt_Badge AS RealDate
      ,DATE_FORMAT(BPP.dt_Badge,'%W') AS DiaNombre
      ,DATE_FORMAT(BPP.dt_Badge,'%d') AS Dia
      ,DATE_FORMAT(BPP.dt_Badge,'%M') AS Mes
      ,DATE_FORMAT(BPP.dt_Badge,'%h:%i %p') AS hourObj
      ,'Badge' as NameObj
      ,IFNULL(A.idNotification,-1) AS idNoti
        FROM badgeperperson AS BPP
        LEFT JOIN (
          SELECT idNotification, idNewObject
            FROM notification
          WHERE idPerson = idPerson_IN
          AND idTypeNoti = getCategoryId('NotificationType','Badge')
        ) AS A
        ON BPP.idBadge = A.idNewObject
        INNER JOIN badges AS B
        ON BPP.idBadge = B.idBadge
      WHERE idSession = getSessionId(idPerson_IN)
      UNION
      SELECT CPP.idCont AS idObj
      ,getPersonName(CPP.idNutri) AS ProName
      ,DC.nameCont AS Data1
      ,DC.thumbnail AS Data2
      ,CPP.dateContent AS RealDate
      ,DATE_FORMAT(CPP.dateContent,'%W') AS DiaNombre
      ,DATE_FORMAT(CPP.dateContent,'%d') AS Dia
      ,DATE_FORMAT(CPP.dateContent,'%M') AS Mes
      ,DATE_FORMAT(CPP.dateContent,'%h:%i %p') AS hourObj
      ,getCategoryName(DC.contType) as NameObj
      ,IFNULL(A.idNotification,-1) AS idNoti
        FROM contentperperson AS CPP
        INNER JOIN digitalcontent AS DC
        ON CPP.idCont = DC.idCont
        LEFT JOIN (
          SELECT idNotification, idNewObject
            FROM notification
          WHERE idPerson = idPerson_IN
          AND (
          idTypeNoti = getCategoryId('NotificationType','Dietas')
          OR 
          idTypeNoti = getCategoryId('NotificationType','Esfuerzo')
          )
        ) AS A
        ON CPP.idCont = A.idNewObject
      WHERE CPP.idPerson = idPerson_IN
      ORDER BY RealDate DESC;
      
  END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notifications_number` (IN `idPerson_IN` INT(11), IN `typeNotification_IN` VARCHAR(25))  BEGIN
    IF STRCMP(typeNotification_IN, 'Event') = 0 THEN
    
      SELECT COUNT(*) AS Pendient
          FROM event AS E
          LEFT JOIN (
            SELECT idNotification, idNewObject
              FROM notification
            WHERE idPerson = idPerson_IN
            AND idTypeNoti = getCategoryId('NotificationType',typeNotification_IN)
          ) AS A
          ON E.idEvent = A.idNewObject
      WHERE NOW() BETWEEN E.beginDate AND E.endDate
      AND IFNULL(A.idNotification,-1) < 0;
      
    ELSEIF STRCMP(typeNotification_IN, 'Personal') = 0 THEN
    
      SELECT SUM(Pendient) AS Pendient
      FROM(
          SELECT COUNT(*) AS Pendient
            FROM badgeperperson AS BPP
            LEFT JOIN (
              SELECT idNotification, idNewObject
                FROM notification
              WHERE idPerson = idPerson_IN
              AND idTypeNoti = getCategoryId('NotificationType','Badge')
            ) AS A
            ON BPP.idBadge = A.idNewObject
        WHERE idSession = getSessionId(idPerson_IN)
        AND IFNULL(A.idNotification,-1) < 0
        UNION ALL
        SELECT COUNT(*) AS Pendient
            FROM nutriappoint AS NA
            LEFT JOIN (
              SELECT idNotification, idNewObject
                FROM notification
              WHERE idPerson = idPerson_IN
              AND idTypeNoti = getCategoryId('NotificationType','Nutricionist')
            ) AS A
            ON NA.idNutriAppoint = A.idNewObject
        WHERE idSession = getSessionId(idPerson_IN)
        AND IFNULL(A.idNotification,-1) < 0
        UNION ALL
        SELECT COUNT(*) AS Pendient
            FROM digitalcontent AS DC
            INNER JOIN contentperperson AS CPP
            ON CPP.idCont = DC.idCont
            LEFT JOIN (
              SELECT idNotification, idNewObject
                FROM notification
              WHERE idPerson = idPerson_IN
              AND (
              idTypeNoti = getCategoryId('NotificationType','Dietas')
              OR 
              idTypeNoti = getCategoryId('NotificationType','Esfuerzo')
              ) 
            ) AS A
            ON DC.idCont = A.idNewObject
        WHERE CPP.idPerson = idPerson_IN
        AND (
        DC.contType = getCategoryId('Contenido','Dietas')
        OR 
        DC.contType = getCategoryId('Contenido','Esfuerzo')
        )
        AND IFNULL(A.idNotification,-1) < 0
      ) AS T;
      
    END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notification_add` (IN `idTypeNoti_IN` VARCHAR(25), IN `idNewObject_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN
  INSERT INTO notification
    (idTypeNoti, dateNoti, idNewObject, idPerson) 
  VALUES 
    (getCategoryId('NotificationType',idTypeNoti_IN)
    ,NOW()
    ,idNewObject_IN
    ,idPerson_IN);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_notification_get` (IN `idTypeNoti_IN` VARCHAR(25), IN `idNewObject_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN
  SELECT COUNT(*) AS Total
    FROM notification
  WHERE idTypeNoti = 
  getCategoryId('NotificationType',idTypeNoti_IN)
  AND idPerson = idPerson_IN
  AND idNewObject = idNewObject_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_add` (IN `idSession_IN` INT(11), IN `idNutricionist_IN` INT(11), IN `idPlace_IN` INT(11), IN `dt_Appointment_IN` VARCHAR(50), IN `Points_IN` INT(11), IN `idAppointType_IN` INT(11), IN `idMeasureType_IN` INT(11), IN `type_IN` INT(11), OUT `last_id` INT(11))  BEGIN

    INSERT INTO nutriappoint(
        idSession
        , idNutricionist
        , idPlace
        , dt_Appointment
        , Points
        , idAppointType
        , idMeasureType
        , type) 
    VALUES (
        idSession_IN
        , idNutricionist_IN
        , idPlace_IN
        , STR_TO_DATE(dt_Appointment_IN, '%Y-%m-%d %H:%i')
        , Points_IN
        , idAppointType_IN
        , idMeasureType_IN
        , type_IN);
    
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_assign_date` (IN `idNutriAppoint_IN` INT(11), IN `dt_Appointment_IN` VARCHAR(25))  BEGIN
  
  UPDATE nutriappoint 
      SET dt_Appointment = DATE_FORMAT(dt_Appointment_IN,'%Y-%m-%d %h:%i')
  WHERE idNutriAppoint = idNutriAppoint_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_assign_points` (IN `idNutriAppoint_IN` INT(11), IN `value_IN` VARCHAR(10))  BEGIN
  
  UPDATE nutriappoint 
      SET Points = value_IN
  WHERE idNutriAppoint = idNutriAppoint_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriAppoint_check` (IN `idPerson_IN` INT(11))  BEGIN
  SET @idDetails = NULL;
  SET @idDetails = IFNULL((
    SELECT COUNT(*)
    FROM nutriappoint
    WHERE idSession = getSessionId(idPerson_IN)
    AND DATE_FORMAT(dt_Appointment,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m'))
  ,0);

  IF (@idDetails > 0) THEN
    SELECT getCategoryId('AcumulacionDePuntos','Extraordinario') AS PointType;
  ELSEIF (@idDetails = 0) THEN
    SELECT getCategoryId('AcumulacionDePuntos','Ordinario') AS PointType;
  END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_NutriAppoint_Date` (IN `idNutriAppoint_IN` INT(11))  BEGIN
    SET lc_time_names = 'es_GT';
    SELECT 
    CONCAT(DAYNAME(dt_Appointment),', ',DATE_FORMAT(dt_Appointment,'%d de %M')) as Fecha
        FROM nutriappoint
    WHERE idNutriAppoint = idNutriAppoint_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_dates_list` (IN `dateReport_IN` VARCHAR(100), IN `idNutricionist_IN` INT(11), IN `type_IN` VARCHAR(20))  BEGIN
    
    IF STRCMP(type_IN, 'Month') = 0 THEN
  
        SELECT DISTINCT DATE_FORMAT(dt_Appointment,'%Y-%m-%d') AS Fecha
            FROM nutriappoint AS N
            INNER JOIN session AS S
            ON N.idSession = S.idSession
        WHERE N.idNutricionist = idNutricionist_IN
        #AND MONTH(dt_Appointment) = MONTH(dateReport_IN)
        AND S.active = 1
            ORDER BY dt_Appointment;
    
    ELSEIF STRCMP(type_IN, 'Day') = 0 THEN
    
        SELECT N.idNutriAppoint
            ,DATE_FORMAT(dt_Appointment,'%Y-%m-%d') AS Fecha
            ,DATE_FORMAT(dt_Appointment,'%h:%i %p') AS Hora
            ,getCategoryName(N.idPlace) as Place
            ,getPersonName(S.idPerson) AS Nombre
            ,getCategoryName(N.idAppointType) AS TipoDeCita
            ,getCategoryName(N.idMeasureType) AS TipoDeMedicion
        FROM nutriappoint AS N
        INNER JOIN session AS S
        ON N.idSession = S.idSession
            WHERE DATE_FORMAT(dt_Appointment,'%Y-%m-%d') = STR_TO_DATE(dateReport_IN,'%Y-%m-%d')
            AND N.idNutricionist = idNutricionist_IN
            AND S.active = 1
        ORDER BY dt_Appointment ASC;

    END IF;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_delete` (IN `idNutriAppoint_IN` INT(11))  BEGIN
  
  DELETE FROM nutriappoint 
  WHERE idNutriAppoint = idNutriAppoint_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_list` (IN `idNutriAppoint_IN` INT(11))  BEGIN
    
    SELECT idPlace
        ,getCategoryName(idPlace) AS namePlace
        ,DATE_FORMAT(dt_Appointment, '%H:%i') AS Hora
        ,idAppointType
        ,getCategoryName(idAppointType) AS NameAppointType
        ,idMeasureType
        ,getCategoryName(idMeasureType) AS NameMeasureType
    FROM nutriappoint
    WHERE idNutriAppoint = idNutriAppoint_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_past_details` (IN `idNutriAppoint_IN` INT(11))  BEGIN
  SELECT DNA.idDetails, DNA.data, DNA.value 
      FROM nutriappoint AS NA
      INNER JOIN detailsnutriappoint AS DNA
      ON NA.idNutriAppoint = DNA.idNutriAppoint
  WHERE NA.idNutriAppoint = idNutriAppoint_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_past_general` (IN `idNutriAppoint_IN` INT(11))  BEGIN
    SELECT getPersonName(idNutricionist) AS NutriName, 
    getCategoryName(idPlace) AS Place, 
    DATE_FORMAT(dt_Appointment,'%d/%m/%Y') AS Fecha, 
    DATE_FORMAT(dt_Appointment,'%H:%i') AS Hora, 
    Points, 
    getCategoryName(idAppointType) AS appointType, 
    getCategoryName(idMeasureType) AS measureType, 
    getCategoryName(type) AS Type
        FROM nutriappoint
    WHERE idNutriAppoint = idNutriAppoint_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_past_list` (IN `idPerson_IN` INT(11))  BEGIN
  SET lc_time_names = 'es_GT';
  
  SELECT idNutriAppoint, 
  CONCAT(DAYNAME(dt_Appointment),', ',DATE_FORMAT(dt_Appointment,'%d de %M')) as Fecha,
  idMeasureType,
  REPLACE(getCategoryName(idMeasureType),'medida ','') AS MeasureType
      FROM nutriappoint
  WHERE idSession = getSessionId(idPerson_IN)
  ORDER BY dt_Appointment;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_update` (IN `idNutriAppoint_IN` INT(11), IN `idPlace_IN` INT(11), IN `time_Appoint_IN` VARCHAR(50), IN `idAppointType_IN` INT(11), IN `idMeasureType_IN` INT(11))  BEGIN
  
  UPDATE nutriappoint 
      SET idPlace = idPlace_IN
      ,dt_Appointment = STR_TO_DATE(time_Appoint_IN, '%Y-%m-%d %H:%i')
      ,idAppointType = idAppointType_IN
      ,idMeasureType = idMeasureType_IN
  WHERE idNutriAppoint = idNutriAppoint_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_nutriappoint_update_date_points` (IN `idNutriAppoint_IN` INT(11), IN `dt_Appoint_IN` VARCHAR(25), IN `hr_Appoint_IN` VARCHAR(25), IN `points_IN` INT(11))  BEGIN
  
  UPDATE nutriappoint 
      SET dt_Appointment = 
        DATE_FORMAT(
          STR_TO_DATE(
            CONCAT(dt_Appoint_IN,' ',hr_Appoint_IN)
          ,'%d/%m/%Y %H:%i')
        ,'%Y-%m-%d %H:%i')
      ,Points = points_IN
  WHERE idNutriAppoint = idNutriAppoint_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_peopleperevent_add` (IN `idEvent_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN
      
INSERT INTO peopleperevent
(idEvent, idPerson) 
VALUES (idEvent_IN, idPerson_IN);

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_peopleperevent_remove` (IN `idEvent_IN` INT(11), IN `idPerson_IN` INT(11))  BEGIN

  DELETE FROM peopleperevent 
  WHERE idEvent = idEvent_IN 
  AND idPerson = idPerson_IN;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_add` (IN `new_name` VARCHAR(200), IN `new_nick` VARCHAR(100), IN `new_gender` INT(11), IN `new_height` VARCHAR(4), IN `new_birthday` VARCHAR(10), IN `new_phone` VARCHAR(13), IN `new_email` VARCHAR(100), IN `new_idPlace` INT(11), IN `new_userType` INT(11), OUT `last_id` INT(11))  BEGIN

  START TRANSACTION;
    
    SET @userType = NULL;
    IF new_userType < 0 THEN
      SELECT 
      getCategoryId('RolDeUsuario', 'Integrante') 
      INTO @userType;
    ELSE 
      SET @userType = new_userType;
    END IF;
    
    #INSERT
      INSERT INTO person
        (namePerson, nickName, birthday, height, gender, telephone, mail, typePerson, typeRegistration, pass) 
      VALUES (
        new_name, 
        new_nick,
        STR_TO_DATE(new_birthday,'%d/%m/%Y'),
        new_height, 
        new_gender,
        new_phone, 
        new_email,
        @userType,
        getCategoryId('EstadoDeUsuario', 'Nuevo'),
        MD5(new_email)
      );
      
    #GET ID PERSON
      SET last_id = LAST_INSERT_ID(); 

    #CREATE SESSION
    INSERT INTO session
      (idPerson, BeginDate, EndDate, idPlace, active) 
    VALUES (
      last_id
      ,NOW()
      ,DATE_ADD(NOW(), INTERVAL 6 MONTH)
      ,new_idPlace
      ,1);
  
  COMMIT;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_age` (IN `idPerson_IN` INT(11), IN `date_IN` DATE)  BEGIN
    
    SELECT P.idPerson
      ,P.namePerson
      ,P.birthday
      ,MONTH(P.birthday) as mes
      ,TIMESTAMPDIFF(YEAR, P.birthday, CURDATE()) AS age
    FROM person AS P
    INNER JOIN session AS S
    ON P.idPerson = S.idPerson
    WHERE S.idPlace = getPlace(idPerson_IN)
    AND S.active = 1;
    #AND MONTH(P.birthday) = MONTH(date_IN);

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_attendance_by_time` (IN `idUser_IN` INT(11), IN `dateBgn_IN` VARCHAR(100), IN `dateEnd_IN` VARCHAR(100), IN `timeType_IN` VARCHAR(100))  BEGIN

  SET lc_time_names = 'es_GT';

  IF STRCMP(timeType_IN, 'Range') = 0 THEN
  
      SELECT 
          DATE_FORMAT(S.BeginDate,'%Y-%m-%d') AS BeginDate
          ,DATE_FORMAT(S.EndDate,'%Y-%m-%d') AS EndDate
          #CONCAT('01-',DATE_FORMAT(S.BeginDate,'%m-%Y')) AS BeginDate
          #,CONCAT('01-',DATE_FORMAT(S.EndDate,'%m-%Y')) AS EndDate
      FROM person as P
      INNER JOIN session AS S ON P.idPerson = S.idPerson
          WHERE P.idPerson = idUser_IN
          AND S.active = 1;

  ELSEIF STRCMP(timeType_IN, 'Details') = 0 THEN

      SELECT 
      DATE_FORMAT(A.dt_Assistance,'%d/%b/%Y') AS FECHA_LIST
      ,DATE_FORMAT(A.dt_Assistance,'%Y-%m-%d') AS FECHA_CALENDAR
      ,getCategoryName(A.idClass) AS CLASE
      ,A.Points AS POINTS
      ,CASE getCategoryName(A.type) 
      WHEN 'Ordinario' THEN 'Puntos Ordinarios'
      WHEN 'Extraordinario' THEN 'Millas Extras'
      END AS TYPE
          FROM person as P
          INNER JOIN session AS S ON P.idPerson = S.idPerson
          INNER JOIN assistance as A ON S.idSession = A.idSession
      WHERE P.idPerson = idUser_IN
      AND S.active = 1
      AND DATE_FORMAT(A.dt_Assistance,'%Y-%m-%d') BETWEEN 
          DATE_FORMAT(STR_TO_DATE(dateBgn_IN,'%Y-%m-%d'),'%Y-%m-%d') AND 
          DATE_FORMAT(STR_TO_DATE(dateEnd_IN,'%Y-%m-%d'),'%Y-%m-%d')
      ORDER BY A.dt_Assistance;
  
  ELSEIF STRCMP(timeType_IN, 'Session') = 0 THEN
  
      #SESSION
      SELECT 
      getCategoryName(A.type) AS TYPE
      ,CONCAT('01-',DATE_FORMAT(A.dt_Assistance,'%m-%Y')) AS MES
      ,COUNT(MONTH(A.dt_Assistance)) AS ASISTENCIAS
          FROM person as P
          INNER JOIN session AS S ON P.idPerson = S.idPerson
          INNER JOIN assistance as A ON S.idSession = A.idSession
      WHERE P.idPerson = idUser_IN
      AND S.active = 1
      GROUP BY A.type, YEAR(A.dt_Assistance), MONTH(A.dt_Assistance);
      
  ELSEIF STRCMP(timeType_IN, 'Month') = 0 THEN
  
      #MONTH
      SET @v_counter = 1;
      SET @week_bgn = WEEK(dateBgn_IN);
      SET @week_end = WEEK(dateEnd_IN);
      
      CREATE TEMPORARY TABLE Semana (
        idWeek INT UNSIGNED NOT NULL,
        nameWeek INT UNSIGNED NOT NULL
      );
      
      WHILE @week_bgn <= @week_end DO
        INSERT INTO Semana(idWeek,nameWeek) VALUES (@week_bgn,@v_counter);
        SET @v_counter = @v_counter + 1;
        SET @week_bgn = @week_bgn + 1;
      END WHILE;
      
      SELECT 
          IFNULL(TYPE,'') AS TYPE
          ,S.nameWeek AS SEMANA
          ,IFNULL(T.ASISTENCIAS,0) AS ASISTENCIAS
      FROM Semana as S LEFT JOIN
      (
          SELECT 
          getCategoryName(A.type) AS TYPE
          ,WEEK(A.dt_Assistance) AS SEMANA
          ,COUNT(WEEK(A.dt_Assistance)) AS ASISTENCIAS
              FROM person as P
              INNER JOIN session AS S ON P.idPerson = S.idPerson
              INNER JOIN assistance as A ON S.idSession = A.idSession
          WHERE P.idPerson = idUser_IN
          AND S.active = 1
          AND YEAR(A.dt_Assistance) = YEAR(dateBgn_IN) 
          AND MONTH(A.dt_Assistance) = MONTH(dateBgn_IN)
          GROUP BY A.type,WEEK(A.dt_Assistance)
      ) AS T ON T.SEMANA = S.idWeek
      ORDER BY S.nameWeek;
      
      DROP TABLE Semana;
      
  ELSEIF STRCMP(timeType_IN, 'Week') = 0 THEN
  
      #WEEK
      SELECT 
      getCategoryName(A.type) AS TYPE
      ,dayname(A.dt_Assistance) as DIA
      ,1 as ASISTENCIAS
          FROM person as P
          INNER JOIN session AS S ON P.idPerson = S.idPerson
          INNER JOIN assistance as A ON S.idSession = A.idSession
      WHERE P.idPerson = idUser_IN
      AND S.active = 1
      AND A.dt_Assistance BETWEEN dateBgn_IN AND dateEnd_IN;
      
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_attendance_details` (IN `idUser_IN` INT(11), IN `Type_IN` VARCHAR(40), IN `Category_IN` VARCHAR(40))  BEGIN
    
  IF STRCMP(Category_IN, 'Points') = 0 THEN
      
      SELECT 
          DATE_FORMAT(A.dt_Assistance,'%Y-%m-%d') AS ASISTENCIAS
          ,A.Points AS PUNTOS
      FROM person as P
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      INNER JOIN assistance as A ON S.idSession = A.idSession
          WHERE P.idPerson = idUser_IN
          AND S.active = 1
          AND A.type = getCategoryId('AcumulacionDePuntos',Type_IN)
      ORDER BY A.dt_Assistance;
  
  ELSEIF STRCMP(Category_IN, 'Nutrition') = 0 THEN
      
      SELECT DATE_FORMAT(N.dt_Appointment,'%Y-%m-%d') AS ASISTENCIAS, N.Points as PUNTOS
          FROM person AS P
          INNER JOIN session AS S ON P.idPerson = S.idPerson
          INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
      WHERE P.idPerson = idUser_IN
      AND S.active = 1
      AND N.type = getCategoryId('AcumulacionDePuntos',Type_IN)
      AND N.idAppointType IN (
          getCategoryId('TipoDeCitaNutricional','Realizada')
          ,getCategoryId('TipoDeCitaNutricional','El mismo dia')
      )
      ORDER BY N.dt_Appointment;
  
  ELSEIF STRCMP(Category_IN, 'Both') = 0 THEN
      
      SELECT 
          DATE_FORMAT(A.dt_Assistance,'%Y-%m-%d') AS ASISTENCIAS
          ,SUM(A.Points) AS PUNTOS
      FROM person as P
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      INNER JOIN assistance as A ON S.idSession = A.idSession
          WHERE P.idPerson = idUser_IN
          AND S.active = 1
          AND A.type = getCategoryId('AcumulacionDePuntos',Type_IN)
      GROUP BY DATE_FORMAT(A.dt_Assistance,'%Y-%m-%d')
      UNION
      SELECT 
          DATE_FORMAT(N.dt_Appointment,'%Y-%m-%d') AS ASISTENCIAS
          ,SUM(N.Points) as PUNTOS
          FROM person AS P
          INNER JOIN session AS S ON P.idPerson = S.idPerson
          INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
      WHERE P.idPerson = idUser_IN
      AND S.active = 1
      AND N.type = getCategoryId('AcumulacionDePuntos',Type_IN)
      AND N.idAppointType IN (
          getCategoryId('TipoDeCitaNutricional','Realizada')
          ,getCategoryId('TipoDeCitaNutricional','El mismo dia')
      )
      GROUP BY DATE_FORMAT(N.dt_Appointment,'%Y-%m-%d')
      ORDER BY ASISTENCIAS;
      
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_calculate_positions` (IN `dateReport_IN` VARCHAR(100), IN `idPlace_IN` INT(11), IN `typePoints_IN` VARCHAR(40))  BEGIN

    SET @prev_value = NULL;
    SET @rank_count = 0;
    
    #INSERT INTO sessionresult
    #    (dt_Result, idPlace, position, idSession, idPerson, namePerson, Points, Picture) 
    SELECT  
        STR_TO_DATE(CONCAT(dateReport_IN,' ',DATE_FORMAT(NOW(),'%H:%i:%s')), '%Y-%m-%d %H:%i:%s') AS DATE,
        idPlace_IN AS IDPLACE,
        CASE
            WHEN @prev_value = PUNTOS THEN @rank_count
            WHEN @prev_value := PUNTOS THEN @rank_count := @rank_count + 1
        END AS RANK
        ,SESSION
        ,ID
        ,NOMBRE
        ,PUNTOS
        ,PIC
    FROM (
      
        SELECT idSession AS SESSION
            ,idPerson AS ID
            ,namePerson AS NOMBRE
            ,profilePic AS PIC
            ,SUM(Puntos) AS PUNTOS
        FROM (
            #---------------------------------------------------------
            #points for assistance
            #---------------------------------------------------------
            SELECT S.idSession
                ,P.idPerson
                ,P.namePerson
                ,CONCAT(PPP.idPicPerPerson,'.',PPP.extension) as profilePic
                ,SUM(A.Points) AS Puntos
            FROM person as P
            INNER JOIN session AS S ON P.idPerson = S.idPerson
            INNER JOIN assistance as A ON S.idSession = A.idSession
            INNER JOIN picsperperson as PPP ON PPP.idPerson = S.idPerson
                WHERE S.active = 1
                AND PPP.active = 1
                AND S.idPlace = idPlace_IN
                AND A.type = getCategoryId('AcumulacionDePuntos',typePoints_IN)
                AND YEAR(A.dt_Assistance) = YEAR(dateReport_IN)
                AND MONTH(A.dt_Assistance) = MONTH(dateReport_IN)
            GROUP BY P.idPerson
            #---------------------------------------------------------
            UNION
            #---------------------------------------------------------
            #points for nutrition dates      
            #---------------------------------------------------------
            SELECT S.idSession
                ,P.idPerson
                ,P.namePerson
                ,CONCAT(PPP.idPicPerPerson,'.',PPP.extension) as profilePic
                ,SUM(N.Points) AS Puntos
            FROM person as P
            INNER JOIN session AS S ON P.idPerson = S.idPerson
            INNER JOIN nutriappoint as N ON S.idSession = N.idSession
            INNER JOIN picsperperson as PPP ON PPP.idPerson = S.idPerson
                WHERE S.active = 1
                AND PPP.active = 1
                AND S.idPlace = idPlace_IN
                AND N.type = getCategoryId('AcumulacionDePuntos',typePoints_IN)
                AND YEAR(N.dt_Appointment) = YEAR(dateReport_IN)
                AND MONTH(N.dt_Appointment) = MONTH(dateReport_IN)
            GROUP BY P.idPerson
            #---------------------------------------------------------
        ) AS T
        GROUP BY idPerson,profilePic
        ORDER BY Puntos DESC
        
    ) AS BT
    ORDER BY NOMBRE;
      
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_calendar_list` (IN `idPerson_IN` INT(11), IN `date_IN` DATE, IN `type_IN` VARCHAR(100))  BEGIN
    SET lc_time_names = 'es_GT';
      
    IF STRCMP(type_IN, 'General') = 0 THEN

        SELECT titleEvent AS Nombre
        ,DATE_FORMAT(dateEvent,'%Y-%m-%d') AS Fecha
            FROM event
        UNION /***********/
        SELECT 
            'CitaNutricional' AS Nombre
            ,DATE_FORMAT(N.dt_Appointment,'%Y-%m-%d') AS Fecha 
        FROM nutriappoint AS N
        INNER JOIN session AS S ON S.idSession = N.idSession
        INNER JOIN person AS P ON P.idPerson = S.idPerson
            WHERE P.idPerson = idPerson_IN
            AND S.active = 1
            AND N.idAppointType = getCategoryId('TipoDeCitaNutricional','Solicitada')
        UNION /***********/
        SELECT 'Cumpleanos' AS Nombre
          ,CONCAT(YEAR(NOW()),'-',DATE_FORMAT(P.birthday,'%m-%d')) AS Birthday 
        FROM person AS P
        INNER JOIN session AS S
        ON P.idPerson = S.idPerson
        WHERE S.idPlace = getPlace(idPerson_IN)
        AND S.active = 1;
           
    ELSEIF STRCMP(type_IN, 'Details') = 0 THEN
    
        SELECT titleEvent AS Nombre
            ,DATE_FORMAT(dateEvent,'%M') AS Mes
            ,DATE_FORMAT(dateEvent,'%Y') AS Anio
            ,DATE_FORMAT(dateEvent,'%W') AS DiaNombre
            ,DATE_FORMAT(dateEvent,'%d') AS Dia
            ,DATE_FORMAT(dateEvent,'%h:%i %p') AS Hora
            ,placeEvent AS Lugar
        FROM event
        WHERE DATE_FORMAT(dateEvent,'%Y-%m-%d') = date_IN
        UNION /***********/
        SELECT 
            'Cita Nutricional' AS Nombre
            ,DATE_FORMAT(N.dt_Appointment,'%M') AS Mes
            ,DATE_FORMAT(N.dt_Appointment,'%Y') AS Anio
            ,DATE_FORMAT(N.dt_Appointment,'%W') AS DiaNombre
            ,DATE_FORMAT(N.dt_Appointment,'%d') AS Dia
            ,DATE_FORMAT(N.dt_Appointment,'%h:%i %p') AS Hora
            ,getCategoryName(N.idPlace) AS Lugar
        FROM nutriappoint AS N
        INNER JOIN session AS S ON S.idSession = N.idSession
        INNER JOIN person AS P ON P.idPerson = S.idPerson
            WHERE P.idPerson = idPerson_IN
            AND S.active = 1
            AND DATE_FORMAT(N.dt_Appointment,'%Y-%m-%d') = date_IN
            AND N.idAppointType = getCategoryId('TipoDeCitaNutricional','Solicitada')
        UNION /***********/
        SELECT P.namePerson AS Nombre
            ,DATE_FORMAT(P.birthday,'%M') AS Mes
            ,YEAR(NOW()) AS Anio
            ,DATE_FORMAT(P.birthday,'%W') AS DiaNombre
            ,DATE_FORMAT(P.birthday,'%d') AS Dia
            ,'Todo el dia' AS Hora
            ,'Cumpleanos' AS Lugar
        FROM person AS P
        INNER JOIN session AS S
        ON P.idPerson = S.idPerson
        WHERE S.idPlace = getPlace(idPerson_IN)
        AND S.active = 1
        AND CONCAT(YEAR(NOW()),'-',DATE_FORMAT(P.birthday,'%m-%d')) = date_IN;
            
    END IF;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_datesNumber_per_month` (IN `idSession_IN` INT(11), IN `dt_Appointment_IN` VARCHAR(20))  BEGIN
  
  SELECT COUNT(type) AS Number
    FROM nutriappoint
  WHERE idSession = idSession_IN
  AND MONTH(dt_Appointment) = MONTH(dt_Appointment_IN);

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_delete` (IN `idPerson_IN` INT(11), IN `reason_IN` VARCHAR(200))  BEGIN
  
  UPDATE session 
      SET active = '0',
      reason = reason_IN
  WHERE idPerson = idPerson_IN
  AND active = 1;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_getAll` ()  BEGIN

    SELECT DISTINCT P.idPerson
        FROM person AS P
        INNER JOIN session as S
        ON P.idPerson = S.idPerson
    WHERE S.active = 1;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_list` (IN `idPerson_IN` INT(11))  BEGIN

    SELECT P.idPerson
        ,P.namePerson AS Nombre
        ,DATE_FORMAT(P.birthday,'%d/%m/%Y') AS 'Fecha de Nac.'
        ,TIMESTAMPDIFF(YEAR,P.birthday,NOW()) AS Edad
        ,P.height as Altura
        ,getCategoryName(P.gender) as Genero
        ,P.telephone as Telefono
        ,P.mail as Correo
        ,CONCAT(PPP.idPicPerPerson,'.',PPP.extension) as Foto
        ,S.idSession as Temporada
        ,getCategoryName(S.idPlace) as Sede
    FROM person AS P
      INNER JOIN session as S ON P.idPerson = S.idPerson
      INNER JOIN picsperperson as PPP ON PPP.idPerson = S.idPerson
    WHERE P.idPerson = idPerson_IN
    AND S.active = 1
    AND PPP.active = 1;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_monthly_rank` (IN `dateReport_IN` VARCHAR(100), IN `idPlace_IN` INT(11))  BEGIN

    SELECT position
    ,idSession
    ,idPerson
    ,namePerson
    ,Points
    ,Picture
        FROM sessionresult
    WHERE idPlace = idPlace_IN
        AND YEAR(dt_Result) = YEAR(dateReport_IN)
        AND MONTH(dt_Result) = MONTH(dateReport_IN)
    ORDER BY position
    LIMIT 10;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_nutriAppointNumber_per_month` (IN `idSession_IN` INT(11), IN `dt_Appointment_IN` VARCHAR(20))  BEGIN
  
  SELECT COUNT(type) AS Number
    FROM nutriappoint
  WHERE idSession = idSession_IN
  AND MONTH(dt_Appointment) = MONTH(dt_Appointment_IN);

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_nutrition_list` (IN `idUser_IN` INT(11), IN `Indicador_IN` INT(11), IN `type_IN` VARCHAR(50))  BEGIN

  SET lc_time_names = 'es_GT';

  IF STRCMP(type_IN, 'Details') = 0 THEN
  
      SELECT getCategoryName(D.data) as NameIndicator
          ,DATE_FORMAT(N.dt_Appointment,'%d-%b') AS Tiempo
          ,D.value AS Valor
      FROM person AS P
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
      INNER JOIN detailsnutriappoint AS D ON N.idNutriAppoint = D.idNutriAppoint
          WHERE P.idPerson = idUser_IN
          AND S.active = 1
          AND D.data = Indicador_IN
          AND N.idMeasureType = getCategoryid('TiposDeMedicionEnCitaNutricional','medida actual')
      #GROUP BY DATE_FORMAT(N.dt_Appointment,'%b')          
      ORDER BY N.dt_Appointment;
  
  ELSEIF STRCMP(type_IN, 'Ranges') = 0 THEN      
      
      SELECT IVR.valueMin
      ,IVR.valueMax
      ,getCategoryName(IVR.Color) AS Color
          FROM indicatorvaluerange AS IVR
          INNER JOIN indicatorinformation AS II
          ON IVR.idIndicatorInfo = II.idIndicatorInformation
      WHERE II.idIndicador = Indicador_IN
      AND II.gender = getPersonGender(idUser_IN)
      AND getPersonAge(idUser_IN) BETWEEN II.ageBegin AND II.ageEnd
      AND IVR.idCatalog != getCategoryId('IndicatorTable','Valid');
      
  ELSEIF STRCMP(type_IN, 'Status') = 0 THEN

      SELECT 
          DATE_FORMAT(N.dt_Appointment,'%d-%M-%Y') AS FirstDate
          ,DATE_FORMAT(S.EndDate,'%d-%M-%Y') AS GoalDate
          ,getCategoryName(N.idMeasureType) AS Lectura
          ,D.value AS Valor
      FROM person AS P
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
      INNER JOIN detailsnutriappoint AS D ON N.idNutriAppoint = D.idNutriAppoint
          WHERE P.idPerson = idUser_IN
          AND S.active = 1
          AND D.data = Indicador_IN
          AND N.idMeasureType IN (
              getCategoryid('TiposDeMedicionEnCitaNutricional','medida inicial')
              ,getCategoryid('TiposDeMedicionEnCitaNutricional','medida meta'))
      ORDER BY N.dt_Appointment;
  
  ELSEIF STRCMP(type_IN, 'Last') = 0 THEN
      
      SELECT DATE_FORMAT(N.dt_Appointment,'%d-%M-%Y') AS Tiempo
          ,D.value AS Valor
      FROM person AS P
      INNER JOIN session AS S ON P.idPerson = S.idPerson
      INNER JOIN nutriappoint AS N ON S.idSession = N.idSession
      INNER JOIN detailsnutriappoint AS D ON N.idNutriAppoint = D.idNutriAppoint
          WHERE P.idPerson = idUser_IN
          AND S.active = 1
          AND D.data = Indicador_IN
          AND N.idMeasureType = getCategoryid('TiposDeMedicionEnCitaNutricional','medida actual')
      ORDER BY N.dt_Appointment DESC
      LIMIT 1;
      
  ELSEIF STRCMP(type_IN, 'UDM') = 0 THEN
      
      SELECT getCategoryName(Indicador_IN) as nameIndicador
            ,'UDM' as UDM;
  
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_picture` (IN `idPerson_IN` INT(11), IN `type_IN` VARCHAR(25))  BEGIN
    
  IF STRCMP(type_IN, 'ALL') = 0 THEN
  
    SELECT CONCAT(PPP.idPicPerPerson,'.',PPP.extension) AS url, PPP.active, PPP.DatePic
        FROM person AS P
        INNER JOIN picsperperson AS PPP
        ON P.idPerson = PPP.idPerson
    WHERE P.idPerson = idPerson_IN
    ORDER BY PPP.active DESC;
    
  ELSEIF STRCMP(type_IN, 'ACTIVE') = 0 THEN
  
    SELECT CONCAT(PPP.idPicPerPerson,'.',PPP.extension) AS url
        FROM person AS P
        INNER JOIN picsperperson AS PPP
        ON P.idPerson = PPP.idPerson
    WHERE P.idPerson = idPerson_IN
    AND PPP.active = 1;
    
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_session` (IN `idPerson_IN` INT(11))  BEGIN
  
  SELECT S.idSession
    FROM person P 
    INNER JOIN session AS S 
    ON P.idPerson = S.idPerson
  WHERE P.idPerson = idPerson_IN
  AND S.active = 1;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_person_update` (IN `idPerson` INT(11), IN `column_name` VARCHAR(15), IN `texto` VARCHAR(200))  BEGIN
  
  #begin
  SET @QUERY = CONCAT(
    'UPDATE Person', 
    ' SET ', column_name, ' = ');
    
  IF STRCMP(column_name, 'birthday') = 0 THEN
    SET @QUERY = CONCAT(@QUERY, 'DATE(STR_TO_DATE(''',texto,''',''%d/%m/%Y''))');
  ELSEIF STRCMP(column_name, 'height') = 0 THEN
    SET @QUERY = CONCAT(@QUERY, 'CAST(''',texto,''' AS DOUBLE(3,2))'); 
  ELSE
    SET @QUERY = CONCAT(@QUERY,'''',texto,'''');
  END IF;    
  
  SET @QUERY = CONCAT(@QUERY,' WHERE idPerson = ', idPerson);
  
  #SELECT @QUERY;
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_phrase_add` (IN `titlePhrase_IN` VARCHAR(20), IN `textPhrase_IN` VARCHAR(200), IN `idCategory_IN` INT(11), IN `beginDate_IN` VARCHAR(10), IN `endDate_IN` VARCHAR(10), OUT `last_id` INT(11))  BEGIN
    INSERT INTO phrases
        (titlePhrase
        ,textPhrase
        ,idCategory
        ,beginDate
        ,endDate) 
    VALUES 
        (titlePhrase_IN
        ,textPhrase_IN
        ,idCategory_IN
        ,DATE_FORMAT(STR_TO_DATE(CONCAT(beginDate_IN,' 5:00 PM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i')
        ,DATE_FORMAT(STR_TO_DATE(CONCAT(endDate_IN,' 4:59 AM'),'%d/%m/%Y %h:%i %p'),'%Y-%m-%d %H:%i')
    );
    
    SET last_id = LAST_INSERT_ID(); 
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_phrase_list` (IN `type_IN` VARCHAR(50), IN `contType_IN` VARCHAR(50), IN `idPhrase_IN` INT(11))  BEGIN

  IF STRCMP(type_IN, 'List') = 0 THEN
    
    SELECT idPhrase, titlePhrase, textPhrase
      FROM phrases
    WHERE idCategory = contType_IN;

  ELSEIF STRCMP(type_IN, 'Specific') = 0 THEN
    
    SELECT idPhrase, 
    titlePhrase, 
    textPhrase,
    idCategory,
    getCategoryName(idCategory) AS CategoryName
      FROM phrases
    WHERE idPhrase = idPhrase_IN;
  
  ELSEIF STRCMP(type_IN, 'Random') = 0 THEN
    
    SELECT idPhrase
      FROM phrases;
 
  ELSEIF STRCMP(type_IN, 'Today') = 0 THEN
  
    SELECT titlePhrase, textPhrase, beginDate, endDate 
      FROM phrases
    WHERE NOW() BETWEEN beginDate AND endDate;
      
  END IF;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_picsperperson_add` (IN `idPerson_IN` INT(11), IN `extension_IN` VARCHAR(3), OUT `last_id` INT(11))  BEGIN

  UPDATE picsperperson
  SET active = 0
  WHERE idPerson = idPerson_IN;
  
  INSERT INTO picsperperson
    (idPerson, DatePic, extension, active) 
  VALUES (idPerson_IN, NOW(), extension_IN, 1);
  
  SET last_id = LAST_INSERT_ID(); 

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_professional_attendance` (`date_IN` VARCHAR(10), `idPro_IN` INT(11), `typePro_IN` VARCHAR(20))  BEGIN
    IF STRCMP(typePro_IN, 'Entrenador') = 0 THEN
        SELECT 
        DATE_FORMAT(dt_Assistance,'%b-%d') AS Fecha,
        DATE_FORMAT(dt_Assistance,'%H') AS Hora,
        COUNT(dt_Assistance) AS Asistencia
          FROM assistance AS A
        WHERE idCoach = idPro_IN
        AND MONTH(dt_Assistance) = MONTH(date_IN)
        GROUP BY DATE_FORMAT(dt_Assistance,'%b-%d %H')
        ORDER BY dt_Assistance;
    ELSEIF STRCMP(typePro_IN, 'Nutricionista') = 0 THEN
        SELECT 
        DATE_FORMAT(dt_Appointment,'%b-%d') AS Fecha,
        DATE_FORMAT(dt_Appointment,'%H') AS Hora,
        getCategoryName(idAppointType) AS AppointType
        FROM nutriappoint AS A
        WHERE idNutricionist = idPro_IN
        AND MONTH(dt_Appointment) = MONTH(date_IN)
        ORDER BY dt_Appointment;
    END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_professional_graph_monthly` (`date_IN` VARCHAR(10), `idPro_IN` INT(11), `typePro_IN` VARCHAR(20))  BEGIN
  IF STRCMP(typePro_IN, 'Entrenador') = 0 THEN
      
      SELECT 
      DATE_FORMAT(dt_Assistance,'%b-%d') AS Fecha,
      COUNT(dt_Assistance) AS Asistencia
        FROM assistance AS A
      WHERE idCoach = idPro_IN
      AND MONTH(dt_Assistance) = MONTH(date_IN)
      GROUP BY DATE_FORMAT(dt_Assistance,'%b-%d');
      
  ELSEIF STRCMP(typePro_IN, 'Nutricionista') = 0 THEN
  
      SELECT nameCatalog as TipoDeCita, IFNULL(Citas,0) as Citas
      FROM (
          SELECT idCatalog, nameCatalog
              FROM catalog
          WHERE idMeta = getMetaId('TipoDeCitaNutricional')
      ) AS N
      LEFT JOIN 
      (
        SELECT idAppointType,COUNT(idNutriAppoint) AS Citas
        FROM nutriappoint AS A
        WHERE MONTH(dt_Appointment) = MONTH(date_IN)
        AND A.idNutricionist = idPro_IN
        GROUP BY idNutricionist,idAppointType
      ) AS A
      ON N.idCatalog = A.idAppointType;
      
  END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_professional_list` (`typeRol_IN` VARCHAR(25))  BEGIN

    SELECT idPerson, namePerson
        FROM person
    WHERE typePerson = getCategoryId('RolDeUsuario',typeRol_IN)
    AND typeRegistration != getCategoryId('EstadoDeUsuario','De Baja');

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_professional_performance` (`date_IN` VARCHAR(10), `typePro_IN` VARCHAR(20))  BEGIN
  IF STRCMP(typePro_IN, 'Entrenador') = 0 THEN
      SELECT 
      P.idPerson
      ,P.namePerson
      ,IFNULL(Horas,0) AS Horas
      ,IFNULL(Asistencia,0) AS Asistencias
      ,IFNULL(CAST(Asistencia/Horas AS DECIMAL(5,2)),0) AS AsistenciaPorClase
      ,getRatePro(P.idPerson,date_IN) AS Rate
      FROM (
          SELECT idPerson, namePerson
              FROM person
          WHERE typePerson = getCategoryId('RolDeUsuario','Entrenador')
          AND typeRegistration != getCategoryId('EstadoDeUsuario','De Baja')
        ) AS P
      LEFT JOIN 
        (
          SELECT idCoach,
          COUNT(DISTINCT DATE_FORMAT(dt_Assistance,'%b-%d %H')) AS Horas,
          COUNT(idAssistance) as Asistencia
              FROM assistance AS A
          WHERE MONTH(dt_Assistance) = MONTH(date_IN)
          GROUP BY idCoach
        ) AS A
      ON P.idPerson = A.idCoach;
  ELSEIF STRCMP(typePro_IN, 'Nutricionista') = 0 THEN
      SELECT 
      N.idPerson,
      N.namePerson,
      IFNULL(GROUP_CONCAT(if(idAppointType = getCategoryId('TipoDeCitaNutricional', 'Solicitada'), Citas, NULL)),0) AS 'Solicitada',  
      IFNULL(GROUP_CONCAT(if(idAppointType = getCategoryId('TipoDeCitaNutricional', 'El mismo dia'), Citas, NULL)),0) AS 'ElMismoDia',  
      IFNULL(GROUP_CONCAT(if(idAppointType = getCategoryId('TipoDeCitaNutricional', 'No se presento'), Citas, NULL)),0) AS 'NoSePresento',  
      IFNULL(GROUP_CONCAT(if(idAppointType = getCategoryId('TipoDeCitaNutricional', 'Realizada'), Citas, NULL)),0) AS 'Realizada', 
      getRatePro(idPerson,date_IN) AS Rate
      FROM (
          SELECT idPerson, namePerson
              FROM person
          WHERE typePerson = getCategoryId('RolDeUsuario','Nutricionista')
          AND typeRegistration != getCategoryId('EstadoDeUsuario','De Baja')
      ) AS N
      LEFT JOIN 
      (
        SELECT idNutricionist,idAppointType,COUNT(idNutriAppoint) AS Citas
        FROM nutriappoint AS A
        WHERE MONTH(dt_Appointment) = MONTH(date_IN)
        GROUP BY idNutricionist,idAppointType
      ) AS A
      ON N.idPerson = A.idNutricionist
      GROUP BY N.idPerson,N.namePerson;
  END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_professional_statistics` (IN `idPerson_IN` INT(11), IN `dateBgn_IN` VARCHAR(25), IN `dateEnd_IN` VARCHAR(25), IN `type_date_IN` VARCHAR(25), IN `type_pro_IN` VARCHAR(25))  BEGIN
  
  IF STRCMP(type_pro_IN, 'Entrenador') = 0 THEN
  
    SET @QUERY = CONCAT(
      ' SELECT A.nameCatalog AS Clase,',
      ' IFNULL(B.Total,0) AS Total',
      ' FROM ',
      ' (',
        ' SELECT idCatalog,nameCatalog',
          ' FROM catalog',
        ' WHERE idMeta = getMetaId(''NombreDeClase'')',
      ' ) AS A',
      ' LEFT JOIN',
      ' (',
        ' SELECT A.idClass AS Clase,',
        ' COUNT(A.idClass) AS Total',
          ' FROM assistance AS A',
        ' WHERE A.idCoach = ',idPerson_IN);
        
    CASE type_date_IN
      WHEN 'Dia' THEN 
        SET @QUERY = CONCAT(@QUERY,
        ' AND DATE_FORMAT(dt_Assistance,''%Y/%m/%d'')', 
        ' = DATE_FORMAT(STR_TO_DATE(''',dateBgn_IN,''',''%Y/%m/%d''),''%Y/%m/%d'')'); 
      WHEN 'Mes' THEN
        SET @QUERY = CONCAT(@QUERY,
        ' AND MONTH(dt_Assistance)', 
        ' = MONTH(STR_TO_DATE(''',dateBgn_IN,''',''%Y/%m/%d''))');  
      WHEN 'Temporada' THEN
        SET @QUERY = CONCAT(@QUERY,
        ' AND DATE_FORMAT(dt_Assistance,''%Y/%m/%d'')', 
        ' BETWEEN DATE_FORMAT(STR_TO_DATE(''',dateBgn_IN,''',''%Y/%m/%d''),''%Y/%m/%d'')', 
        ' AND DATE_FORMAT(STR_TO_DATE(''',dateEnd_IN,''',''%Y/%m/%d''),''%Y/%m/%d'')');
    END CASE;
    
    SET @QUERY = CONCAT(@QUERY,
        ' GROUP BY A.idClass',
      ' ) AS B',
      ' ON A.idCatalog = B.Clase;');
      
  ELSEIF STRCMP(type_pro_IN, 'Nutricionista') = 0 THEN
  
    SET @QUERY = CONCAT(
      ' SELECT A.nameCatalog AS Cita,',
      ' IFNULL(B.Total,0) AS Total',
      ' FROM ',
      '(',
        ' SELECT idCatalog,nameCatalog',
          ' FROM catalog',
        ' WHERE idMeta = getMetaId(''TipoDeCitaNutricional'')',
      ') AS A',
      ' LEFT JOIN',
      '(',
        ' SELECT NA.idAppointType AS Cita,',
        ' COUNT(NA.idAppointType) AS Total',
          ' FROM nutriappoint AS NA',
        ' WHERE NA.idNutricionist = ',idPerson_IN);
    
    CASE type_date_IN
      WHEN 'Dia' THEN 
        SET @QUERY = CONCAT(@QUERY,
        ' AND DATE_FORMAT(NA.dt_Appointment,''%Y/%m/%d'')', 
        ' = DATE_FORMAT(STR_TO_DATE(''',dateBgn_IN,''',''%Y/%m/%d''),''%Y/%m/%d'')'); 
      WHEN 'Mes' THEN
        SET @QUERY = CONCAT(@QUERY,
        ' AND MONTH(NA.dt_Appointment)', 
        ' = MONTH(STR_TO_DATE(''',dateBgn_IN,''',''%Y/%m/%d''))');  
      WHEN 'Temporada' THEN
        SET @QUERY = CONCAT(@QUERY,
        ' AND DATE_FORMAT(NA.dt_Appointment,''%Y/%m/%d'')', 
        ' BETWEEN DATE_FORMAT(STR_TO_DATE(''',dateBgn_IN,''',''%Y/%m/%d''),''%Y/%m/%d'')', 
        ' AND DATE_FORMAT(STR_TO_DATE(''',dateEnd_IN,''',''%Y/%m/%d''),''%Y/%m/%d'')');
    END CASE;
  
    SET @QUERY = CONCAT(@QUERY,
        ' GROUP BY NA.idAppointType',
      ') AS B',
      ' ON A.idCatalog = B.Cita;');
    END IF;
  
  #SELECT @QUERY;
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_rateteacher_add` (IN `idTeacher_IN` INT(11), IN `idPerson_IN` INT(11), IN `idValue_IN` INT(11), IN `Note_IN` VARCHAR(100))  BEGIN
    
  INSERT INTO rateteacher(
      idTeacher
      ,idPerson
      ,idValue
      ,Note
      ,dt_Rate) 
  VALUES (
      idTeacher_IN
      ,idPerson_IN
      ,idValue_IN
      ,Note_IN
      ,NOW());

END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_sales_age_gender_by_place` (IN `idPlace_IN` INT(11))  BEGIN

  SELECT P.idPerson
  ,P.namePerson
  ,getCategoryName(P.gender) AS Genero
  ,TIMESTAMPDIFF(YEAR, P.birthday, CURDATE()) AS Edad
    FROM person AS P
    INNER JOIN session AS S
    ON P.idPerson = S.idPerson
  WHERE S.active = 1
  AND S.idPlace = idPlace_IN;
      
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_sales_camp_by_place` (IN `idPlace_IN` INT(11))  BEGIN

  SELECT idPerson, namePerson, Puntos,
  CASE 
    WHEN Puntos <= 2000  THEN '1'
    WHEN Puntos > 2000 AND Puntos <= 4000 THEN '2'
    WHEN Puntos > 4000 AND Puntos <= 6000 THEN '3'
    WHEN Puntos > 6000 AND Puntos <= 8000 THEN '4'
    ELSE '0' 
  END AS Campo
  FROM (
    SELECT idPerson, namePerson, SUM(Puntos) AS Puntos 
    FROM (
      #---------------------------------------------------------
      #points for normal attendance
      #---------------------------------------------------------
      SELECT P.idPerson,P.namePerson,SUM(A.Points) AS Puntos
        FROM person as P
        INNER JOIN session AS S ON P.idPerson = S.idPerson
        INNER JOIN assistance as A ON S.idSession = A.idSession
      WHERE S.active = 1
      AND S.idPlace = idPlace_IN
      AND A.type = getCategoryId('AcumulacionDePuntos','Ordinario')
        GROUP BY P.idPerson,P.namePerson
      UNION
      #---------------------------------------------------------
      #points for normal nutrition dates      
      #---------------------------------------------------------
      SELECT P.idPerson,P.namePerson,SUM(N.Points) AS Puntos
        FROM person as P
        INNER JOIN session AS S ON P.idPerson = S.idPerson
        INNER JOIN nutriappoint as N ON S.idSession = N.idSession
      WHERE S.active = 1
      AND S.idPlace = idPlace_IN
      AND N.type = getCategoryId('AcumulacionDePuntos','Ordinario')
        GROUP BY P.idPerson,P.namePerson
    ) AS T
    GROUP BY idPerson, namePerson
  ) AS BT
  ORDER BY Campo;
      
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_sales_goal_by_place` (IN `idPlace_IN` INT(11), IN `type_measurement_IN` VARCHAR(100))  BEGIN

  SELECT P.idPerson
  ,P.namePerson
  ,NA.dt_Appointment
  ,'Inicial' AS Medicion
  ,ND.data AS idData
  ,getCategoryName(ND.data) AS Indicador
  ,ND.value
    FROM person AS P
    INNER JOIN session AS S ON P.idPerson = S.idPerson
    INNER JOIN nutriappoint AS NA ON S.idSession = NA.idSession
    INNER JOIN detailsnutriappoint AS ND ON NA.idNutriAppoint = ND.idNutriAppoint
  WHERE S.active = 1
  AND S.idPlace = idPlace_IN
  AND NA.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional',type_measurement_IN)
  AND ND.data IN ( 
    getCategoryId('Indicadores','Peso')
    ,getCategoryId('Indicadores','Porcentaje De Grasa Corporal')
  )
  ORDER BY P.idPerson,ND.data,Medicion;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_sales_indicators_by_place` (IN `idPlace_IN` INT(11))  BEGIN

  SELECT P.idPerson
  ,P.namePerson
  ,ND.data AS idData
  ,getCategoryName(ND.data) AS nameIndicador
  ,ND.value AS valueIndicador
  ,DATE_FORMAT(NA.dt_Appointment,'%d-%m-%Y') AS Fecha
    FROM person AS P
    INNER JOIN session AS S ON P.idPerson = S.idPerson
    INNER JOIN nutriappoint AS NA ON S.idSession = NA.idSession
    INNER JOIN detailsnutriappoint AS ND ON NA.idNutriAppoint = ND.idNutriAppoint
  WHERE S.active = 1
  AND S.idPlace = idPlace_IN
  AND NA.idMeasureType = getCategoryId('TiposDeMedicionEnCitaNutricional','medida actual')
  AND ND.data IN ( 
    getCategoryId('Indicadores','Peso')
    ,getCategoryId('Indicadores','Porcentaje De Grasa Corporal')
  )
  ORDER BY P.idPerson, ND.data, NA.dt_Appointment;
      
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `sp_user_login` (IN `mail_IN` VARCHAR(100), IN `pass_IN` VARCHAR(32))  BEGIN
    
    DECLARE Type VARCHAR(20);
    DECLARE Id INT(11);
    DECLARE Place INT(11) DEFAULT 0;
  
    SELECT getCategoryName(typePerson) AS typePerson, idPerson
    INTO Type,Id
      FROM person
    WHERE mail = mail_IN
      AND pass = pass_IN;
    
    IF STRCMP(Type, 'Integrante') = 0 THEN
      SELECT idPlace INTO Place
        FROM session
      WHERE idPerson = Id
        AND active = 1;      
    END IF;
    
    SELECT Type,Id,Place;
  
END$$

--
-- Funciones
--
CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getCategoryId` (`metaCategory_IN` VARCHAR(100), `category_IN` VARCHAR(100)) RETURNS INT(11) BEGIN
  
  SET @idObj = NULL;
  
  SELECT C.idCatalog INTO @idObj
      FROM metacatalog AS MC
      INNER JOIN catalog AS C
      ON MC.idMeta = C.idMeta
  WHERE MC.nameMeta = metaCategory_IN
  AND C.nameCatalog = category_IN;
  
	RETURN @idObj;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getCategoryName` (`category_IN` INT(11)) RETURNS VARCHAR(100) CHARSET latin1 BEGIN
  
  SET @nameObj = NULL;
  
  SELECT nameCatalog INTO @nameObj
      FROM catalog AS C
  WHERE idCatalog = category_IN;
  
	RETURN @nameObj;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getMetaId` (`metaName_IN` VARCHAR(100)) RETURNS INT(11) BEGIN
  
  SET @idMeta = NULL;
  
  SELECT MC.idMeta INTO @idMeta
      FROM metacatalog AS MC
  WHERE MC.nameMeta = metaName_IN;
  
	RETURN @idMeta;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPersonAge` (`idPerson_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @edad = NULL;
  
  SELECT TIMESTAMPDIFF(YEAR, P.birthday, CURDATE()) INTO @edad
    FROM person AS P
    INNER JOIN session AS S
    ON P.idPerson = S.idPerson
  WHERE S.idPerson = idPerson_IN
  AND S.active = 1;
  
	RETURN @edad;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPersonGender` (`idPerson_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @gender = NULL;
  
  SELECT P.gender INTO @gender
    FROM person AS P
    INNER JOIN session AS S
    ON P.idPerson = S.idPerson
  WHERE S.idPerson = idPerson_IN
  AND S.active = 1;
  
	RETURN @gender;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPersonId` (`idSession_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @idPerson = NULL;
  
  SELECT S.idPerson INTO @idPerson
      FROM session AS S
  WHERE S.idSession = idSession_IN
  AND S.active = 1;
  
	RETURN @idPerson;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPersonName` (`idPerson_IN` INT(11)) RETURNS VARCHAR(100) CHARSET latin1 BEGIN
  
  SET @namePerson = NULL;
  
  SELECT P.namePerson INTO @namePerson
      FROM person AS P
  WHERE P.idPerson = idPerson_IN;
  
	RETURN @namePerson;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPlace` (`idPerson_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @idPlace = NULL;
  
  SELECT S.idPlace INTO @idPlace
    FROM person AS P
    INNER JOIN session AS S
    ON P.idPerson = S.idPerson
  WHERE P.idPerson = idPerson_IN
  AND S.active = 1;
  
	RETURN @idPlace;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPlaceId` (`idPerson_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @idPlace = NULL;
  
  SELECT S.idPlace INTO @idPlace
      FROM session AS S
  WHERE S.idPerson = idPerson_IN
  AND S.active = 1;
  
	RETURN @idPlace;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getRatePro` (`idPerson_IN` INT(11), `Date_IN` VARCHAR(10)) RETURNS DECIMAL(3,2) BEGIN
  
  SET @rate = NULL;
  
  SELECT IFNULL(AVG(idValue),-1) INTO @rate
    FROM ratepro
  WHERE idPro = idPerson_IN
  AND MONTH(dt_Rate) = MONTH(Date_IN);
  
	RETURN @rate;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getSessionId` (`idPerson_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @idSession = NULL;
  
  SELECT S.idSession INTO @idSession
      FROM session AS S
  WHERE S.idPerson = idPerson_IN
  AND S.active = 1;
  
	RETURN @idSession;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getSessionNumber` (`idPerson_IN` INT(11)) RETURNS INT(11) BEGIN
  
  SET @NoSession = NULL;
  
  SELECT COUNT(S.idSession) INTO @NoSession
      FROM session AS S
  WHERE S.idPerson = idPerson_IN;
  
	RETURN @NoSession;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `SPLIT_STR` (`x` VARCHAR(255), `delim` VARCHAR(12), `pos` INT) RETURNS VARCHAR(255) CHARSET latin1 RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '')$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `assistance`
--

CREATE TABLE `assistance` (
  `idAssistance` int(11) NOT NULL,
  `dt_Assistance` datetime DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idClass` int(11) DEFAULT NULL,
  `idCoach` int(11) DEFAULT NULL,
  `Points` int(11) DEFAULT NULL,
  `idSession` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `assistance`
--

INSERT INTO `assistance` (`idAssistance`, `dt_Assistance`, `idPlace`, `idClass`, `idCoach`, `Points`, `idSession`, `type`) VALUES
(204, '2017-04-15 14:43:00', 10, 18, 155, 130, 4, 122),
(205, '2017-04-15 10:59:00', 10, 21, 155, 130, 1, 122),
(206, '2017-04-15 10:43:00', 10, 21, 155, 130, 4, 122),
(207, '2017-04-15 14:51:00', 10, 20, 155, 130, 5, 122),
(208, '2017-04-18 10:48:00', 10, 20, 155, 130, 1, 122),
(209, '2017-04-21 17:15:00', 10, 16, 155, 130, 3, 122),
(210, '2017-04-21 15:22:00', 10, 15, 155, 130, 2, 122),
(211, '2017-04-21 17:50:00', 10, 16, 155, 130, 5, 122),
(212, '2017-04-15 18:12:00', 10, 18, 155, 130, 1, 122),
(213, '2017-04-15 14:37:00', 10, 16, 155, 130, 4, 122),
(214, '2017-04-15 10:59:00', 10, 19, 155, 130, 2, 122),
(215, '2017-04-15 14:52:00', 10, 13, 155, 130, 4, 122),
(216, '2017-04-15 14:29:00', 10, 13, 155, 130, 1, 122),
(217, '2017-04-15 10:44:00', 10, 17, 155, 130, 3, 122),
(218, '2017-04-15 18:42:00', 10, 17, 155, 130, 1, 122),
(219, '2017-04-18 14:17:00', 10, 15, 155, 130, 5, 122),
(220, '2017-04-18 14:51:00', 10, 14, 155, 130, 3, 123),
(221, '2017-04-18 18:42:00', 10, 17, 155, 130, 5, 122),
(222, '2017-04-18 18:56:00', 10, 20, 155, 130, 1, 123),
(223, '2017-04-18 10:54:00', 10, 21, 155, 130, 2, 123),
(224, '2017-04-18 14:28:00', 10, 16, 155, 130, 4, 123),
(225, '2017-04-21 17:57:00', 10, 13, 155, 130, 1, 123),
(226, '2017-04-21 17:43:00', 10, 16, 155, 130, 3, 123),
(227, '2017-04-21 17:25:00', 10, 13, 155, 130, 4, 123),
(228, '2017-04-21 17:51:00', 10, 17, 155, 130, 2, 123),
(229, '2017-04-24 10:54:00', 10, 15, 155, 130, 1, 123),
(230, '2017-04-24 10:36:00', 10, 20, 155, 130, 5, 123),
(231, '2017-04-24 10:10:00', 10, 15, 155, 130, 4, 123),
(232, '2017-04-24 10:27:00', 10, 14, 155, 130, 2, 123),
(233, '2017-04-24 10:31:00', 10, 13, 155, 130, 5, 123),
(234, '2017-04-24 10:19:00', 10, 16, 155, 130, 1, 123),
(235, '2017-04-24 10:44:00', 10, 16, 155, 130, 5, 123),
(236, '2017-05-03 14:29:00', 10, 20, 155, 130, 4, 122),
(237, '2017-05-03 14:15:00', 10, 21, 155, 130, 1, 122),
(238, '2017-05-03 19:46:00', 10, 15, 155, 130, 4, 122),
(239, '2017-05-03 19:53:00', 10, 16, 155, 130, 2, 122),
(240, '2017-05-03 19:53:00', 10, 19, 155, 130, 5, 122),
(241, '2017-05-06 07:41:00', 10, 14, 155, 130, 3, 122),
(242, '2017-05-06 11:15:00', 10, 18, 155, 130, 2, 122),
(243, '2017-05-06 16:25:00', 10, 18, 155, 130, 3, 122),
(244, '2017-05-06 16:59:00', 10, 17, 155, 130, 5, 122),
(245, '2017-05-11 07:56:00', 10, 18, 155, 130, 2, 122),
(246, '2017-05-11 07:46:00', 10, 18, 155, 130, 4, 122),
(247, '2017-05-11 07:30:00', 10, 20, 155, 130, 4, 122),
(248, '2017-05-11 07:18:00', 10, 16, 155, 130, 1, 122),
(249, '2017-05-14 17:40:00', 10, 20, 155, 130, 2, 122),
(250, '2017-05-14 17:17:00', 10, 16, 155, 130, 3, 122),
(251, '2017-05-14 16:29:00', 10, 15, 155, 130, 5, 123),
(252, '2017-05-14 15:52:00', 10, 20, 155, 130, 3, 122),
(253, '2017-05-14 15:53:00', 10, 14, 155, 130, 4, 123),
(254, '2017-05-18 16:51:00', 10, 19, 155, 130, 5, 123),
(255, '2017-05-18 16:59:00', 10, 21, 155, 130, 4, 123),
(256, '2017-05-18 16:28:00', 10, 13, 155, 130, 1, 123),
(257, '2017-05-18 17:17:00', 10, 14, 155, 130, 5, 123),
(258, '2017-05-22 12:39:00', 10, 15, 155, 130, 3, 123),
(259, '2017-05-22 12:56:00', 10, 20, 155, 130, 4, 123),
(260, '2017-05-22 12:24:00', 10, 19, 155, 130, 1, 123),
(261, '2017-05-22 12:25:00', 10, 14, 155, 130, 5, 123),
(262, '2017-05-22 14:28:00', 10, 21, 155, 130, 5, 123),
(263, '2017-05-22 16:15:00', 10, 18, 155, 130, 3, 123),
(264, '2017-05-22 16:48:00', 10, 16, 155, 130, 5, 123),
(265, '2017-05-22 16:41:00', 10, 15, 155, 130, 4, 123),
(266, '2017-05-03 14:17:00', 10, 20, 155, 130, 4, 122),
(267, '2017-05-03 19:21:00', 10, 19, 155, 130, 5, 122),
(268, '2017-05-03 14:11:00', 10, 16, 155, 130, 2, 122),
(269, '2017-05-06 11:42:00', 10, 18, 155, 130, 5, 122),
(270, '2017-05-06 11:52:00', 10, 21, 155, 130, 1, 122),
(271, '2017-05-06 11:26:00', 10, 18, 155, 130, 5, 122),
(272, '2017-05-06 11:38:00', 10, 16, 155, 130, 1, 122),
(273, '2017-05-06 16:53:00', 10, 17, 155, 130, 4, 122),
(274, '2017-05-06 16:33:00', 10, 19, 155, 130, 1, 122),
(275, '2017-05-06 16:15:00', 10, 17, 155, 130, 2, 122),
(276, '2017-05-06 16:58:00', 10, 14, 155, 130, 3, 122),
(277, '2017-05-06 16:24:00', 10, 21, 155, 130, 4, 122),
(278, '2017-05-11 17:56:00', 10, 16, 155, 130, 5, 122),
(279, '2017-05-14 17:10:00', 10, 13, 155, 130, 3, 122),
(280, '2017-05-14 17:50:00', 10, 14, 155, 130, 2, 123),
(281, '2017-05-14 17:17:00', 10, 13, 155, 130, 5, 122),
(282, '2017-05-14 17:36:00', 10, 14, 155, 130, 3, 123),
(283, '2017-05-14 16:46:00', 10, 19, 155, 130, 5, 123),
(284, '2017-05-14 16:58:00', 10, 16, 155, 130, 1, 123),
(285, '2017-05-14 15:51:00', 10, 17, 155, 130, 1, 123),
(286, '2017-05-14 15:18:00', 10, 13, 155, 130, 2, 123),
(287, '2017-05-14 15:42:00', 10, 15, 155, 130, 1, 123),
(288, '2017-05-22 10:43:00', 10, 20, 155, 130, 1, 123),
(289, '2017-05-22 10:12:00', 10, 16, 155, 130, 3, 123),
(290, '2017-05-22 10:59:00', 10, 14, 155, 130, 3, 123),
(291, '2017-05-22 10:31:00', 10, 19, 155, 130, 5, 123),
(292, '2017-05-22 10:24:00', 10, 18, 155, 130, 1, 123),
(293, '2017-05-22 10:48:00', 10, 18, 155, 130, 3, 123),
(294, '2017-05-22 12:56:00', 10, 21, 155, 130, 3, 123),
(295, '2017-05-22 12:45:00', 10, 15, 155, 130, 5, 123),
(296, '2017-05-22 12:22:00', 10, 20, 155, 130, 4, 123),
(297, '2017-05-22 12:16:00', 10, 16, 155, 130, 5, 123),
(298, '2017-05-22 12:28:00', 10, 20, 155, 130, 3, 123),
(299, '2017-05-22 16:57:00', 10, 21, 155, 130, 4, 123),
(300, '2017-05-22 16:45:00', 10, 19, 155, 130, 4, 123),
(301, '2017-05-22 18:54:00', 10, 19, 155, 130, 1, 123),
(302, '2017-05-22 18:45:00', 10, 15, 155, 130, 5, 123),
(303, '2017-05-22 18:25:00', 10, 13, 155, 130, 4, 123),
(304, '2017-05-22 18:37:00', 10, 19, 155, 130, 3, 123),
(305, '2017-05-22 18:51:00', 10, 16, 155, 130, 2, 123),
(306, '2017-05-03 09:42:00', 10, 15, 155, 130, 2, 122),
(307, '2017-05-03 09:54:00', 10, 17, 155, 130, 1, 122),
(308, '2017-05-03 14:54:00', 10, 16, 155, 130, 4, 122),
(309, '2017-05-03 14:11:00', 10, 14, 155, 130, 3, 122),
(310, '2017-05-03 14:29:00', 10, 16, 155, 130, 5, 122),
(311, '2017-05-06 07:11:00', 10, 14, 155, 130, 2, 122),
(312, '2017-05-06 11:17:00', 10, 16, 155, 130, 5, 122),
(313, '2017-05-06 07:40:00', 10, 18, 155, 130, 1, 122),
(314, '2017-05-06 11:28:00', 10, 21, 155, 130, 3, 122),
(315, '2017-05-11 07:29:00', 10, 13, 155, 130, 3, 122),
(316, '2017-05-11 07:24:00', 10, 21, 155, 130, 1, 122),
(317, '2017-05-14 17:51:00', 10, 18, 155, 130, 5, 122),
(318, '2017-05-11 17:23:00', 10, 15, 155, 130, 1, 122),
(319, '2017-05-14 17:41:00', 10, 19, 155, 130, 2, 122),
(320, '2017-05-14 17:39:00', 10, 17, 155, 130, 1, 122),
(321, '2017-05-14 16:39:00', 10, 15, 155, 130, 2, 122),
(322, '2017-05-14 16:57:00', 10, 14, 155, 130, 4, 122),
(323, '2017-05-18 17:43:00', 10, 18, 155, 130, 3, 123),
(324, '2017-05-22 10:21:00', 10, 14, 155, 130, 3, 123),
(325, '2017-05-22 10:22:00', 10, 13, 155, 130, 2, 123),
(326, '2017-05-22 10:41:00', 10, 13, 155, 130, 2, 123),
(327, '2017-05-22 10:52:00', 10, 18, 155, 130, 4, 123),
(328, '2017-05-22 10:29:00', 10, 15, 155, 130, 4, 123),
(329, '2017-05-22 12:43:00', 10, 15, 155, 130, 1, 123),
(330, '2017-05-22 14:21:00', 10, 19, 155, 130, 4, 123),
(331, '2017-05-22 14:19:00', 10, 16, 155, 130, 3, 123),
(332, '2017-05-22 14:47:00', 10, 20, 155, 130, 2, 123),
(333, '2017-05-22 16:40:00', 10, 15, 155, 130, 2, 123),
(334, '2017-05-22 16:40:00', 10, 17, 155, 130, 2, 123),
(335, '2017-05-22 18:16:00', 10, 18, 155, 130, 2, 123),
(336, '2017-05-22 18:58:00', 10, 20, 155, 130, 5, 123),
(337, '2017-05-22 18:22:00', 10, 13, 155, 130, 2, 123),
(338, '2017-05-22 18:14:00', 10, 15, 155, 130, 3, 123),
(339, '2017-05-03 14:26:00', 10, 16, 155, 130, 5, 122),
(340, '2017-05-03 14:43:00', 10, 17, 155, 130, 4, 122),
(341, '2017-05-03 14:39:00', 10, 17, 155, 130, 4, 122),
(342, '2017-05-03 14:26:00', 10, 16, 155, 130, 3, 122),
(343, '2017-05-06 07:13:00', 10, 17, 155, 130, 3, 122),
(344, '2017-05-06 07:32:00', 10, 16, 155, 130, 5, 122),
(345, '2017-05-06 16:46:00', 10, 21, 155, 130, 5, 122),
(346, '2017-05-06 16:51:00', 10, 15, 155, 130, 1, 122),
(347, '2017-05-06 16:17:00', 10, 17, 155, 130, 1, 122),
(348, '2017-05-06 16:33:00', 10, 18, 155, 130, 2, 122),
(349, '2017-05-06 16:37:00', 10, 16, 155, 130, 5, 122),
(350, '2017-05-06 16:24:00', 10, 18, 155, 130, 5, 122),
(351, '2017-05-11 07:35:00', 10, 17, 155, 130, 1, 122),
(352, '2017-05-11 07:56:00', 10, 18, 155, 130, 5, 122),
(353, '2017-05-11 07:58:00', 10, 19, 155, 130, 3, 123),
(354, '2017-05-11 17:24:00', 10, 14, 155, 130, 1, 123),
(355, '2017-05-11 17:44:00', 10, 14, 155, 130, 2, 122),
(356, '2017-05-11 17:43:00', 10, 21, 155, 130, 5, 123),
(357, '2017-05-14 16:40:00', 10, 21, 155, 130, 1, 123),
(358, '2017-05-14 16:41:00', 10, 18, 155, 130, 5, 123),
(359, '2017-05-14 16:29:00', 10, 18, 155, 130, 2, 123),
(360, '2017-05-14 15:46:00', 10, 19, 155, 130, 5, 123),
(361, '2017-05-14 15:19:00', 10, 14, 155, 130, 5, 123),
(362, '2017-05-14 15:16:00', 10, 17, 155, 130, 5, 123),
(363, '2017-05-18 16:39:00', 10, 19, 155, 130, 3, 123),
(364, '2017-05-18 17:20:00', 10, 15, 155, 130, 5, 123),
(365, '2017-05-22 12:46:00', 10, 18, 155, 130, 3, 123),
(366, '2017-05-22 12:52:00', 10, 20, 155, 130, 4, 123),
(367, '2017-05-22 12:42:00', 10, 15, 155, 130, 1, 123),
(368, '2017-05-22 14:42:00', 10, 13, 155, 130, 2, 123),
(369, '2017-05-22 16:43:00', 10, 21, 155, 130, 3, 123),
(370, '2017-05-22 16:45:00', 10, 13, 155, 130, 4, 123),
(371, '2017-05-22 16:31:00', 10, 18, 155, 130, 1, 123),
(372, '2017-05-22 18:42:00', 10, 21, 155, 130, 2, 123),
(373, '2017-05-22 18:46:00', 10, 19, 155, 130, 5, 123),
(374, '2017-05-22 18:53:00', 10, 20, 155, 130, 3, 123),
(375, '2017-04-15 18:11:00', 10, 14, 155, 130, 5, 122),
(376, '2017-04-24 10:47:00', 10, 20, 155, 130, 3, 123),
(377, '2017-04-24 16:59:00', 10, 15, 155, 130, 3, 123),
(378, '2017-05-06 07:15:00', 10, 19, 155, 130, 2, 122),
(379, '2017-05-06 11:23:00', 10, 13, 155, 130, 1, 122),
(380, '2017-05-11 07:58:00', 10, 14, 155, 130, 3, 122),
(381, '2017-05-14 15:14:00', 10, 17, 155, 130, 2, 123),
(382, '2017-05-06 11:58:00', 10, 19, 155, 130, 4, 122),
(383, '2017-05-22 12:44:00', 10, 18, 155, 130, 1, 123),
(384, '2017-05-03 09:31:00', 10, 16, 155, 130, 5, 122),
(385, '2017-05-06 11:14:00', 10, 14, 155, 130, 5, 122),
(386, '2017-05-22 10:43:00', 10, 16, 155, 130, 4, 123),
(387, '2017-05-22 18:59:00', 10, 20, 155, 130, 3, 123),
(388, '2017-05-06 07:54:00', 10, 20, 155, 130, 5, 122),
(389, '2017-05-06 16:11:00', 10, 17, 155, 130, 1, 122),
(390, '2017-05-22 10:37:00', 10, 13, 155, 130, 1, 123),
(391, '2017-05-22 12:25:00', 10, 14, 155, 130, 2, 123),
(392, '2017-04-24 16:59:00', 10, 20, 155, 130, 1, 123),
(393, '2017-05-03 14:10:00', 10, 16, 155, 130, 5, 122),
(394, '2017-05-14 16:23:00', 10, 13, 155, 130, 4, 122),
(395, '2017-05-14 15:52:00', 10, 16, 155, 130, 1, 123),
(396, '2017-05-18 17:53:00', 10, 14, 155, 130, 4, 123),
(397, '2017-05-22 16:50:00', 10, 21, 155, 130, 2, 123),
(398, '2017-05-06 11:37:00', 10, 18, 155, 130, 2, 122),
(399, '2017-05-14 17:15:00', 10, 20, 155, 130, 4, 123),
(400, '2017-05-14 17:14:00', 10, 20, 155, 130, 4, 123),
(401, '2017-05-14 16:42:00', 10, 18, 155, 130, 4, 123),
(402, '2017-05-14 16:35:00', 10, 21, 155, 130, 3, 122),
(403, '2017-05-18 16:11:00', 10, 15, 155, 130, 1, 122),
(404, '2017-05-18 17:57:00', 10, 13, 155, 130, 4, 123),
(405, '2017-05-22 16:40:00', 10, 15, 155, 130, 1, 123),
(406, '2017-05-22 16:10:00', 10, 17, 155, 130, 4, 123),
(407, '2017-05-03 14:44:00', 10, 19, 155, 130, 1, 122),
(408, '2017-05-06 07:55:00', 10, 16, 155, 130, 3, 122),
(409, '2017-05-06 16:15:00', 10, 18, 155, 130, 3, 122),
(410, '2017-05-14 15:50:00', 10, 19, 155, 130, 4, 123),
(411, '2017-05-18 17:29:00', 10, 19, 155, 130, 3, 123),
(412, '2017-05-22 12:23:00', 10, 14, 155, 130, 2, 123),
(413, '2017-05-22 16:41:00', 10, 13, 155, 130, 3, 123),
(414, '2017-04-15 10:43:00', 10, 21, 155, 130, 2, 122),
(415, '2017-04-15 14:57:00', 10, 20, 155, 130, 3, 122),
(416, '2017-05-22 10:14:00', 10, 20, 155, 130, 4, 123),
(417, '2017-05-22 12:10:00', 10, 19, 155, 130, 2, 123),
(418, '2017-05-22 16:39:00', 10, 20, 155, 130, 5, 123),
(419, '2017-05-22 18:19:00', 10, 21, 155, 130, 1, 123),
(420, '2017-05-03 09:30:00', 10, 20, 155, 130, 3, 122),
(421, '2017-05-06 11:10:00', 10, 17, 155, 130, 3, 122),
(422, '2017-05-06 16:42:00', 10, 18, 155, 130, 1, 122),
(423, '2017-05-22 16:22:00', 10, 18, 155, 130, 3, 123),
(424, '2017-05-11 07:57:00', 10, 20, 155, 130, 5, 122),
(425, '2017-05-18 17:39:00', 10, 13, 155, 130, 2, 123),
(426, '2017-05-14 15:38:00', 10, 21, 155, 130, 1, 123),
(427, '2017-05-18 17:32:00', 10, 13, 155, 130, 4, 123),
(428, '2017-05-22 14:33:00', 10, 18, 155, 130, 5, 123),
(429, '2017-04-15 10:51:00', 10, 17, 155, 130, 1, 122),
(430, '2017-04-15 14:39:00', 10, 15, 155, 130, 3, 122),
(431, '2017-04-21 17:48:00', 10, 15, 155, 130, 3, 123),
(432, '2017-05-11 17:21:00', 10, 13, 155, 130, 5, 122),
(433, '2017-05-03 09:37:00', 10, 21, 155, 130, 2, 122),
(434, '2017-05-06 07:19:00', 10, 20, 155, 130, 4, 122),
(435, '2017-05-22 12:53:00', 10, 14, 155, 130, 2, 123),
(436, '2017-05-14 17:39:00', 10, 18, 155, 130, 2, 122),
(437, '2017-05-22 14:26:00', 10, 19, 155, 130, 5, 123),
(438, '2017-05-03 14:18:00', 10, 13, 155, 130, 2, 122),
(439, '2017-05-06 16:26:00', 10, 21, 155, 130, 3, 122),
(440, '2017-05-06 16:46:00', 10, 20, 155, 130, 3, 122),
(441, '2017-05-14 16:56:00', 10, 14, 155, 130, 3, 123),
(442, '2017-05-14 16:19:00', 10, 13, 155, 130, 1, 122),
(443, '2017-05-18 16:22:00', 10, 14, 155, 130, 2, 123),
(444, '2017-05-22 16:55:00', 10, 15, 155, 130, 4, 123),
(445, '2017-05-14 17:20:00', 10, 13, 155, 130, 1, 123),
(446, '2017-05-14 15:51:00', 10, 20, 155, 130, 5, 123),
(447, '2017-05-22 18:58:00', 10, 18, 155, 130, 5, 123),
(448, '2017-05-18 16:52:00', 10, 17, 155, 130, 4, 122),
(449, '2017-05-18 16:37:00', 10, 14, 155, 130, 1, 123),
(450, '2017-05-03 14:53:00', 10, 14, 155, 130, 2, 122),
(451, '2017-05-06 07:30:00', 10, 15, 155, 130, 1, 122),
(452, '2017-05-14 15:58:00', 10, 17, 155, 130, 3, 123),
(453, '2017-05-22 12:42:00', 10, 17, 155, 130, 4, 123),
(454, '2017-05-18 17:36:00', 10, 16, 155, 130, 2, 123),
(455, '2017-05-22 12:23:00', 10, 15, 155, 130, 3, 123),
(456, '2017-05-22 14:29:00', 10, 14, 155, 130, 3, 123),
(457, '2017-05-22 18:59:00', 10, 17, 155, 130, 2, 123),
(458, '2017-05-03 09:23:00', 10, 20, 155, 130, 4, 122),
(459, '2017-05-06 11:23:00', 10, 17, 155, 130, 2, 122),
(460, '2017-05-11 07:43:00', 10, 17, 155, 130, 2, 122),
(461, '2017-05-22 10:23:00', 10, 15, 155, 130, 3, 123),
(462, '2017-05-14 15:24:00', 10, 14, 155, 130, 1, 123),
(463, '2017-05-18 16:55:00', 10, 21, 155, 130, 4, 123),
(464, '2017-05-18 17:50:00', 10, 20, 155, 130, 1, 123),
(465, '2017-05-22 12:19:00', 10, 17, 155, 130, 3, 123),
(466, '2017-04-18 10:33:00', 10, 15, 155, 130, 4, 123),
(467, '2017-04-21 17:13:00', 10, 19, 155, 130, 1, 123),
(468, '2017-05-11 07:16:00', 10, 17, 155, 130, 2, 122),
(469, '2017-05-14 17:48:00', 10, 18, 155, 130, 4, 122),
(470, '2017-05-18 17:40:00', 10, 18, 155, 130, 3, 123),
(471, '2017-05-03 14:36:00', 10, 16, 155, 130, 2, 122),
(472, '2017-05-11 17:49:00', 10, 17, 155, 130, 4, 122),
(473, '2017-05-06 16:13:00', 10, 15, 155, 130, 4, 122),
(474, '2017-05-06 16:27:00', 10, 15, 155, 130, 4, 122),
(475, '2017-05-22 12:11:00', 10, 14, 155, 130, 5, 123),
(476, '2017-05-14 16:24:00', 10, 20, 155, 130, 2, 123),
(477, '2017-05-14 16:27:00', 10, 16, 155, 130, 3, 123),
(478, '2017-05-18 16:54:00', 10, 18, 155, 130, 5, 123),
(479, '2017-04-15 10:46:00', 10, 18, 155, 130, 5, 122),
(480, '2017-05-22 18:48:00', 10, 17, 155, 130, 5, 123),
(481, '2017-05-03 09:57:00', 10, 16, 155, 130, 1, 122),
(482, '2017-05-06 11:25:00', 10, 19, 155, 130, 3, 122),
(483, '2017-05-06 16:23:00', 10, 20, 155, 130, 3, 122),
(484, '2017-05-18 17:35:00', 10, 16, 155, 130, 5, 123),
(485, '2017-05-22 10:19:00', 10, 20, 155, 130, 2, 123),
(486, '2017-05-14 15:56:00', 10, 14, 155, 130, 2, 123),
(487, '2017-04-15 18:15:00', 10, 16, 155, 130, 5, 122),
(488, '2017-04-21 15:51:00', 10, 17, 155, 130, 5, 123),
(489, '2017-05-14 17:44:00', 10, 20, 155, 130, 5, 122),
(490, '2017-05-22 16:44:00', 10, 20, 155, 130, 4, 123),
(491, '2017-05-03 09:12:00', 10, 16, 155, 130, 5, 122),
(492, '2017-05-06 11:42:00', 10, 14, 155, 130, 4, 122),
(493, '2017-05-06 16:48:00', 10, 21, 155, 130, 5, 122),
(494, '2017-05-22 12:47:00', 10, 19, 155, 130, 4, 123),
(495, '2017-05-03 14:27:00', 10, 18, 155, 130, 1, 122),
(496, '2017-05-14 17:50:00', 10, 15, 155, 130, 3, 122),
(497, '2017-05-03 14:26:00', 10, 21, 155, 130, 1, 122),
(498, '2017-05-14 16:14:00', 10, 16, 155, 130, 4, 123),
(499, '2017-05-22 16:51:00', 10, 14, 155, 130, 4, 123),
(500, '2017-04-18 14:55:00', 10, 17, 155, 130, 3, 122),
(501, '2017-04-21 15:56:00', 10, 19, 155, 130, 4, 122),
(502, '2017-04-15 10:38:00', 10, 14, 155, 130, 3, 122),
(503, '2017-04-15 14:27:00', 10, 17, 155, 130, 3, 122),
(504, '2017-04-15 18:39:00', 10, 13, 155, 130, 4, 122),
(505, '2017-04-18 10:57:00', 10, 21, 155, 130, 2, 122),
(506, '2017-04-18 10:52:00', 10, 18, 155, 130, 2, 123),
(507, '2017-04-24 10:29:00', 10, 13, 155, 130, 2, 123),
(508, '2017-04-24 10:59:00', 10, 20, 155, 130, 3, 123),
(509, '2017-04-24 16:12:00', 10, 19, 155, 130, 4, 123),
(510, '2017-04-24 10:24:00', 10, 15, 155, 130, 3, 123),
(511, '2017-05-03 14:54:00', 10, 20, 155, 130, 3, 122),
(512, '2017-05-06 07:23:00', 10, 16, 155, 130, 3, 122),
(513, '2017-05-06 11:11:00', 10, 17, 155, 130, 4, 122),
(514, '2017-05-06 16:15:00', 10, 14, 155, 130, 1, 122),
(515, '2017-05-11 07:48:00', 10, 15, 155, 130, 5, 122),
(516, '2017-05-11 07:40:00', 10, 17, 155, 130, 3, 122),
(517, '2017-05-22 12:43:00', 10, 15, 155, 130, 1, 123),
(518, '2017-05-22 16:27:00', 10, 17, 155, 130, 2, 123),
(519, '2017-05-22 16:39:00', 10, 16, 155, 130, 2, 123),
(520, '2017-05-03 14:19:00', 10, 16, 155, 130, 2, 122),
(521, '2017-05-03 14:23:00', 10, 21, 155, 130, 3, 122),
(522, '2017-05-06 07:28:00', 10, 18, 155, 130, 1, 122),
(523, '2017-05-06 16:40:00', 10, 13, 155, 130, 2, 122),
(524, '2017-05-11 17:20:00', 10, 13, 155, 130, 1, 122),
(525, '2017-05-22 10:28:00', 10, 20, 155, 130, 5, 123),
(526, '2017-05-22 10:27:00', 10, 20, 155, 130, 2, 123),
(527, '2017-05-22 12:29:00', 10, 15, 155, 130, 2, 123),
(528, '2017-05-22 16:27:00', 10, 17, 155, 130, 5, 123),
(529, '2017-05-03 09:10:00', 10, 21, 155, 130, 4, 122),
(530, '2017-05-06 07:58:00', 10, 17, 155, 130, 3, 122),
(531, '2017-05-14 17:22:00', 10, 15, 155, 130, 3, 122),
(532, '2017-05-14 16:10:00', 10, 17, 155, 130, 5, 122),
(533, '2017-05-22 10:56:00', 10, 13, 155, 130, 5, 123),
(534, '2017-05-22 16:31:00', 10, 13, 155, 130, 5, 123),
(535, '2017-05-03 14:54:00', 10, 16, 155, 130, 3, 122),
(536, '2017-05-06 11:56:00', 10, 14, 155, 130, 4, 122),
(537, '2017-05-06 16:36:00', 10, 18, 155, 130, 2, 122),
(538, '2017-05-11 07:50:00', 10, 21, 155, 130, 2, 122),
(539, '2017-05-11 17:43:00', 10, 17, 155, 130, 4, 123),
(540, '2017-05-14 15:24:00', 10, 20, 155, 130, 4, 123),
(541, '2017-05-18 17:28:00', 10, 18, 155, 130, 2, 123),
(542, '2017-05-22 12:37:00', 10, 19, 155, 130, 1, 123),
(543, '2017-05-22 16:11:00', 10, 21, 155, 130, 5, 123),
(544, '2017-05-22 18:46:00', 10, 14, 155, 130, 1, 123),
(545, '2017-04-15 14:32:00', 10, 14, 155, 130, 2, 122),
(546, '2017-04-24 10:59:00', 10, 14, 155, 130, 2, 123),
(547, '2017-05-06 11:40:00', 10, 19, 155, 130, 5, 122),
(548, '2017-05-06 11:54:00', 10, 16, 155, 130, 5, 122),
(549, '2017-05-14 15:30:00', 10, 15, 155, 130, 5, 123),
(550, '2017-05-03 09:39:00', 10, 13, 155, 130, 3, 122),
(551, '2017-05-06 11:34:00', 10, 17, 155, 130, 4, 122),
(552, '2017-05-18 17:59:00', 10, 18, 155, 130, 5, 123),
(553, '2017-05-06 07:30:00', 10, 20, 155, 130, 1, 122),
(554, '2017-05-03 14:55:00', 10, 16, 155, 130, 2, 122),
(555, '2017-05-14 17:43:00', 10, 21, 155, 130, 2, 123),
(556, '2017-05-14 15:42:00', 10, 18, 155, 130, 2, 123),
(557, '2017-05-22 16:30:00', 10, 18, 155, 130, 3, 123),
(558, '2017-05-03 14:47:00', 10, 20, 155, 130, 3, 122),
(559, '2017-05-06 07:48:00', 10, 15, 155, 130, 5, 122),
(560, '2017-05-06 16:53:00', 10, 18, 155, 130, 2, 122),
(561, '2017-05-22 18:43:00', 10, 16, 155, 130, 4, 123),
(562, '2017-05-06 16:48:00', 10, 13, 155, 130, 5, 122),
(563, '2017-04-15 14:39:00', 10, 14, 155, 130, 2, 122),
(564, '2017-05-22 12:17:00', 10, 17, 155, 130, 1, 123),
(565, '2017-05-22 14:40:00', 10, 17, 155, 130, 1, 123),
(566, '2017-05-18 16:27:00', 10, 20, 155, 130, 2, 123),
(567, '2017-05-22 10:58:00', 10, 18, 155, 130, 5, 123),
(568, '2017-05-11 17:28:00', 10, 19, 155, 130, 4, 122),
(569, '2017-05-14 17:22:00', 10, 15, 155, 130, 1, 122),
(570, '2017-05-18 17:39:00', 10, 13, 155, 130, 1, 123),
(571, '2017-05-06 16:55:00', 10, 15, 155, 130, 2, 122),
(572, '2017-04-15 14:53:00', 10, 17, 155, 130, 5, 122),
(573, '2017-04-15 10:30:00', 10, 20, 155, 130, 4, 122),
(574, '2017-04-18 14:13:00', 10, 19, 155, 130, 1, 123),
(575, '2017-04-18 18:39:00', 10, 18, 155, 130, 3, 123),
(576, '2017-04-21 15:38:00', 10, 13, 155, 130, 5, 123),
(577, '2017-04-21 17:32:00', 10, 13, 155, 130, 2, 123),
(578, '2017-04-24 10:40:00', 10, 21, 155, 130, 2, 123),
(579, '2017-04-24 16:29:00', 10, 13, 155, 130, 4, 123),
(580, '2017-05-06 16:55:00', 10, 21, 155, 130, 4, 122),
(581, '2017-05-11 07:49:00', 10, 14, 155, 130, 1, 122),
(582, '2017-05-14 15:55:00', 10, 17, 155, 130, 2, 123),
(583, '2017-05-22 14:21:00', 10, 18, 155, 130, 1, 123),
(584, '2017-05-22 16:59:00', 10, 14, 155, 130, 1, 123),
(585, '2017-05-03 14:44:00', 10, 15, 155, 130, 3, 122),
(586, '2017-05-06 16:17:00', 10, 14, 155, 130, 4, 122),
(587, '2017-05-14 16:48:00', 10, 13, 155, 130, 2, 123),
(588, '2017-05-14 15:16:00', 10, 13, 155, 130, 5, 123),
(589, '2017-05-14 15:32:00', 10, 17, 155, 130, 3, 123),
(590, '2017-05-22 10:32:00', 10, 17, 155, 130, 2, 123),
(591, '2017-05-22 10:34:00', 10, 18, 155, 130, 4, 123),
(592, '2017-05-22 10:27:00', 10, 19, 155, 130, 1, 123),
(593, '2017-05-22 18:28:00', 10, 18, 155, 130, 2, 123),
(594, '2017-05-22 18:49:00', 10, 14, 155, 130, 1, 123),
(595, '2017-05-06 07:49:00', 10, 15, 155, 130, 5, 122),
(596, '2017-05-06 16:50:00', 10, 14, 155, 130, 2, 122),
(597, '2017-05-14 17:21:00', 10, 20, 155, 130, 4, 122),
(598, '2017-05-14 16:16:00', 10, 15, 155, 130, 1, 122),
(599, '2017-05-22 10:30:00', 10, 19, 155, 130, 1, 123),
(600, '2017-05-22 18:38:00', 10, 18, 155, 130, 5, 123),
(601, '2017-05-22 18:20:00', 10, 14, 155, 130, 1, 123),
(602, '2017-05-03 19:56:00', 10, 16, 155, 130, 4, 122),
(603, '2017-05-06 07:54:00', 10, 17, 155, 130, 2, 122),
(604, '2017-05-06 16:35:00', 10, 13, 155, 130, 4, 122),
(605, '2017-05-11 07:39:00', 10, 17, 155, 130, 4, 122),
(606, '2017-05-14 16:42:00', 10, 21, 155, 130, 1, 123),
(607, '2017-05-22 12:20:00', 10, 14, 155, 130, 5, 123),
(608, '2017-04-24 16:58:00', 10, 13, 155, 130, 4, 123),
(609, '2017-05-06 07:30:00', 10, 16, 155, 130, 1, 122),
(610, '2017-05-22 10:21:00', 10, 17, 155, 130, 2, 123),
(611, '2017-04-15 18:36:00', 10, 14, 155, 130, 2, 122),
(612, '2017-05-22 16:17:00', 10, 13, 155, 130, 3, 123),
(613, '2017-05-22 18:10:00', 10, 16, 155, 130, 3, 123),
(614, '2017-05-18 16:54:00', 10, 19, 155, 130, 2, 123),
(615, '2017-05-18 16:51:00', 10, 21, 155, 130, 3, 123),
(616, '2017-05-06 16:52:00', 10, 15, 155, 130, 1, 122),
(617, '2017-05-22 14:32:00', 10, 13, 155, 130, 1, 123),
(618, '2017-04-15 18:17:00', 10, 15, 155, 130, 2, 122),
(619, '2017-04-18 10:52:00', 10, 19, 155, 130, 4, 122),
(620, '2017-04-21 15:47:00', 10, 17, 155, 130, 1, 122),
(621, '2017-04-15 14:32:00', 10, 21, 155, 130, 1, 122),
(622, '2017-04-18 14:47:00', 10, 16, 155, 130, 3, 122),
(623, '2017-05-06 16:22:00', 10, 21, 155, 130, 2, 122),
(624, '2017-05-22 12:11:00', 10, 13, 155, 130, 2, 123),
(625, '2017-05-22 10:22:00', 10, 18, 155, 130, 2, 123),
(626, '2017-05-22 16:32:00', 10, 20, 155, 130, 3, 123),
(627, '2017-05-22 18:21:00', 10, 19, 155, 130, 4, 123),
(929, '2017-05-06 11:35:00', 10, 17, 155, 130, 9, 122),
(930, '2017-05-06 16:31:00', 10, 15, 155, 130, 6, 122),
(931, '2017-05-14 15:45:00', 10, 14, 155, 130, 6, 122),
(932, '2017-05-14 15:41:00', 10, 15, 155, 130, 6, 122),
(933, '2017-05-18 16:36:00', 10, 14, 155, 130, 8, 122),
(934, '2017-05-22 14:29:00', 10, 19, 155, 130, 9, 122),
(935, '2017-05-06 16:34:00', 10, 20, 155, 130, 8, 122),
(936, '2017-05-14 17:50:00', 10, 14, 155, 130, 9, 122),
(937, '2017-05-14 16:12:00', 10, 21, 155, 130, 9, 122),
(938, '2017-05-22 10:50:00', 10, 19, 155, 130, 6, 122),
(939, '2017-05-06 07:43:00', 10, 16, 155, 130, 6, 122),
(940, '2017-05-11 07:43:00', 10, 21, 155, 130, 7, 122),
(941, '2017-05-22 18:46:00', 10, 14, 155, 130, 9, 123),
(942, '2017-05-03 09:48:00', 10, 14, 155, 130, 6, 122),
(943, '2017-05-11 07:15:00', 10, 21, 155, 130, 6, 122),
(944, '2017-05-03 09:25:00', 10, 19, 155, 130, 8, 122),
(945, '2017-05-03 19:49:00', 10, 19, 155, 130, 7, 122),
(946, '2017-05-14 16:13:00', 10, 13, 155, 130, 8, 123),
(947, '2017-05-22 10:34:00', 10, 17, 155, 130, 8, 123),
(948, '2017-05-03 14:30:00', 10, 16, 155, 130, 8, 122),
(949, '2017-05-22 12:40:00', 10, 16, 155, 130, 7, 122),
(950, '2017-05-18 17:58:00', 10, 17, 155, 130, 6, 122),
(951, '2017-05-22 16:51:00', 10, 17, 155, 130, 8, 123),
(952, '2017-05-06 11:11:00', 10, 14, 155, 130, 6, 122),
(953, '2017-05-22 10:59:00', 10, 13, 155, 130, 9, 122),
(954, '2017-05-03 14:32:00', 10, 21, 155, 130, 9, 122),
(955, '2017-05-14 17:37:00', 10, 15, 155, 130, 8, 123),
(956, '2017-05-06 11:43:00', 10, 21, 155, 130, 7, 122),
(957, '2017-05-11 07:37:00', 10, 14, 155, 130, 6, 122),
(958, '2017-05-22 10:30:00', 10, 18, 155, 130, 7, 122),
(959, '2017-05-06 16:53:00', 10, 13, 155, 130, 6, 122),
(960, '2017-05-06 07:51:00', 10, 18, 155, 130, 6, 122),
(961, '2017-05-06 16:10:00', 10, 15, 155, 130, 6, 122),
(962, '2017-05-11 07:45:00', 10, 18, 155, 130, 8, 122),
(963, '2017-05-14 16:25:00', 10, 18, 155, 130, 7, 122),
(964, '2017-05-22 16:37:00', 10, 21, 155, 130, 8, 123),
(965, '2017-05-22 16:48:00', 10, 13, 155, 130, 9, 123),
(966, '2017-05-22 16:39:00', 10, 18, 155, 130, 8, 123),
(967, '2017-05-22 16:23:00', 10, 13, 155, 130, 6, 122),
(968, '2017-05-22 12:52:00', 10, 17, 155, 130, 9, 123),
(969, '2017-05-06 07:41:00', 10, 14, 155, 130, 9, 122),
(970, '2017-05-14 16:20:00', 10, 16, 155, 130, 8, 122),
(971, '2017-05-22 14:26:00', 10, 20, 155, 130, 6, 123),
(972, '2017-05-03 14:38:00', 10, 16, 155, 130, 8, 122),
(973, '2017-05-14 16:53:00', 10, 16, 155, 130, 7, 122),
(974, '2017-05-22 12:29:00', 10, 15, 155, 130, 8, 123),
(975, '2017-05-06 11:39:00', 10, 16, 155, 130, 6, 122),
(976, '2017-05-22 18:49:00', 10, 13, 155, 130, 8, 123),
(977, '2017-05-11 07:37:00', 10, 14, 155, 130, 8, 122),
(978, '2017-05-06 16:20:00', 10, 17, 155, 130, 6, 122),
(979, '2017-05-14 17:24:00', 10, 21, 155, 130, 9, 122),
(980, '2017-05-06 16:38:00', 10, 14, 155, 130, 8, 122),
(981, '2017-05-14 17:18:00', 10, 17, 155, 130, 9, 122),
(982, '2017-05-06 16:11:00', 10, 14, 155, 130, 7, 122),
(983, '2017-05-06 07:58:00', 10, 13, 155, 130, 9, 122),
(984, '2017-05-11 17:20:00', 10, 16, 155, 130, 8, 122),
(985, '2017-05-22 12:15:00', 10, 14, 155, 130, 8, 122),
(986, '2017-05-14 15:24:00', 10, 20, 155, 130, 7, 122),
(987, '2017-05-06 16:35:00', 10, 17, 155, 130, 8, 122),
(988, '2017-05-22 18:11:00', 10, 13, 155, 130, 6, 122),
(989, '2017-05-18 16:52:00', 10, 14, 155, 130, 7, 122),
(990, '2017-05-22 10:53:00', 10, 14, 155, 130, 6, 122),
(991, '2017-05-22 14:50:00', 10, 15, 155, 130, 9, 122),
(992, '2017-05-06 16:38:00', 10, 19, 155, 130, 9, 122),
(993, '2017-05-22 12:32:00', 10, 17, 155, 130, 6, 122),
(994, '2017-05-06 07:34:00', 10, 14, 155, 130, 9, 122),
(995, '2017-05-22 10:22:00', 10, 18, 155, 130, 9, 122),
(996, '2017-05-11 17:26:00', 10, 19, 155, 130, 8, 122),
(997, '2017-05-11 07:10:00', 10, 17, 155, 130, 7, 122),
(998, '2017-05-14 17:28:00', 10, 18, 155, 130, 8, 122),
(999, '2017-05-22 12:47:00', 10, 21, 155, 130, 7, 122),
(1000, '2017-05-06 07:50:00', 10, 20, 155, 130, 6, 122),
(1001, '2017-05-22 10:54:00', 10, 16, 155, 130, 9, 122),
(1002, '2017-05-03 14:43:00', 10, 20, 155, 130, 9, 122),
(1003, '2017-05-22 10:42:00', 10, 14, 155, 130, 8, 123),
(1004, '2017-05-14 17:34:00', 10, 19, 155, 130, 8, 122),
(1005, '2017-05-22 16:27:00', 10, 13, 155, 130, 8, 123),
(1006, '2017-05-06 16:27:00', 10, 15, 155, 130, 8, 122),
(1007, '2017-05-14 15:35:00', 10, 14, 155, 130, 9, 122),
(1008, '2017-05-03 19:31:00', 10, 18, 155, 130, 8, 122),
(1009, '2017-05-22 16:35:00', 10, 17, 155, 130, 6, 123),
(1010, '2017-05-03 14:58:00', 10, 18, 155, 130, 7, 122),
(1011, '2017-05-03 19:44:00', 10, 19, 155, 130, 9, 122),
(1012, '2017-05-14 16:24:00', 10, 14, 155, 130, 6, 122),
(1013, '2017-05-22 10:36:00', 10, 15, 155, 130, 6, 122),
(1014, '2017-05-06 11:24:00', 10, 17, 155, 130, 8, 122),
(1015, '2017-05-06 16:52:00', 10, 19, 155, 130, 8, 122),
(1016, '2017-05-06 16:30:00', 10, 19, 155, 130, 7, 122),
(1017, '2017-05-22 10:10:00', 10, 14, 155, 130, 8, 122),
(1018, '2017-05-06 07:45:00', 10, 19, 155, 130, 7, 122),
(1019, '2017-05-06 07:33:00', 10, 19, 155, 130, 8, 122),
(1020, '2017-05-03 14:42:00', 10, 14, 155, 130, 8, 122),
(1021, '2017-05-22 16:19:00', 10, 19, 155, 130, 7, 122),
(1022, '2017-05-06 16:50:00', 10, 15, 155, 130, 9, 122),
(1023, '2017-05-06 07:34:00', 10, 14, 155, 130, 8, 122),
(1024, '2017-05-14 15:14:00', 10, 14, 155, 130, 6, 122),
(1025, '2017-05-11 07:29:00', 10, 21, 155, 130, 8, 122),
(1026, '2017-05-14 15:45:00', 10, 19, 155, 130, 7, 122),
(1027, '2017-05-14 17:57:00', 10, 14, 155, 130, 9, 122),
(1028, '2017-05-22 16:48:00', 10, 15, 155, 130, 7, 123),
(1029, '2017-05-03 14:14:00', 10, 18, 155, 130, 7, 122),
(1030, '2017-05-06 11:55:00', 10, 19, 155, 130, 7, 122),
(1031, '2017-05-22 18:35:00', 10, 16, 155, 130, 6, 123),
(1032, '2017-05-06 16:33:00', 10, 17, 155, 130, 7, 122),
(1033, '2017-05-14 17:18:00', 10, 19, 155, 130, 6, 122),
(1034, '2017-05-18 16:21:00', 10, 16, 155, 130, 9, 122),
(1035, '2017-05-03 14:50:00', 10, 13, 155, 130, 8, 122),
(1036, '2017-05-03 19:25:00', 10, 16, 155, 130, 6, 122),
(1037, '2017-05-18 17:30:00', 10, 17, 155, 130, 6, 122),
(1038, '2017-05-06 07:21:00', 10, 19, 155, 130, 7, 122),
(1039, '2017-05-22 18:39:00', 10, 19, 155, 130, 7, 122),
(1040, '2017-05-03 09:18:00', 10, 16, 155, 130, 8, 122),
(1041, '2017-05-06 07:47:00', 10, 16, 155, 130, 7, 122),
(1042, '2017-05-18 16:55:00', 10, 13, 155, 130, 7, 122),
(1043, '2017-05-22 12:32:00', 10, 16, 155, 130, 7, 122),
(1044, '2017-05-14 16:24:00', 10, 15, 155, 130, 7, 122),
(1045, '2017-05-22 18:42:00', 10, 20, 155, 130, 8, 123),
(1046, '2017-05-22 16:39:00', 10, 15, 155, 130, 9, 122),
(1047, '2017-05-22 14:26:00', 10, 20, 155, 130, 7, 123),
(1048, '2017-05-06 11:18:00', 10, 14, 155, 130, 6, 122),
(1049, '2017-05-06 16:23:00', 10, 14, 155, 130, 8, 122),
(1050, '2017-05-14 17:28:00', 10, 15, 155, 130, 8, 122),
(1051, '2017-05-18 16:44:00', 10, 15, 155, 130, 8, 123),
(1052, '2017-05-03 14:47:00', 10, 15, 155, 130, 9, 122),
(1053, '2017-05-03 14:59:00', 10, 18, 155, 130, 8, 122),
(1054, '2017-05-18 17:13:00', 10, 14, 155, 130, 9, 122),
(1055, '2017-05-06 07:37:00', 10, 19, 155, 130, 9, 122),
(1056, '2017-05-03 09:41:00', 10, 18, 155, 130, 9, 122),
(1057, '2017-05-06 07:17:00', 10, 16, 155, 130, 9, 122),
(1058, '2017-05-18 16:15:00', 10, 16, 155, 130, 9, 122),
(1059, '2017-05-22 12:47:00', 10, 13, 155, 130, 6, 122),
(1060, '2017-05-22 18:27:00', 10, 17, 155, 130, 9, 123),
(1061, '2017-05-14 16:10:00', 10, 18, 155, 130, 8, 122),
(1062, '2017-05-22 18:15:00', 10, 13, 155, 130, 7, 123),
(1063, '2017-05-22 16:52:00', 10, 20, 155, 130, 7, 123),
(1064, '2017-05-22 14:26:00', 10, 15, 155, 130, 8, 123),
(1065, '2017-05-22 18:35:00', 10, 15, 155, 130, 8, 123),
(1066, '2017-05-03 14:50:00', 10, 16, 155, 130, 7, 122),
(1067, '2017-05-06 07:50:00', 10, 15, 155, 130, 8, 122),
(1068, '2017-05-03 09:57:00', 10, 13, 155, 130, 7, 122),
(1069, '2017-05-06 07:57:00', 10, 17, 155, 130, 8, 122),
(1070, '2017-05-18 16:32:00', 10, 18, 155, 130, 8, 123),
(1071, '2017-05-14 16:15:00', 10, 16, 155, 130, 9, 122),
(1072, '2017-05-22 18:59:00', 10, 18, 155, 130, 6, 122),
(1073, '2017-05-22 18:58:00', 10, 13, 155, 130, 6, 122),
(1074, '2017-05-03 14:35:00', 10, 17, 155, 130, 7, 122),
(1075, '2017-05-06 11:11:00', 10, 18, 155, 130, 9, 122),
(1076, '2017-05-06 16:50:00', 10, 19, 155, 130, 9, 122),
(1077, '2017-05-14 16:19:00', 10, 18, 155, 130, 7, 122),
(1078, '2017-05-06 16:33:00', 10, 15, 155, 130, 9, 122),
(1079, '2017-05-06 07:16:00', 10, 19, 155, 130, 10, 122),
(1080, '2017-05-06 11:19:00', 10, 17, 155, 130, 12, 122),
(1081, '2017-05-06 16:25:00', 10, 20, 155, 130, 11, 122),
(1082, '2017-05-14 17:15:00', 10, 13, 155, 130, 11, 122),
(1083, '2017-05-14 16:28:00', 10, 13, 155, 130, 11, 122),
(1084, '2017-05-14 15:57:00', 10, 13, 155, 130, 12, 122),
(1085, '2017-05-22 10:44:00', 10, 19, 155, 130, 10, 122),
(1086, '2017-05-22 18:49:00', 10, 17, 155, 130, 10, 123),
(1087, '2017-05-06 11:36:00', 10, 21, 155, 130, 10, 122),
(1088, '2017-05-06 16:55:00', 10, 21, 155, 130, 11, 122),
(1089, '2017-05-14 16:35:00', 10, 21, 155, 130, 10, 122),
(1090, '2017-05-18 17:29:00', 10, 20, 155, 130, 12, 122),
(1091, '2017-05-22 10:47:00', 10, 15, 155, 130, 10, 122),
(1092, '2017-05-22 18:52:00', 10, 19, 155, 130, 11, 123),
(1093, '2017-05-03 14:24:00', 10, 15, 155, 130, 10, 122),
(1094, '2017-05-03 14:57:00', 10, 20, 155, 130, 10, 122),
(1095, '2017-05-06 07:35:00', 10, 15, 155, 130, 11, 122),
(1096, '2017-05-06 11:14:00', 10, 14, 155, 130, 10, 122),
(1097, '2017-05-11 07:23:00', 10, 21, 155, 130, 11, 122),
(1098, '2017-05-11 07:17:00', 10, 16, 155, 130, 11, 122),
(1099, '2017-05-14 16:24:00', 10, 21, 155, 130, 11, 122),
(1100, '2017-05-14 15:26:00', 10, 13, 155, 130, 11, 122),
(1101, '2017-05-18 17:39:00', 10, 19, 155, 130, 11, 123),
(1102, '2017-05-22 12:30:00', 10, 13, 155, 130, 10, 122),
(1103, '2017-05-22 12:26:00', 10, 16, 155, 130, 12, 122),
(1104, '2017-05-03 14:36:00', 10, 19, 155, 130, 11, 122),
(1105, '2017-05-06 16:19:00', 10, 20, 155, 130, 12, 122),
(1106, '2017-05-06 16:43:00', 10, 15, 155, 130, 12, 122),
(1107, '2017-05-14 17:20:00', 10, 20, 155, 130, 12, 122),
(1108, '2017-05-14 15:47:00', 10, 18, 155, 130, 11, 122),
(1109, '2017-05-22 10:27:00', 10, 13, 155, 130, 11, 123),
(1110, '2017-05-22 16:49:00', 10, 14, 155, 130, 11, 123),
(1111, '2017-05-22 18:59:00', 10, 15, 155, 130, 11, 123),
(1112, '2017-05-03 09:36:00', 10, 16, 155, 130, 11, 122),
(1113, '2017-05-03 14:15:00', 10, 21, 155, 130, 10, 122),
(1114, '2017-05-06 11:22:00', 10, 20, 155, 130, 10, 122),
(1115, '2017-05-11 07:53:00', 10, 18, 155, 130, 12, 122),
(1116, '2017-05-11 07:20:00', 10, 20, 155, 130, 12, 122),
(1117, '2017-05-14 17:24:00', 10, 17, 155, 130, 10, 122),
(1118, '2017-05-14 17:46:00', 10, 19, 155, 130, 12, 122),
(1119, '2017-05-14 16:26:00', 10, 19, 155, 130, 11, 122),
(1120, '2017-05-22 16:45:00', 10, 13, 155, 130, 11, 123),
(1121, '2017-05-22 16:42:00', 10, 21, 155, 130, 12, 123),
(1122, '2017-05-14 16:59:00', 10, 18, 155, 130, 10, 122),
(1123, '2017-05-14 16:10:00', 10, 17, 155, 130, 12, 122),
(1124, '2017-05-18 16:29:00', 10, 13, 155, 130, 10, 122),
(1125, '2017-05-22 18:23:00', 10, 20, 155, 130, 12, 122),
(1126, '2017-05-03 09:34:00', 10, 16, 155, 130, 12, 122),
(1127, '2017-05-03 14:57:00', 10, 14, 155, 130, 10, 122),
(1128, '2017-05-06 11:46:00', 10, 16, 155, 130, 12, 122),
(1129, '2017-05-14 16:24:00', 10, 18, 155, 130, 12, 122),
(1130, '2017-05-22 10:59:00', 10, 14, 155, 130, 11, 122),
(1131, '2017-05-22 12:12:00', 10, 13, 155, 130, 11, 123),
(1132, '2017-05-22 16:20:00', 10, 14, 155, 130, 10, 122),
(1133, '2017-05-06 07:41:00', 10, 18, 155, 130, 11, 122),
(1134, '2017-05-14 17:52:00', 10, 20, 155, 130, 11, 122),
(1135, '2017-05-14 16:39:00', 10, 17, 155, 130, 12, 122),
(1136, '2017-05-18 17:33:00', 10, 19, 155, 130, 10, 122),
(1137, '2017-05-22 14:45:00', 10, 19, 155, 130, 12, 122),
(1138, '2017-05-03 19:20:00', 10, 15, 155, 130, 11, 122),
(1139, '2017-05-06 07:41:00', 10, 14, 155, 130, 12, 122),
(1140, '2017-05-06 16:57:00', 10, 15, 155, 130, 11, 122),
(1141, '2017-05-14 15:40:00', 10, 17, 155, 130, 11, 122),
(1142, '2017-05-03 14:54:00', 10, 14, 155, 130, 11, 122),
(1143, '2017-05-06 11:30:00', 10, 13, 155, 130, 10, 122),
(1144, '2017-05-06 16:49:00', 10, 14, 155, 130, 11, 122),
(1145, '2017-05-11 17:20:00', 10, 17, 155, 130, 10, 122),
(1146, '2017-05-14 16:53:00', 10, 21, 155, 130, 10, 122),
(1147, '2017-05-18 17:50:00', 10, 18, 155, 130, 12, 123),
(1148, '2017-05-22 12:31:00', 10, 17, 155, 130, 10, 122),
(1149, '2017-05-22 18:51:00', 10, 17, 155, 130, 11, 123),
(1150, '2017-05-06 11:53:00', 10, 13, 155, 130, 12, 122),
(1151, '2017-05-11 07:12:00', 10, 21, 155, 130, 11, 122),
(1152, '2017-05-18 17:20:00', 10, 21, 155, 130, 11, 122),
(1153, '2017-05-22 10:37:00', 10, 19, 155, 130, 12, 122),
(1154, '2017-05-22 12:33:00', 10, 16, 155, 130, 11, 122),
(1155, '2017-05-22 16:18:00', 10, 16, 155, 130, 12, 123),
(1156, '2017-05-22 16:50:00', 10, 15, 155, 130, 12, 123),
(1157, '2017-05-06 07:50:00', 10, 18, 155, 130, 10, 122),
(1158, '2017-05-06 16:21:00', 10, 21, 155, 130, 10, 122),
(1159, '2017-05-22 10:13:00', 10, 15, 155, 130, 12, 122),
(1160, '2017-05-22 12:28:00', 10, 17, 155, 130, 11, 123),
(1161, '2017-05-22 16:18:00', 10, 20, 155, 130, 12, 122),
(1162, '2017-05-08 09:00:00', 11, 18, 260, 130, 21, 122),
(1163, '2017-05-08 09:00:00', 11, 17, 260, 130, 21, 122),
(1164, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 122),
(1165, '2017-05-08 09:00:00', 11, 18, 260, 130, 21, 122),
(1166, '2017-05-08 09:00:00', 11, 15, 260, 130, 21, 122),
(1167, '2017-05-08 09:00:00', 11, 17, 260, 130, 21, 122),
(1168, '2017-05-08 09:00:00', 11, 13, 260, 130, 21, 122),
(1169, '2017-05-08 09:00:00', 11, 14, 260, 130, 21, 122),
(1170, '2017-05-08 09:00:00', 11, 15, 260, 130, 21, 122),
(1171, '2017-05-08 09:00:00', 11, 20, 260, 130, 21, 122),
(1172, '2017-05-08 09:00:00', 11, 16, 260, 130, 21, 123),
(1173, '2017-05-08 09:00:00', 11, 18, 260, 130, 21, 123),
(1174, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 123),
(1175, '2017-05-08 09:00:00', 11, 14, 260, 130, 21, 123),
(1176, '2017-05-08 09:00:00', 11, 18, 260, 130, 21, 123),
(1177, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 123),
(1178, '2017-05-08 09:00:00', 11, 17, 260, 130, 21, 123),
(1179, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 123),
(1180, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 122),
(1181, '2017-05-08 09:00:00', 11, 14, 260, 130, 21, 122),
(1182, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 122),
(1183, '2017-05-08 09:00:00', 11, 16, 260, 130, 21, 122),
(1184, '2017-05-08 09:00:00', 11, 16, 260, 130, 21, 122),
(1185, '2017-05-08 09:00:00', 11, 20, 260, 130, 21, 122),
(1186, '2017-05-08 09:00:00', 11, 19, 260, 130, 21, 122),
(1187, '2017-05-08 09:00:00', 11, 17, 260, 130, 21, 122),
(1188, '2017-05-08 09:00:00', 11, 20, 260, 130, 21, 123),
(1189, '2017-05-08 09:00:00', 11, 21, 260, 130, 21, 123),
(1190, '2017-05-08 09:00:00', 11, 16, 260, 130, 21, 123),
(1191, '2017-05-08 09:00:00', 11, 15, 260, 130, 21, 123),
(1192, '2017-05-08 09:00:00', 11, 20, 260, 130, 21, 123),
(1193, '2017-05-08 09:00:00', 11, 18, 260, 130, 21, 123),
(1194, '2017-05-08 09:00:00', 11, 14, 260, 130, 21, 123),
(1195, '2017-05-08 09:00:00', 11, 20, 260, 130, 21, 123),
(1196, '2017-05-11 07:00:00', 11, 21, 260, 130, 21, 122),
(1197, '2017-05-11 07:00:00', 11, 16, 260, 130, 21, 122),
(1198, '2017-05-11 07:00:00', 11, 15, 260, 130, 21, 122),
(1199, '2017-05-11 07:00:00', 11, 16, 260, 130, 21, 122),
(1200, '2017-05-11 07:00:00', 11, 20, 260, 130, 21, 122),
(1201, '2017-05-11 07:00:00', 11, 21, 260, 130, 21, 122),
(1202, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 122),
(1203, '2017-05-11 07:00:00', 11, 19, 260, 130, 21, 122),
(1204, '2017-05-11 07:00:00', 11, 16, 260, 130, 21, 123),
(1205, '2017-05-11 07:00:00', 11, 16, 260, 130, 21, 123),
(1206, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 123),
(1207, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 123),
(1208, '2017-05-11 07:00:00', 11, 19, 260, 130, 21, 123),
(1209, '2017-05-11 07:00:00', 11, 18, 260, 130, 21, 123),
(1210, '2017-05-11 07:00:00', 11, 17, 260, 130, 21, 122),
(1211, '2017-05-11 07:00:00', 11, 20, 260, 130, 21, 122),
(1212, '2017-05-11 07:00:00', 11, 15, 260, 130, 21, 122),
(1213, '2017-05-11 07:00:00', 11, 20, 260, 130, 21, 122),
(1214, '2017-05-11 07:00:00', 11, 18, 260, 130, 21, 122),
(1215, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 122),
(1216, '2017-05-11 07:00:00', 11, 14, 260, 130, 21, 122),
(1217, '2017-05-11 07:00:00', 11, 15, 260, 130, 21, 122),
(1218, '2017-05-11 07:00:00', 11, 14, 260, 130, 21, 123),
(1219, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 123),
(1220, '2017-05-11 07:00:00', 11, 20, 260, 130, 21, 123),
(1221, '2017-05-11 07:00:00', 11, 21, 260, 130, 21, 123),
(1222, '2017-05-11 07:00:00', 11, 16, 260, 130, 21, 123),
(1223, '2017-05-11 07:00:00', 11, 18, 260, 130, 21, 123),
(1224, '2017-05-11 07:00:00', 11, 14, 260, 130, 21, 123),
(1225, '2017-05-11 07:00:00', 11, 20, 260, 130, 21, 123),
(1226, '2017-05-11 07:00:00', 11, 21, 260, 130, 21, 123),
(1227, '2017-05-11 07:00:00', 11, 14, 260, 130, 21, 123),
(1228, '2017-05-11 07:00:00', 11, 21, 260, 130, 21, 122),
(1229, '2017-05-11 07:00:00', 11, 18, 260, 130, 21, 122),
(1230, '2017-05-11 07:00:00', 11, 15, 260, 130, 21, 122),
(1231, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 122),
(1232, '2017-05-11 07:00:00', 11, 18, 260, 130, 21, 122),
(1233, '2017-05-11 07:00:00', 11, 13, 260, 130, 21, 122),
(1234, '2017-05-11 07:00:00', 11, 15, 260, 130, 21, 122),
(1235, '2017-05-11 07:00:00', 11, 17, 260, 130, 21, 122),
(1236, '2017-05-11 07:00:00', 11, 15, 260, 130, 21, 123),
(1237, '2017-05-11 19:00:00', 11, 21, 260, 130, 21, 123),
(1238, '2017-05-11 19:00:00', 11, 14, 260, 130, 21, 123),
(1239, '2017-05-11 19:00:00', 11, 21, 260, 130, 21, 123),
(1240, '2017-05-11 19:00:00', 11, 20, 260, 130, 21, 123),
(1241, '2017-05-11 19:00:00', 11, 16, 260, 130, 21, 123),
(1242, '2017-05-11 19:00:00', 11, 18, 260, 130, 21, 123),
(1243, '2017-05-11 19:00:00', 11, 13, 260, 130, 21, 123),
(1244, '2017-05-11 19:00:00', 11, 20, 260, 130, 21, 123),
(1245, '2017-05-11 19:00:00', 11, 14, 260, 130, 21, 123),
(1246, '2017-05-11 19:00:00', 11, 13, 260, 130, 21, 123),
(1247, '2017-05-11 19:00:00', 11, 14, 260, 130, 21, 123),
(1248, '2017-05-11 19:00:00', 11, 14, 260, 130, 21, 122),
(1249, '2017-05-11 19:00:00', 11, 21, 260, 130, 21, 122),
(1250, '2017-05-11 19:00:00', 11, 21, 260, 130, 21, 122),
(1251, '2017-05-11 19:00:00', 11, 21, 260, 130, 21, 122),
(1252, '2017-05-11 19:00:00', 11, 17, 260, 130, 21, 122),
(1253, '2017-05-11 19:00:00', 11, 17, 260, 130, 21, 122),
(1254, '2017-05-11 19:00:00', 11, 15, 260, 130, 21, 122),
(1255, '2017-05-11 19:00:00', 11, 20, 260, 130, 21, 122),
(1256, '2017-05-11 19:00:00', 11, 14, 260, 130, 21, 123),
(1257, '2017-05-11 19:00:00', 11, 20, 260, 130, 21, 123),
(1258, '2017-05-11 19:00:00', 11, 18, 260, 130, 21, 123),
(1259, '2017-05-11 19:00:00', 11, 19, 260, 130, 21, 123),
(1260, '2017-05-11 19:00:00', 11, 16, 260, 130, 21, 123),
(1261, '2017-05-11 19:00:00', 11, 15, 260, 130, 41, 122),
(1262, '2017-05-11 19:00:00', 11, 17, 260, 130, 41, 122),
(1263, '2017-05-18 18:00:00', 11, 13, 260, 130, 41, 122),
(1264, '2017-05-18 18:00:00', 11, 15, 260, 130, 41, 122),
(1265, '2017-05-18 18:00:00', 11, 13, 260, 130, 41, 122),
(1266, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1267, '2017-05-18 18:00:00', 11, 13, 260, 130, 41, 122),
(1268, '2017-05-18 18:00:00', 11, 15, 260, 130, 41, 122),
(1269, '2017-05-18 18:00:00', 11, 19, 260, 130, 41, 122),
(1270, '2017-05-18 18:00:00', 11, 15, 260, 130, 41, 122),
(1271, '2017-05-18 18:00:00', 11, 16, 260, 130, 41, 122),
(1272, '2017-05-18 18:00:00', 11, 18, 260, 130, 41, 122),
(1273, '2017-05-18 18:00:00', 11, 20, 260, 130, 41, 122),
(1274, '2017-05-18 18:00:00', 11, 21, 260, 130, 41, 122),
(1275, '2017-05-18 18:00:00', 11, 19, 260, 130, 41, 122),
(1276, '2017-05-18 18:00:00', 11, 19, 260, 130, 41, 122),
(1277, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1278, '2017-05-18 18:00:00', 11, 18, 260, 130, 41, 122),
(1279, '2017-05-18 18:00:00', 11, 21, 260, 130, 41, 122),
(1280, '2017-05-18 18:00:00', 11, 16, 260, 130, 41, 122),
(1281, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1282, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1283, '2017-05-18 18:00:00', 11, 20, 260, 130, 41, 122),
(1284, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1285, '2017-05-18 18:00:00', 11, 17, 260, 130, 41, 122),
(1286, '2017-05-18 18:00:00', 11, 16, 260, 130, 41, 122),
(1287, '2017-05-18 18:00:00', 11, 17, 260, 130, 41, 122),
(1288, '2017-05-18 18:00:00', 11, 20, 260, 130, 41, 122),
(1289, '2017-05-18 18:00:00', 11, 19, 260, 130, 41, 122),
(1290, '2017-05-18 18:00:00', 11, 16, 260, 130, 41, 122),
(1291, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1292, '2017-05-18 18:00:00', 11, 18, 260, 130, 41, 122),
(1293, '2017-05-18 18:00:00', 11, 13, 260, 130, 41, 122),
(1294, '2017-05-18 18:00:00', 11, 21, 260, 130, 41, 122),
(1295, '2017-05-18 18:00:00', 11, 19, 260, 130, 41, 122),
(1296, '2017-05-18 18:00:00', 11, 20, 260, 130, 41, 122),
(1297, '2017-05-18 18:00:00', 11, 13, 260, 130, 41, 122),
(1298, '2017-05-18 18:00:00', 11, 18, 260, 130, 41, 122),
(1299, '2017-05-18 18:00:00', 11, 19, 260, 130, 41, 122),
(1300, '2017-05-18 18:00:00', 11, 16, 260, 130, 41, 122),
(1301, '2017-05-18 18:00:00', 11, 20, 260, 130, 41, 122),
(1302, '2017-05-18 18:00:00', 11, 14, 260, 130, 41, 122),
(1303, '2017-05-18 18:00:00', 11, 15, 260, 130, 41, 122),
(1304, '2017-05-22 09:00:00', 11, 17, 260, 130, 41, 123),
(1305, '2017-05-22 09:00:00', 11, 15, 260, 130, 41, 122),
(1306, '2017-05-22 09:00:00', 11, 18, 260, 130, 41, 122),
(1307, '2017-05-22 09:00:00', 11, 20, 260, 130, 66, 122),
(1308, '2017-05-22 09:00:00', 11, 21, 260, 130, 66, 122),
(1309, '2017-05-22 09:00:00', 11, 21, 260, 130, 66, 122),
(1310, '2017-05-22 09:00:00', 11, 19, 260, 130, 66, 122),
(1311, '2017-05-22 09:00:00', 11, 15, 260, 130, 66, 122),
(1312, '2017-05-22 09:00:00', 11, 19, 260, 130, 66, 122),
(1313, '2017-05-22 09:00:00', 11, 13, 260, 130, 66, 122),
(1314, '2017-05-22 09:00:00', 11, 13, 260, 130, 66, 122),
(1315, '2017-05-22 09:00:00', 11, 21, 260, 130, 66, 122),
(1316, '2017-05-22 09:00:00', 11, 17, 260, 130, 66, 122),
(1317, '2017-05-22 09:00:00', 11, 21, 260, 130, 66, 122),
(1318, '2017-05-22 09:00:00', 11, 14, 260, 130, 66, 122),
(1319, '2017-05-22 09:00:00', 11, 20, 260, 130, 66, 122),
(1320, '2017-05-22 09:00:00', 11, 14, 260, 130, 66, 122),
(1321, '2017-05-22 09:00:00', 11, 16, 260, 130, 66, 122),
(1322, '2017-05-22 09:00:00', 11, 16, 260, 130, 66, 122),
(1323, '2017-05-22 09:00:00', 11, 20, 260, 130, 66, 122),
(1324, '2017-05-22 09:00:00', 11, 14, 260, 130, 66, 122),
(1325, '2017-05-22 09:00:00', 11, 13, 260, 130, 66, 122),
(1326, '2017-05-22 09:00:00', 11, 21, 260, 130, 66, 122),
(1327, '2017-05-22 09:00:00', 11, 13, 260, 130, 66, 122),
(1328, '2017-05-22 09:00:00', 11, 14, 260, 130, 66, 122),
(1329, '2017-05-22 09:00:00', 11, 17, 260, 130, 66, 122),
(1330, '2017-05-22 09:00:00', 11, 21, 260, 130, 66, 122),
(1331, '2017-05-22 09:00:00', 11, 17, 260, 130, 66, 122),
(1332, '2017-05-22 09:00:00', 11, 14, 260, 130, 66, 122),
(1333, '2017-03-14 17:00:00', 11, 14, 260, 130, 66, 122),
(1334, '2017-03-14 17:00:00', 11, 21, 260, 130, 66, 122),
(1335, '2017-03-14 17:00:00', 11, 16, 260, 130, 66, 122),
(1336, '2017-03-14 17:00:00', 11, 16, 260, 130, 66, 122),
(1337, '2017-03-14 17:00:00', 11, 20, 260, 130, 66, 122),
(1338, '2017-03-14 17:00:00', 11, 20, 260, 130, 66, 122),
(1339, '2017-03-14 17:00:00', 11, 21, 260, 130, 66, 122),
(1340, '2017-03-14 17:00:00', 11, 14, 260, 130, 66, 123),
(1341, '2017-03-14 17:00:00', 11, 14, 260, 130, 66, 123),
(1342, '2017-03-14 17:00:00', 11, 15, 260, 130, 66, 122),
(1343, '2017-03-14 17:00:00', 11, 15, 260, 130, 66, 122),
(1344, '2017-03-14 17:00:00', 11, 13, 260, 130, 66, 122),
(1345, '2017-03-14 17:00:00', 11, 21, 260, 130, 66, 122),
(1346, '2017-05-22 15:00:00', 11, 19, 260, 130, 66, 122),
(1347, '2017-05-22 15:00:00', 11, 14, 260, 130, 66, 122),
(1348, '2017-05-22 15:00:00', 11, 16, 260, 130, 66, 122),
(1349, '2017-05-22 15:00:00', 11, 14, 260, 130, 66, 122),
(1350, '2017-05-22 15:00:00', 11, 21, 260, 130, 66, 123),
(1351, '2017-05-22 15:00:00', 11, 21, 260, 130, 66, 122),
(1352, '2017-05-22 15:00:00', 11, 13, 260, 130, 66, 122),
(1353, '2017-05-22 15:00:00', 11, 21, 260, 130, 66, 122),
(1354, '2017-05-22 15:00:00', 11, 17, 260, 130, 66, 122),
(1355, '2017-05-22 15:00:00', 11, 21, 260, 130, 66, 122),
(1356, '2017-05-22 15:12:00', 11, 19, 260, 130, 77, 122),
(1357, '2017-05-22 15:31:00', 11, 13, 260, 130, 77, 122),
(1358, '2017-05-22 15:35:00', 11, 16, 260, 130, 77, 122),
(1359, '2017-05-22 15:59:00', 11, 20, 260, 130, 77, 122),
(1360, '2017-05-22 15:22:00', 11, 15, 260, 130, 77, 122),
(1361, '2017-05-22 15:56:00', 11, 14, 260, 130, 77, 122),
(1362, '2017-05-22 15:24:00', 11, 16, 260, 130, 77, 122),
(1363, '2017-05-22 15:22:00', 11, 18, 260, 130, 77, 122),
(1364, '2017-05-08 16:18:00', 11, 14, 260, 130, 77, 122),
(1365, '2017-05-08 16:58:00', 11, 14, 260, 130, 77, 122),
(1366, '2017-05-08 16:56:00', 11, 21, 260, 130, 77, 122),
(1367, '2017-05-08 16:52:00', 11, 21, 260, 130, 77, 122),
(1368, '2017-05-08 16:50:00', 11, 19, 260, 130, 77, 122),
(1369, '2017-05-08 16:52:00', 11, 21, 260, 130, 77, 122),
(1370, '2017-05-08 16:58:00', 11, 15, 260, 130, 77, 122),
(1371, '2017-05-08 16:37:00', 11, 21, 260, 130, 77, 122),
(1372, '2017-05-08 16:49:00', 11, 17, 260, 130, 77, 122),
(1373, '2017-05-08 16:17:00', 11, 16, 260, 130, 77, 123),
(1374, '2017-05-08 16:40:00', 11, 17, 260, 130, 77, 122),
(1375, '2017-05-08 16:22:00', 11, 20, 260, 130, 77, 122),
(1376, '2017-04-14 08:20:00', 11, 17, 260, 130, 77, 122),
(1377, '2017-04-14 08:50:00', 11, 15, 260, 130, 77, 122),
(1378, '2017-04-14 08:29:00', 11, 17, 260, 130, 77, 123),
(1379, '2017-04-14 08:15:00', 11, 21, 260, 130, 77, 123),
(1380, '2017-04-14 08:42:00', 11, 13, 260, 130, 77, 122),
(1381, '2017-04-14 08:28:00', 11, 16, 260, 130, 77, 122),
(1382, '2017-04-14 08:15:00', 11, 18, 260, 130, 77, 122),
(1383, '2017-04-14 08:55:00', 11, 19, 260, 130, 77, 122),
(1384, '2017-04-14 08:20:00', 11, 20, 260, 130, 77, 122),
(1385, '2017-04-14 08:47:00', 11, 16, 260, 130, 77, 122),
(1386, '2017-04-14 08:34:00', 11, 21, 260, 130, 77, 122),
(1387, '2017-04-14 08:32:00', 11, 19, 260, 130, 77, 122),
(1388, '2017-04-14 08:10:00', 11, 13, 260, 130, 77, 122),
(1389, '2017-04-14 17:13:00', 11, 21, 260, 130, 77, 122),
(1390, '2017-04-14 17:18:00', 11, 14, 260, 130, 77, 122),
(1391, '2017-04-14 17:39:00', 11, 13, 260, 130, 77, 122),
(1392, '2017-04-14 17:41:00', 11, 18, 260, 130, 77, 122),
(1393, '2017-04-14 17:52:00', 11, 16, 260, 130, 77, 122),
(1394, '2017-04-14 17:37:00', 11, 20, 260, 130, 77, 122),
(1395, '2017-04-14 17:44:00', 11, 21, 260, 130, 77, 122),
(1396, '2017-04-14 17:21:00', 11, 20, 260, 130, 77, 122),
(1397, '2017-04-14 17:11:00', 11, 13, 260, 130, 77, 122),
(1398, '2017-04-14 17:44:00', 11, 15, 260, 130, 77, 123),
(1399, '2017-04-14 17:50:00', 11, 21, 260, 130, 77, 123),
(1400, '2017-04-14 17:18:00', 11, 15, 260, 130, 77, 123),
(1401, '2017-04-14 17:00:00', 11, 21, 260, 130, 20, 122),
(1402, '2017-04-14 17:00:00', 11, 15, 260, 130, 20, 122),
(1403, '2017-04-14 17:00:00', 11, 16, 260, 130, 20, 122),
(1404, '2017-04-14 17:00:00', 11, 17, 260, 130, 20, 122),
(1405, '2017-04-14 17:00:00', 11, 21, 260, 130, 20, 122),
(1406, '2017-04-14 17:00:00', 11, 15, 260, 130, 20, 122),
(1407, '2017-04-14 17:00:00', 11, 14, 260, 130, 20, 122),
(1408, '2017-04-14 17:00:00', 11, 20, 260, 130, 20, 122),
(1409, '2017-04-14 17:00:00', 11, 16, 260, 130, 20, 122),
(1410, '2017-04-14 17:00:00', 11, 19, 260, 130, 20, 122),
(1411, '2017-04-14 17:00:00', 11, 18, 260, 130, 20, 123),
(1412, '2017-04-14 17:00:00', 11, 19, 260, 130, 20, 123),
(1413, '2017-04-14 17:00:00', 11, 19, 260, 130, 20, 123);
INSERT INTO `assistance` (`idAssistance`, `dt_Assistance`, `idPlace`, `idClass`, `idCoach`, `Points`, `idSession`, `type`) VALUES
(1414, '2017-04-14 17:00:00', 11, 13, 260, 130, 20, 123),
(1415, '2017-04-21 08:00:00', 11, 17, 260, 130, 20, 123),
(1416, '2017-04-21 08:00:00', 11, 17, 260, 130, 20, 123),
(1417, '2017-04-21 08:00:00', 11, 13, 260, 130, 20, 123),
(1418, '2017-04-21 08:00:00', 11, 20, 260, 130, 20, 123),
(1419, '2017-04-21 08:00:00', 11, 17, 260, 130, 20, 122),
(1420, '2017-04-21 08:00:00', 11, 15, 260, 130, 20, 122),
(1421, '2017-04-21 08:00:00', 11, 20, 260, 130, 20, 122),
(1422, '2017-04-21 08:00:00', 11, 18, 260, 130, 20, 122),
(1423, '2017-04-21 08:00:00', 11, 17, 260, 130, 20, 122),
(1424, '2017-04-21 08:00:00', 11, 14, 260, 130, 20, 122),
(1425, '2017-04-21 08:00:00', 11, 16, 260, 130, 20, 122),
(1426, '2017-04-21 08:00:00', 11, 13, 260, 130, 20, 122),
(1427, '2017-04-21 08:00:00', 11, 19, 260, 130, 20, 123),
(1428, '2017-04-21 08:00:00', 11, 15, 260, 130, 20, 123),
(1429, '2017-04-21 08:00:00', 11, 16, 260, 130, 20, 123),
(1430, '2017-04-21 17:00:00', 11, 13, 260, 130, 20, 123),
(1431, '2017-04-21 17:00:00', 11, 16, 260, 130, 20, 123),
(1432, '2017-04-21 17:00:00', 11, 18, 260, 130, 20, 123),
(1433, '2017-04-21 17:00:00', 11, 21, 260, 130, 20, 123),
(1434, '2017-04-21 17:00:00', 11, 19, 260, 130, 20, 123),
(1435, '2017-04-21 17:00:00', 11, 16, 260, 130, 20, 122),
(1436, '2017-04-21 17:00:00', 11, 13, 260, 130, 20, 122),
(1437, '2017-04-21 17:00:00', 11, 13, 260, 130, 20, 122),
(1438, '2017-04-21 17:00:00', 11, 19, 260, 130, 20, 122),
(1439, '2017-04-21 17:00:00', 11, 21, 260, 130, 20, 122),
(1440, '2017-04-21 17:00:00', 11, 15, 260, 130, 20, 122),
(1441, '2017-04-21 17:00:00', 11, 21, 260, 130, 20, 122),
(1442, '2017-04-21 17:00:00', 11, 20, 260, 130, 20, 122),
(1443, '2017-04-21 17:00:00', 11, 21, 260, 130, 20, 123),
(1444, '2017-04-21 17:00:00', 11, 20, 260, 130, 20, 123),
(1445, '2017-04-21 17:00:00', 11, 16, 260, 130, 20, 123),
(1446, '2017-04-21 17:00:00', 11, 19, 260, 130, 20, 123),
(1447, '2017-04-21 17:00:00', 11, 20, 260, 130, 20, 123),
(1448, '2017-04-21 17:00:00', 11, 15, 260, 130, 20, 123),
(1449, '2017-04-21 17:00:00', 11, 15, 260, 130, 20, 122),
(1450, '2017-04-21 17:00:00', 11, 20, 260, 130, 20, 122),
(1451, '2017-04-21 17:00:00', 11, 17, 260, 130, 20, 122),
(1452, '2017-04-21 17:00:00', 11, 13, 260, 130, 20, 122),
(1453, '2017-04-21 17:00:00', 11, 18, 260, 130, 20, 122),
(1454, '2017-04-21 17:00:00', 11, 19, 260, 130, 20, 122),
(1455, '2017-04-21 17:00:00', 11, 20, 260, 130, 20, 122),
(1456, '2017-04-21 17:00:00', 11, 16, 260, 130, 20, 122),
(1457, '2017-04-21 17:00:00', 11, 18, 260, 130, 20, 123),
(1458, '2017-04-21 17:00:00', 11, 17, 260, 130, 20, 123),
(1459, '2017-04-21 17:00:00', 11, 18, 260, 130, 20, 123),
(1460, '2017-04-21 17:00:00', 11, 21, 260, 130, 20, 123),
(1461, '2017-04-21 17:00:00', 11, 13, 260, 130, 20, 123),
(1462, '2017-04-21 17:00:00', 11, 19, 260, 130, 20, 123),
(1463, '2017-04-21 17:00:00', 11, 21, 260, 130, 20, 123),
(1464, '2017-04-21 17:00:00', 11, 19, 260, 130, 20, 123),
(1465, '2017-04-28 17:00:00', 11, 19, 260, 130, 20, 123),
(1466, '2017-04-28 17:00:00', 11, 16, 260, 130, 20, 123),
(1467, '2017-04-28 17:00:00', 11, 17, 260, 130, 20, 122),
(1468, '2017-04-28 17:00:00', 11, 19, 260, 130, 20, 122),
(1469, '2017-04-28 17:00:00', 11, 18, 260, 130, 20, 122),
(1470, '2017-04-28 17:00:00', 11, 15, 260, 130, 20, 122),
(1471, '2017-04-28 17:00:00', 11, 19, 260, 130, 20, 122),
(1472, '2017-04-28 17:00:00', 11, 14, 260, 130, 20, 122),
(1473, '2017-04-28 17:00:00', 11, 16, 260, 130, 20, 122),
(1474, '2017-04-28 17:00:00', 11, 15, 260, 130, 20, 122),
(1475, '2017-04-28 17:00:00', 11, 16, 260, 130, 20, 123),
(1476, '2017-04-28 17:00:00', 11, 16, 260, 130, 20, 123),
(1477, '2017-04-28 17:00:00', 11, 17, 260, 130, 20, 123),
(1478, '2017-04-28 17:00:00', 11, 15, 260, 130, 20, 123),
(1479, '2017-04-28 17:00:00', 11, 17, 260, 130, 20, 123),
(1480, '2017-04-28 17:00:00', 11, 14, 260, 130, 20, 123),
(1481, '2017-04-28 08:00:00', 11, 17, 260, 130, 20, 123),
(1482, '2017-04-28 08:00:00', 11, 14, 260, 130, 20, 123),
(1483, '2017-04-28 08:00:00', 11, 15, 260, 130, 20, 123),
(1484, '2017-04-28 08:00:00', 11, 15, 260, 130, 20, 123),
(1485, '2017-04-28 08:00:00', 11, 20, 260, 130, 20, 123),
(1486, '2017-04-28 08:00:00', 11, 13, 260, 130, 20, 123),
(1487, '2017-04-28 08:00:00', 11, 21, 260, 130, 20, 122),
(1488, '2017-04-28 08:00:00', 11, 14, 260, 130, 20, 122),
(1489, '2017-04-28 08:00:00', 11, 19, 260, 130, 20, 122),
(1490, '2017-04-28 08:00:00', 11, 14, 260, 130, 20, 122),
(1491, '2017-04-28 08:00:00', 11, 19, 260, 130, 20, 122),
(1492, '2017-04-28 08:00:00', 11, 13, 260, 130, 20, 122),
(1493, '2017-04-28 08:00:00', 11, 18, 260, 130, 20, 122),
(1494, '2017-04-28 08:00:00', 11, 16, 260, 130, 20, 122),
(1495, '2017-04-28 08:00:00', 11, 18, 260, 130, 20, 123),
(1496, '2017-04-28 08:00:00', 11, 20, 260, 130, 20, 123),
(1497, '2017-04-28 08:00:00', 11, 19, 260, 130, 20, 123),
(1498, '2017-04-28 08:00:00', 11, 20, 260, 130, 40, 122),
(1499, '2017-04-28 08:00:00', 11, 21, 260, 130, 40, 122),
(1500, '2017-04-28 08:00:00', 11, 16, 260, 130, 40, 122),
(1501, '2017-04-28 08:00:00', 11, 19, 260, 130, 40, 122),
(1502, '2017-04-28 08:00:00', 11, 21, 260, 130, 40, 122),
(1503, '2017-04-28 08:00:00', 11, 21, 260, 130, 40, 122),
(1504, '2017-04-28 08:00:00', 11, 18, 260, 130, 40, 122),
(1505, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1506, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1507, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1508, '2017-04-21 17:00:00', 11, 20, 260, 130, 40, 122),
(1509, '2017-04-21 17:00:00', 11, 13, 260, 130, 40, 122),
(1510, '2017-04-21 17:00:00', 11, 20, 260, 130, 40, 122),
(1511, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1512, '2017-04-21 17:00:00', 11, 19, 260, 130, 40, 122),
(1513, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1514, '2017-04-21 17:00:00', 11, 21, 260, 130, 40, 122),
(1515, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1516, '2017-04-21 17:00:00', 11, 16, 260, 130, 40, 122),
(1517, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1518, '2017-04-21 17:00:00', 11, 20, 260, 130, 40, 122),
(1519, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1520, '2017-04-21 17:00:00', 11, 19, 260, 130, 40, 122),
(1521, '2017-04-21 17:00:00', 11, 20, 260, 130, 40, 122),
(1522, '2017-04-21 17:00:00', 11, 17, 260, 130, 40, 122),
(1523, '2017-04-21 17:00:00', 11, 19, 260, 130, 40, 122),
(1524, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1525, '2017-04-21 17:00:00', 11, 14, 260, 130, 40, 122),
(1526, '2017-04-21 17:00:00', 11, 15, 260, 130, 40, 122),
(1527, '2017-04-21 17:00:00', 11, 16, 260, 130, 40, 122),
(1528, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1529, '2017-04-21 17:00:00', 11, 19, 260, 130, 40, 122),
(1530, '2017-04-21 17:00:00', 11, 20, 260, 130, 40, 122),
(1531, '2017-04-21 17:00:00', 11, 16, 260, 130, 40, 122),
(1532, '2017-04-21 17:00:00', 11, 17, 260, 130, 40, 122),
(1533, '2017-04-21 17:00:00', 11, 13, 260, 130, 40, 122),
(1534, '2017-04-21 17:00:00', 11, 21, 260, 130, 40, 122),
(1535, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1536, '2017-04-21 17:00:00', 11, 16, 260, 130, 40, 122),
(1537, '2017-04-21 17:00:00', 11, 16, 260, 130, 40, 122),
(1538, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1539, '2017-04-21 17:00:00', 11, 20, 260, 130, 40, 122),
(1540, '2017-04-21 17:00:00', 11, 18, 260, 130, 40, 122),
(1541, '2017-04-21 17:00:00', 11, 16, 260, 130, 40, 123),
(1542, '2017-04-21 17:00:00', 11, 20, 260, 130, 65, 122),
(1543, '2017-04-21 17:00:00', 11, 20, 260, 130, 65, 122),
(1544, '2017-04-21 17:00:00', 11, 20, 260, 130, 65, 122),
(1545, '2017-03-14 17:00:00', 11, 19, 260, 130, 65, 122),
(1546, '2017-03-14 17:00:00', 11, 15, 260, 130, 65, 122),
(1547, '2017-03-14 17:00:00', 11, 16, 260, 130, 65, 122),
(1548, '2017-03-14 17:00:00', 11, 18, 260, 130, 65, 122),
(1549, '2017-03-14 17:00:00', 11, 21, 260, 130, 65, 122),
(1550, '2017-03-14 17:00:00', 11, 18, 260, 130, 65, 122),
(1551, '2017-03-14 17:00:00', 11, 20, 260, 130, 65, 122),
(1552, '2017-04-21 17:00:00', 11, 21, 260, 130, 65, 122),
(1553, '2017-04-21 17:00:00', 11, 21, 260, 130, 65, 122),
(1554, '2017-04-21 17:00:00', 11, 16, 260, 130, 65, 122),
(1555, '2017-05-11 19:00:00', 11, 16, 260, 130, 65, 122),
(1556, '2017-05-11 19:00:00', 11, 18, 260, 130, 65, 122),
(1557, '2017-05-11 19:00:00', 11, 20, 260, 130, 65, 122),
(1558, '2017-05-11 19:00:00', 11, 15, 260, 130, 65, 122),
(1559, '2017-03-14 17:00:00', 11, 15, 260, 130, 65, 122),
(1560, '2017-03-14 17:00:00', 11, 15, 260, 130, 65, 122),
(1561, '2017-03-14 17:00:00', 11, 13, 260, 130, 65, 122),
(1562, '2017-03-14 17:00:00', 11, 16, 260, 130, 65, 122),
(1563, '2017-03-14 17:00:00', 11, 13, 260, 130, 65, 122),
(1564, '2017-03-14 17:00:00', 11, 15, 260, 130, 65, 122),
(1565, '2017-03-14 17:00:00', 11, 18, 260, 130, 65, 122),
(1566, '2017-03-14 17:00:00', 11, 13, 260, 130, 65, 122),
(1567, '2017-05-11 19:00:00', 11, 21, 260, 130, 65, 122),
(1568, '2017-05-11 19:00:00', 11, 21, 260, 130, 65, 122),
(1569, '2017-05-11 19:00:00', 11, 18, 260, 130, 65, 122),
(1570, '2017-05-11 19:00:00', 11, 15, 260, 130, 65, 122),
(1571, '2017-05-11 19:00:00', 11, 21, 260, 130, 65, 122),
(1572, '2017-05-11 19:00:00', 11, 17, 260, 130, 65, 122),
(1573, '2017-05-11 19:00:00', 11, 19, 260, 130, 65, 122),
(1574, '2017-05-11 19:00:00', 11, 20, 260, 130, 65, 122),
(1575, '2017-05-11 19:00:00', 11, 14, 260, 130, 65, 123),
(1576, '2017-05-11 19:00:00', 11, 18, 260, 130, 65, 123),
(1577, '2017-05-11 19:00:00', 11, 18, 260, 130, 65, 122),
(1578, '2017-05-18 18:00:00', 11, 19, 260, 130, 65, 122),
(1579, '2017-05-18 18:00:00', 11, 17, 260, 130, 65, 122),
(1580, '2017-05-18 18:00:00', 11, 20, 260, 130, 65, 122),
(1581, '2017-05-18 18:00:00', 11, 15, 260, 130, 65, 122),
(1582, '2017-05-18 18:00:00', 11, 13, 260, 130, 65, 122),
(1583, '2017-05-18 18:00:00', 11, 15, 260, 130, 65, 122),
(1584, '2017-05-18 18:00:00', 11, 18, 260, 130, 65, 122),
(1585, '2017-05-18 18:00:00', 11, 13, 260, 130, 65, 123),
(1586, '2017-05-18 18:00:00', 11, 15, 260, 130, 65, 122),
(1587, '2017-05-18 18:00:00', 11, 18, 260, 130, 65, 122),
(1588, '2017-05-18 18:00:00', 11, 20, 260, 130, 65, 122),
(1589, '2017-06-02 05:16:00', 11, 21, 260, 130, 76, 122),
(1590, '2017-03-03 06:18:00', 11, 13, 260, 130, 76, 122),
(1591, '2017-06-04 06:51:00', 11, 15, 260, 130, 76, 122),
(1592, '2017-04-05 06:13:00', 11, 20, 260, 130, 76, 122),
(1593, '2017-06-06 06:15:00', 11, 15, 260, 130, 76, 122),
(1594, '2017-03-07 06:27:00', 11, 21, 260, 130, 76, 122),
(1595, '2017-03-08 06:35:00', 11, 16, 260, 130, 76, 122),
(1596, '2017-06-09 06:45:00', 11, 21, 260, 130, 76, 122),
(1597, '2017-04-10 06:52:00', 11, 18, 260, 130, 76, 123),
(1598, '2017-04-11 06:44:00', 11, 19, 260, 130, 76, 122),
(1599, '2017-03-12 06:37:00', 11, 13, 260, 130, 76, 122),
(1600, '2017-04-13 05:25:00', 11, 16, 260, 130, 76, 122),
(1601, '2017-03-14 05:58:00', 11, 21, 260, 130, 76, 122),
(1602, '2017-04-15 05:44:00', 11, 14, 260, 130, 76, 122),
(1603, '2017-06-16 05:33:00', 11, 18, 260, 130, 76, 122),
(1604, '2017-06-17 05:35:00', 11, 20, 260, 130, 76, 122),
(1605, '2017-04-18 05:33:00', 11, 21, 260, 130, 76, 122),
(1606, '2017-04-19 05:27:00', 11, 16, 260, 130, 76, 123),
(1607, '2017-03-20 06:30:00', 11, 20, 260, 130, 76, 123),
(1608, '2017-04-21 06:50:00', 11, 17, 260, 130, 76, 122),
(1609, '2017-03-22 06:38:00', 11, 15, 260, 130, 76, 122),
(1610, '2017-06-23 09:33:00', 11, 14, 260, 130, 76, 122),
(1611, '2017-06-24 09:57:00', 11, 13, 260, 130, 76, 122),
(1612, '2017-06-25 09:55:00', 11, 16, 260, 130, 76, 123),
(1613, '2017-03-26 09:44:00', 11, 19, 260, 130, 76, 122),
(1614, '2017-03-27 09:55:00', 11, 15, 260, 130, 76, 122),
(1615, '2017-03-28 09:14:00', 11, 16, 260, 130, 76, 122),
(1616, '2017-05-01 09:34:00', 11, 20, 260, 130, 76, 122),
(1617, '2017-05-02 09:38:00', 11, 15, 260, 130, 76, 122),
(1618, '2017-05-03 09:23:00', 11, 18, 260, 130, 76, 122),
(1619, '2017-05-04 09:59:00', 11, 18, 260, 130, 76, 122),
(1620, '2017-05-05 09:25:00', 11, 14, 260, 130, 76, 122),
(1621, '2017-05-06 09:59:00', 11, 15, 260, 130, 76, 122),
(1622, '2017-05-07 09:56:00', 11, 18, 260, 130, 76, 122),
(1623, '2017-05-08 09:50:00', 11, 19, 260, 130, 76, 122),
(1624, '2017-05-09 09:59:00', 11, 16, 260, 130, 76, 122),
(1625, '2017-04-10 05:29:00', 11, 18, 260, 130, 76, 122),
(1626, '2017-05-11 05:44:00', 11, 14, 260, 130, 76, 122),
(1627, '2017-05-12 05:41:00', 11, 18, 260, 130, 76, 122),
(1628, '2017-05-13 05:52:00', 11, 19, 260, 130, 76, 122),
(1629, '2017-04-14 09:37:00', 11, 14, 260, 130, 76, 122),
(1630, '2017-05-15 09:52:00', 11, 18, 260, 130, 76, 122),
(1631, '2017-05-16 09:19:00', 11, 14, 260, 130, 76, 122),
(1632, '2017-05-17 09:13:00', 11, 13, 260, 130, 76, 123),
(1633, '2017-05-18 09:40:00', 11, 17, 260, 130, 76, 123),
(1634, '2017-03-14 17:14:00', 11, 13, 260, 130, 100, 122),
(1635, '2017-04-14 17:13:00', 11, 19, 260, 130, 100, 122),
(1636, '2017-03-14 17:33:00', 11, 13, 260, 130, 100, 122),
(1637, '2017-03-14 17:26:00', 11, 16, 260, 130, 100, 122),
(1638, '2017-03-14 17:54:00', 11, 14, 260, 130, 100, 122),
(1639, '2017-03-14 17:10:00', 11, 19, 260, 130, 100, 122),
(1640, '2017-03-14 17:13:00', 11, 21, 260, 130, 100, 122),
(1641, '2017-03-14 17:33:00', 11, 17, 260, 130, 100, 122),
(1642, '2017-05-22 09:29:00', 11, 14, 260, 130, 100, 122),
(1643, '2017-05-22 09:58:00', 11, 14, 260, 130, 100, 122),
(1644, '2017-05-22 09:12:00', 11, 21, 260, 130, 100, 122),
(1645, '2017-05-22 09:26:00', 11, 21, 260, 130, 100, 122),
(1646, '2017-05-22 09:55:00', 11, 18, 260, 130, 100, 122),
(1647, '2017-05-22 09:14:00', 11, 14, 260, 130, 100, 122),
(1648, '2017-05-22 09:35:00', 11, 15, 260, 130, 100, 122),
(1649, '2017-05-22 09:13:00', 11, 20, 260, 130, 100, 122),
(1650, '2017-05-22 09:46:00', 11, 14, 260, 130, 100, 122),
(1651, '2017-05-22 09:27:00', 11, 20, 260, 130, 100, 122),
(1652, '2017-05-22 09:11:00', 11, 14, 260, 130, 100, 122),
(1653, '2017-05-22 09:29:00', 11, 15, 260, 130, 100, 122),
(1654, '2017-05-22 09:23:00', 11, 13, 260, 130, 100, 122),
(1655, '2017-05-22 09:41:00', 11, 19, 260, 130, 100, 123),
(1656, '2017-05-22 09:39:00', 11, 20, 260, 130, 100, 123),
(1657, '2017-05-22 09:35:00', 11, 16, 260, 130, 100, 122),
(1658, '2017-05-22 09:35:00', 11, 19, 260, 130, 100, 122),
(1659, '2017-03-14 17:17:00', 11, 15, 260, 130, 100, 122),
(1660, '2017-03-14 17:18:00', 11, 16, 260, 130, 100, 122),
(1661, '2017-03-14 17:54:00', 11, 21, 260, 130, 100, 122),
(1662, '2017-03-14 17:32:00', 11, 13, 260, 130, 100, 122),
(1663, '2017-03-14 17:28:00', 11, 17, 260, 130, 100, 122),
(1664, '2017-05-22 09:54:00', 11, 21, 260, 130, 100, 122),
(1665, '2017-05-22 09:37:00', 11, 14, 260, 130, 100, 122),
(1666, '2017-05-22 09:43:00', 11, 21, 260, 130, 100, 122),
(1667, '2017-05-22 09:44:00', 11, 16, 260, 130, 100, 122),
(1668, '2017-05-22 09:17:00', 11, 18, 260, 130, 100, 122),
(1669, '2017-05-22 09:33:00', 11, 17, 260, 130, 100, 122),
(1670, '2017-05-22 09:48:00', 11, 14, 260, 130, 100, 122),
(1671, '2017-05-22 09:20:00', 11, 13, 260, 130, 100, 122),
(1672, '2017-05-22 09:11:00', 11, 21, 260, 130, 100, 122),
(1673, '2017-05-22 09:45:00', 11, 16, 260, 130, 100, 122),
(1674, '2017-05-22 09:16:00', 11, 17, 260, 130, 100, 122),
(1675, '2017-05-22 09:10:00', 11, 21, 260, 130, 100, 122),
(1676, '2017-05-22 09:55:00', 11, 20, 260, 130, 100, 122),
(1677, '2017-05-22 09:19:00', 11, 20, 260, 130, 100, 123),
(1678, '2017-05-22 09:26:00', 11, 18, 260, 130, 100, 122),
(1690, '2017-05-19 08:08:00', 11, 21, 260, 130, 76, 122),
(1691, '2017-05-20 08:08:00', 11, 16, 260, 130, 76, 123),
(1692, '2017-04-21 08:09:00', 11, 16, 260, 130, 76, 123),
(1693, '2017-06-22 08:11:00', 11, 15, 260, 130, 76, 123),
(1694, '2017-05-23 08:12:00', 11, 17, 260, 130, 76, 123),
(1695, '2017-04-25 18:52:35', 11, 21, 155, 130, 125, 122),
(1696, '2017-04-25 18:52:47', 11, 19, 155, 130, 125, 123),
(1697, '2017-04-25 18:59:34', 11, 21, 155, 130, 125, 123),
(1698, '2017-04-25 18:59:54', 11, 15, 155, 130, 125, 123),
(1699, '2017-04-25 19:00:02', 11, 21, 155, 130, 125, 123),
(1700, '2017-04-25 19:01:12', 11, 19, 155, 130, 125, 123),
(1701, '2017-04-25 19:01:19', 11, 16, 155, 130, 125, 123),
(1702, '2017-04-25 19:01:31', 11, 16, 155, 130, 125, 123),
(1703, '2017-04-25 19:05:37', 11, 21, 155, 130, 125, 123),
(1704, '2017-04-25 19:05:50', 11, 21, 155, 130, 125, 123),
(1705, '2017-04-25 19:05:53', 11, 19, 155, 130, 125, 123),
(1706, '2017-04-25 19:06:34', 11, 21, 155, 130, 125, 123),
(1707, '2017-04-25 19:08:26', 11, 17, 155, 130, 125, 123),
(1708, '2017-04-25 19:09:40', 11, 20, 155, 130, 125, 123),
(1709, '2017-04-25 19:35:47', 11, 20, 155, 130, 125, 123),
(1710, '2017-04-25 19:35:50', 11, 20, 155, 130, 125, 123),
(1711, '2017-04-25 19:36:07', 11, 15, 155, 130, 125, 123),
(1712, '2017-04-25 19:37:07', 11, 19, 155, 130, 125, 123),
(1715, '2017-04-25 20:22:24', 11, 21, 155, 130, 125, 123),
(1716, '2017-04-25 20:23:11', 11, 21, 155, 130, 125, 123),
(1717, '2017-04-25 20:23:16', 11, 17, 155, 130, 125, 123),
(1718, '2017-04-25 20:23:27', 11, 20, 155, 130, 125, 123),
(1719, '2017-04-25 20:23:31', 11, 15, 155, 130, 125, 123),
(1720, '2017-04-25 20:23:48', 11, 15, 155, 130, 125, 123),
(1722, '2017-04-25 20:25:44', 11, 20, 155, 130, 125, 123),
(1723, '2017-04-25 20:26:07', 11, 18, 155, 130, 125, 123),
(1724, '2017-04-25 20:26:14', 11, 15, 155, 130, 125, 123),
(1725, '2017-04-25 20:26:33', 11, 17, 155, 130, 125, 123),
(1726, '2017-04-25 20:26:37', 11, 15, 155, 130, 125, 123),
(1727, '2017-04-25 20:26:41', 11, 19, 155, 130, 125, 123),
(1728, '2017-04-25 20:27:01', 11, 19, 155, 130, 125, 123),
(1729, '2017-03-26 23:16:26', 12, 19, 155, 130, 139, 122),
(1730, '2017-03-26 23:29:06', 12, 20, 155, 130, 133, 122),
(1731, '2017-03-26 23:30:50', 12, 16, 155, 130, 136, 122),
(1732, '2017-03-27 20:33:55', 12, 15, 155, 130, 139, 122),
(1733, '2017-03-27 20:35:27', 12, 16, 155, 130, 139, 123),
(1734, '2017-03-27 20:35:32', 12, 18, 155, 130, 139, 123),
(1735, '2017-03-27 20:36:37', 12, 17, 155, 130, 139, 123),
(1736, '2017-03-27 21:44:53', 12, 17, 155, 130, 139, 123),
(1737, '2017-03-27 21:45:34', 12, 16, 155, 130, 139, 123),
(1738, '2017-03-27 21:47:06', 12, 15, 155, 130, 139, 123),
(1739, '2017-03-27 21:49:36', 12, 15, 155, 130, 138, 122),
(1740, '2017-03-27 21:51:06', 12, 15, 155, 130, 138, 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `badgeperperson`
--

CREATE TABLE `badgeperperson` (
  `idBPP` int(11) NOT NULL,
  `idSession` int(11) DEFAULT NULL,
  `idBadge` int(11) DEFAULT NULL,
  `dt_Badge` datetime DEFAULT NULL,
  `idNutri` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `badgeperperson`
--

INSERT INTO `badgeperperson` (`idBPP`, `idSession`, `idBadge`, `dt_Badge`, `idNutri`) VALUES
(2, 113, 1, '2016-04-25 00:00:00', 205),
(6, 100, 5, '2016-10-08 11:19:50', 205),
(7, 100, 13, '2016-10-08 11:19:50', 205),
(40, 113, 13, '2016-10-08 15:33:00', 205),
(43, 76, 1, '2016-10-08 17:38:57', 205),
(45, 76, 3, '2016-10-08 17:38:59', 205),
(46, 76, 13, '2016-10-08 17:39:49', 205),
(48, 1, 14, '2016-10-08 18:26:00', 205),
(49, 76, 14, '2016-10-08 18:29:28', 205),
(50, 1, 1, '2017-02-12 13:36:16', 205),
(51, 1, 3, '2017-02-12 13:36:24', 205),
(53, 98, 1, '2017-02-12 15:10:48', 205),
(54, 2, 1, '2017-02-12 15:12:01', 205),
(55, 9, 1, '2017-02-12 15:12:17', 205),
(59, 125, 3, '2017-03-04 23:50:23', 205),
(61, 3, 1, '2017-03-12 20:31:15', 205),
(65, 139, 14, '2017-03-12 21:21:06', 205),
(66, 139, 1, '2017-03-12 21:27:05', 205),
(67, 139, 13, '2017-03-12 21:36:34', 205),
(68, 125, 1, '2017-03-26 17:17:36', 205);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `badges`
--

CREATE TABLE `badges` (
  `idBadge` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `badges`
--

INSERT INTO `badges` (`idBadge`, `name`, `description`, `value`, `image`) VALUES
(1, 'chicharron con pelos', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, 'medal13-512.png'),
(3, 'Campo Santa Marta', 'You should really accept jonstjohn answer The number of upvotes on both the question and answer indicates a lot of people have run into this.', 150, 'medal11-512.png'),
(5, 'Destamalizate 2017', 'more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 1000, '5.png'),
(13, 'sarajuana', ' It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages', 200, '13.png'),
(14, 'eeee', 'eeee', 0, '14.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalog`
--

CREATE TABLE `catalog` (
  `idCatalog` int(11) NOT NULL,
  `nameCatalog` varchar(200) DEFAULT NULL,
  `idMeta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catalog`
--

INSERT INTO `catalog` (`idCatalog`, `nameCatalog`, `idMeta`) VALUES
(3, 'Semana', 1),
(4, 'Mes', 1),
(5, 'Temporada', 1),
(6, 'Integrante', 2),
(7, 'Nutricionista', 2),
(8, 'Administrador', 2),
(9, 'Entrenador', 2),
(10, 'TigoFit Plaza Tigo', 3),
(11, 'TigoFit zona 4', 3),
(12, 'TigoFit zona 12', 3),
(13, 'Bicicleta', 4),
(14, 'Carrera', 4),
(15, 'Fitness', 4),
(16, 'Runners', 4),
(17, 'Gimnasio', 4),
(18, 'Podometro', 4),
(19, 'Prueba de Esfuerzo', 4),
(20, 'Yoga', 4),
(21, 'Zumba', 4),
(22, 'Peso', 5),
(23, 'Porcentaje Grasa Visceral', 5),
(24, 'Porcentaje De Agua', 5),
(25, 'Edad Metabolica', 5),
(26, 'Cadera', 5),
(27, 'Cintura', 5),
(29, 'Presion Arterial - PA', 5),
(30, 'Frecuencia Cardiaca - FC', 5),
(31, 'Podometro', 5),
(32, 'Indice de Masa Corporal - IMC', 5),
(33, 'Porcentaje De Grasa Corporal', 5),
(34, 'Indice Cintura Cadera - ICC', 5),
(36, 'Nuevo', 6),
(37, 'Reingreso', 6),
(38, 'De Baja', 6),
(48, 'Aderezos y Salsas', 9),
(49, 'Ensaladas y Sopas', 9),
(50, 'Licuados y Bebidas', 9),
(51, 'Platos Fuertes', 9),
(52, 'Postres Saludables', 9),
(53, 'Epocas Festivas', 9),
(54, 'Sin Azucar', 9),
(55, 'Snacks Saludables', 9),
(56, 'De Verano', 9),
(57, 'Sustitutos', 9),
(58, 'Calentamiento Inicial', 10),
(59, 'Principiantes', 10),
(60, 'Intermedios', 10),
(61, 'Avanzados', 10),
(62, 'Calentamiento Final', 10),
(63, 'Estilo de Vida', 11),
(64, 'Alimentacion', 11),
(65, 'Recetas', 12),
(66, 'Articulos', 12),
(67, 'Videos', 12),
(69, 'Solicitada', 13),
(72, 'El mismo dia', 13),
(73, 'medida inicial', 32),
(74, 'medida meta', 32),
(122, 'Ordinario', 31),
(123, 'Extraordinario', 31),
(124, 'medida actual', 32),
(127, 'No se presento', 13),
(128, 'Hombre', 33),
(129, 'Mujer', 33),
(130, 'Dia', 1),
(131, 'Motivacional', 34),
(132, 'Fuerza', 34),
(133, 'Paz', 34),
(134, 'Salud', 34),
(135, 'Realizada', 13),
(137, 'Underfat', 35),
(138, 'Healthy', 35),
(139, 'Overfat', 35),
(140, 'Obese', 35),
(141, 'LowRisk', 35),
(142, 'ModerateRisk', 35),
(143, 'HighRisk', 35),
(144, 'Valid', 35),
(145, 'Excesive', 35),
(146, 'Normal', 35),
(147, 'Risk', 35),
(148, 'yellow', 36),
(149, 'green', 36),
(150, 'red', 36),
(151, 'blue', 36),
(152, 'Dietas', 12),
(153, 'Esfuerzo', 12),
(154, 'Event', 37),
(155, 'Nutricionist', 37),
(156, 'Badge', 37),
(157, 'Dietas', 37),
(158, 'Esfuerzo', 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chocale`
--

CREATE TABLE `chocale` (
  `idChocale` int(11) NOT NULL,
  `idSession` int(11) DEFAULT NULL,
  `idGiver` int(11) DEFAULT NULL,
  `dt_Chocale` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `chocale`
--

INSERT INTO `chocale` (`idChocale`, `idSession`, `idGiver`, `dt_Chocale`) VALUES
(2, 41, 114, '2017-02-26 09:00:00'),
(3, 66, 136, '2017-04-06 09:00:00'),
(5, 20, 260, '2017-05-18 09:00:00'),
(6, 40, 187, '2017-05-19 09:00:00'),
(7, 65, 209, '2017-06-06 09:00:00'),
(10, 101, 195, '2017-12-14 10:00:00'),
(12, 98, 170, '2017-04-22 10:00:00'),
(13, 21, 262, '2017-06-06 10:00:00'),
(34, 101, 302, '2017-01-04 13:00:00'),
(38, 41, 176, '2017-06-01 13:00:00'),
(39, 66, 136, '2017-09-15 13:00:00'),
(40, 77, 294, '2017-10-11 13:00:00'),
(42, 40, 187, '2017-12-26 13:00:00'),
(44, 76, 273, '2017-06-23 14:00:00'),
(45, 99, 151, '2017-07-05 14:00:00'),
(46, 101, 178, '2017-07-18 14:00:00'),
(50, 41, 209, '2017-11-26 14:00:00'),
(52, 77, 120, '2017-03-29 14:00:00'),
(53, 20, 195, '2017-06-18 14:00:00'),
(57, 99, 309, '2017-02-26 15:00:00'),
(58, 101, 204, '2017-02-28 15:00:00'),
(61, 21, 230, '2017-07-11 15:00:00'),
(65, 20, 232, '2017-12-03 16:00:00'),
(74, 41, 205, '2017-12-05 17:00:00'),
(80, 76, 251, '2017-05-18 18:00:00'),
(81, 99, 258, '2017-05-19 18:00:00'),
(84, 98, 268, '2017-12-08 19:00:00'),
(85, 21, 154, '2017-12-14 19:00:00'),
(86, 41, 235, '2017-01-05 19:00:00'),
(89, 20, 120, '2017-10-04 19:00:00'),
(93, 99, 134, '2017-05-19 20:00:00'),
(100, 77, 244, '2017-12-14 21:00:00'),
(102, 40, 168, '2017-06-28 09:00:00'),
(104, 76, 138, '2017-08-09 09:00:00'),
(111, 66, 114, '2017-05-08 10:00:00'),
(112, 77, 268, '2017-05-11 10:00:00'),
(113, 20, 179, '2017-06-01 10:00:00'),
(114, 40, 141, '2017-09-15 10:00:00'),
(121, 21, 202, '2017-07-18 11:00:00'),
(127, 65, 278, '2017-03-29 12:00:00'),
(129, 99, 128, '2017-10-25 12:00:00'),
(130, 101, 251, '2017-11-27 12:00:00'),
(131, 100, 187, '2017-12-25 12:00:00'),
(143, 100, 120, '2017-04-25 14:00:00'),
(145, 21, 232, '2017-08-08 14:00:00'),
(147, 66, 120, '2017-10-05 14:00:00'),
(149, 20, 117, '2017-12-05 15:00:00'),
(150, 40, 159, '2017-12-29 15:00:00'),
(155, 100, 138, '2017-03-11 15:00:00'),
(156, 98, 235, '2017-03-21 16:00:00'),
(161, 20, 151, '2017-09-18 16:00:00'),
(162, 40, 187, '2017-11-30 16:00:00'),
(163, 65, 176, '2017-01-11 17:00:00'),
(166, 101, 268, '2017-04-01 17:00:00'),
(167, 100, 120, '2017-04-09 17:00:00'),
(169, 21, 277, '2017-11-06 17:00:00'),
(177, 99, 152, '2017-03-24 18:00:00'),
(179, 100, 115, '2017-05-09 18:00:00'),
(180, 98, 144, '2017-07-27 18:00:00'),
(181, 21, 275, '2017-11-24 18:00:00'),
(182, 41, 274, '2017-01-06 18:00:00'),
(184, 77, 232, '2017-05-02 19:00:00'),
(185, 20, 243, '2017-05-03 19:00:00'),
(189, 99, 121, '2017-09-15 19:00:00'),
(190, 101, 235, '2017-09-18 19:00:00'),
(191, 100, 176, '2017-12-12 19:00:00'),
(193, 21, 294, '2017-03-14 20:00:00'),
(198, 40, 128, '2017-08-29 20:00:00'),
(199, 65, 189, '2017-10-15 20:00:00'),
(210, 40, 202, '2017-12-14 10:00:00'),
(211, 65, 120, '2017-01-05 10:00:00'),
(214, 101, 161, '2017-10-04 10:00:00'),
(225, 99, 260, '2017-12-14 11:00:00'),
(226, 101, 121, '2017-03-06 12:00:00'),
(228, 98, 176, '2017-07-12 12:00:00'),
(230, 41, 117, '2017-08-18 12:00:00'),
(231, 66, 152, '2017-09-02 12:00:00'),
(232, 77, 302, '2017-09-25 12:00:00'),
(234, 40, 134, '2017-01-04 13:00:00'),
(236, 76, 273, '2017-05-08 13:00:00'),
(238, 101, 147, '2017-06-01 13:00:00'),
(243, 66, 117, '2017-12-31 14:00:00'),
(244, 77, 165, '2017-06-23 14:00:00'),
(246, 40, 277, '2017-07-18 14:00:00'),
(257, 20, 155, '2017-02-26 15:00:00'),
(258, 40, 144, '2017-02-28 15:00:00'),
(261, 99, 114, '2017-07-11 15:00:00'),
(262, 101, 155, '2017-08-28 16:00:00'),
(266, 41, 189, '2017-01-04 16:00:00'),
(267, 66, 202, '2017-04-14 16:00:00'),
(269, 20, 260, '2017-06-17 17:00:00'),
(272, 76, 243, '2017-10-05 17:00:00'),
(275, 100, 159, '2017-12-29 17:00:00'),
(277, 21, 178, '2017-02-26 17:00:00'),
(281, 20, 187, '2017-05-19 18:00:00'),
(282, 40, 159, '2017-06-06 18:00:00'),
(293, 20, 128, '2017-05-19 20:00:00'),
(298, 101, 273, '2017-11-20 20:00:00'),
(301, 21, 258, '2017-03-06 09:00:00'),
(305, 20, 128, '2017-08-18 09:00:00'),
(308, 76, 230, '2017-10-25 09:00:00'),
(309, 99, 209, '2017-01-04 09:00:00'),
(312, 98, 114, '2017-05-11 10:00:00'),
(314, 41, 205, '2017-09-15 10:00:00'),
(317, 20, 204, '2017-12-26 11:00:00'),
(323, 100, 138, '2017-08-24 11:00:00'),
(324, 98, 115, '2017-09-16 11:00:00'),
(325, 21, 273, '2017-11-26 11:00:00'),
(327, 66, 116, '2017-03-29 12:00:00'),
(329, 20, 191, '2017-10-25 12:00:00'),
(331, 65, 134, '2017-12-25 12:00:00'),
(344, 76, 121, '2017-06-17 14:00:00'),
(347, 100, 159, '2017-10-05 14:00:00'),
(350, 41, 117, '2017-12-29 15:00:00'),
(352, 77, 116, '2017-02-24 15:00:00'),
(353, 20, 187, '2017-02-25 15:00:00'),
(361, 4, 155, '2017-09-18 16:00:00'),
(372, 98, 170, '2017-05-10 17:00:00'),
(373, 21, 178, '2017-11-02 18:00:00'),
(378, 40, 232, '2017-04-20 18:00:00'),
(379, 65, 147, '2017-05-09 18:00:00'),
(389, 4, 195, '2017-09-15 19:00:00'),
(395, 100, 310, '2017-06-09 20:00:00'),
(396, 98, 294, '2017-06-30 20:00:00'),
(452, 76, 229, '2017-08-02 22:55:32'),
(467, 3, 229, '2017-08-28 20:38:47'),
(472, 2, 229, '2017-09-19 21:50:39'),
(473, 1, 229, '2017-10-04 23:14:36'),
(482, 4, 229, '2017-03-05 00:00:00'),
(483, 4, 310, '2017-03-09 00:00:00'),
(484, 4, 328, '2017-03-09 23:53:56'),
(495, 9, 328, '2017-03-10 00:12:20'),
(496, 12, 328, '2017-03-10 00:13:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contentperperson`
--

CREATE TABLE `contentperperson` (
  `id` int(11) NOT NULL,
  `idCont` int(11) DEFAULT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `idNutri` int(11) DEFAULT NULL,
  `dateContent` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contentperperson`
--

INSERT INTO `contentperperson` (`id`, `idCont`, `idPerson`, `idNutri`, `dateContent`) VALUES
(1, 11, 229, 205, '2017-03-18 20:27:28'),
(2, 12, 229, 205, '2017-03-18 20:27:28'),
(3, 13, 229, 205, '2017-03-18 20:27:28'),
(7, 14, 229, 205, '2017-03-18 20:27:28'),
(8, 15, 229, 205, '2017-03-18 20:27:28'),
(48, 71, 152, 205, '2017-03-18 20:27:28'),
(49, 71, 278, 205, '2017-03-18 20:27:28'),
(50, 71, 179, 205, '2017-03-18 20:27:28'),
(51, 71, 159, 205, '2017-03-18 20:27:28'),
(52, 71, 165, 205, '2017-03-18 20:27:28'),
(53, 71, 234, 205, '2017-03-18 20:27:28'),
(54, 71, 187, 205, '2017-03-18 20:27:28'),
(55, 71, 230, 205, '2017-03-18 20:27:28'),
(56, 71, 134, 205, '2017-03-18 20:27:28'),
(57, 71, 277, 205, '2017-03-18 20:27:28'),
(58, 71, 178, 205, '2017-03-18 20:27:28'),
(59, 71, 176, 205, '2017-03-18 20:27:28'),
(60, 71, 115, 205, '2017-03-18 20:27:28'),
(61, 71, 168, 205, '2017-03-18 20:27:28'),
(62, 71, 114, 205, '2017-03-18 20:27:28'),
(63, 71, 116, 205, '2017-03-18 20:27:28'),
(64, 71, 275, 205, '2017-03-18 20:27:28'),
(65, 71, 138, 205, '2017-03-18 20:27:28'),
(66, 71, 262, 205, '2017-03-18 20:27:28'),
(67, 71, 229, 205, '2017-03-18 20:27:28'),
(68, 71, 202, 205, '2017-03-18 20:27:28'),
(69, 71, 209, 205, '2017-03-18 20:27:28'),
(70, 71, 268, 205, '2017-03-18 20:27:28'),
(71, 71, 310, 205, '2017-03-18 20:27:28'),
(72, 71, 243, 205, '2017-03-18 20:27:28'),
(73, 71, 161, 205, '2017-03-18 20:27:28'),
(74, 71, 235, 205, '2017-03-18 20:27:28'),
(75, 71, 189, 205, '2017-03-18 20:27:28'),
(76, 71, 121, 205, '2017-03-18 20:27:28'),
(77, 71, 144, 205, '2017-03-18 20:27:28'),
(78, 71, 232, 205, '2017-03-18 20:27:28'),
(79, 71, 258, 205, '2017-03-18 20:27:28'),
(80, 71, 274, 205, '2017-03-18 20:27:28'),
(81, 71, 143, 205, '2017-03-18 20:27:28'),
(82, 71, 170, 205, '2017-03-18 20:27:28'),
(83, 71, 204, 205, '2017-03-18 20:27:28'),
(84, 71, 244, 205, '2017-03-18 20:27:28'),
(85, 71, 251, 205, '2017-03-18 20:27:28'),
(86, 72, 229, 205, '2017-03-18 20:27:28'),
(87, 72, 161, 205, '2017-03-18 20:27:28'),
(88, 73, 202, 205, '2017-03-18 20:27:28'),
(89, 73, 189, 205, '2017-03-18 20:27:28'),
(90, 74, 165, 205, '2017-03-18 20:27:28'),
(91, 74, 235, 205, '2017-03-18 20:27:28'),
(92, 74, 134, 205, '2017-03-18 20:27:28'),
(93, 74, 230, 205, '2017-03-18 20:27:28'),
(94, 74, 278, 205, '2017-03-18 20:27:28'),
(95, 74, 187, 205, '2017-03-18 20:27:28'),
(99, 86, 229, 205, '2017-03-18 20:27:28'),
(117, 16, 314, 205, '2017-03-18 20:27:28'),
(118, 17, 314, 205, '2017-03-18 20:27:28'),
(119, 18, 314, 205, '2017-03-18 20:27:28'),
(120, 19, 314, 205, '2017-03-18 20:27:28'),
(121, 20, 314, 205, '2017-03-18 20:27:28'),
(122, 21, 314, 205, '2017-03-18 20:27:28'),
(123, 22, 314, 205, '2017-03-18 20:27:28'),
(124, 23, 314, 205, '2017-03-18 20:27:28'),
(125, 26, 314, 205, '2017-03-18 20:27:28'),
(126, 55, 314, 205, '2017-03-18 20:27:28'),
(127, 56, 314, 205, '2017-03-18 20:27:28'),
(128, 57, 314, 205, '2017-03-18 20:27:28'),
(129, 58, 314, 205, '2017-03-18 20:27:28'),
(130, 87, 314, 205, '2017-03-18 20:27:28'),
(131, 88, 314, 205, '2017-03-18 20:27:28'),
(132, 89, 314, 205, '2017-03-18 20:27:28'),
(133, 20, 328, 205, '2017-03-10 20:27:28'),
(134, 21, 328, 205, '2017-03-12 20:27:28'),
(135, 22, 328, 205, '2017-03-16 20:27:28'),
(136, 23, 328, 205, '2017-03-18 20:27:28'),
(137, 95, 328, 205, '2017-03-18 21:22:05'),
(138, 96, 328, 205, '2017-03-26 19:31:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detailsnutriappoint`
--

CREATE TABLE `detailsnutriappoint` (
  `idDetails` int(11) NOT NULL,
  `idNutriAppoint` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detailsnutriappoint`
--

INSERT INTO `detailsnutriappoint` (`idDetails`, `idNutriAppoint`, `data`, `value`) VALUES
(1, 19, 22, '71'),
(2, 19, 23, '14'),
(3, 19, 24, '73'),
(4, 19, 25, '120'),
(5, 19, 26, '91'),
(6, 19, 27, '99'),
(7, 19, 29, '8'),
(8, 19, 30, '24'),
(9, 19, 31, '12090'),
(10, 19, 32, '22'),
(11, 19, 33, '24'),
(12, 19, 34, '0.82'),
(13, 27, 22, '343'),
(14, 27, 23, '7'),
(15, 27, 24, '41'),
(16, 27, 25, '61'),
(17, 27, 26, '182'),
(18, 27, 27, '133'),
(19, 27, 29, '43'),
(20, 27, 30, '97'),
(21, 27, 31, '23933'),
(22, 27, 32, '20'),
(23, 27, 33, '48'),
(24, 27, 34, '0.89'),
(25, 21, 22, '80'),
(26, 21, 23, '11'),
(27, 21, 24, '37'),
(28, 21, 25, '53'),
(29, 21, 26, '143'),
(30, 21, 27, '138'),
(31, 21, 29, '4'),
(32, 21, 30, '88'),
(33, 21, 31, '28'),
(34, 21, 32, '21'),
(35, 21, 33, '43'),
(36, 21, 34, '0.93'),
(37, 17, 22, '59'),
(38, 17, 23, '11'),
(39, 17, 24, '54'),
(40, 17, 25, '118'),
(41, 17, 26, '159'),
(42, 17, 27, '56'),
(43, 17, 29, '92'),
(44, 17, 30, '58'),
(45, 17, 31, '19259'),
(46, 17, 32, '22'),
(47, 17, 33, '35'),
(48, 17, 34, '0.83'),
(49, 18, 22, '365'),
(50, 18, 23, '6'),
(51, 18, 24, '58'),
(52, 18, 25, '22'),
(53, 18, 26, '141'),
(54, 18, 27, '182'),
(55, 18, 29, '32'),
(56, 18, 30, '11'),
(57, 18, 31, '18706'),
(58, 18, 32, '23'),
(59, 18, 33, '20'),
(60, 18, 34, '0.88'),
(61, 6, 22, '52'),
(62, 6, 23, '6'),
(63, 6, 24, '41'),
(64, 6, 25, '39'),
(65, 6, 26, '109'),
(66, 6, 27, '188'),
(67, 6, 29, '19'),
(68, 6, 30, '92'),
(69, 6, 31, '24355'),
(70, 6, 32, '32'),
(71, 6, 33, '26'),
(72, 6, 34, '0.88'),
(73, 7, 22, '279'),
(74, 7, 23, '14'),
(75, 7, 24, '78'),
(76, 7, 25, '24'),
(77, 7, 26, '44'),
(78, 7, 27, '59'),
(79, 7, 29, '\n30'),
(80, 7, 30, '338'),
(81, 7, 31, '33207'),
(82, 7, 32, '35'),
(83, 7, 33, '7'),
(84, 7, 34, '0.98'),
(85, 8, 22, '300'),
(86, 8, 23, '14'),
(87, 8, 24, '55'),
(88, 8, 25, '101'),
(89, 8, 26, '68'),
(90, 8, 27, '130'),
(91, 8, 29, '35'),
(92, 8, 30, '3'),
(93, 8, 31, '37243'),
(94, 8, 32, '20'),
(95, 8, 33, '42'),
(96, 8, 34, '0.96'),
(109, 10, 22, '388'),
(110, 10, 23, '8'),
(111, 10, 24, '36'),
(112, 10, 25, '64'),
(113, 10, 26, '179'),
(114, 10, 27, '155'),
(115, 10, 29, '91'),
(116, 10, 30, '57'),
(117, 10, 31, '26421'),
(118, 10, 32, '31'),
(119, 10, 33, '12'),
(120, 10, 34, '0.84'),
(133, 12, 22, '171'),
(134, 12, 23, '5'),
(135, 12, 24, '40'),
(136, 12, 25, '33'),
(137, 12, 26, '65'),
(138, 12, 27, '138'),
(139, 12, 29, '4'),
(140, 12, 30, '70'),
(141, 12, 31, '15291'),
(142, 12, 32, '23'),
(143, 12, 33, '29'),
(144, 12, 34, '0.91'),
(145, 13, 22, '196'),
(146, 13, 23, '16'),
(147, 13, 24, '70'),
(148, 13, 25, '92'),
(149, 13, 26, '198'),
(150, 13, 27, '59'),
(151, 13, 29, '23'),
(152, 13, 30, '78'),
(153, 13, 31, '5137'),
(154, 13, 32, '38'),
(155, 13, 33, '6'),
(156, 13, 34, '0.87'),
(157, 14, 22, '240'),
(158, 14, 23, '5'),
(159, 14, 24, '39'),
(160, 14, 25, '54'),
(161, 14, 26, '181'),
(162, 14, 27, '85'),
(163, 14, 29, '69'),
(164, 14, 30, '51'),
(165, 14, 31, '31894'),
(166, 14, 32, '36'),
(167, 14, 33, '26'),
(168, 14, 34, '0.83'),
(169, 15, 22, '355'),
(170, 15, 23, '12'),
(171, 15, 24, '54'),
(172, 15, 25, '88'),
(173, 15, 26, '192'),
(174, 15, 27, '180'),
(175, 15, 29, '15'),
(176, 15, 30, '33'),
(177, 15, 31, '37486'),
(178, 15, 32, '31'),
(179, 15, 33, '49'),
(180, 15, 34, '0.92'),
(181, 24, 22, '350'),
(182, 24, 23, '350'),
(183, 24, 24, '350'),
(184, 24, 25, '150'),
(185, 24, 33, '85'),
(186, 24, 27, '90'),
(187, 1, 22, '220'),
(188, 1, 23, '9'),
(189, 1, 24, '58'),
(190, 1, 25, '19'),
(191, 1, 26, '73'),
(192, 1, 27, '145'),
(193, 1, 29, '30'),
(194, 1, 30, '81'),
(195, 1, 31, '6651'),
(196, 1, 32, '40'),
(197, 1, 33, '8'),
(198, 1, 34, '0.98'),
(199, 2, 22, '195'),
(200, 2, 23, '9'),
(201, 2, 24, '61'),
(202, 2, 25, '36'),
(203, 2, 26, '55'),
(204, 2, 27, '166'),
(205, 2, 29, '30'),
(206, 2, 30, '77'),
(207, 2, 31, '18712'),
(208, 2, 32, '34'),
(209, 2, 33, '36'),
(210, 2, 34, '0.89'),
(211, 3, 22, '184'),
(212, 3, 23, '10'),
(213, 3, 24, '73'),
(214, 3, 25, '37'),
(215, 3, 26, '121'),
(216, 3, 27, '162'),
(217, 3, 29, '40'),
(218, 3, 30, '39'),
(219, 3, 31, '8450'),
(220, 3, 32, '40'),
(221, 3, 33, '19'),
(222, 3, 34, '0.96'),
(259, 20, 22, '161'),
(260, 20, 23, '9'),
(261, 20, 24, '76'),
(262, 20, 25, '52'),
(263, 20, 26, '62'),
(264, 20, 27, '100'),
(265, 20, 29, '36'),
(266, 20, 30, '28'),
(267, 20, 31, '15998'),
(268, 20, 32, '38'),
(269, 20, 33, '27'),
(270, 20, 34, '0.96'),
(271, 22, 22, '96'),
(272, 22, 23, '9'),
(273, 22, 24, '75'),
(274, 22, 25, '18'),
(275, 22, 26, '104'),
(276, 22, 27, '81'),
(277, 22, 29, '34'),
(278, 22, 30, '38'),
(279, 22, 31, '7739'),
(280, 22, 32, '36'),
(281, 22, 33, '24'),
(282, 22, 34, '0.96'),
(283, 23, 22, '120'),
(284, 23, 23, '14'),
(285, 23, 24, '42'),
(286, 23, 25, '39'),
(287, 23, 26, '61'),
(288, 23, 27, '77'),
(289, 23, 29, '22'),
(290, 23, 30, '64'),
(291, 23, 31, '31800'),
(292, 23, 32, '38'),
(293, 23, 33, '16'),
(294, 23, 34, '0.91'),
(295, 26, 22, '207'),
(296, 26, 23, '10'),
(297, 26, 24, '65'),
(298, 26, 25, '52'),
(299, 26, 26, '125'),
(300, 26, 27, '121'),
(301, 26, 29, '30'),
(302, 26, 30, '64'),
(303, 26, 31, '5950'),
(304, 26, 32, '34'),
(305, 26, 33, '43'),
(306, 26, 34, '0.94'),
(307, 24, 26, '73'),
(308, 24, 29, '30'),
(309, 24, 30, '81'),
(310, 24, 31, '6651'),
(311, 24, 32, '40'),
(312, 24, 34, '0.98'),
(314, 16, 34, '.94'),
(315, 16, 26, '88'),
(316, 16, 24, '87'),
(317, 16, 22, '150'),
(318, 30, 22, '150'),
(319, 30, 23, '8'),
(320, 30, 24, '20'),
(321, 32, 22, '180'),
(322, 32, 23, '8'),
(593, 63, 34, NULL),
(594, 63, 33, NULL),
(595, 63, 32, NULL),
(596, 63, 31, NULL),
(597, 63, 30, NULL),
(598, 63, 29, NULL),
(599, 63, 27, NULL),
(600, 63, 26, NULL),
(601, 63, 25, NULL),
(602, 63, 24, NULL),
(603, 63, 23, '5'),
(604, 63, 22, '20'),
(605, 64, 34, NULL),
(606, 64, 33, NULL),
(607, 64, 32, NULL),
(608, 64, 31, NULL),
(609, 64, 30, NULL),
(610, 64, 29, NULL),
(611, 64, 27, NULL),
(612, 64, 26, NULL),
(613, 64, 25, NULL),
(614, 64, 24, '40'),
(615, 64, 23, NULL),
(616, 64, 22, NULL),
(617, 65, 34, NULL),
(618, 65, 33, NULL),
(619, 65, 32, NULL),
(620, 65, 31, NULL),
(621, 65, 30, NULL),
(622, 65, 29, NULL),
(623, 65, 27, NULL),
(624, 65, 26, NULL),
(625, 65, 25, NULL),
(626, 65, 24, NULL),
(627, 65, 23, NULL),
(628, 65, 22, NULL),
(629, 66, 34, NULL),
(630, 66, 33, NULL),
(631, 66, 32, NULL),
(632, 66, 31, NULL),
(633, 66, 30, NULL),
(634, 66, 29, NULL),
(635, 66, 27, NULL),
(636, 66, 26, NULL),
(637, 66, 25, NULL),
(638, 66, 24, NULL),
(639, 66, 23, NULL),
(640, 66, 22, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `digitalcontent`
--

CREATE TABLE `digitalcontent` (
  `idCont` int(11) NOT NULL,
  `nameCont` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `valueCont` mediumtext,
  `contType` int(11) DEFAULT NULL,
  `contCategory` int(11) DEFAULT NULL,
  `beginDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `digitalcontent`
--

INSERT INTO `digitalcontent` (`idCont`, `nameCont`, `thumbnail`, `valueCont`, `contType`, `contCategory`, `beginDate`, `endDate`) VALUES
(11, 'Sentadilla', 'AP-mR5C9tr4', 'AP-mR5C9tr4', 67, 59, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(12, 'Despechada', 'R1jJBJr_oR0', 'R1jJBJr_oR0', 67, 59, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(13, 'Payasitos', '9jatDFKEQmU', '9jatDFKEQmU', 67, 59, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(14, 'Abdominales', '5_5Noi0ABqA', '5_5Noi0ABqA', 67, 59, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(15, 'Saltar la cuerda', 'z8FAoKTaL9A', 'z8FAoKTaL9A', 67, 59, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(16, 'Como comer saludable cuando viajas', 'thumb1.jpg', 'article1.pdf', 153, NULL, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(17, 'Que debo comer si me enfermo de gripe', 'thumb2.jpg', 'article2.pdf', 153, NULL, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(18, 'Que realmente quiere decir LIGHT', 'thumb3.jpg', 'article3.pdf', 153, NULL, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(19, 'Alimentos para prevenir migranas', 'thumb4.jpg', 'article4.pdf', 153, NULL, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(20, 'Beneficios de la Quinoa', 'thumb5.jpg', 'article5.pdf', 153, NULL, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(21, 'Como disminuir tu consumo de sal', 'thumb6.jpg', 'article6.pdf', 153, NULL, '2017-03-01 10:48:00', '2017-03-28 10:48:00'),
(22, 'greek-style flank steak with tangy yogurt sauce', 'thumb7.jpg', 'recipe6.pdf', 152, NULL, '2017-03-01 00:52:00', '2017-03-28 00:00:00'),
(23, 'thai-style chicken curry', 'thumb8.jpg', 'recipe5.pdf', 152, NULL, '2017-03-01 00:00:00', '2017-03-28 00:00:00'),
(26, 'pork mignons with french applesauce', 'thumb11.jpg', 'recipe2.pdf', 152, NULL, '2017-03-01 00:00:00', '2017-03-28 00:00:00'),
(55, 'The crossfit journal', 'thumb3.jpg', 'diet3.pdf', 152, NULL, '2017-03-01 00:00:00', '2017-03-28 00:00:00'),
(56, 'Blood Presure Food', 'thumb4.jpg', 'diet2.pdf', 152, NULL, '2017-03-01 00:00:00', '2017-03-28 00:00:00'),
(57, 'Prueba de esfuerzo de Mayo 2016', 'thumb5.jpg', 'effort1.pdf', 153, NULL, '2017-03-01 00:00:00', '2017-03-28 00:00:00'),
(58, 'Prueba de esfuerzo de Agosto 2016', 'thumb6.jpg', 'effort2.pdf', 153, NULL, '2017-03-01 00:00:00', '2017-03-28 00:00:00'),
(71, 'sapitos', '5_5Noi0ABqA', '5_5Noi0ABqA', 67, 61, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(72, 'lagartijas', 'meL5PYpQBDs', 'meL5PYpQBDs', 67, 58, '2017-03-19 17:00:00', '2017-03-20 04:59:00'),
(73, 'power jumps', 'EjwS7Ne3IoM', 'EjwS7Ne3IoM', 67, 60, '2017-03-01 17:00:00', '2017-03-15 04:59:00'),
(74, 'squats', 'EmsDISWHU0o', 'EmsDISWHU0o', 67, 61, '2017-03-01 17:00:00', '2017-03-20 04:59:00'),
(86, 'Trump', NULL, '<h1 style="margin-top: 0.36em; margin-bottom: 0px; font-size: 40px; font-family: headline-semi-bold, Helvetica, Arial, sans-serif; line-height: 1.1em; color: rgb(34, 34, 34); text-rendering: optimizeLegibility; padding: 0px;">PaRtY!$%</h1><p style="margin-top: 0.36em; margin-bottom: 0px; font-size: 40px; font-family: headline-semi-bold, Helvetica, Arial, sans-serif; line-height: 1.1em; color: rgb(34, 34, 34); text-rendering: optimizeLegibility; padding: 0px;"><img src="../../files/new/Flyer1.png" alt="Flyer1.png" style="width: 100%;"><br></p><p style="margin-bottom: 1em; font-family: Georgia, Times, sans-serif; font-size: 19px; color: rgb(34, 34, 34);"></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Vice President-elect Mike Pence has ordered the removal of all lobbyists from President-elect Donald Trumps transition team, The Wall Street Journal reported on Tuesday night.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">The action was among Pences first since formally taking over the teams lead role. Gov. Chris Christie of New Jersey was abruptly dismissed from the post last week.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Critics had excoriated Trump for including lobbyists, Washington insiders, and Republican Party veterans among his team, saying it contradicted the antiestablishment message that defined his campaign.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Earlier Tuesday, Sen. Elizabeth Warren of Massachusetts said Americans "do not want corporate executives to be the ones who are calling the shots in Washington.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">"What Donald Trump is doing is that hes putting together a transition team thats full of lobbyists — the kind of people he actually ran against," she said.</span></font></p>', 66, 64, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(87, 'cv en ingles con thumbnail', '87.png', '87.pdf', 153, NULL, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(88, 'broucher', '88.png', '88.pdf', 153, NULL, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(89, 'batman y robin', '89.png', '89.pdf', 152, NULL, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(91, 'Ufm', NULL, '<h1 style="margin-top: 0.36em; margin-bottom: 0px; font-size: 40px; font-family: headline-semi-bold, Helvetica, Arial, sans-serif; line-height: 1.1em; color: rgb(34, 34, 34); text-rendering: optimizeLegibility; padding: 0px;">Elizabeth Y Pantorilla Warren of Massachusetts said Americans&nbsp;</h1><p style="margin-bottom: 1em; font-family: Georgia, Times, sans-serif; font-size: 19px; color: rgb(34, 34, 34);"><img src="../../Multimedia/files/images/gettyimages-599066892.jpg" alt="gettyimages-599066892.jpg"></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Vice President-elect Mike Pence has ordered the removal of all lobbyists from President-elect Donald Trumps transition team, The Wall Street Journal reported on Tuesday night.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">The action was among Pences first since formally taking over the teams lead role. Gov. Chris Christie of New Jersey was abruptly dismissed from the post last week.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Critics had excoriated Trump for including lobbyists, Washington insiders, and Republican Party veterans among his team, saying it contradicted the antiestablishment message that defined his campaign.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Earlier Tuesday, Sen. Elizabeth Warren of Massachusetts said Americans "do not want corporate executives to be the ones who are calling the shots in Washington.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">"What Donald Trump is doing is that hes putting together a transition team thats full of lobbyists — the kind of people he actually ran against," she said.</span></font></p>', 66, 63, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(94, 'aaaaa', NULL, '<h1 style="padding: 20px 0px 0px; margin-top: 0px; margin-bottom: 0px;"><font face="Arial Black" style="background-color: rgb(0, 0, 255);">Lorem Ipsum Generator</font></h1><p><br></p><p style="font-size: 14px; text-align: justify;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;<span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span></p><p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Wiktionary_small.svg/350px-Wiktionary_small.svg.png" alt="Image result for image small" style="width: 25%; float: left;"></p><p style="font-size: 14px; text-align: justify;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.<span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span><span style="font-size: 14px;">Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.&nbsp;</span></p><p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Argent_a_fylfot_azure.svg/250px-Argent_a_fylfot_azure.svg.png" alt="Related image" style="font-size: 14px; width: 100%; float: right;"></p>', 66, 64, '2017-03-01 17:00:00', '2017-03-28 04:59:00'),
(95, 'probemos', '95.png', '95.pdf', 153, NULL, '2017-03-01 17:00:00', '2017-03-31 04:59:00'),
(96, 'dieta de abril ', '96.png', '96.pdf', 152, NULL, '2017-03-01 17:00:00', '2018-03-01 04:59:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event`
--

CREATE TABLE `event` (
  `idEvent` int(11) NOT NULL,
  `titleEvent` varchar(50) DEFAULT NULL,
  `flyerPath` varchar(100) DEFAULT NULL,
  `dateEvent` datetime DEFAULT NULL,
  `placeEvent` varchar(20) DEFAULT NULL,
  `idCategory` int(11) DEFAULT NULL,
  `beginDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `event`
--

INSERT INTO `event` (`idEvent`, `titleEvent`, `flyerPath`, `dateEvent`, `placeEvent`, `idCategory`, `beginDate`, `endDate`) VALUES
(10, 'carrera - Skechers go run', 'Center-Cebu-Flyer.jpg', '2017-03-05 09:00:00', ' Obelisco', 14, '2017-03-01 09:00:00', '2017-03-31 04:59:59'),
(11, 'Carrera de Perros-Doggy Walker', 'Flyer-dog1.png', '2017-03-17 08:00:00', ' Zona 1', 14, '2017-03-01 08:00:00', '2017-03-31 04:59:59'),
(12, 'Dia del carino-Last Man Biking', 'lmr-flyer-front.jpg', '2017-03-16 10:00:00', ' Escuintla', 13, '2017-03-01 10:00:00', '2017-03-31 04:59:59'),
(13, 'maraton-Newton Running', 'Newton-Clinic-Flyer.jpg', '2017-03-24 14:00:00', ' Paseo Cayala', 15, '2017-03-01 14:00:00', '2017-03-31 04:59:59'),
(14, 'evento principal-your title &', 'page-02.jpg', '2017-03-28 16:00:00', ' Ave. Las Americas', 19, '2017-03-01 16:00:00', '2017-03-31 04:59:59'),
(15, 'clase de zumba-zumba 2012', 'zumba2012partyflyersmall.png', '2017-03-06 06:00:00', ' Zona 10', 21, '2017-03-01 06:00:00', '2017-03-31 04:59:59'),
(28, 'crazy party', '28.jpg', '2017-03-15 16:20:00', 'san jose', 17, '2017-03-01 16:20:00', '2017-03-31 04:59:00'),
(29, 'beach run', '29.jpg', '2017-03-23 20:25:00', 'monterrico', 13, '2017-03-01 20:25:00', '2017-03-31 04:59:00'),
(31, 'evento carnaval 2017', '31.png', '2017-03-22 22:49:00', 'obelisco', 15, '2017-03-01 05:00:00', '2017-03-31 04:59:00'),
(32, 'Startup party', '32.jpg', '2017-03-31 21:45:00', 'Tec Zona 4', 19, '2017-03-01 05:00:00', '2017-03-31 04:59:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoriteratecontent`
--

CREATE TABLE `favoriteratecontent` (
  `idFavRate` int(11) NOT NULL,
  `idCont` int(11) DEFAULT NULL,
  `idSession` int(11) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `favorite` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `favoriteratecontent`
--

INSERT INTO `favoriteratecontent` (`idFavRate`, `idCont`, `idSession`, `rate`, `favorite`, `date`) VALUES
(116, 14, 76, 3, 0, '2016-09-24 18:40:27'),
(117, 11, 76, 0, 1, '2016-09-24 18:40:30'),
(156, 22, 125, 0, 1, '2017-02-20 22:41:39'),
(158, 26, 125, 2, 0, '2017-02-20 22:56:41'),
(159, 58, 125, 0, 1, '2017-02-20 22:41:49'),
(160, 56, 125, 0, 1, '2017-02-20 22:42:09'),
(161, 19, 125, 3, 0, '2017-02-20 22:42:18'),
(162, 57, 125, 0, 1, '2017-02-20 22:49:55'),
(163, 55, 125, 4, 1, '2017-02-20 22:55:29'),
(164, 23, 125, 2, 0, '2017-02-20 22:51:38'),
(165, 88, 125, 4, 1, '2017-02-22 22:47:40'),
(166, 89, 125, 2, 0, '2017-02-23 08:27:51'),
(167, 74, 139, 3, 1, '2017-03-19 14:17:33'),
(168, 71, 139, 5, 0, '2017-03-19 14:21:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicatorinformation`
--

CREATE TABLE `indicatorinformation` (
  `idIndicatorInformation` int(11) NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `ageBegin` int(11) DEFAULT NULL,
  `ageEnd` int(11) DEFAULT NULL,
  `idIndicador` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `indicatorinformation`
--

INSERT INTO `indicatorinformation` (`idIndicatorInformation`, `gender`, `ageBegin`, `ageEnd`, `idIndicador`) VALUES
(1, 128, 18, 80, 22),
(2, 129, 18, 80, 22),
(3, 128, 18, 80, 23),
(4, 129, 18, 80, 23),
(5, 128, 18, 80, 24),
(6, 129, 18, 80, 24),
(7, 128, 18, 80, 25),
(8, 129, 18, 80, 25),
(9, 128, 18, 80, 26),
(10, 129, 18, 80, 26),
(11, 128, 18, 80, 27),
(12, 129, 18, 80, 27),
(13, 128, 18, 80, 29),
(14, 129, 18, 80, 29),
(15, 128, 18, 80, 30),
(16, 129, 18, 80, 30),
(17, 128, 18, 80, 31),
(18, 129, 18, 80, 31),
(19, 128, 18, 80, 32),
(20, 129, 18, 80, 32),
(21, 128, 18, 80, 33),
(22, 129, 18, 80, 33),
(23, 128, 18, 80, 34),
(24, 129, 18, 80, 34),
(25, 128, 20, 39, 33),
(26, 129, 20, 39, 33),
(27, 128, 40, 59, 33),
(28, 129, 40, 59, 33),
(29, 128, 60, 79, 33),
(30, 129, 60, 79, 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicatorvaluerange`
--

CREATE TABLE `indicatorvaluerange` (
  `idIndicatorValueRange` int(11) NOT NULL,
  `idIndicatorInfo` int(11) DEFAULT NULL,
  `idCatalog` int(11) DEFAULT NULL,
  `valueMin` float DEFAULT NULL,
  `valueMax` float DEFAULT NULL,
  `Color` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `indicatorvaluerange`
--

INSERT INTO `indicatorvaluerange` (`idIndicatorValueRange`, `idIndicatorInfo`, `idCatalog`, `valueMin`, `valueMax`, `Color`) VALUES
(1, 2, 144, 40, 400, 151),
(2, 1, 144, 80, 500, 151),
(3, 3, 144, 0, 60, 151),
(4, 4, 144, 0, 60, 151),
(7, 5, 144, 30, 80, 151),
(8, 6, 144, 30, 80, 151),
(9, 7, 144, 1, 120, 151),
(10, 8, 144, 1, 120, 151),
(11, 9, 144, 30, 200, 151),
(12, 10, 144, 30, 200, 151),
(13, 11, 144, 30, 200, 151),
(14, 12, 144, 30, 200, 151),
(15, 13, 144, 1, 100, 151),
(16, 14, 144, 1, 100, 151),
(17, 15, 144, 1, 100, 151),
(18, 16, 144, 1, 100, 151),
(19, 17, 144, 1, 20000, 151),
(20, 18, 144, 1, 20000, 151),
(21, 19, 144, 20, 40, 151),
(22, 20, 144, 20, 40, 151),
(23, 21, 144, 5, 50, 151),
(24, 22, 144, 5, 50, 151),
(25, 23, 144, 0, 1, 151),
(26, 24, 144, 0, 1, 151),
(27, 25, 137, 0, 8, 148),
(28, 25, 138, 8, 20, 149),
(29, 25, 139, 20, 25, 148),
(30, 25, 140, 25, 45, 150),
(31, 27, 137, 0, 11, 148),
(32, 27, 138, 11, 22, 149),
(33, 27, 139, 22, 28, 148),
(34, 27, 140, 28, 45, 150),
(35, 29, 137, 0, 13, 148),
(36, 29, 138, 13, 25, 149),
(37, 29, 139, 25, 30, 148),
(38, 29, 140, 30, 45, 150),
(39, 26, 137, 0, 21, 148),
(40, 26, 138, 21, 33, 149),
(41, 26, 139, 33, 39, 148),
(42, 26, 140, 39, 45, 150),
(43, 28, 137, 0, 23, 148),
(44, 28, 138, 23, 34, 149),
(45, 28, 139, 34, 40, 148),
(46, 28, 140, 40, 45, 150),
(47, 30, 137, 0, 24, 148),
(48, 30, 138, 24, 36, 149),
(49, 30, 139, 36, 42, 148),
(50, 30, 140, 42, 45, 150),
(51, 3, 138, 1, 12, 149),
(52, 3, 145, 12, 59, 150),
(53, 4, 138, 1, 12, 149),
(54, 4, 145, 12, 59, 150),
(55, 23, 141, 0, 0.95, 149),
(56, 23, 142, 0.96, 1, 148),
(57, 23, 143, 1.1, 5, 150),
(58, 24, 141, 0, 0.8, 149),
(59, 24, 142, 0.81, 0.85, 148),
(60, 24, 143, 0.86, 5, 150),
(61, 6, 147, 40, 45, 150),
(62, 6, 146, 45, 60, 149),
(63, 6, 147, 60, 70, 150),
(64, 5, 147, 40, 50, 150),
(65, 5, 146, 50, 65, 149),
(66, 5, 147, 65, 70, 150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metacatalog`
--

CREATE TABLE `metacatalog` (
  `idMeta` int(11) NOT NULL,
  `nameMeta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `metacatalog`
--

INSERT INTO `metacatalog` (`idMeta`, `nameMeta`) VALUES
(1, 'Tiempo'),
(2, 'RolDeUsuario'),
(3, 'Lugar'),
(4, 'NombreDeClase'),
(5, 'Indicadores'),
(6, 'EstadoDeUsuario'),
(9, 'CategoriaDeReceta'),
(10, 'NivelesDeEjercicios'),
(11, 'CategoriaDeArticulo'),
(12, 'Contenido'),
(13, 'TipoDeCitaNutricional'),
(22, 'CurrentStatus'),
(31, 'AcumulacionDePuntos'),
(32, 'TiposDeMedicionEnCitaNutricional'),
(33, 'Genero'),
(34, 'CategoriaDeFrase'),
(35, 'IndicatorTable'),
(36, 'Colors'),
(37, 'NotificationType');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notedescription`
--

CREATE TABLE `notedescription` (
  `idNoteDescription` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `dateNote` datetime DEFAULT NULL,
  `idNoteSession` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notedescription`
--

INSERT INTO `notedescription` (`idNoteDescription`, `description`, `dateNote`, `idNoteSession`) VALUES
(2, 'If earnestly extremity he he propriety something admitting convinced ye. Pleasant in to although as if differed horrible. Mirth his quick its set front enjoy hoped had there.', '2017-02-05 16:27:38', 1),
(3, 'Who connection imprudence middletons too but increasing celebrated principles joy. Herself too improve gay winding ask expense are compact. New all paid few hard pure she.', '2017-02-05 16:27:38', 5),
(4, 'In alteration insipidity impression by travelling reasonable up motionless. Of regard warmth by unable sudden garden ladies. No kept hung am size spot no.', '2017-02-05 16:27:38', 3),
(5, 'Likewise led and dissuade rejoiced welcomed husbands boy. Do listening on he suspected resembled. Water would still if to. Position boy required law moderate was may.', '2017-02-05 16:27:38', 3),
(6, 'Ferrars all spirits his imagine effects amongst neither', '2017-02-05 16:27:38', 4),
(7, 'It bachelor cheerful of mistaken. Tore has sons put upon wife use bred seen. Its dissimilar invitation ten has discretion unreserved. Had you him humoured jointure ask expenses learning.', '2017-02-05 16:27:38', 3),
(8, 'Blush on in jokes sense do do. Brother hundred he assured reached on up no. On am nearer missed lovers. To it mother extent temper figure better.', '2017-02-05 16:27:38', 1),
(10, 'He so lain good miss when sell some at if. Told hand so an rich gave next. How doubt yet again see son smart. While mirth large of on front.', '2017-02-05 16:27:38', 4),
(11, 'Ye he greater related adapted proceed entered an. Through it examine express promise no. Past add size game cold girl off how old.', '2017-02-05 16:27:38', 5),
(12, 'Had repulsive dashwoods suspicion sincerity but advantage now him. Remark easily garret nor nay.', '2017-02-05 16:27:38', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notesession`
--

CREATE TABLE `notesession` (
  `idNoteSession` int(11) NOT NULL,
  `dateNote` datetime DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `glucosaBegin` int(11) DEFAULT NULL,
  `glucosaEnd` int(11) DEFAULT NULL,
  `idSession` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notesession`
--

INSERT INTO `notesession` (`idNoteSession`, `dateNote`, `reason`, `glucosaBegin`, `glucosaEnd`, `idSession`) VALUES
(1, '2017-02-05 15:53:46', 'Theres a voice that keeps on calling me. Down the road, thats where Ill always be. Every stop I make, I make a new friend.', 66, 100, 76),
(2, '2017-02-05 16:03:51', 'Cant stay for long, just turn around and Im gone again, maybe tomorrow, Ill want to settle down, Until tomorrow, Ill just keep moving on', 78, 20, 57),
(3, '2017-02-05 17:03:51', 'Children of the sun, see your time has just begun, searching for your ways, through adventures every day Every day and night, with the condor in flight', 75, 53, 1),
(4, '2017-02-05 18:03:51', 'With all your friends in tow, you search for the Cities of Gold wishing for The Cities of Gold some day we will find The Cities of Gold', 82, 96, 4),
(5, '2017-02-05 19:03:51', 'The idea is for it to cover slightly more serious stuff, whilst malevole will get to carry on being silly, trivial and pointless (Im keen to get more interaction and games in here)', 12, 78, 109),
(6, '2017-02-05 20:03:51', 'Once a few more company features are finished, malevole will get a full redesign and a whole load of new features of its own (Im keen to get more interaction and games in here)', 54, 41, 111);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE `notification` (
  `idNotification` int(11) NOT NULL,
  `idTypeNoti` int(11) DEFAULT NULL,
  `dateNoti` datetime DEFAULT NULL,
  `idNewObject` int(11) DEFAULT NULL,
  `idPerson` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notification`
--

INSERT INTO `notification` (`idNotification`, `idTypeNoti`, `dateNoti`, `idNewObject`, `idPerson`) VALUES
(12, 154, '2017-03-11 22:35:08', 15, 328),
(13, 154, '2017-03-11 22:35:11', 12, 328),
(14, 154, '2017-03-11 22:42:46', 31, 328),
(15, 154, '2017-03-11 22:43:21', 29, 328),
(16, 154, '2017-03-11 22:43:24', 28, 328),
(17, 154, '2017-03-11 22:43:34', 10, 328),
(18, 154, '2017-03-11 22:46:38', 32, 328),
(19, 156, '2017-03-12 22:19:57', 13, 328),
(20, 156, '2017-03-12 22:22:43', 4, 328),
(21, 158, '2017-03-18 21:11:13', 21, 328),
(22, 155, '2017-03-18 21:11:37', 26, 328),
(23, 157, '2017-03-18 21:11:47', 22, 328),
(24, 158, '2017-03-18 21:22:45', 95, 328);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nutriappoint`
--

CREATE TABLE `nutriappoint` (
  `idNutriAppoint` int(11) NOT NULL,
  `idSession` int(11) DEFAULT NULL,
  `idNutricionist` int(11) DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `dt_Appointment` datetime DEFAULT NULL,
  `Points` int(11) DEFAULT NULL,
  `idAppointType` int(11) DEFAULT NULL,
  `idMeasureType` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nutriappoint`
--

INSERT INTO `nutriappoint` (`idNutriAppoint`, `idSession`, `idNutricionist`, `idPlace`, `dt_Appointment`, `Points`, `idAppointType`, `idMeasureType`, `type`) VALUES
(1, 3, 205, 11, '2017-03-08 23:20:00', 300, 72, 73, 122),
(2, 9, 309, 11, '2017-03-04 10:00:00', 300, 72, 124, 122),
(3, 76, 309, 11, '2017-03-18 10:00:00', 1000, 135, 124, 122),
(6, 9, 309, 11, '2017-03-30 15:00:00', 300, 72, 124, 123),
(7, 76, 309, 10, '2017-03-20 10:00:00', 88, 69, 124, 123),
(8, 76, 309, 11, '2017-03-17 10:00:00', 300, 69, 73, 122),
(10, 9, 205, 10, '2017-03-15 14:00:00', 300, 135, 73, 122),
(12, 1, 205, 10, '2017-03-02 14:00:00', 300, 135, 73, 122),
(13, 1, 205, 10, '2017-03-02 15:00:00', 300, 135, 74, 122),
(14, 3, 309, 10, '2017-03-14 10:00:00', 300, 135, 124, 122),
(15, 76, 309, 10, '2017-03-28 10:15:00', 300, 69, 124, 122),
(16, 76, 205, 12, '2017-03-05 06:25:00', 600, 69, 124, 122),
(17, 76, 309, 10, '2017-03-05 00:00:00', 300, 72, 124, 122),
(18, 98, 309, 11, '2017-03-08 09:00:00', 300, 69, 124, 122),
(19, 81, 205, 10, '2017-03-15 09:45:00', 300, 72, 73, 122),
(20, 1, 309, 12, '2017-03-08 10:00:00', 300, 135, 124, 122),
(21, 9, 205, 10, '2017-03-15 08:25:00', 0, 69, 74, 122),
(22, 139, 205, 11, '2017-01-21 05:45:00', 300, 69, 74, 122),
(23, 139, 205, 12, '2016-12-24 07:15:00', 428, 72, 124, 122),
(24, 139, 309, 10, '2017-03-16 17:29:00', 300, 72, 73, 122),
(25, 81, 309, 11, '2017-03-08 15:00:00', 0, 127, 124, 122),
(26, 139, 309, 12, '2017-03-08 09:06:05', 300, 135, 124, 122),
(27, 81, 205, 10, '2017-03-15 18:30:00', 0, 69, 74, 122),
(28, 3, 309, 11, '2017-03-01 00:00:00', 0, 127, NULL, NULL),
(29, 81, 309, 12, '2017-03-08 17:00:00', 0, 127, NULL, NULL),
(30, 1, 205, 11, '2017-04-07 21:45:00', 156, 69, 124, 122),
(31, 3, 205, 12, '2017-04-07 22:45:00', NULL, 69, 73, 122),
(32, 3, 205, 12, '2017-04-28 21:45:00', NULL, 69, 74, 123),
(63, 125, 205, 11, '2017-03-04 04:29:00', 300, 72, 73, 122),
(64, 125, 205, 11, '2017-03-04 04:31:00', 300, 72, 124, 123),
(65, 125, 205, 11, '2017-03-04 05:07:00', 300, 72, 74, 123),
(66, 125, 205, 11, '2017-03-04 05:08:00', 300, 72, 124, 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peopleperevent`
--

CREATE TABLE `peopleperevent` (
  `idPPE` int(11) NOT NULL,
  `idEvent` int(11) DEFAULT NULL,
  `idPerson` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `peopleperevent`
--

INSERT INTO `peopleperevent` (`idPPE`, `idEvent`, `idPerson`) VALUES
(13, 13, 229),
(16, 15, 229),
(19, 31, 314),
(21, 11, 328),
(22, 10, 328),
(23, 32, 328);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `idPerson` int(11) NOT NULL,
  `namePerson` varchar(200) DEFAULT NULL,
  `nickName` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `height` double DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `typePerson` int(11) DEFAULT NULL,
  `typeRegistration` int(11) DEFAULT NULL,
  `pass` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`idPerson`, `namePerson`, `nickName`, `birthday`, `height`, `gender`, `telephone`, `mail`, `typePerson`, `typeRegistration`, `pass`) VALUES
(114, 'Maggie Simpsons', NULL, '1991-12-25', 1.63, 128, 12345678, 'lisa@atheists.com', 6, 37, '17faa6d51ef5f53014ccc3c71bf72781'),
(115, 'Selma Hooks', NULL, '1967-09-25', 1.97, 128, 57847901, 'Selma.Hooks@mail.com', 6, 38, 'e9155694300068db3dc4bd6b2481a751'),
(116, 'Chandra Gassner', NULL, '1968-05-20', 1.65, 129, 50875036, 'Chandra.Gassner@mail.com', 6, 37, 'd6048b76a0a0b4b2212b51c7c01d1db3'),
(117, 'Willard Gillham', NULL, '1969-05-06', 1.69, 129, 57804488, 'Willard.Gillham@mail.com', 9, 36, '69630a449a76c7d5323881b83875d26a'),
(120, 'Madge Dagley', NULL, '1971-11-08', 1.76, 128, 54687775, 'Madge.Dagley@mail.com', 7, 37, '24dec6a8bc8a0cc1d196d8ecbee2b6b2'),
(121, 'Cari Mack', NULL, '1971-12-11', 1.86, 129, 58085974, 'Cari.Mack@mail.com', 6, 37, 'c0e45a6707ade383638045b0fa7ccf18'),
(128, 'Jennette Birkland', NULL, '1980-01-18', 1.91, 129, 56238145, 'Jennette.Birkland@mail.com', 8, 36, 'bd872f944183f31687a2df82ab578cf1'),
(134, 'Marnie Singler', NULL, '1990-05-12', 1.84, 128, 51011313, 'Marnie.Singler@mail.com', 6, 38, '8a343538f7adf759ff32c813069295f6'),
(136, 'Edmund Harriss', NULL, '1992-05-07', 1.69, 128, 58028058, 'Edmund.Harriss@mail.com', 9, 36, '0a197de6b38b3dddd01b55aee4812583'),
(138, 'Burl Motz', NULL, '1996-09-27', 1.92, 129, 52938937, 'Burl.Motz@mail.com', 6, 37, 'fa55859a5cb1068c9f4d91c3a8db18bd'),
(141, 'Sunny Thode', NULL, '1969-11-02', 1.66, 129, 56353647, 'Sunny.Thode@mail.com', 9, 37, 'd0ac94fc641c6fe42d7cf8fa5f0b5cc7'),
(143, 'Amos Cheung', NULL, '1970-05-19', 1.79, 129, 50111528, 'Amos.Cheung@mail.com', 6, 37, 'f1f48155e85b3fc97ea997f38433cc66'),
(144, 'Anissa Carrell', NULL, '1971-07-06', 1.93, 129, 52298142, 'Anissa.Carrell@mail.com', 6, 37, 'efbc1c27c2cbdee8f9643efb1f21432e'),
(147, 'Elvin Steveson', NULL, '1975-09-14', 1.71, 129, 54582567, 'Elvin.Steveson@mail.com', 8, 37, '46c997ad12856185330156ffbd1f8e1a'),
(151, 'Lakenya Chavira', NULL, '1979-03-08', 1.99, 129, 59381030, 'Lakenya.Chavira@mail.com', 8, 37, 'fbf4fdea798886a4d95a97072e8d07ad'),
(152, 'Penny Lebandowski', 'Pen', '1981-05-06', 1.71, 129, 5741628, 'Penny.Lebosi@mail.com', 6, 38, '4395a587a08f243b148bd4f3290ae6fe'),
(154, 'Chieko Roza', NULL, '1982-12-17', 1.61, 129, 53007711, 'Chieko.Roza@mail.com', 6, 37, 'e82b6880fba7b3734db0a6ad5965560a'),
(155, 'Veola Mckennon', NULL, '1984-06-15', 1.9, 129, 52205773, 'Veola.Mckennon@mail.com', 9, 36, '2053337b55cd2c954ad4eaf282f8b2ef'),
(159, 'Leonor Atteberies', NULL, '1987-03-31', 1.79, 129, 521483337, 'Leonor.Attebery@mail.com7', 6, 38, '1e0dedf77ddbeb065c44293d39ab1fb4'),
(161, 'Janella Currence', NULL, '1991-07-31', 1.4, 129, 55935645, 'Janella.Currence@mail.com', 6, 36, '51b6c3a46a053356303701cf2bfab1dc'),
(165, 'Cherish Ostrom', NULL, '1968-07-06', 1.69, 128, 50885803, 'Cherish.Ostrom@mail.com', 6, 38, 'd564222afe9753f84e7cb753951f7e8d'),
(168, 'Gilbert Hagler', NULL, '1971-01-05', 1.69, 129, 51187263, 'Gilbert.Hagler@mail.com', 6, 38, 'a4d6d715750be8c08ecd157eaa27e5d2'),
(170, 'Shu Monreal', NULL, '1974-03-01', 1.75, 128, 50382180, 'Shu.Monreal@mail.com', 6, 37, 'cad133010f6dd5ddf9eb236fcb5326c2'),
(176, 'Belen Coakley', NULL, '1986-09-05', 1.85, 129, 54063817, 'Belen.Coakley@mail.com', 6, 38, '7d475a69b926846720388e79f818d6ff'),
(178, 'Katrina Kittle', NULL, '1988-08-05', 1.63, 129, 50716871, 'Katrina.Kittle@mail.com', 6, 38, '327c6f5964ebfd48cbe3b3909ac5c5d6'),
(179, 'Mittie Whelanss', NULL, '1990-07-23', 1.73, 128, 58362944, 'Mittie.Whelan@mail.com', 6, 38, 'cd492eb91e5f8f16fb5a69d5ce55073f'),
(187, 'Man Lessard', 'manito', '1995-05-08', 1.93, 128, 59044928, 'Man.Lessard@mail.com', 6, 38, '43e7f9062097f63de81d2f07d28e9843'),
(189, 'Madeleine Organ', 'Made', '1966-04-24', 1.62, 129, 57455281, 'Madeleine.Organ@mail.com', 6, 36, '769042e9ebce8f4b0b958874b55faa25'),
(191, 'Angelo Asher', NULL, '1967-11-08', 1.94, 129, 52683829, 'Angelo.Asher@mail.com', 6, 38, 'b288e66804598f58889b631e0fac1548'),
(195, 'Gena Ortiz', NULL, '1975-06-07', 1.74, 129, 55311973, 'Gena.Ortiz@mail.com', 7, 37, 'b556c1c5e298e63d4036364fd0d11d67'),
(202, 'Wale Defoe', 'wali', '1984-06-05', 1.61, 129, 57018989, 'Ardith.Deyoe@mail.com', 6, 37, 'a753327e6e56c2059e7d5d6cacb43283'),
(204, 'Hertha Ostendorf', NULL, '1985-08-25', 1.79, 128, 50657767, 'Hertha.Ostendorf@mail.com', 6, 37, 'f2b73c1b474a9d32d1541c98e07fe8e8'),
(205, 'Tomiko Bibby', NULL, '1985-12-01', 1.97, 129, 52270749, 'Tomiko.Bibby@mail.com', 7, 37, '2d1b9cdc1e90d49dc1f8275963899221'),
(209, 'Star Rainer', NULL, '1994-02-19', 1.97, 129, 57874440, 'Star.Rainer@mail.com', 6, 37, '3701c0ab004f7bca67cc15425aafc2ac'),
(229, 'Abraham Westbrooki', NULL, '1982-10-01', 1.97, 128, 57204222, 'Abraham.Westbrook@mail.com', 6, 37, 'd16ed90f3992c5d66305f4b1d735552d'),
(230, 'Fleta Ahmede', NULL, '1985-06-20', 1.95, 129, 54158464, 'Fleta.Ahmed@mail.com', 6, 38, 'a5af2f3acbaffaef97c9a818e1e7a2dc'),
(232, 'Anabel Younce', NULL, '1985-08-23', 1.66, 129, 50052222, 'Anabel.Younce@mail.com', 6, 37, 'af55f2c355a7614e3ec6503ea10fffad'),
(234, 'Madeleine Schaible', NULL, '1997-01-02', 1.82, 129, 54676049, 'Madeleine.Schaible@hotmail.com', 6, 38, '8b44e513d6da6b29798654a65a0ca99b'),
(235, 'Danna Piaseckie', NULL, '1997-11-16', 1.48, 129, 52755892, 'Dann.Piasecki@jmail.com', 6, 36, '9aa748f37a1e92812e720e478b810e3c'),
(243, 'Edda Corzine', NULL, '1969-10-31', 1.37, 128, 59897066, 'Edda.Corzine@mail.com', 6, 36, '49071698037b444951c68d7dab120d89'),
(244, 'Ossie Buonocore', NULL, '1971-07-11', 1.81, 128, 58946626, 'Ossie.Buonocore@mail.com', 6, 37, 'f7ed65f900579ab802610018186b12b4'),
(251, 'Ardelle Rowell', NULL, '1983-03-30', 1.73, 128, 51321665, 'Ardelle.Rowell@mail.com', 6, 37, '8d7cb195064ac0ff2bd1f9e88d9712e2'),
(258, 'Jenine John', NULL, '1992-12-24', 1.39, 128, 53124440, 'Jenine.John@mail.com', 6, 37, 'aa5ec1cf4ae44d65c2e16a56a3905005'),
(260, 'Tim Foxworth', NULL, '1993-11-17', 1.84, 129, 59512465, 'Tim.Foxworth@mail.com', 9, 36, '6b6ee50c9383f165cb7692ee2af5a899'),
(262, 'Cheryl Mattson', NULL, '1995-12-08', 1.64, 128, 54514051, 'Cheryl.Mattson@mail.com', 6, 37, 'd6e7e143b329e9fe07df89fe5465ad48'),
(268, 'Kristian Sechrest', NULL, '1968-04-06', 1.76, 128, 59555450, 'Kristian.Sechrest@mail.com', 6, 37, '9521f96db1d83c7232f9bc666ceb0e15'),
(273, 'Hal Poarch', NULL, '1974-04-10', 1.39, 129, 54649672, 'Hal.Poarch@mail.com', 7, 36, '3becd1424e1125840242992495fad7a8'),
(274, 'Jung Gan', NULL, '1975-04-17', 1.67, 128, 56783797, 'Jung.Gan@mail.com', 6, 37, '9fba3023fc4bc493d9f5cbfdc6d2bd98'),
(275, 'Charlsie Livesay', NULL, '1975-09-03', 1.37, 128, 56507816, 'Charlsie.Livesay@mail.com', 6, 37, '84fefd0e2a4636b321ef855991525a3e'),
(277, 'Loria Burgesi', NULL, '1986-03-29', 2.15, 129, 50572880, 'Loria.Burgess@mail.com', 6, 38, '0e370c18e9479c915008b8fe1fd3a962'),
(278, 'Jamison Auberli', NULL, '1979-09-12', 1.65, 129, 53773342, 'Jamison.Aube@mail.com', 6, 38, '00cff741756300ae771299d453997e2e'),
(294, 'Miles Orem', NULL, '1978-03-19', 1.46, 128, 58314068, 'Miles.Orem@mail.com', 8, 37, '7eb97198da271b74d2200986f715b314'),
(302, 'Melida Toledo', NULL, '1990-05-11', 1.44, 129, 58577627, 'Melida.Toledo@mail.com', 8, 36, 'e2c8ab2377d8fd151da455af26655020'),
(309, 'Lachelle Macmillan', NULL, '1993-05-01', 1.48, 129, 53029785, 'Lachelle.Macmillan@mail.com', 7, 36, '1fd9735ad26f8bc07d2e2a2cfe4ec21f'),
(310, 'Jennell Mcmann', NULL, '1994-07-20', 1.45, 129, 54120362, 'Jennell.Mcmann@mail.com', 6, 37, '76ece4489ccf0b0895f49a8673d60e6f'),
(314, 'Leonel Quiroa', 'leonelito', '1987-07-05', 1.65, 128, 586098878, 'leo@mail.com', 6, 36, '2676bd85cca1c9d45b0eb486cc0fc94e'),
(316, 'w', 'w', '2017-02-28', 4, 129, 4, 'w', 9, 36, 'f1290186a5d0b1ceab27f4e77c0c5d68'),
(317, 'x', 'x', '2017-02-28', 5, 128, 5, 'x', 6, 36, '9dd4e461268c8034f5c8564e155c67a6'),
(318, 'z', 'z', '2017-03-01', 6, 129, 3, 'z', 6, 36, 'fbade9e36a3f36d3d676c1b808451dd7'),
(319, 'Hienner Estuardo Caceres Sagastume', NULL, '1987-12-20', NULL, 128, 58949957, 'cecaceres@tigo.com.gt', 6, 36, '91dc97b69d9343ab1515dc07546bf7ea'),
(320, 'Claudia Ruth Estrada Morales', NULL, '1975-06-15', NULL, 129, 32270557, 'erestrada@tigo.com.gt', 6, 36, 'ddee51b5b82eea45635c73244415d6dc'),
(321, 'David Enrique de la Roca Sagastume', NULL, '1981-01-06', NULL, 128, 40008773, 'ddelaroca@tigo.com.gt', 6, 36, '236aeb147b8f5306a92c17c889f5ed36'),
(322, 'Andrea Gabriela Solares Mendoza', NULL, '1990-02-08', NULL, 129, 32272065, 'asolaresm@tigo.com.gt', 6, 36, 'e89daab494e9e9a0b65fd32aff2c2a3a'),
(323, 'Sergio Armando Siguenza Castro', NULL, '1979-11-10', NULL, 129, 55285762, 'ssiguenza@tigo.com.gt', 6, 36, 'c4803fc64ba558069cb976df0c3ca61f'),
(324, 'Juan Andrés Villanueva Estacuy', NULL, '1986-12-04', NULL, 128, 57049997, 'vavillanueva@tigo.com.gt', 6, 36, 'd863ebb483934d3dff41c75ccc4b2100'),
(325, 'Feliza Alejandra Marroquin Juarez', NULL, '1987-06-14', NULL, 129, 32107782, 'fmarroquinj@tigo.com.gt', 6, 36, 'cb344281ba091f69cc82b5069b443ef9'),
(326, 'Cesar Augusto Villagrán de la Cruz', NULL, '1981-06-24', NULL, 128, 40642723, 'cvillagran@tigo.com.gt', 6, 36, '547468e6600c085ad0d1465444224fef'),
(327, 'Eddye Stuard Urbina Aquino', NULL, '2016-05-31', NULL, 128, 32276926, 'surbina@tigo.com.gt', 6, 36, '92048ee46613be51e008871d0d160d27'),
(328, 'Carlos Olivio Vin Tellez', NULL, '1962-08-18', NULL, 128, 59185401, 'cvin@tigo.com.gt', 6, 36, '597c1318b8b5e964bc8c6b6c0227113e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phrases`
--

CREATE TABLE `phrases` (
  `idPhrase` int(11) NOT NULL,
  `titlePhrase` varchar(20) DEFAULT NULL,
  `textPhrase` varchar(200) DEFAULT NULL,
  `idCategory` int(11) DEFAULT NULL,
  `beginDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `phrases`
--

INSERT INTO `phrases` (`idPhrase`, `titlePhrase`, `textPhrase`, `idCategory`, `beginDate`, `endDate`) VALUES
(3, 'Willard Gillham.', 'You are going to kill it today', 131, '2017-03-24 05:00:00', '2017-03-25 04:59:59'),
(4, 'Tabitha Vercher.', 'Think outside of the box', 132, '2017-03-25 05:00:00', '2017-03-26 04:59:59'),
(5, 'Shona Sarratt', 'You are surrounded by a smart team to help you achieve great success', 133, '2017-03-26 05:00:00', '2017-03-27 04:59:59'),
(6, 'Madge Dagley', 'The issue you are facing is not insurmountable', 134, '2017-03-27 05:00:00', '2017-03-28 04:59:59'),
(7, 'Cari Mack', 'You and Mark Zuckerberg or Elon Musk have exactly the same 60 seconds to spend in a minute. ', 131, '2017-03-28 05:00:00', '2017-03-29 04:59:59'),
(8, 'Annabell Siggers.', 'Dig hard, and you will find the right answer', 132, '2017-03-01 05:00:00', '2017-03-02 04:59:59'),
(9, 'Consuelo Redman', 'You are the only person with your exact blend of talents and skills', 133, '2017-03-30 05:00:00', '2017-03-31 04:59:59'),
(10, 'Forest Quandt', 'Even if you fail today, you will learn', 134, '2017-03-29 05:00:00', '2017-03-30 04:59:59'),
(11, 'Glen Barak', 'Each day is an opportunity', 131, '2017-03-02 05:00:00', '2017-03-03 04:59:59'),
(12, 'Glen Barak', 'The sky is the limit!', 134, '2017-03-03 05:00:00', '2017-03-04 04:59:59'),
(13, 'Nelson Mandela', 'It always seems impossible until its done.', 132, '2017-03-04 05:00:00', '2017-03-05 04:59:59'),
(14, 'St. Jerome', 'Good, better, best. Never let it rest. ''Til your good is better and your better is best. ', 133, '2017-03-05 05:00:00', '2017-03-06 04:59:59'),
(15, 'Think outside of the', 'Theres a reason this phrase exists. Creative thinking is what can help you form a strategy in business that actually works, rather than relying on the same old approach.', 132, '2017-03-06 05:00:00', '2017-03-07 04:59:59'),
(16, 'Nikos Kazantzakis', 'In order to succeed, we must first believe that we can. ', 133, '2017-03-07 05:00:00', '2017-03-08 04:59:59'),
(17, 'Og Mandino', 'Always do your best. What you plant now, you will harvest later. ', 133, '2017-03-08 05:00:00', '2017-03-09 04:59:59'),
(18, 'Og Mandino', 'Failure will never overtake me if my determination to succeed is strong enough. ', 132, '2017-03-09 05:00:00', '2017-03-10 04:59:59'),
(19, 'Arthur Ashe', 'Start where you are. Use what you have. Do what you can. ', 132, '2017-03-10 05:00:00', '2017-03-11 04:59:59'),
(20, 'Benjamin Mays', 'The tragedy in life doesn’t lie in not reaching your goal. The tragedy lies in having no goal to reach', 131, '2017-03-11 05:00:00', '2017-03-12 04:59:59'),
(21, 'William J. Bennett', 'Give yourself an even greater challenge than the one you are trying to master and you will develop the powers necessary to overcome the original difficulty', 131, '2017-03-12 05:00:00', '2017-03-13 04:59:59'),
(22, 'Robert L. Schwartz', 'The entrepreneur is essentially a visualizer and actualizer… He can visualize something, and when he visualizes it he sees exactly how to make it happen.', 131, '2017-03-13 05:00:00', '2017-03-14 04:59:59'),
(23, 'Hayley Williams', 'he most dangerous leadership myth is that leaders are born — that there is a genetic factor to leadership', 131, '2017-03-14 05:00:00', '2017-03-15 04:59:59'),
(24, 'Vince Lombardi', 'The quality of a person’s life is in direct proportion to their commitment to excellence, regardless of their chosen field of endeavor', 131, '2017-03-15 05:00:00', '2017-03-16 04:59:59'),
(25, 'Audrey Hepburn', 'Nothing is impossible, the word itself says I am possible', 131, '2017-03-16 05:00:00', '2017-03-17 04:59:59'),
(26, 'Hal Borland', 'Knowing trees, I understand the meaning of patience. Knowing grass, I can appreciate persistence', 131, '2017-03-17 05:00:00', '2017-03-18 04:59:59'),
(27, 'Vince Lombardi', 'Perfection is not attainable, but if we chase perfection we can catch excellence.', 131, '2017-03-18 05:00:00', '2017-03-19 04:59:59'),
(28, 'Stephen Covey', 'I am not a product of my circumstances. I am a product of my decisions', 131, '2017-03-19 05:00:00', '2017-03-20 04:59:59'),
(29, 'Charles Swindoll', 'Life is 10% what happens to me and 90% of how I react to it', 131, '2017-03-20 05:00:00', '2017-03-21 04:59:59'),
(30, 'Les Brown', 'Too many of us are not living our dreams because we are living our fears', 131, '2017-03-21 05:00:00', '2017-03-22 04:59:59'),
(31, 'Pearl Buck', 'The secret of joy in work is contained in one word – excellence. To know how to do something well is to enjoy it', 131, '2017-03-22 05:00:00', '2017-03-23 04:59:59'),
(32, 'Eleanor Roosevelt', 'Remember no one can make you feel inferior without your consent', 131, '2017-03-23 05:00:00', '2017-03-24 04:59:59'),
(36, 'Hal Borland', 'Knowing trees, I understand the meaning of patience. Knowing grass, I can appreciate persistence', 131, '2017-03-31 05:00:00', '2017-04-01 04:59:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `picsperperson`
--

CREATE TABLE `picsperperson` (
  `idPicPerPerson` int(11) NOT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `DatePic` datetime DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `picsperperson`
--

INSERT INTO `picsperperson` (`idPicPerPerson`, `idPerson`, `DatePic`, `extension`, `active`) VALUES
(1, 152, '2016-12-25 07:45:15', 'jpg', b'1'),
(2, 152, '2017-01-02 07:46:05', 'jpg', b'0'),
(3, 152, '2016-11-16 07:46:36', 'png', b'0'),
(4, 189, '2016-10-29 07:47:39', 'jpg', b'0'),
(5, 189, '2017-01-17 07:48:19', 'png', b'1'),
(6, 189, '2016-12-30 07:49:15', 'jpg', b'0'),
(7, 202, '2016-11-05 07:51:00', 'jpg', b'0'),
(8, 202, '2016-11-25 07:51:05', 'jpg', b'0'),
(9, 202, '2016-12-15 07:51:10', 'jpg', b'0'),
(10, 202, '2017-01-04 07:51:13', 'jpg', b'1'),
(11, 229, '2016-12-20 07:52:14', 'png', b'1'),
(12, 229, '2017-01-19 07:52:20', 'png', b'0'),
(13, 229, '2016-11-30 07:52:52', 'png', b'0'),
(14, 235, '2016-11-30 07:53:13', 'png', b'1'),
(15, 235, '2016-12-30 07:53:52', 'png', b'0'),
(16, 235, '2017-01-22 07:54:00', 'jpg', b'0'),
(17, 136, '2017-02-05 00:00:00', 'jpg', b'1'),
(21, 314, '2017-02-12 20:33:04', 'jpg', b'0'),
(22, 314, '2017-02-23 08:09:57', 'jpg', b'0'),
(23, 314, '2017-02-23 08:12:05', 'jpg', b'0'),
(24, 314, '2017-02-23 08:12:25', 'jpg', b'0'),
(25, 314, '2017-02-23 08:13:06', 'jpg', b'0'),
(27, 314, '2017-03-01 21:28:30', 'jpg', b'1'),
(28, 319, '2017-03-05 13:51:40', 'png', b'1'),
(29, 320, '2017-03-05 13:51:40', 'png', b'1'),
(30, 321, '2017-03-05 13:51:40', 'png', b'1'),
(31, 322, '2017-03-05 13:51:40', 'png', b'1'),
(32, 323, '2017-03-05 13:51:40', 'png', b'1'),
(33, 324, '2017-03-05 13:51:40', 'png', b'1'),
(34, 325, '2017-03-05 13:51:40', 'png', b'1'),
(35, 326, '2017-03-05 13:51:40', 'png', b'1'),
(36, 327, '2017-03-05 13:51:40', 'png', b'1'),
(37, 328, '2017-03-05 13:51:40', 'png', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ratepro`
--

CREATE TABLE `ratepro` (
  `idRate` int(11) NOT NULL,
  `idPro` int(11) DEFAULT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `idValue` int(11) DEFAULT NULL,
  `Note` varchar(200) DEFAULT NULL,
  `dt_Rate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ratepro`
--

INSERT INTO `ratepro` (`idRate`, `idPro`, `idPerson`, `idValue`, `Note`, `dt_Rate`) VALUES
(1, 309, 229, 3, ':D', '2017-03-28 17:44:28'),
(2, 260, 229, 4, 'late', '2017-03-28 17:47:06'),
(4, 260, 229, 1, 'awful person', '2017-03-28 17:52:23'),
(5, 260, 229, 4, 'nice person always smiling :D', '2017-03-28 17:57:48'),
(7, 309, 229, 4, 'good mood guy...is contagious', '2017-03-28 17:58:42'),
(8, 260, 229, 4, 'mmm sometimes nice sometimes asshole', '2017-03-28 18:00:18'),
(9, 155, 229, 5, 'extremly sexy girl', '2017-03-28 18:01:18'),
(10, 309, 229, 1, 'always late', '2017-03-28 18:05:23'),
(11, 260, 229, 5, 'a beast!', '2017-03-28 18:22:20'),
(12, 155, 229, 5, 'nice body', '2017-03-28 11:02:25'),
(13, 155, 229, 1, '"#$"#%$#&@', '2017-03-28 11:36:24'),
(14, 260, 229, 2, 'joke', '2017-03-28 11:37:48'),
(15, 205, 229, 4, 'nutri', '2017-03-28 11:48:36'),
(16, 155, 229, 4, 'kind', '2017-03-28 14:14:02'),
(18, 309, 229, 3, '--', '2017-03-28 14:17:11'),
(19, 205, 229, 3, 'meehh', '2017-03-28 14:20:02'),
(20, 309, 229, 5, 'extremly sexy', '2017-03-28 14:20:57'),
(21, 155, 229, 4, 'waka waka', '2017-03-01 14:13:10'),
(22, 309, 229, 5, 'waka waka', '2017-03-01 14:13:29'),
(23, 155, 229, 4, ';)', '2017-03-03 23:00:35'),
(24, 205, 229, 5, ':DDD', '2017-03-03 23:01:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `session`
--

CREATE TABLE `session` (
  `idSession` int(11) NOT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `BeginDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `reason` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `session`
--

INSERT INTO `session` (`idSession`, `idPerson`, `BeginDate`, `EndDate`, `idPlace`, `active`, `reason`) VALUES
(1, 152, '2017-07-12', '2017-11-10', 11, '1', NULL),
(2, 278, '2017-07-18', '2017-11-16', 10, '1', NULL),
(3, 179, '2017-06-01', '2017-09-29', 12, '1', NULL),
(4, 159, '2017-02-09', '2017-06-10', 12, '1', NULL),
(5, 191, '2016-07-28', '2016-11-29', 10, '0', NULL),
(6, 178, '2016-07-18', '2016-11-19', 10, '0', NULL),
(7, 165, '2017-06-12', '2017-10-12', 10, '1', NULL),
(8, 234, '2017-03-13', '2017-07-16', 12, '1', NULL),
(9, 187, '2017-05-02', '2017-08-30', 10, '1', NULL),
(10, 230, '2017-04-12', '2017-08-09', 10, '1', NULL),
(11, 134, '2017-05-10', '2017-09-08', 10, '1', NULL),
(12, 277, '2017-05-07', '2017-09-10', 12, '1', NULL),
(13, 176, '2015-04-08', '2015-08-08', 12, '0', NULL),
(14, 115, '2015-04-16', '2015-08-16', 12, '0', NULL),
(15, 168, '2015-04-18', '2015-08-18', 12, '0', NULL),
(16, 275, '2015-04-25', '2015-08-25', 12, '0', NULL),
(17, 138, '2015-05-12', '2015-09-11', 12, '0', NULL),
(18, 114, '2015-05-14', '2015-09-13', 12, '0', NULL),
(19, 262, '2015-05-17', '2015-09-16', 12, '0', NULL),
(20, 229, '2015-05-22', '2015-09-21', 11, '0', NULL),
(21, 202, '2015-05-28', '2015-09-27', 11, '0', NULL),
(22, 209, '2015-05-30', '2015-09-29', 12, '0', NULL),
(23, 116, '2015-06-15', '2015-10-15', 12, '0', NULL),
(24, 268, '2015-06-21', '2015-10-21', 12, '0', NULL),
(25, 310, '2015-06-30', '2015-10-30', 12, '0', NULL),
(26, 178, '2016-02-07', '2016-06-06', 12, '1', NULL),
(27, 165, '2015-08-08', '2015-12-07', 12, '0', NULL),
(28, 234, '2015-08-10', '2015-12-09', 12, '0', NULL),
(29, 187, '2015-08-24', '2015-12-23', 12, '0', NULL),
(30, 230, '2015-09-08', '2016-01-10', 12, '0', NULL),
(31, 134, '2015-09-23', '2016-01-25', 10, '0', NULL),
(32, 277, '2015-10-03', '2016-02-04', 12, '0', NULL),
(33, 176, '2015-10-13', '2016-02-14', 12, '0', NULL),
(34, 115, '2015-10-21', '2016-02-22', 12, '0', NULL),
(35, 168, '2015-10-23', '2016-02-24', 12, '0', 'economic problems'),
(36, 275, '2015-10-30', '2016-03-01', 12, '0', NULL),
(37, 138, '2015-11-16', '2016-03-18', 12, '0', NULL),
(38, 114, '2015-11-18', '2016-03-20', 12, '0', NULL),
(39, 262, '2015-11-21', '2016-03-23', 12, '0', NULL),
(40, 229, '2015-11-26', '2016-03-28', 11, '0', NULL),
(41, 202, '2015-12-02', '2016-04-03', 11, '0', NULL),
(42, 209, '2015-12-04', '2016-04-05', 12, '0', NULL),
(43, 116, '2015-12-20', '2016-04-21', 12, '0', NULL),
(44, 268, '2015-12-26', '2016-04-27', 12, '0', NULL),
(45, 310, '2016-01-04', '2016-05-06', 12, '0', NULL),
(57, 277, '2016-10-11', '2017-02-11', 12, '0', NULL),
(58, 176, '2016-10-21', '2017-02-21', 12, '1', 'death'),
(59, 115, '2016-10-29', '2017-02-28', 12, '1', NULL),
(60, 168, '2016-11-01', '2017-02-28', 12, '1', NULL),
(61, 275, '2016-05-08', '2016-09-07', 12, '0', NULL),
(62, 138, '2016-05-25', '2016-09-24', 12, '0', 'tired'),
(63, 114, '2016-11-27', '2017-03-26', 12, '1', NULL),
(64, 262, '2016-05-30', '2016-09-29', 12, '0', NULL),
(65, 229, '2016-06-04', '2016-10-04', 11, '0', NULL),
(66, 202, '2016-06-10', '2016-10-10', 11, '0', NULL),
(67, 209, '2016-06-12', '2016-10-12', 12, '0', NULL),
(68, 116, '2016-12-28', '2017-04-28', 12, '1', NULL),
(69, 268, '2016-07-03', '2016-11-04', 10, '0', NULL),
(70, 310, '2017-01-25', '2017-05-28', 10, '0', NULL),
(72, 275, '2017-05-10', '2017-09-11', 12, '1', NULL),
(73, 138, '2017-05-27', '2017-09-28', 12, '0', 'no comments'),
(74, 114, '2016-11-29', '2017-03-30', 12, '0', 'travel outside the country'),
(75, 262, '2017-06-02', '2017-10-02', 12, '1', NULL),
(76, 229, '2017-01-01', '2017-06-30', 10, '1', ''),
(77, 202, '2017-06-26', '2017-10-23', 12, '1', 'boring'),
(78, 209, '2017-06-15', '2017-10-15', 12, '1', NULL),
(79, 116, '2016-12-31', '2017-05-01', 12, '0', NULL),
(80, 268, '2017-07-06', '2017-11-07', 12, '1', NULL),
(81, 310, '2017-07-15', '2017-11-16', 12, '1', NULL),
(87, 121, '2015-08-01', '2015-11-30', 12, '0', NULL),
(88, 144, '2015-08-21', '2015-12-20', 12, '0', NULL),
(89, 258, '2015-09-10', '2016-01-12', 12, '0', NULL),
(90, 274, '2015-10-16', '2016-02-17', 12, '0', NULL),
(91, 232, '2015-11-24', '2016-03-26', 12, '0', NULL),
(92, 244, '2015-04-08', '2015-08-08', 12, '0', NULL),
(93, 204, '2015-05-22', '2015-09-21', 12, '0', NULL),
(94, 143, '2015-05-30', '2015-09-29', 12, '0', NULL),
(95, 154, '2015-07-02', '2015-11-01', 12, '0', ''),
(96, 251, '2015-07-17', '2015-11-16', 12, '0', NULL),
(97, 170, '2015-07-22', '2015-11-21', 12, '0', NULL),
(98, 243, '2017-07-12', '2017-11-10', 11, '1', NULL),
(99, 161, '2017-08-27', '2017-12-25', 11, '1', NULL),
(100, 235, '2017-05-13', '2017-09-11', 10, '1', NULL),
(101, 189, '2017-04-12', '2017-08-09', 11, '1', NULL),
(102, 121, '2016-02-03', '2016-06-05', 12, '0', NULL),
(103, 144, '2016-02-23', '2016-06-25', 12, '0', NULL),
(104, 232, '2016-05-29', '2016-09-28', 12, '0', NULL),
(105, 258, '2016-03-15', '2016-07-14', 12, '0', NULL),
(106, 274, '2016-04-20', '2016-08-20', 12, '0', NULL),
(109, 121, '2017-02-12', '2017-06-11', 12, '1', NULL),
(110, 144, '2017-03-01', '2017-07-02', 12, '0', 'sickness'),
(111, 232, '2017-06-05', '2017-10-05', 12, '1', NULL),
(112, 258, '2017-03-21', '2017-07-22', 12, '1', NULL),
(113, 274, '2017-04-27', '2017-08-27', 12, '1', NULL),
(116, 143, '2016-06-04', '2016-10-05', 12, '1', NULL),
(117, 154, '2016-01-06', '2016-05-08', 12, '0', 'sick'),
(118, 170, '2016-07-26', '2016-11-28', 12, '1', NULL),
(119, 204, '2016-05-26', '2016-09-28', 12, '1', NULL),
(120, 244, '2016-04-13', '2016-08-14', 12, '1', NULL),
(121, 251, '2016-07-21', '2016-11-23', 12, '1', NULL),
(125, 314, '2017-02-12', '2017-08-12', 11, '1', NULL),
(126, 205, '2017-01-01', '2017-12-31', 11, '1', NULL),
(127, 316, '2017-03-04', '2017-09-04', 10, '1', NULL),
(128, 317, '2017-03-04', '2017-09-04', 12, '1', NULL),
(129, 318, '2017-03-04', '2017-09-04', 11, '1', NULL),
(130, 319, '2017-03-01', '2017-09-01', 12, '1', NULL),
(131, 320, '2017-03-01', '2017-09-01', 12, '1', NULL),
(132, 321, '2017-03-01', '2017-09-01', 12, '1', NULL),
(133, 322, '2017-03-01', '2017-09-01', 12, '1', NULL),
(134, 323, '2017-03-01', '2017-09-01', 12, '1', NULL),
(135, 324, '2017-03-01', '2017-09-01', 12, '1', NULL),
(136, 325, '2017-03-01', '2017-09-01', 12, '1', NULL),
(137, 326, '2017-03-01', '2017-09-01', 12, '1', NULL),
(138, 327, '2017-03-01', '2017-09-01', 12, '1', NULL),
(139, 328, '2017-03-01', '2017-09-01', 12, '1', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessionresult`
--

CREATE TABLE `sessionresult` (
  `idResult` int(11) NOT NULL,
  `dt_Result` datetime DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `idSession` int(11) DEFAULT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `namePerson` varchar(200) DEFAULT NULL,
  `Points` int(11) DEFAULT NULL,
  `Picture` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sessionresult`
--

INSERT INTO `sessionresult` (`idResult`, `dt_Result`, `idPlace`, `position`, `idSession`, `idPerson`, `namePerson`, `Points`, `Picture`) VALUES
(1, '2017-02-01 22:26:42', 10, 1, 76, 229, 'Abraham Westbrook', 1340, '5.jpg'),
(2, '2017-02-01 22:26:42', 10, 2, 11, 134, 'Marnie Singler', 940, '13.jpg'),
(3, '2017-02-01 22:26:42', 10, 3, 2, 278, 'Jamison Auber', 800, '15.jpg'),
(4, '2017-02-01 22:26:42', 10, 4, 3, 179, 'Mittie Whelans', 700, '8.jpg'),
(5, '2017-02-01 22:26:42', 10, 4, 10, 230, 'Fleta Ahmed', 700, '3.jpg'),
(6, '2017-02-01 22:26:42', 10, 2, 7, 165, 'Cherish Ostrom', 940, '14.jpg'),
(7, '2017-02-01 22:26:42', 10, 1, 1, 152, 'Penny Lebo', 1040, '14.jpg'),
(8, '2017-02-01 22:26:42', 10, 5, 9, 187, 'Man Lessard', 540, '5.jpg'),
(16, '2017-03-01 22:26:51', 10, 1, 11, 134, 'Marnie Singler', 1040, '15.jpg'),
(17, '2017-03-01 22:26:51', 10, 1, 2, 278, 'Jamison Auber', 1040, '16.jpg'),
(18, '2017-03-01 22:26:51', 10, 2, 3, 179, 'Mittie Whelans', 940, '9.jpg'),
(19, '2017-03-01 22:26:51', 10, 3, 7, 165, 'Cherish Ostrom', 850, '16.jpg'),
(20, '2017-03-01 22:26:51', 10, 4, 76, 229, 'Abraham Westbrook', 700, '1.jpg'),
(21, '2017-03-01 22:26:51', 10, 5, 1, 152, 'Penny Lebo', 600, '6.jpg'),
(22, '2017-03-01 22:26:51', 12, 6, 9, 187, 'Man Lessard', 500, '6.jpg'),
(23, '2017-03-01 22:26:51', 12, 7, 10, 230, 'Fleta Ahmed', 400, '4.jpg'),
(31, '2017-04-01 22:26:51', 10, 1, 11, 134, 'Marnie Singler', 1040, '1.jpg'),
(32, '2017-04-01 22:26:51', 10, 2, 2, 278, 'Jamison Auber', 1000, '1.jpg'),
(33, '2017-04-01 22:26:51', 10, 2, 3, 179, 'Mittie Whelans', 1000, '10.jpg'),
(34, '2017-04-01 22:26:51', 10, 3, 10, 230, 'Fleta Ahmed', 900, '5.jpg'),
(35, '2017-04-01 22:26:51', 10, 3, 7, 165, 'Cherish Ostrom', 900, '2.jpg'),
(36, '2017-04-01 22:26:51', 10, 4, 1, 152, 'Penny Lebo', 800, '7.jpg'),
(37, '2017-04-01 22:26:51', 10, 4, 9, 187, 'Man Lessard', 800, '7.jpg'),
(38, '2017-04-01 22:26:51', 10, 5, 76, 229, 'Abraham Westbrook', 600, '2.jpg'),
(46, '2017-02-01 22:26:51', 12, 1, 4, 159, 'Leonor Atteberies', 1170, '9.jpg'),
(47, '2017-02-01 22:26:51', 12, 2, 8, 234, 'Madeleine Schaible', 910, '7.jpg'),
(48, '2017-02-01 22:26:51', 12, 3, 12, 277, 'Loria Burgesi', 130, '12.jpg'),
(49, '2017-03-01 22:26:51', 12, 1, 4, 159, 'Leonor Atteberies', 910, '10.jpg'),
(50, '2017-03-01 22:26:51', 12, 2, 12, 277, 'Loria Burgesi', 390, '13.jpg'),
(51, '2017-03-01 22:26:51', 12, 3, 8, 234, 'Madeleine Schaible', 390, '8.jpg'),
(52, '2017-04-01 22:26:52', 12, 2, 4, 159, 'Leonor Atteberies', 1300, '11.jpg'),
(53, '2017-04-01 22:26:52', 12, 1, 8, 234, 'Madeleine Schaible', 1390, '9.jpg'),
(54, '2017-01-13 20:11:43', 12, 1, 3, 179, 'Mittie Whelanss', 1340, '11.jpg'),
(55, '2017-01-13 20:11:43', 12, 2, 4, 159, 'Leonor Atteberies', 1040, '12.jpg'),
(56, '2017-01-13 20:11:43', 12, 3, 81, 310, 'Jennell Mcmann', 300, '3.jpg'),
(57, '2016-12-27 20:20:43', 12, 1, 3, 179, 'Mittie Whelanss', 4760, '12.jpg'),
(58, '2016-12-27 20:20:43', 12, 2, 8, 234, 'Madeleine Schaible', 4160, '10.jpg'),
(59, '2016-12-27 20:20:43', 12, 2, 4, 159, 'Leonor Atteberies', 4160, '13.jpg'),
(60, '2017-04-27 20:20:43', 12, 3, 12, 277, 'Loria Burgesi', 860, '14.jpg'),
(61, '2016-12-27 20:20:43', 12, 4, 81, 310, 'Jennell Mcmann', 600, '4.jpg'),
(64, '2017-01-01 20:34:33', 10, 1, 9, 187, 'Man Lessard', 4760, '8.jpg'),
(65, '2017-01-01 20:34:33', 10, 2, 2, 278, 'Jamison Auber', 4160, '2.jpg'),
(66, '2017-01-01 20:34:33', 10, 3, 7, 165, 'Cherish Ostrom', 3900, '3.jpg'),
(67, '2017-01-01 20:34:33', 10, 4, 100, 235, 'Danna Piaseckie', 3770, '11.jpg'),
(68, '2017-01-01 20:34:33', 10, 5, 11, 134, 'Marnie Singler', 3120, '4.jpg'),
(69, '2017-01-01 20:34:33', 10, 6, 10, 230, 'Fleta Ahmed', 2990, '6.jpg');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_catalog`
--
CREATE TABLE `view_catalog` (
`idMeta` int(11)
,`nameMeta` varchar(200)
,`idCatalog` int(11)
,`nameCatalog` varchar(200)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_nutri_indicadores`
--
CREATE TABLE `view_nutri_indicadores` (
`idMeta` int(11)
,`nameMeta` varchar(200)
,`idCatalog` int(11)
,`nameCatalog` varchar(200)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_nutri_ranges`
--
CREATE TABLE `view_nutri_ranges` (
`idMeta` int(11)
,`nameMeta` varchar(200)
,`bgn_catalog_id` int(11)
,`bgn_catalog_value` varchar(100)
,`end_catalog_id` int(11)
,`end_catalog_value` varchar(100)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `view_catalog`
--
DROP TABLE IF EXISTS `view_catalog`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leoncio`@`localhost` SQL SECURITY DEFINER VIEW `view_catalog`  AS  select `mc`.`idMeta` AS `idMeta`,`mc`.`nameMeta` AS `nameMeta`,`c`.`idCatalog` AS `idCatalog`,`c`.`nameCatalog` AS `nameCatalog` from (`metacatalog` `mc` join `catalog` `c` on((`mc`.`idMeta` = `c`.`idMeta`))) order by `mc`.`idMeta` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_nutri_indicadores`
--
DROP TABLE IF EXISTS `view_nutri_indicadores`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leoncio`@`localhost` SQL SECURITY DEFINER VIEW `view_nutri_indicadores`  AS  select `mc`.`idMeta` AS `idMeta`,`mc`.`nameMeta` AS `nameMeta`,`c`.`idCatalog` AS `idCatalog`,`c`.`nameCatalog` AS `nameCatalog` from (`metacatalog` `mc` join `catalog` `c` on((`mc`.`idMeta` = `c`.`idMeta`))) where (`mc`.`idMeta` = `getMetaId`('Indicadores')) order by `c`.`nameCatalog` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_nutri_ranges`
--
DROP TABLE IF EXISTS `view_nutri_ranges`;

CREATE ALGORITHM=UNDEFINED DEFINER=`leoncio`@`localhost` SQL SECURITY DEFINER VIEW `view_nutri_ranges`  AS  select `mc`.`idMeta` AS `idMeta`,`mc`.`nameMeta` AS `nameMeta`,min(`c`.`idCatalog`) AS `bgn_catalog_id`,`getCategoryName`(min(`c`.`idCatalog`)) AS `bgn_catalog_value`,max(`c`.`idCatalog`) AS `end_catalog_id`,`getCategoryName`(max(`c`.`idCatalog`)) AS `end_catalog_value` from (`metacatalog` `mc` join `catalog` `c` on((`mc`.`idMeta` = `c`.`idMeta`))) where (`mc`.`nameMeta` like 'Rangos De %') group by `mc`.`nameMeta` order by `mc`.`nameMeta` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `assistance`
--
ALTER TABLE `assistance`
  ADD PRIMARY KEY (`idAssistance`),
  ADD KEY `FK_Assistance_Catalog` (`idClass`),
  ADD KEY `FK_Assistance_Catalog1` (`idPlace`),
  ADD KEY `FK_Assistance_Catalog2` (`type`),
  ADD KEY `FK_Assistance_Person` (`idCoach`),
  ADD KEY `FK_Assistance_Session` (`idSession`);

--
-- Indices de la tabla `badgeperperson`
--
ALTER TABLE `badgeperperson`
  ADD PRIMARY KEY (`idBPP`),
  ADD KEY `FK_BadgePerPerson_Session` (`idSession`),
  ADD KEY `FK_BadgePerPerson_Badge` (`idBadge`),
  ADD KEY `FK_idNutri` (`idNutri`);

--
-- Indices de la tabla `badges`
--
ALTER TABLE `badges`
  ADD PRIMARY KEY (`idBadge`);

--
-- Indices de la tabla `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`idCatalog`),
  ADD KEY `FK_Catalog_MetaCatalog` (`idMeta`);

--
-- Indices de la tabla `chocale`
--
ALTER TABLE `chocale`
  ADD PRIMARY KEY (`idChocale`),
  ADD KEY `FK_Chocale_Person` (`idGiver`),
  ADD KEY `FK_Chocale_Session` (`idSession`);

--
-- Indices de la tabla `contentperperson`
--
ALTER TABLE `contentperperson`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Person` (`idPerson`),
  ADD KEY `FK_person_cont` (`idCont`),
  ADD KEY `FK_Nutricionist` (`idNutri`);

--
-- Indices de la tabla `detailsnutriappoint`
--
ALTER TABLE `detailsnutriappoint`
  ADD PRIMARY KEY (`idDetails`),
  ADD KEY `FK_DetailsNutriAppoint_NutriAppoint` (`idNutriAppoint`),
  ADD KEY `FK_DetailsNutriAppoint_Catalog` (`data`);

--
-- Indices de la tabla `digitalcontent`
--
ALTER TABLE `digitalcontent`
  ADD PRIMARY KEY (`idCont`),
  ADD KEY `FK_DigitalContent_Catalog` (`contCategory`),
  ADD KEY `FK_DigitalContent_Catalog1` (`contType`);

--
-- Indices de la tabla `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`idEvent`),
  ADD KEY `FK_Category_Event` (`idEvent`),
  ADD KEY `FK_event_category` (`idCategory`);

--
-- Indices de la tabla `favoriteratecontent`
--
ALTER TABLE `favoriteratecontent`
  ADD PRIMARY KEY (`idFavRate`),
  ADD KEY `fav_cont` (`idCont`),
  ADD KEY `fav_session` (`idSession`);

--
-- Indices de la tabla `indicatorinformation`
--
ALTER TABLE `indicatorinformation`
  ADD PRIMARY KEY (`idIndicatorInformation`),
  ADD KEY `gender_FK` (`gender`),
  ADD KEY `indicator_FK` (`idIndicador`);

--
-- Indices de la tabla `indicatorvaluerange`
--
ALTER TABLE `indicatorvaluerange`
  ADD PRIMARY KEY (`idIndicatorValueRange`),
  ADD KEY `indicatorvaluerange_ibfk_1` (`idIndicatorInfo`),
  ADD KEY `indicatorvaluerange_ibfk_2` (`idCatalog`),
  ADD KEY `Color` (`Color`);

--
-- Indices de la tabla `metacatalog`
--
ALTER TABLE `metacatalog`
  ADD PRIMARY KEY (`idMeta`);

--
-- Indices de la tabla `notedescription`
--
ALTER TABLE `notedescription`
  ADD PRIMARY KEY (`idNoteDescription`),
  ADD KEY `FK_NoteDescription` (`idNoteSession`);

--
-- Indices de la tabla `notesession`
--
ALTER TABLE `notesession`
  ADD PRIMARY KEY (`idNoteSession`),
  ADD KEY `FK_NoteSession` (`idSession`);

--
-- Indices de la tabla `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`idNotification`),
  ADD KEY `FK_typeNotification` (`idTypeNoti`),
  ADD KEY `FK_personNotification` (`idPerson`);

--
-- Indices de la tabla `nutriappoint`
--
ALTER TABLE `nutriappoint`
  ADD PRIMARY KEY (`idNutriAppoint`),
  ADD KEY `FK_NutriAppoint_Catalog` (`idAppointType`),
  ADD KEY `FK_NutriAppoint_Catalog1` (`type`),
  ADD KEY `FK_NutriAppoint_Catalog2` (`idPlace`),
  ADD KEY `FK_NutriAppoint_Person1` (`idNutricionist`),
  ADD KEY `FK_NutriAppoint_Session` (`idSession`),
  ADD KEY `FK_NutriAppoint_Catalog3` (`idMeasureType`);

--
-- Indices de la tabla `peopleperevent`
--
ALTER TABLE `peopleperevent`
  ADD PRIMARY KEY (`idPPE`),
  ADD KEY `FK_PeoplePerEvent_Event` (`idEvent`),
  ADD KEY `FK_PeoplePerEvent_Person` (`idPerson`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`idPerson`),
  ADD KEY `FK_Person_Catalog` (`typePerson`),
  ADD KEY `FK_Person_Catalog1` (`typeRegistration`);

--
-- Indices de la tabla `phrases`
--
ALTER TABLE `phrases`
  ADD PRIMARY KEY (`idPhrase`),
  ADD KEY `FK_Category_Phrase` (`idCategory`);

--
-- Indices de la tabla `picsperperson`
--
ALTER TABLE `picsperperson`
  ADD PRIMARY KEY (`idPicPerPerson`),
  ADD KEY `fk_picperperson` (`idPerson`);

--
-- Indices de la tabla `ratepro`
--
ALTER TABLE `ratepro`
  ADD PRIMARY KEY (`idRate`),
  ADD KEY `FK_RateTeacher_Catalog` (`idValue`),
  ADD KEY `FK_RateTeacher_Person` (`idPro`),
  ADD KEY `FK_RateTeacher_Person1` (`idPerson`);

--
-- Indices de la tabla `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`idSession`),
  ADD KEY `FK_Session_Person` (`idPerson`),
  ADD KEY `FK_Session_Place` (`idPlace`);

--
-- Indices de la tabla `sessionresult`
--
ALTER TABLE `sessionresult`
  ADD PRIMARY KEY (`idResult`),
  ADD KEY `FK_SessionResult_Catalog` (`idPlace`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `assistance`
--
ALTER TABLE `assistance`
  MODIFY `idAssistance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1741;
--
-- AUTO_INCREMENT de la tabla `badgeperperson`
--
ALTER TABLE `badgeperperson`
  MODIFY `idBPP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT de la tabla `badges`
--
ALTER TABLE `badges`
  MODIFY `idBadge` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `catalog`
--
ALTER TABLE `catalog`
  MODIFY `idCatalog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT de la tabla `chocale`
--
ALTER TABLE `chocale`
  MODIFY `idChocale` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=498;
--
-- AUTO_INCREMENT de la tabla `contentperperson`
--
ALTER TABLE `contentperperson`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT de la tabla `detailsnutriappoint`
--
ALTER TABLE `detailsnutriappoint`
  MODIFY `idDetails` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=641;
--
-- AUTO_INCREMENT de la tabla `digitalcontent`
--
ALTER TABLE `digitalcontent`
  MODIFY `idCont` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT de la tabla `event`
--
ALTER TABLE `event`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `favoriteratecontent`
--
ALTER TABLE `favoriteratecontent`
  MODIFY `idFavRate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT de la tabla `indicatorinformation`
--
ALTER TABLE `indicatorinformation`
  MODIFY `idIndicatorInformation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `indicatorvaluerange`
--
ALTER TABLE `indicatorvaluerange`
  MODIFY `idIndicatorValueRange` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `metacatalog`
--
ALTER TABLE `metacatalog`
  MODIFY `idMeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `notedescription`
--
ALTER TABLE `notedescription`
  MODIFY `idNoteDescription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `notesession`
--
ALTER TABLE `notesession`
  MODIFY `idNoteSession` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `notification`
--
ALTER TABLE `notification`
  MODIFY `idNotification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `nutriappoint`
--
ALTER TABLE `nutriappoint`
  MODIFY `idNutriAppoint` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `peopleperevent`
--
ALTER TABLE `peopleperevent`
  MODIFY `idPPE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `idPerson` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=329;
--
-- AUTO_INCREMENT de la tabla `phrases`
--
ALTER TABLE `phrases`
  MODIFY `idPhrase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `picsperperson`
--
ALTER TABLE `picsperperson`
  MODIFY `idPicPerPerson` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `ratepro`
--
ALTER TABLE `ratepro`
  MODIFY `idRate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `session`
--
ALTER TABLE `session`
  MODIFY `idSession` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;
--
-- AUTO_INCREMENT de la tabla `sessionresult`
--
ALTER TABLE `sessionresult`
  MODIFY `idResult` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `assistance`
--
ALTER TABLE `assistance`
  ADD CONSTRAINT `FK_Assistance_Catalog` FOREIGN KEY (`idClass`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Assistance_Catalog1` FOREIGN KEY (`idPlace`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Assistance_Catalog2` FOREIGN KEY (`type`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Assistance_Person` FOREIGN KEY (`idCoach`) REFERENCES `person` (`idPerson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Assistance_Session` FOREIGN KEY (`idSession`) REFERENCES `session` (`idSession`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `badgeperperson`
--
ALTER TABLE `badgeperperson`
  ADD CONSTRAINT `FK_BadgePerPerson_Badge` FOREIGN KEY (`idBadge`) REFERENCES `badges` (`idBadge`),
  ADD CONSTRAINT `FK_BadgePerPerson_Session` FOREIGN KEY (`idSession`) REFERENCES `session` (`idSession`),
  ADD CONSTRAINT `FK_idNutri` FOREIGN KEY (`idNutri`) REFERENCES `person` (`idPerson`);

--
-- Filtros para la tabla `catalog`
--
ALTER TABLE `catalog`
  ADD CONSTRAINT `FK_Catalog_MetaCatalog` FOREIGN KEY (`idMeta`) REFERENCES `metacatalog` (`idMeta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `chocale`
--
ALTER TABLE `chocale`
  ADD CONSTRAINT `FK_Chocale_Person` FOREIGN KEY (`idGiver`) REFERENCES `person` (`idPerson`),
  ADD CONSTRAINT `FK_Chocale_Session` FOREIGN KEY (`idSession`) REFERENCES `session` (`idSession`);

--
-- Filtros para la tabla `contentperperson`
--
ALTER TABLE `contentperperson`
  ADD CONSTRAINT `FK_Nutricionist` FOREIGN KEY (`idNutri`) REFERENCES `person` (`idPerson`),
  ADD CONSTRAINT `FK_person` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`),
  ADD CONSTRAINT `FK_person_cont` FOREIGN KEY (`idCont`) REFERENCES `digitalcontent` (`idCont`);

--
-- Filtros para la tabla `detailsnutriappoint`
--
ALTER TABLE `detailsnutriappoint`
  ADD CONSTRAINT `FK_DetailsNutriAppoint_Catalog` FOREIGN KEY (`data`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DetailsNutriAppoint_NutriAppoint` FOREIGN KEY (`idNutriAppoint`) REFERENCES `nutriappoint` (`idNutriAppoint`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `digitalcontent`
--
ALTER TABLE `digitalcontent`
  ADD CONSTRAINT `FK_DigitalContent_Catalog` FOREIGN KEY (`contCategory`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DigitalContent_Catalog1` FOREIGN KEY (`contType`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_event_category` FOREIGN KEY (`idCategory`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `favoriteratecontent`
--
ALTER TABLE `favoriteratecontent`
  ADD CONSTRAINT `fav_cont` FOREIGN KEY (`idCont`) REFERENCES `digitalcontent` (`idCont`),
  ADD CONSTRAINT `fav_session` FOREIGN KEY (`idSession`) REFERENCES `session` (`idSession`);

--
-- Filtros para la tabla `indicatorinformation`
--
ALTER TABLE `indicatorinformation`
  ADD CONSTRAINT `gender_FK` FOREIGN KEY (`gender`) REFERENCES `catalog` (`idCatalog`),
  ADD CONSTRAINT `indicator_FK` FOREIGN KEY (`idIndicador`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `indicatorvaluerange`
--
ALTER TABLE `indicatorvaluerange`
  ADD CONSTRAINT `indicatorvaluerange_ibfk_1` FOREIGN KEY (`Color`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `notedescription`
--
ALTER TABLE `notedescription`
  ADD CONSTRAINT `FK_NoteDescription` FOREIGN KEY (`idNoteSession`) REFERENCES `notesession` (`idNoteSession`);

--
-- Filtros para la tabla `notesession`
--
ALTER TABLE `notesession`
  ADD CONSTRAINT `FK_NoteSession` FOREIGN KEY (`idSession`) REFERENCES `session` (`idSession`);

--
-- Filtros para la tabla `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `FK_personNotification` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`),
  ADD CONSTRAINT `FK_typeNotification` FOREIGN KEY (`idTypeNoti`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `nutriappoint`
--
ALTER TABLE `nutriappoint`
  ADD CONSTRAINT `FK_NutriAppoint_Catalog` FOREIGN KEY (`idAppointType`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NutriAppoint_Catalog1` FOREIGN KEY (`type`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NutriAppoint_Catalog2` FOREIGN KEY (`idPlace`) REFERENCES `catalog` (`idCatalog`),
  ADD CONSTRAINT `FK_NutriAppoint_Catalog3` FOREIGN KEY (`idMeasureType`) REFERENCES `catalog` (`idCatalog`),
  ADD CONSTRAINT `FK_NutriAppoint_Person1` FOREIGN KEY (`idNutricionist`) REFERENCES `person` (`idPerson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NutriAppoint_Session` FOREIGN KEY (`idSession`) REFERENCES `session` (`idSession`);

--
-- Filtros para la tabla `peopleperevent`
--
ALTER TABLE `peopleperevent`
  ADD CONSTRAINT `FK_PeoplePerEvent_Event` FOREIGN KEY (`idEvent`) REFERENCES `event` (`idEvent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PeoplePerEvent_Person` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `FK_Person_Catalog` FOREIGN KEY (`typePerson`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Person_Catalog1` FOREIGN KEY (`typeRegistration`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `phrases`
--
ALTER TABLE `phrases`
  ADD CONSTRAINT `FK_Category_Phrase` FOREIGN KEY (`idCategory`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `picsperperson`
--
ALTER TABLE `picsperperson`
  ADD CONSTRAINT `fk_picperperson` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`);

--
-- Filtros para la tabla `ratepro`
--
ALTER TABLE `ratepro`
  ADD CONSTRAINT `FK_RateTeacher_Person` FOREIGN KEY (`idPro`) REFERENCES `person` (`idPerson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_RateTeacher_Person1` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `FK_Session_Person` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Session_Place` FOREIGN KEY (`idPlace`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sessionresult`
--
ALTER TABLE `sessionresult`
  ADD CONSTRAINT `FK_SessionResult_Catalog` FOREIGN KEY (`idPlace`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
