<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Entrenador'){header("Location:../General/login.php");}
?> 
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>Tomar Asistencia</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div id="top">
        <div class="row">
            <div class="col-xs-9" id="div_logo">
                <img src="../../Multimedia/img/LogoTigofit.png" alt="" id="logo">
            </div>
            <div class="col-xs-3" id="div_logout" style="text-align: right;">
                <a href="../General/logout.php">
                    <i class="fa fa-power-off fa-2x" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 white-div"></div>
    <div style="padding-left: 10px; padding-right: 10px; text-align: center">
        <div id="userInfo_div"></div>
        <br/>
        <div id="ddl_activity_div"></div>
        <br/>
        <button type="button" class="btn btn-info" id="btn_register">Registrar</button>
        <br/><br/>
        <p id="a" class="fadeeffect" style="background-color: #32CD32;"></p>
        <p id="b" class="fadeeffect" style="background-color: red;"></p>
    </div>
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="../../Controller/js/User/Appearance.js" type="text/javascript"></script>
</body>
</html>
