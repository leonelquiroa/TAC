<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Integrante'){header("Location:../General/login.php");}
?> 
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>Usuario</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link href="../../Controller/css/User/barProgress.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/animation.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/ownTable4col.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div id="mySidenav" class="sidenav">
        <a href="../User/Home.php"><img src="../../Multimedia/img/MenuIcons/inicio.png" height="30" alt=""><br>Inicio</a>
        <a href="../User/Calendar.php"><img src="../../Multimedia/img/MenuIcons/calendarios.png" height="30" alt=""><br>Calendario</a>
        <a href="../User/Attendance.php"><img src="../../Multimedia/img/MenuIcons/asistencias.png" height="30" alt=""><br>Asistencias</a>
        <a href="../User/Control.php"><img src="../../Multimedia/img/MenuIcons/controlnutricional.png" height="30" alt=""><br>Control Nutricional</a>
        <a href="../User/Diet_Effort.php"><img src="../../Multimedia/img/MenuIcons/dietas.png" height="30" alt=""><br>Dietas & Pruebas</a>
        <a href="../User/Routine.php"><img src="../../Multimedia/img/MenuIcons/videos.png" height="30" alt=""><br>Rutinas</a>
        <a href="../User/Article_Recipe.php"><img src="../../Multimedia/img/MenuIcons/articulosrecetas.png" height="30" alt=""><br>Articulos & Recetas</a>
        <a href="../General/logout.php"><img src="../../Multimedia/img/MenuIcons/salir.png" height="30" alt=""><br>Salir</a>
    </div>
    <div id="top">
        <div class="col-xs-10" id="div_logo">
            <a href="../User/Home.php">
                <img src="../../Multimedia/img/LogoTigofit.png" alt="" id="logo">
            </a>
        </div>
        <div class="col-xs-2" id="div_three_bars">
            <span onclick="openCloseNavMenu()">
                <i id="three_bars" class="fa fa-bars" aria-hidden="true" style="color: white;"></i>
            </span>
        </div>
    </div>
    <div id="notifications">
        <div class="container_row">
            <div class="block">
                <span class="badge number-notify" id="span_bell"></span>
            </div>
            <div class="block">
                <span class="badge number-notify" id="span_calendar"></span>
            </div>
            <div class="block"></div>
        </div>
        <div class="container_row">
            <div class="block" id="notifyPersonal">
                <a href="../User/notifications.php">
                    <img src="../../Multimedia/img/Notifications/campana.png" height="30" alt="" class="icon-notify">
                </a>
            </div>
            <div class="block" id="notifyCalendar">
                <a href="../User/notifications.php">
                    <img src="../../Multimedia/img/Notifications/calendario.png" height="30" alt="" class="icon-notify">
                </a>
            </div>
            <div class="block">
                <div id="profilePic_div"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 notify-white-div"></div>
    <div style="padding-left: 10px; padding-right: 10px;">
        <br/>
        <div id="Phrase_div"></div>
        <br/>
        <div style="background-color: #e3e3ec; text-align: center; font-size: 17px;">EL CAMINO HACIA MI <strong>CUMBRE</strong></div>
        <div class="center-block anima" id="anima_div"></div>
        <br/>
        <div id="Bars_div"></div>
        <br/>
        <div style="background-color: #efdd97; font-size: 16px;">
            <img src="../../Multimedia/img/crownIcon.png" height="30" alt=""> <strong>LOGROS</strong>
        </div>
        <br/>
        <div id="Badges_div"></div>
        <br/>
        <div style="font-size: 18px;">
            <img src="../../Multimedia/img/laurelIcon.png" height="30" alt=""> <strong>LOS 10 +</strong>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-7">
                <div id="ddl_place_div"></div>
            </div>
            <div class="col-xs-5">
                <div id="ddl_time_div"></div>
            </div>
        </div>
        <br/>
        <div id="TopTen_div"></div>
        <div style="font-size: 18px;">
            <i class="fa fa-qrcode" aria-hidden="true"></i>
            <strong>QR Code</strong>
        </div>
        <div style="text-align: center;">
            <div id="qrimage_div"></div>
        </div>
        <br/>
    </div>
    <div id="dummy_div"></div>
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="../../Controller/js/User/home.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>    
</body>
</html>
