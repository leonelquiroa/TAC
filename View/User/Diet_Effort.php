<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Integrante'){header("Location:../General/login.php");}
?> 
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">        
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>Dietas & Pruebas de Esfuerzo</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/horizontalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/User/ownStyle.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/favContent.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <div id="mySidenav" class="sidenav">
        <a href="../User/Home.php"><img src="../../Multimedia/img/MenuIcons/inicio.png" height="30" alt=""><br>Inicio</a>
        <a href="../User/Calendar.php"><img src="../../Multimedia/img/MenuIcons/calendarios.png" height="30" alt=""><br>Calendario</a>
        <a href="../User/Attendance.php"><img src="../../Multimedia/img/MenuIcons/asistencias.png" height="30" alt=""><br>Asistencias</a>
        <a href="../User/Control.php"><img src="../../Multimedia/img/MenuIcons/controlnutricional.png" height="30" alt=""><br>Control Nutricional</a>
        <a href="../User/Diet_Effort.php"><img src="../../Multimedia/img/MenuIcons/dietas.png" height="30" alt=""><br>Dietas & Pruebas</a>
        <a href="../User/Routine.php"><img src="../../Multimedia/img/MenuIcons/videos.png" height="30" alt=""><br>Rutinas</a>
        <a href="../User/Article_Recipe.php"><img src="../../Multimedia/img/MenuIcons/articulosrecetas.png" height="30" alt=""><br>Articulos & Recetas</a>
        <a href="../General/logout.php"><img src="../../Multimedia/img/MenuIcons/salir.png" height="30" alt=""><br>Salir</a>
    </div>
    <div id="top">
        <div class="col-xs-10" id="div_logo">
            <a href="../User/Home.php">
                <img src="../../Multimedia/img/LogoTigofit.png" alt="" id="logo">
            </a>
        </div>
        <div class="col-xs-2" id="div_three_bars">
            <span onclick="openCloseNavMenu()">
                <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </div>
    </div>
    <!--<div id="myAlert" class="alert alert-info" hidden="true"></div>-->
    <div class="col-xs-12 white-div"></div>
    <!-- ################################################# MENU ################################################# -->
    <div style="padding-left: 10px; padding-right: 10px;">
        <div style="color: #1DB5C0; font-size: 20px;">
            <i class="fa fa-cutlery" aria-hidden="true"></i>
            <strong>DIETAS</strong>
        </div>
        <br/>            
        <div id="table_diet_div"></div> 
        <div id="signs_Dietas_fav_rate_div"></div>
        <br/>
        <div id="table_Dietas_Fav_Rate_div"></div> 
        <br/>
        <div style="background-color: lightgray;"><br/></div>
        <br/>
        <div style="color: #1DB5C0; font-size: 18px;">
            <i class="fa fa-gratipay" aria-hidden="true"></i>
            <strong>PRUEBAS DE ESFUERZO</strong>
        </div>
        <br/>            
        <div id="table_effort_div"></div> 
        <div id="signs_Esfuerzo_fav_rate_div"></div>
        <br/>
        <div id="table_Esfuerzo_Fav_Rate_div"></div> 
        <br/>
        <div id="dummy_div"></div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/Diet_Effort.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/RatingContent.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>        
</body>
</html>
