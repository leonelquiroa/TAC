<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Integrante'){header("Location:../General/login.php");}
?> 
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">        
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>Rutinas</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/horizontalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/favContent.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div id="mySidenav" class="sidenav">
        <a href="../User/Home.php"><img src="../../Multimedia/img/MenuIcons/inicio.png" height="30" alt=""><br>Inicio</a>
        <a href="../User/Calendar.php"><img src="../../Multimedia/img/MenuIcons/calendarios.png" height="30" alt=""><br>Calendario</a>
        <a href="../User/Attendance.php"><img src="../../Multimedia/img/MenuIcons/asistencias.png" height="30" alt=""><br>Asistencias</a>
        <a href="../User/Control.php"><img src="../../Multimedia/img/MenuIcons/controlnutricional.png" height="30" alt=""><br>Control Nutricional</a>
        <a href="../User/Diet_Effort.php"><img src="../../Multimedia/img/MenuIcons/dietas.png" height="30" alt=""><br>Dietas & Pruebas</a>
        <a href="../User/Routine.php"><img src="../../Multimedia/img/MenuIcons/videos.png" height="30" alt=""><br>Rutinas</a>
        <a href="../User/Article_Recipe.php"><img src="../../Multimedia/img/MenuIcons/articulosrecetas.png" height="30" alt=""><br>Articulos & Recetas</a>
        <a href="../General/logout.php"><img src="../../Multimedia/img/MenuIcons/salir.png" height="30" alt=""><br>Salir</a>
    </div>
    <div id="top">
        <div class="col-xs-10" id="div_logo">
            <a href="../User/Home.php">
                <img src="../../Multimedia/img/LogoTigofit.png" alt="" id="logo">
            </a>
        </div>
        <div class="col-xs-2" id="div_three_bars">
            <span onclick="openCloseNavMenu()">
                <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </div>
    </div>
    <div class="col-xs-12 white-div"></div>
    <div style="padding-left: 10px; padding-right: 10px;">
        <div style="color: #1DB5C0; font-size: 20px;">
            <i class="fa fa-video-camera" aria-hidden="true"></i>
            <strong>VIDEOS</strong>
        </div>
        <br/>
        <div id="level_div"></div> 
        <br/>
        <div id="table_routine_div"></div>
        <br/>
        <div class="important_message">
            <h3><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Importante</h3>
            <p>¡Evita Lesiones!. Asegurate de guiarte con el video de <strong>calentamiento</strong> y <strong>enfriamiento</strong> antes y después de hacer tu rutina.</p> 
        </div>
        <br/>
        <div id="signs_fav_rate_div"></div>
        <br/>
        <div id="table_Videos_Fav_Rate_div"></div>
        <br/>
    </div>
    <div id="dummy_div"></div>
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="../../Controller/js/User/Routine.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/RatingContent.js" type="text/javascript"></script>
    <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>
</body>
</html>
