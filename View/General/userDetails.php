<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Ususario - Inicio</title>    
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/jasny-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/Form.css" rel="stylesheet" type="text/css"/>  
    <link href="../../Controller/css/General/userControl.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div class="bigTitle">
                <i class="fa fa-user"></i> USUARIOS
            </div>
            <br/>
            <div id="bigContainer">
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="text-align: center;">
                        <div id="all_images_div"></div>
                    </div>
                    <div class="col-md-7">
                        <div id="top_details_div"></div>
                        <br/><br/>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="Bars_div"></div>
                            </div>
                            <div class="col-md-6">
                                <p class="pIndicators">ABC</p>
                                <table class="table table-condensed">
                                    <thead>
                                      <tr>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="success">
                                        <td>Success</td>
                                        <td>Doe</td>
                                        <td>john@example.com</td>
                                      </tr>
                                      <tr class="danger">
                                        <td>Danger</td>
                                        <td>Moe</td>
                                        <td>mary@example.com</td>
                                      </tr>
                                      <tr class="info">
                                        <td>Info</td>
                                        <td>Dooley</td>
                                        <td>july@example.com</td>
                                      </tr>
                                    </tbody>
                                </table>                          
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <ul class="nav nav-stacked internalMenu">
                            <li><a href="users.php">Listado</a></li>
                            <li><a href="userDetails.php">Detalles</a></li>
                            <li><a href="userControl.php">Control</a></li>
                            <li><a href="userFollowing.php">Seguimiento</a></li>
                            <li><a href="userBadges.php">Badges</a></li>
                        </ul>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-8">
                                <div id="table_details_div"></div>
                            </div>
                            <div class="col-md-4" style="text-align: center;">
                                <div id="qr_div"></div>
                                <button type="button" class="btn btn-default" id="regenerateQR">Regenerar QR</button>
                                <div id="image_div"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
            </div>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/moment.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jquery.dataTables.1.10.12.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/dataTables.responsive.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/userDetails.js" type="text/javascript"></script>        
</body>
</html>

