<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Ususario - Inicio</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/userControl.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div class="bigTitle">
                <i class="fa fa-user"></i> USUARIOS
            </div>
            <br/>
            <div id="bigContainer">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="nav nav-tabs internalMenu">
                            <li><a href="users.php">Listado</a></li>
                            <li><a href="userDetails.php">Detalles</a></li>
                            <li><a href="userControl.php">Control</a></li>
                            <li><a href="userFollowing.php">Seguimiento</a></li>
                            <li><a href="userBadges.php">Badges</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div id="list_badges_div"></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/userBadges.js" type="text/javascript"></script>    
    <script src="../../Controller/js/Nutricionist/gauge.js" type="text/javascript"></script>    
</body>
</html>

