<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Badges</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/verticalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/badges.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                <i class="fa fa-trophy"></i> BADGES
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <strong>Listado de Badges</strong>
                    </div>
                    <br/>
                    <div id="all_badges_list_div"></div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <strong>Agregar un nuevo Badge</strong>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="txt_title" placeholder="Titulo"><br/>
                    <input type="text" class="form-control" id="txt_value" placeholder="Valor*"><br/>
                    <textarea class="form-control" rows="3" id="txt_description" placeholder="Description"></textarea><br/>
                    <button class="btn btn-block btn-success" type="button" id="btn_save">Guardar</button>
                </div>
                <div class="col-md-3" style="text-align: center;">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width:200px; height:200px">
                            <img src="../../Multimedia/img/badges/no_image.png" alt="..." id="imgBadge">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;" id="new_photo_form_preview"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Buscar</span>
                                <span class="fileinput-exists">¿Otra?</span>
                                <input type="file" name="..." id="ephoto-upload" >
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Cancelar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <hr/>
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <strong>Asignación de Badges</strong>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <div id="place_div"></div>
                </div>
                <div class="col-md-4">
                    <div id="people_div"></div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div id="example-2-1">
                        <div class="column left">
                            <div id="badges_list_div"></div>
                        </div>
                        <div class="column right">
                            <div id="badges_assigned_div"></div>
                        </div>
                    </div>
                    <div class="clearer"></div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-2">
                    <button class="btn btn-block btn-success" type="button" id="btn_assign">Guardar</button>
                </div>
                <div class="col-md-5"></div>
            </div>  
            <br/>
            <div id="dummy_div"></div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script src="../../Controller/js/General/badges.js" type="text/javascript"></script>        
</body>
</html>
