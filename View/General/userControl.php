<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])){header("Location:../General/login.php");}
    if($_SESSION['typePerson']!='Administrador'&&$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Ususario - Inicio</title>    
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css">
    <link href="../../Controller/css/General/userControl.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div class="bigTitle">
                <i class="fa fa-user"></i> USUARIOS
            </div>
            <br/>
            <div id="bigContainer">
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="text-align: center;">
                        <div id="all_images_div"></div>
                    </div>
                    <div class="col-md-7">
                        <div id="top_details_div"></div>
                        <br/><br/>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="Bars_div"></div>
                            </div>
                            <div class="col-md-6">
                                <p class="pIndicators">ABC</p>
                                <table class="table table-condensed">
                                    <thead>
                                      <tr>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="success">
                                        <td>Success</td>
                                        <td>Doe</td>
                                        <td>john@example.com</td>
                                      </tr>
                                      <tr class="danger">
                                        <td>Danger</td>
                                        <td>Moe</td>
                                        <td>mary@example.com</td>
                                      </tr>
                                      <tr class="info">
                                        <td>Info</td>
                                        <td>Dooley</td>
                                        <td>july@example.com</td>
                                      </tr>
                                    </tbody>
                                </table>                          
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <ul class="nav nav-stacked internalMenu">
                            <li><a href="users.php">Listado</a></li>
                            <li><a href="userDetails.php">Detalles</a></li>
                            <li><a href="userControl.php">Control</a></li>
                            <li><a href="userFollowing.php">Seguimiento</a></li>
                            <li><a href="userBadges.php">Badges</a></li>
                        </ul>
                        <br/>
                        <canvas id="temp-gauge-0"></canvas>
                        <canvas id="temp-gauge-1"></canvas>
                        <canvas id="temp-gauge-2"></canvas>
                        <canvas id="temp-gauge-3"></canvas>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-9">
                                <div id="indicator_ddl_div"></div>
                                <div id="ddl_type_appoint_div"></div>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-success" style="width: 100%;" id="btn_new_appoint">Nueva</button>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-success" style="width: 100%;" id="btn_save_new">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger" style="width: 100%;" id="btn_cancel_new">
                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br/>
                        <div id="indicator_past_div"></div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-default" style="width: 100%;" id="btn_update">Actualizar</button>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br/>
                        <div id="indicator_table_div"></div>
                        <br/>
                        <div id="canvas_div"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
            </div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script src="../../Controller/js/Nutricionist/gauge.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/userControl.js" type="text/javascript"></script>        
</body>
</html>

