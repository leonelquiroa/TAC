<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Administrador'){header("Location:../General/login.php");}
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Administrador - Calificaciones</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/verticalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>    
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <!-- Top Menu Items -->
        <div id="MenuTopDiv"></div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div id="MenuContentDiv"></div>
    </nav>
    <!-- ################################################# MENU ################################################# -->      
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">          
                <div class="white-div"></div>
                <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                    <i class="fa fa-area-chart"></i> RENDIMIENTO
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-2">
                        <div id="rol_type_div"></div>
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="date_div">
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div id="table_pro_div"></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div id="table_details_div"></div>
                    </div>
                    <div class="col-md-5">
                        <div id="graph_div"></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div><!-- /#wrapper --><!-- /#page-wrapper --><!-- /.container-fluid -->
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/Admin/rate.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/verticalScroll.js" type="text/javascript"></script>    
</body>
</html>
