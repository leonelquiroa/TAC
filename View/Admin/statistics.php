<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Administrador'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Administrador - Estadísticas</title>
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>    
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <!-- Top Menu Items -->
        <div id="MenuTopDiv"></div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div id="MenuContentDiv"></div>
    </nav>
    <!-- ################################################# MENU ################################################# -->      
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="white-div"></div>
                <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                    <i class="fa fa-bar-chart-o"></i> ESTADISTICAS
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <div id="filter_div"></div>
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="time_span" placeholder="dd/mm/yyyy">
                    </div>
                    <div class="col-md-7"></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Movimiento de personas </h3>
                            </div>
                            <div class="panel-body">
                                <div id="graphPeople_div"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Asistencia por Género </h3>
                            </div>
                            <div class="panel-body">
                                <div id="graphGender_div"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Asistencia por Horario </h3>
                            </div>
                            <div class="panel-body">
                                <div id="graphSchedule_div"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Asistencia por Actividad </h3>
                            </div>
                            <div class="panel-body">
                                <div id="graphClass_div"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div><!-- /#wrapper --><!-- /#page-wrapper --><!-- /.container-fluid -->
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>    
    <script src="../../Controller/js/Admin/statistics.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/menu.js" type="text/javascript"></script>    
</body>
</html>
