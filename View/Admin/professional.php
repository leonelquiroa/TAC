<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Administrador'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Administrador - Profesionales</title>    
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/jasny-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/Form.css" rel="stylesheet" type="text/css"/>    
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>        
</head>
<body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <!-- Top Menu Items -->
        <div id="MenuTopDiv"></div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div id="MenuContentDiv"></div>
    </nav>
    <!-- ################################################# MENU ################################################# -->       
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="white-div"></div>
                <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                    <i class="fa fa-users"></i> PROFESIONAL
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-2">
                        <div id="rol_type_div"></div>
                    </div>
                    <div class="col-md-2">
                        <div id="rol_list_div"></div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div id="pro_details_div"></div>
                    </div>
                    <div class="col-md-3"></div>
                </div>    
                <hr/>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-2">
                        <div id="dll_pro_time_div"></div>
                    </div>
                    <div class="col-md-2">
                        <input type="hidden" class="form-control" id="txt_time_pro_bgn">
                    </div>
                    <div class="col-md-2">
                        <input type="hidden" class="form-control" id="txt_time_pro_end">
                    </div>
                    <div class="col-md-3"></div>
                </div> 
                <hr/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                       <div id="chart_div"></div> 
                    </div>
                    <div class="col-md-3">
                       <div id="table_div"></div> 
                    </div>
                    <div class="col-md-2"></div>
                </div> 
            </div>
        </div>
    </div><!-- /#wrapper --><!-- /#page-wrapper --><!-- /.container-fluid -->
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/Admin/professional.js" type="text/javascript"></script>    
</body>
</html>

