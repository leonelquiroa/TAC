<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Administrador'){header("Location:../General/login.php");}
?> 
<html>
<head>
    <meta charset="UTF-8">
    <title>Ventas</title>    
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>        
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">           
                <div class="white-div"></div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div id="place_div"></div>
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="date_sedentary_active">
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-heartbeat"></i> Sedentarios/Activos </h3>
                            </div>
                            <div class="panel-body">
                                <div id="chart_div_1"></div>
                                <div id="sedentary_active_div"></div>
                            </div>
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-venus"></i><i class="fa fa-mars"></i> Genero </h3>
                            </div>
                            <div class="panel-body">
                                <div id="chart_div_2"></div>
                                <div id="gender_div"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-hourglass-end"></i> Rangos de Edad </h3>
                            </div>
                            <div class="panel-body">
                                <div id="chart_div_3"></div>
                                <div id="age_div"></div>
                            </div>
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cubes"></i> Campos </h3>
                            </div>
                            <div class="panel-body">
                                <div id="chart_div_4"></div>
                                <div id="camp_div"></div>
                            </div>
                        </div>                
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-globe"></i> Peso & IMC - Rangos </h3>
                            </div>
                            <div class="panel-body">        
                                <div id="global_stats_table"></div>
                            </div>
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-trophy"></i> Peso & IMC - Exito y Fallos </h3>
                            </div>
                            <div class="panel-body">        
                                <div id="goal_table"></div>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/Sales/report.js" type="text/javascript"></script>    
</body>
</html>
