<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Indicadores</title>        
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/horizontalScroll.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-fixed-top" role="navigation"> 
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div id="MenuHeaderDiv"></div>
    </div>
    <div id="MenuTopDiv"></div>
    <div id="MenuContentDiv"></div>
</nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                <i class="fa fa-bar-chart"></i> INDICADORES
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <div id="place_div"></div>
                </div>
                <div class="col-md-3">
                    <div id="people_div"></div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-4">
                            <div style="color: gray; font-size: 20px;">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                <strong>Detalle</strong>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div id="indicator_ddl_div"></div>
                        </div>
                    </div>
                    <br/>
                    <div id="indicator_table_div"></div>
                </div>
                <div class="col-md-4">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <strong>Información</strong>
                    </div>
                    <div id="indicator_past_div"></div>
                    <br/>
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <strong>Tiempo</strong>
                    </div>
                    <div id="canvas_div"></div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8" style="text-align: center;">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <strong>Principales Indicadores</strong>
                    </div>            
                    <canvas id="temp-gauge-0"></canvas>
                    <canvas id="temp-gauge-1"></canvas>
                    <canvas id="temp-gauge-2"></canvas>
                    <canvas id="temp-gauge-3"></canvas>
                    <div id="console"></div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <hr/>
            <div id="dummy_div"></div>
        </div>
    </div>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/indicators.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/gauge.js" type="text/javascript"></script>        
</body>
</html>
