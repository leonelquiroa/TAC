<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="TAC Peakfit site">
        <meta name="author" content="@leoquiroa">
        <title>Nutricionista - Citas</title>        
        <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/External/zabuto_calendar.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/External/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/External/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/General/colorCalendar.css" rel="stylesheet" type="text/css"/>
        <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <nav class="navbar navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="white-div"></div>
            <div style="font-size: 24px; text-align: center; color: #22CEDC;">
                <i class="fa fa-calendar"></i> CITAS
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <strong>Calendario</strong>
                    </div>
                    <br/>
                    <div class="zabuto_calendar"></div>
                    <hr/>
                    <div style="color: gray; font-size: 20px;">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <strong>Detalles</strong>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-4" style="text-align: center">
                            <div id="Date_div"></div>
                            <div class="input-group clockpicker" data-autoclose="true" id="input_hour">
                                <input type="text" class="form-control" value="" placeholder="Hora" id="txt_hour">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                            <div id="place_div"></div>
                            <div id="people_div"></div>
                            <div id="measurement_div"></div>
                            <br/>
                            <button type="button" class="btn btn-success" style="display: block; width: 100%;" id="btn_save">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar Cita
                            </button>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-success" id="btn_update">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> Actualizar
                                    </button>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-danger" id="btn_cancel">
                                        <i class="fa fa-ban" aria-hidden="true"></i> Cancelar
                                    </button>
                                </div>
                            </div>
                            <br/>
                        </div>
                        <div class="col-md-8">
                            <div id="Details_div"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div id="dummy_div"></div>
        </div>
    </div>   
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>       
    <script src="../../Controller/js/External/zabuto_calendar.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-clockpicker.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jquery.dataTables.1.10.12.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/dataTables.responsive.js" type="text/javascript"></script>
    <script src="../../Controller/js/General/verticalScroll.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/appointment.js" type="text/javascript"></script>        
    </body>
</html>