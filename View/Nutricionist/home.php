<?php 
    session_start();
    if(!isset($_SESSION['typePerson'])||$_SESSION['typePerson']!='Nutricionista'){header("Location:../General/login.php");}
?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="TAC Peakfit site">
    <meta name="author" content="@leoquiroa">
    <title>Nutricionista - Inicio</title>
    <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-select.1.10.0.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/bootstrap-datepicker.es.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/moment.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/Chart.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/home.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/lineGraph.js" type="text/javascript"></script>
    <script src="../../Controller/js/Nutricionist/barGraph.js" type="text/javascript"></script>
    
    <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/External/jasny-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/Form.css" rel="stylesheet" type="text/css"/>  
    <link href="../../Controller/css/General/topMenu.css" rel="stylesheet" type="text/css"/>
    <link href="../../Controller/css/General/userControl.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="MenuHeaderDiv"></div>
        </div>
        <div id="MenuTopDiv"></div>
        <div id="MenuContentDiv"></div>
    </nav>
    <div id="wrapper">    
        <div id="page-wrapper">
            <div class="container-fluid">
                <!--Title-->
                <div class="row">
                    <br/><br/>
                    <div class="col-md-12">
                        <p style="font-size: 32px; font-weight: bold; color: grey;">Dashboard</p>
                        <p style="font-size: 18px; color: grey;"><b>Frase del día:</b> El dolor que sientes hoy es la fuerza que sentirás mañana</p>
                    </div>
                </div>
                <!--Notes-->
                <div class="form-group">
                    <textarea class="form-control" rows="3" id="comment" style="background-color: #FFF69F; padding-left: 10px; width: 100%;">Recuerda que esta semana son las evaluaciones finales del Reto Destamalizate</textarea>
                </div>
                <br/>
                <!--Todos and dates-->
                <div class="row"> 
                    <!--Todos-->
                    <div class="col-md-5">
                        <button id="showAddForm" type="button" class="btn btn-default" style="width: 100%; height: 60px; background-color: #16C4BB; font-size: 26px; color: white;">
                            <i class="fa fa-user-plus" aria-hidden="true"></i> Agregar Usuario
                        </button>
                        <br/><br/>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color: red; font-weight: bold; font-size: 16px;"><i class="fa fa-clock-o fa-fw"></i> RECORDATORIOS</h3>
                            </div>
                            <div class="panel-body" >
                                <div class="list-group" style="height: 400px; overflow-y: scroll; font-size: 12px;">
                                    <a href="#" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Dates-->
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color: #F9CC00; font-weight: bold; font-size: 16px;"><i class="fa fa-money fa-fw" ></i> CITAS DE HOY</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive" style="height: 450px; overflow-y: scroll;">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Order #</th>
                                                <th>Order Date</th>
                                                <th>Order Time</th>
                                                <th>Amount (USD)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>3326</td>
                                                <td>10/21/2013</td>
                                                <td>3:29 PM</td>
                                                <td>$321.33</td>
                                            </tr>
                                            <tr>
                                                <td>3325</td>
                                                <td>10/21/2013</td>
                                                <td>3:20 PM</td>
                                                <td>$234.34</td>
                                            </tr>
                                            <tr>
                                                <td>3324</td>
                                                <td>10/21/2013</td>
                                                <td>3:03 PM</td>
                                                <td>$724.17</td>
                                            </tr>
                                            <tr>
                                                <td>3323</td>
                                                <td>10/21/2013</td>
                                                <td>3:00 PM</td>
                                                <td>$23.71</td>
                                            </tr>
                                            <tr>
                                                <td>3322</td>
                                                <td>10/21/2013</td>
                                                <td>2:49 PM</td>
                                                <td>$8345.23</td>
                                            </tr>
                                            <tr>
                                                <td>3321</td>
                                                <td>10/21/2013</td>
                                                <td>2:23 PM</td>
                                                <td>$245.12</td>
                                            </tr>
                                            <tr>
                                                <td>3320</td>
                                                <td>10/21/2013</td>
                                                <td>2:15 PM</td>
                                                <td>$5663.54</td>
                                            </tr>
                                            <tr>
                                                <td>3319</td>
                                                <td>10/21/2013</td>
                                                <td>2:13 PM</td>
                                                <td>$943.45</td>
                                            </tr>
                                            <tr>
                                                <td>3326</td>
                                                <td>10/21/2013</td>
                                                <td>3:29 PM</td>
                                                <td>$321.33</td>
                                            </tr>
                                            <tr>
                                                <td>3325</td>
                                                <td>10/21/2013</td>
                                                <td>3:20 PM</td>
                                                <td>$234.34</td>
                                            </tr>
                                            <tr>
                                                <td>3324</td>
                                                <td>10/21/2013</td>
                                                <td>3:03 PM</td>
                                                <td>$724.17</td>
                                            </tr>
                                            <tr>
                                                <td>3323</td>
                                                <td>10/21/2013</td>
                                                <td>3:00 PM</td>
                                                <td>$23.71</td>
                                            </tr>
                                            <tr>
                                                <td>3322</td>
                                                <td>10/21/2013</td>
                                                <td>2:49 PM</td>
                                                <td>$8345.23</td>
                                            </tr>
                                            <tr>
                                                <td>3321</td>
                                                <td>10/21/2013</td>
                                                <td>2:23 PM</td>
                                                <td>$245.12</td>
                                            </tr>
                                            <tr>
                                                <td>3320</td>
                                                <td>10/21/2013</td>
                                                <td>2:15 PM</td>
                                                <td>$5663.54</td>
                                            </tr>
                                            <tr>
                                                <td>3319</td>
                                                <td>10/21/2013</td>
                                                <td>2:13 PM</td>
                                                <td>$943.45</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">Ver más <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--indicators and dates-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color: #3598DB; font-weight: bold; font-size: 16px;"><i class="fa fa-money fa-fw"></i> INDICADOR GLOBAL % DE GRASA</h3>
                            </div>
                            <div class="panel-body">
                                <canvas id="canvas-line"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color: #3598DB; font-weight: bold; font-size: 16px;"><i class="fa fa-money fa-fw"></i> CITAS ATENDIDAS</h3>
                            </div>
                            <div class="panel-body">
                                <canvas id="canvas-bar"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!--events-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="font-size: 16px; color: #16C4BB; font-weight: bold;"><i class="fa fa-money fa-fw"></i> PRÓXIMOS RETOS Y EVENTOS</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered" style="text-align:center;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/lmr-flyer-front.jpg"><img src="../../Multimedia/img/flyers/lmr-flyer-front.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/29.jpg"><img src="../../Multimedia/img/flyers/29.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/30.jpg"><img src="../../Multimedia/img/flyers/30.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/lmr-flyer-front.jpg"><img src="../../Multimedia/img/flyers/lmr-flyer-front.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/29.jpg"><img src="../../Multimedia/img/flyers/29.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/30.jpg"><img src="../../Multimedia/img/flyers/30.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/lmr-flyer-front.jpg"><img src="../../Multimedia/img/flyers/lmr-flyer-front.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/29.jpg"><img src="../../Multimedia/img/flyers/29.jpg" height="190">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="../../Multimedia/img/flyers/30.jpg"><img src="../../Multimedia/img/flyers/30.jpg" height="190">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-16<br>En  Escuintla<br>A las 10:00 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-23<br>En monterrico<br>A las 08:25 PM<br>
                                                </td>
                                                <td>
                                                    <b>Carrera</b><br>El 2016-11-01<br>En r<br>A las 07:30 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-16<br>En  Escuintla<br>A las 10:00 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-23<br>En monterrico<br>A las 08:25 PM<br>
                                                </td>
                                                <td>
                                                    <b>Carrera</b><br>El 2016-11-01<br>En r<br>A las 07:30 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-16<br>En  Escuintla<br>A las 10:00 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-23<br>En monterrico<br>A las 08:25 PM<br>
                                                </td>
                                                <td>
                                                    <b>Carrera</b><br>El 2016-11-01<br>En r<br>A las 07:30 AM<br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">Ver más <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--schedule and badges-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color: #16C4BB; font-weight: bold; font-size: 16px;"><i class="fa fa-money fa-fw"></i> HORARIOS</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#menu1">Plaza Pradera</a></li>
                                        <li><a data-toggle="tab" href="#menu2">Zona 4</a></li>
                                        <li><a data-toggle="tab" href="#menu3">Zona 9</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="menu1" class="tab-pane fade in active">
                                            <table class="table table-bordered table-hover table-striped" style="font-size: 14px;">
                                                <thead>
                                                    <tr>
                                                        <th>HORA</th>
                                                        <th>LUNES</th>
                                                        <th>MARTES</th>
                                                        <th>MIERCOLES</th>
                                                        <th>JUEVES</th>
                                                        <th>VIERNES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5:00</td>
                                                        <td>Yoga</td>
                                                        <td>Zumba</td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                        <td>Runners</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5:30</td>
                                                        <td>Runners</td>
                                                        <td>RTX</td>
                                                        <td></td>
                                                        <td>Zumba</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6:00</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Foodies</td>
                                                        <td>Yoga</td>
                                                        <td>Runners</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6:30</td>
                                                        <td></td>
                                                        <td>Fitness</td>
                                                        <td>Zumba</td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7:00</td>
                                                        <td>Yoga</td>
                                                        <td></td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                        <td>RTX</td>
                                                    </tr>
                                                    <tr>
                                                        <td>7:30</td>
                                                        <td>Yoga</td>
                                                        <td>Zumba</td>
                                                        <td>RTX</td>
                                                        <td></td>
                                                        <td>Yoga</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="menu2" class="tab-pane fade">
                                            <table class="table table-bordered table-hover table-striped" style="font-size: 14px;">
                                                <thead>
                                                    <tr>
                                                        <th>HORA</th>
                                                        <th>LUNES</th>
                                                        <th>MARTES</th>
                                                        <th>MIERCOLES</th>
                                                        <th>JUEVES</th>
                                                        <th>VIERNES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5:00</td>
                                                        <td>Yoga</td>
                                                        <td>Zumba</td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                        <td>Runners</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5:30</td>
                                                        <td>Runners</td>
                                                        <td>RTX</td>
                                                        <td></td>
                                                        <td>Zumba</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6:00</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Foodies</td>
                                                        <td>Yoga</td>
                                                        <td>Runners</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6:30</td>
                                                        <td></td>
                                                        <td>Fitness</td>
                                                        <td>Zumba</td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7:00</td>
                                                        <td>Yoga</td>
                                                        <td></td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                        <td>RTX</td>
                                                    </tr>
                                                    <tr>
                                                        <td>7:30</td>
                                                        <td>Yoga</td>
                                                        <td>Zumba</td>
                                                        <td>RTX</td>
                                                        <td></td>
                                                        <td>Yoga</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="menu3" class="tab-pane fade">
                                            <table class="table table-bordered table-hover table-striped" style="font-size: 14px;">
                                                <thead>
                                                    <tr>
                                                        <th>HORA</th>
                                                        <th>LUNES</th>
                                                        <th>MARTES</th>
                                                        <th>MIERCOLES</th>
                                                        <th>JUEVES</th>
                                                        <th>VIERNES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5:00</td>
                                                        <td>Yoga</td>
                                                        <td>Zumba</td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                        <td>Runners</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5:30</td>
                                                        <td>Runners</td>
                                                        <td>RTX</td>
                                                        <td></td>
                                                        <td>Zumba</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6:00</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Foodies</td>
                                                        <td>Yoga</td>
                                                        <td>Runners</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6:30</td>
                                                        <td></td>
                                                        <td>Fitness</td>
                                                        <td>Zumba</td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7:00</td>
                                                        <td>Yoga</td>
                                                        <td></td>
                                                        <td>Fitness</td>
                                                        <td></td>
                                                        <td>RTX</td>
                                                    </tr>
                                                    <tr>
                                                        <td>7:30</td>
                                                        <td>Yoga</td>
                                                        <td>Zumba</td>
                                                        <td>RTX</td>
                                                        <td></td>
                                                        <td>Yoga</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="font-size: 16px; color: #16C4BB; font-weight: bold;"><i class="fa fa-money fa-fw"></i> MEDALLAS DISPONIBLES</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed" style="text-align:center;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/10_flag_512.png" height="100" alt="">
                                                </td>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/14.png" height="100" alt="">
                                                </td>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/Badge-icon.png" height="100" alt="">
                                                </td>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/medal11-512.png" height="100" alt="">
                                                </td>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/prize.png" height="100" alt="">
                                                </td>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/Badge-icon.png" height="100" alt="">
                                                </td>
                                                <td>
                                                    <img src="../../Multimedia/img/badges/medal11-512.png" height="100" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-16<br>En  Escuintla<br>A las 10:00 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-23<br>En monterrico<br>A las 08:25 PM<br>
                                                </td>
                                                <td>
                                                    <b>Carrera</b><br>El 2016-11-01<br>En r<br>A las 07:30 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-16<br>En  Escuintla<br>A las 10:00 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-23<br>En monterrico<br>A las 08:25 PM<br>
                                                </td>
                                                <td>
                                                    <b>Carrera</b><br>El 2016-11-01<br>En r<br>A las 07:30 AM<br>
                                                </td>
                                                <td>
                                                    <b>Bicicleta</b><br>El 2016-11-16<br>En  Escuintla<br>A las 10:00 AM<br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">Ver más <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- add a new user -->
            <div id="addform" class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" id="new_name" placeholder="Nombre completo" class="form-control nuevo"/>
                        <input type="text" id="new_nick" placeholder="Nick" class="form-control nuevo"/>
                        <div id="new_user_gender_div" ></div>
                        <input type="text" id="new_height" placeholder="Altura" class="form-control nuevo"/>
                        <input type="text" id="new_birthday" placeholder="Fecha de Nac. - dd/mm/yyyy" class="form-control nuevo"/>
                        <input type="text" id="new_phone" placeholder="No. de celular" class="form-control nuevo"/>
                        <input type="text" id="new_email" placeholder="Correo electrónico" class="form-control nuevo"/>
                        <div id="new_user_place_div" ></div>
                    </div>
                    <div class="col-md-6">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width:200px; height:190px">
                                <img src="../../Multimedia/img/people/no_image.png" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;" id="new_photo_form_preview"></div>
                            <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Buscar</span>
                                    <span class="fileinput-exists">Mejor otra</span>
                                    <input type="file" name="..." id="new_photo_form">
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Cancelar</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nuevo_btns">
                                    <button class="btn btn-success" id="performsave"><i class="fa fa-save"></i> Guardar</button>
                                    <button class="btn btn-danger" id="cancelsave"><i class="fa fa-remove"></i> Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>




