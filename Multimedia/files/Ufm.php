
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="description" content="TAC Peakfit site">
                        <meta name="author" content="@leoquiroa">
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
                        <title>Usuario</title>
                        <!--Third CSS-->
                        <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
                        <!--Own CSS-->
                        <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
                        <!--Third JS-->
                        <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
                        <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
                        <!--Own JS-->
                        <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>
                    </head>
                    <body>
                        <!-- ################################################# MENU ################################################# -->
                        <div id="mySidenav" class="sidenav">
                            <a href="../User/home.php"><i class="fa fa-home" aria-hidden="true"></i><br>Inicio</a>
                            <a href="../User/Calendar.php"><i class="fa fa-calendar-o" aria-hidden="true"></i><br>Calendario</a>
                            <a href="../User/Attendance.php"><img src="../../Multimedia/img/mountainIcon.png" height="30" alt="" class="invert"><br>Asistencias</a>
                            <a href="../User/Control.php"><img src="../../Multimedia/img/scaleIcon.png" height="30" alt="" class="invert"><br>Control Nutricional</a>
                            <a href="../User/Diet_Effort.php"><i class="fa fa-info-circle" aria-hidden="true"></i><br>Dietas & Pruebas</a>
                            <a href="../User/Routine.php"><i class="fa fa-video-camera" aria-hidden="true"></i><br>Rutinas</a>
                            <a href="../User/Article_Recipe.php"><i class="fa fa-book" aria-hidden="true"></i> <i class="fa fa-cutlery" aria-hidden="true"></i><br>Articulos & Recetas</a>
                            <a href="../General/logout.php"><i class="fa fa-power-off" aria-hidden="true"></i><br>Salir</a>
                        </div>
                        <div id="top">
                            <div class="col-xs-10" id="div_logo">
                                <img src="../../Multimedia/img/tigoMenu.png" alt="" id="logo">
                            </div>
                            <div class="col-xs-2" id="div_three_bars">
                                <span onclick="openCloseNavMenu()">
                                    <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 white-div"></div>
                        <!-- ################################################# MENU ################################################# -->
                        <div style="padding-left: 10px; padding-right: 10px;"><h1 style="margin-top: 0.36em; margin-bottom: 0px; font-size: 40px; font-family: headline-semi-bold, Helvetica, Arial, sans-serif; line-height: 1.1em; color: rgb(34, 34, 34); text-rendering: optimizeLegibility; padding: 0px;">Elizabeth Y Pantorilla Warren of Massachusetts said Americans&nbsp;</h1><p style="margin-bottom: 1em; font-family: Georgia, Times, sans-serif; font-size: 19px; color: rgb(34, 34, 34);"><img src="../../Multimedia/files/images/gettyimages-599066892.jpg" alt="gettyimages-599066892.jpg"></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Vice President-elect Mike Pence has ordered the removal of all lobbyists from President-elect Donald Trumps transition team, The Wall Street Journal reported on Tuesday night.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">The action was among Pences first since formally taking over the teams lead role. Gov. Chris Christie of New Jersey was abruptly dismissed from the post last week.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Critics had excoriated Trump for including lobbyists, Washington insiders, and Republican Party veterans among his team, saying it contradicted the antiestablishment message that defined his campaign.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">Earlier Tuesday, Sen. Elizabeth Warren of Massachusetts said Americans "do not want corporate executives to be the ones who are calling the shots in Washington.</span></font></p><p style="margin-bottom: 1em;"><font color="#222222" face="Georgia, Times, sans-serif"><span style="font-size: 19px;">"What Donald Trump is doing is that hes putting together a transition team thats full of lobbyists — the kind of people he actually ran against," she said.</span></font></p>        
                        </div>
                    </body>
                </html>