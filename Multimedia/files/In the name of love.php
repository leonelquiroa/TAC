
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="description" content="TAC Peakfit site">
                        <meta name="author" content="@leoquiroa">
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
                        <title>Usuario</title>
                        <!--Third CSS-->
                        <link href="../../Controller/css/External/bootstrap.3.3.6.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/bootstrap-select.1.10.0.min.css" rel="stylesheet" type="text/css"/>
                        <link href="../../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
                        <!--Own CSS-->
                        <link href="../../Controller/css/User/leftMenu.css" rel="stylesheet" type="text/css"/>
                        <!--Third JS-->
                        <script src="../../Controller/js/External/jquery.2.2.0.min.js" type="text/javascript"></script>
                        <script src="../../Controller/js/External/bootstrap.3.3.6.min.js" type="text/javascript"></script>
                        <!--Own JS-->
                        <script src="../../Controller/js/User/leftMenu.js" type="text/javascript"></script>
                    </head>
                    <body>
                        <!-- ################################################# MENU ################################################# -->
                        <div id="mySidenav" class="sidenav">
                            <a href="../../View/User/home.php"><i class="fa fa-home" aria-hidden="true"></i><br>Inicio</a>
                            <a href="../../View/User/Calendar.php"><i class="fa fa-calendar-o" aria-hidden="true"></i><br>Calendario</a>
                            <a href="../../View/User/Attendance.php"><img src="../img/mountainIcon.png" height="30" alt="" class="invert"><br>Asistencias</a>
                            <a href="../../View/User/Control.php"><img src="../img/scaleIcon.png" height="30" alt="" class="invert"><br>Control Nutricional</a>
                            <a href="../../View/User/Diet_Effort.php"><i class="fa fa-info-circle" aria-hidden="true"></i><br>Dietas & Pruebas</a>
                            <a href="../../View/User/Routine.php"><i class="fa fa-video-camera" aria-hidden="true"></i><br>Rutinas</a>
                            <a href="../../View/User/Article_Recipe.php"><i class="fa fa-book" aria-hidden="true"></i> <i class="fa fa-cutlery" aria-hidden="true"></i><br>Articulos & Recetas</a>
                            <a href="../../View/General/logout.php"><i class="fa fa-power-off" aria-hidden="true"></i><br>Salir</a>
                        </div>
                        <div id="top">
                            <div class="col-xs-10" id="div_logo">
                                <img src="../img/tigoMenu.png" alt="" id="logo">
                            </div>
                            <div class="col-xs-2" id="div_three_bars">
                                <span onclick="openCloseNavMenu()">
                                    <i id="three_bars" class="fa fa-bars" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 white-div"></div>
                        <!-- ################################################# MENU ################################################# -->
                        <div style="padding-left: 10px; padding-right: 10px;"><h1>LEAN ON ME!!!</h1><p><img src="../../Multimedia/files/30.jpg" alt="30.jpg" style="width: 100%;"><br></p><p><span style="background-color: rgb(0, 255, 0);">Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lacinia molestie dui. Praesent blandit dolor. Sed non quam. In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel, egestas et, augue. Vestibulum tincidunt malesuada tellus.&nbsp;</span></p>        
                        </div>
                    </body>
                </html>